﻿using System.Web;
using System.Web.Optimization;

namespace ClientDashboard
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                       "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/bootstrap.css",
                "~/Content/jquery.dataTables.css",
                "~/Content/dataTables.responsive.css",
                "~/Content/buttons.dataTables.css",
                 "~/Content/fixedHeader.dataTables.css",
                "~/Content/site.css",
                "~/Content/datepicker.css",
                "~/Content/sumoselect.css",
                "~/Content/themes/base/jquery.ui.dialog.css",
                 "~/Content/toastr.css",
                 "~/Content/Slider.css",
                 "~/Content/jquery.mCustomScrollbar.css"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.unobtrusive*"//,
                // "~/Scripts/jquery.validate*"
                        ));

            bundles.Add(new ScriptBundle("~/bundles/js").Include(
                "~/Scripts/jquery-1.9.1.js",
                       "~/Scripts/DataTables/js/jquery.dataTables.js",
                       "~/Scripts/DataTables/js/dataTables.responsive.js",
                       "~/Scripts/DataTables/js/fnProcessingIndicator.js",
                       "~/Scripts/DataTables/js/dataTables.buttons.js",
                       "~/Scripts/DataTables/js/jszip.js",
                       "~/Scripts/DataTables/js/pdfmake.js",
                       "~/Scripts/DataTables/js/vfs_fonts.js",
                       "~/Scripts/DataTables/js/buttons.html5.js",
                        "~/Scripts/DataTables/js/datatables.fixedcolumns.js",
                         "~/Scripts/DataTables/js/datatables.fixedHeader.js",
                        "~/Scripts/loadingoverlay.js",
                        "~/Scripts/datepicker.js",
                        "~/Scripts/jquery.signalR-1.2.1.js",
                        "~/Scripts/jquery.sumoselect.js",
                        "~/Scripts/jquery-ui.js",
                        "~/Scripts/toastr.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/Highcharts").Include(
                       "~/Scripts/HighCharts/js/highcharts.js",
                        "~/Scripts/HighCharts/js/highcharts-more.js",
                         "~/Scripts/HighCharts/js/modules/solid-gauge.js",
                       "~/Scripts/HighCharts/js/modules/no-data-to-display.js",
                       "~/Scripts/HighCharts/js/modules/exporting.js",
                       "~/Scripts/HighCharts/js/modules/offline-exporting.js",
                          "~/Scripts/HighCharts/js/modules/treemap.js",
                       "~/Scripts/HighCharts/js/modules/drilldown.js"
                       ));
            bundles.Add(new ScriptBundle("~/bundles/Highstock").Include(
               "~/Scripts/Highstock/js/highstock.js",
                 "~/Scripts/HighCharts/js/highcharts-more.js",
               "~/Scripts/Highstock/js/modules/no-data-to-display.js",
            "~/Scripts/Highstock/js/modules/treemap.js",
                  "~/Scripts/Highstock/js/modules/drilldown.js",
                  "~/Scripts/Highstock/js/modules/solid-gauge.js"));

            bundles.Add(new ScriptBundle("~/bundles/homejs").Include(
                "~/Scripts/jquery.mCustomScrollbar.concat.min.js",
                      "~/Scripts/Slider/modernizr.custom.63321.js",
                       "~/Scripts/Slider/jquery.catslider.js",
                       "~/Scripts/Custom/Home.js",
                        "~/Scripts/jquery-ui.js"

                      ));
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        }
    }
}
