﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http.Headers;
using System.Web.Http;

namespace ClientDashboard
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.JsonFormatter.SupportedMediaTypes.Add(new MediaTypeHeaderValue("text/html"));
            config.Routes.MapHttpRoute(
              name: "JobCaseApi",
              routeTemplate: "JobCase/{controller}/{campaignid}",
              defaults: new { controller = "CampaignStats", action = "Get", campaignid = RouteParameter.Optional }
          );
            //GlobalConfiguration.Configuration.Filters.Add(new BasicAuthenticationAttribute());
            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

        }
    }
}
