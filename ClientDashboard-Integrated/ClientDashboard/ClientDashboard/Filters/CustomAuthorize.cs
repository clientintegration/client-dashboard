﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Mvc;
using ClientDashboard.Models;
using System.Web;
using System.Linq;
using System.Web.Routing;
using System.Collections.Generic;
using System.Text;
using Microsoft.AspNet.Identity;
namespace ClientDashboard.Filters
{
   

    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        ClientDashboardEntities context = new ClientDashboardEntities();
        private readonly string module;
        public CustomAuthorizeAttribute(string moduleName)
        {
            module = moduleName;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            if (httpContext.Request.Url.Segments.Length > 3)
            {
                int len = httpContext.Request.Url.Segments.Length;
                string campaign = httpContext.Request.Url.Segments[len - 1];
                string currentUserId = HttpContext.Current.User.Identity.GetUserId();
                var user = context.AspNetUserRoles.Include(x => x.AspNetRole).Include(x => x.Module).Where(x => x.UserId == currentUserId && x.Module.Name == module && x.AspNetRole.Name == campaign);
                if (user.Count() > 0)
                {
                    authorize = true; /* return true if Entity has current user(active) with specific module and campaign*/
                }
            }
            else
                authorize = true;
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }

    public class BasicAuthenticationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {

        private Boolean IsUserValid(Dictionary<string, string> credentials)
        {

            if (credentials["UserName"].Equals("WebAPI") && credentials["Password"].Equals("WebAPI123"))
                return true;

            return false;

        }

        private Dictionary<string, string> ParseRequestHeaders(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            Dictionary<string, string> credentials = new Dictionary<string, string>();
            var httpRequestHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
            httpRequestHeader = httpRequestHeader.Substring("Authorization".Length);
            string[] httpRequestHeaderValues = httpRequestHeader.Split(':');
            string username = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[0]));
            string password = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[1]));
            //   username = httpRequestHeaderValues[0];
            //password = httpRequestHeaderValues[1];
            credentials.Add("UserName", username);
            credentials.Add("Password", password);

            return credentials;

        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            try
            {

                if (actionContext.Request.Headers.Authorization == null)
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }

                else
                {
                    Dictionary<string, string> credentials = ParseRequestHeaders(actionContext);
                    if (!IsUserValid(credentials))
                        //    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);
                        //else
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

                }
            }

            catch
            {

                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);

            }

        }

    }
}
