﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using Microsoft.AspNet.Identity;
namespace ClientDashboard.Filters
{
    public class LogAttribute : ActionFilterAttribute
    {
        public string Activity { get; set; }
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Stores the Request in an Accessible object
            var request = filterContext.HttpContext.Request;

            string userId = HttpContext.Current.User.Identity.GetUserId();
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);

            ActivityLog log = new ActivityLog()
            {


                UserId = (request.IsAuthenticated) ? userId : "0",
                PageUrl = request.RawUrl,
                Activity = Activity,
                TimeStamp = easternTime
            };

            ClientDashboardEntities context = new ClientDashboardEntities();
            try
            {
                context.ActivityLogs.Add(log);
                context.SaveChanges();
            }
            catch (Exception ex)
            {

                logger.Error((request.IsAuthenticated) ? userId.ToString() : "None", ex);

            }

            //Finishes executing the Action as normal 
            base.OnActionExecuting(filterContext);
        }
    }
}