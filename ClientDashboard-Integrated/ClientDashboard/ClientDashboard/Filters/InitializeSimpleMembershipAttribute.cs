﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Mvc;
using WebMatrix.WebData;
using ClientDashboard.Models;
using System.Web;
using System.Linq;
using System.Web.Routing;
using System.Collections.Generic;
using System.Text;
namespace ClientDashboard.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    // WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: false);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }


    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        ClientDashboardEntities context = new ClientDashboardEntities();
        private readonly string module;
        public CustomAuthorizeAttribute(string moduleName)
        {
            module = moduleName;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            if (httpContext.Request.Url.Segments.Length > 3)
            {
                int len = httpContext.Request.Url.Segments.Length;
                string campaign = httpContext.Request.Url.Segments[len - 1];
                int currentUserId = WebSecurity.GetUserId(httpContext.User.Identity.Name);
                var user = context.webpages_UsersInRoles.Include(x => x.webpages_Roles).Include(x => x.Module).Where(x => x.UserId == currentUserId && x.Module.Name == module && x.webpages_Roles.RoleName == campaign);
                if (user.Count() > 0)
                {
                    authorize = true; /* return true if Entity has current user(active) with specific module and campaign*/
                }
            }
            else
                authorize = true;
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }

    public class BasicAuthenticationAttribute : System.Web.Http.Filters.ActionFilterAttribute
    {

        private Boolean IsUserValid(Dictionary<string, string> credentials)
        {

            if (credentials["UserName"].Equals("WebAPI") && credentials["Password"].Equals("WebAPI123"))
                return true;

            return false;

        }

        private Dictionary<string, string> ParseRequestHeaders(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            Dictionary<string, string> credentials = new Dictionary<string, string>();
            var httpRequestHeader = actionContext.Request.Headers.GetValues("Authorization").FirstOrDefault();
            httpRequestHeader = httpRequestHeader.Substring("Authorization".Length);
            string[] httpRequestHeaderValues = httpRequestHeader.Split(':');
            string username = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[0]));
            string password = Encoding.UTF8.GetString(Convert.FromBase64String(httpRequestHeaderValues[1]));
         //   username = httpRequestHeaderValues[0];
            //password = httpRequestHeaderValues[1];
            credentials.Add("UserName", username);
            credentials.Add("Password", password);

            return credentials;

        }

        public override void OnActionExecuting(System.Web.Http.Controllers.HttpActionContext actionContext)
        {

            try
            {

                if (actionContext.Request.Headers.Authorization == null)
                {
                    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);
                }

                else
                {
                    Dictionary<string, string> credentials = ParseRequestHeaders(actionContext);
                    if (!IsUserValid(credentials))
                    //    actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.OK);
                    //else
                        actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.Unauthorized);

                }
            }

            catch
            {

                actionContext.Response = new System.Net.Http.HttpResponseMessage(System.Net.HttpStatusCode.InternalServerError);

            }

        }

    }
}
