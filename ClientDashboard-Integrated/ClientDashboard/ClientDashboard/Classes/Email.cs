﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using ClientDashboard.Models;

namespace ClientDashboard.Classes
{
    public class Email
    {

        public bool SendEmailToUser(string subject, string toDistro, string EmailMessage)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
               System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(HttpRuntime.AppDomainAppPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(HttpRuntime.AppDomainAppPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);
                
                body = body.Replace("@@BodyOfEmail@@", EmailMessage);


                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
                mail.To.Add(toDistro);

             //   mail.Bcc.Add(CDDistro);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
      
        public bool SendEmailToUsers(string subject, string[] toDistro, string EmailMessage, HttpPostedFileBase file)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);

                body = body.Replace("@@BodyOfEmail@@", EmailMessage);

                if (file != null)
                {
                    string fileName = Path.GetFileName(file.FileName);
                    mail.Attachments.Add(new Attachment(file.InputStream, fileName));
                }

                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
                mail.To.Add(CDDistro);
                if (toDistro != null)
                    foreach (var Addr in toDistro)
                        mail.Bcc.Add(Addr);
                mail.Subject = subject;
                mail.Body =  body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
        public bool SendThresholdEmail(ThresholdModel obj)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(HttpRuntime.AppDomainAppPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(HttpRuntime.AppDomainAppPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);
                string EmailMessage = obj.Name + ",<br/>";
                EmailMessage += "This is to inform you that the value for " + obj.MetricDescription + " has reached " + obj.CurrentValue + ". <br/>";
                EmailMessage += "Please see below details: <br/><br/>";
                EmailMessage += "<b>Campaign</b>: " + obj.CampaignID+"<br/>";
                EmailMessage += "<b>Metric</b>: " + obj.MetricDescription + "<br/>";
                EmailMessage += "<b>Threshold Value</b>: " + (obj.IfAbove == true ? "Above" : "Below") + " " + obj.ThresholdValue + "<br/>";
                EmailMessage += "<b>Current Value</b>: " + obj.CurrentValue + "<br/>";
                body = body.Replace("@@BodyOfEmail@@", EmailMessage);

                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
              //  mail.CC.Add(CDDistro);

                mail.To.Add(obj.Email);
                mail.Subject = "Threshold Notification | Client Dashboard";
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
    }
}