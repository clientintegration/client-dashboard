﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Hubs;
using ClientDashboard.Models;

namespace ClientDashboard.Classes
{
    public class StatisticsRepository
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public DateTime GetEasternTime()
        {
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            return easternTime;
        }
        public string GetTimeDiff(DateTime dt1, DateTime dt2)
        {
            string differnce = "", hrs, mins, secs;
            hrs = (dt1 - dt2).Hours.ToString();
            mins = (dt1 - dt2).Minutes.ToString();
            secs = (dt1 - dt2).Seconds.ToString();
            hrs = Convert.ToInt32(hrs) < 10 ? '0' + hrs : hrs;
            mins = Convert.ToInt32(mins) < 10 ? '0' + mins : mins;
            secs = Convert.ToInt32(secs) < 10 ? '0' + secs : secs;
            differnce = hrs + ":" + mins + ":" + secs;
            return differnce;

        }
        public List<AgentCountReport> GetAgentCountStats(bool IsRequested)
        {


            var AgentCount = new List<AgentCountReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT CampaignID,[AgentsOnBreak] ,[AgentsOnTalk],[AgentsOnWait],[AgentsOnWrap] FROM [dbo].[AgentCountReport] where updatedon>=@date", connection))
                    {
                        command.Notification = null;

                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AgentCountStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentCount.Add(item: new AgentCountReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                AgentsOnBreak = (int)reader["AgentsOnBreak"],
                                AgentsOnTalk = (int)reader["AgentsOnTalk"],
                                AgentsOnWait = (int)reader["AgentsOnWait"],
                                AgentsOnWrap = (int)reader["AgentsOnWrap"]

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return AgentCount;
        }
        private void AgentCountStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AgentCountReport> AgentCount = GetAgentCountStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAgentCountStats(AgentCount);

                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<AgentStatusReport> GetAgentStatusStats(bool IsRequested)
        {


            var AgentStatus = new List<AgentStatusReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT [CampaignID]      ,[AgentState]      ,[AgentCount]  FROM [dbo].[AgentStatusReport] where updatedon>=@date and [AgentCount]>0", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AgentStatusStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentStatus.Add(item: new AgentStatusReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                AgentState = (string)reader["AgentState"],
                                AgentCount = (int)reader["AgentCount"]

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return AgentStatus;
        }
        private void AgentStatusStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AgentStatusReport> AgentStatus = GetAgentStatusStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAgentStatusStats(AgentStatus);

                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<RoutingQueueStat> GetRoutingQueueStats(bool IsRequested)
        {
            var Queue = new List<RoutingQueueStat>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT CampaignID,sum([QueuedCalls]) QueuedCalls,count_big(*) as QueuedCalls FROM [dbo].[RoutingQueueStats] where updatedon>=@date group by CampaignID", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(RoutingQueueStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();
                        string campaignid = "";
                        while (reader.Read())
                        {
                            campaignid = ((string)reader["CampaignID"] == "MIM") ? "MIP" : (string)reader["CampaignID"];
                            Queue.Add(item: new RoutingQueueStat
                            {
                                CampaignID = campaignid,
                                QueuedCalls = (int)reader["QueuedCalls"]

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return Queue;
        }
        private void RoutingQueueStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<RoutingQueueStat> Queue = GetRoutingQueueStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendQueueStats(Queue);

                }
                catch (Exception ex)
                {
                   
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<TfnQueueDispositionReport> GetTFNQueueStats(bool IsRequested)
        {
            var TFNQueue = new List<TfnQueueDispositionReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    // using (var command = new SqlCommand(@"select t.campaignid,t.Tfnqueue,Disposition,Description ,callsoffered,callshandled,callsabandon,callcount from ibdispositionreport d inner join tfnqueuereport t on t.campaignid=d.campaignid and t.tfnqueue=d.tfnqueue where t.updatedon>=@date and CallsOffered<>0 and Callcount<>0 ", connection))
                    using (var command = new SqlCommand(@"select t.campaignid,t.Tfnqueue,d.Disposition,d.Description ,t.callsoffered,t.callshandled,t.callsabandon,d.callcount from dbo.ibdispositionreport d inner join dbo.tfnqueuereport t on t.campaignid=d.campaignid and t.tfnqueue=d.tfnqueue where t.updatedon>=@date and CallsOffered<>0 and Callcount<>0  ", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(TFNQueueStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            TFNQueue.Add(item: new TfnQueueDispositionReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                TfnQueue = (string)reader["TfnQueue"],
                                CallsHandled = (int)reader["CallsHandled"],
                                CallsAbandon = (int)reader["CallsAbandon"],
                                CallsOffered = (int)reader["CallsOffered"],
                                CallCount = (int)reader["CallCount"],
                                Disposition = (string)(reader["Disposition"] + "-" + reader["Description"]),

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return TFNQueue;
        }
        private void TFNQueueStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<TfnQueueDispositionReport> TFNQueue = GetTFNQueueStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendTFNQueueStats(TFNQueue);

                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<CampaignsReport> GetIntervalStats(bool IsRequested)
        {
            var Interval = new List<CampaignsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select CampaignID,TimeZone ,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z  from dbo.campaignsreport where updatedon>=@date and (B<> '0' or F <> '0.00' )";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(IntervalStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Interval.Add(item: new CampaignsReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                TimeZone = (string)reader["TimeZone"],
                                A = (string)reader["A"],
                                B = (string)reader["B"],
                                C = (string)reader["C"],
                                D = (string)reader["D"],
                                E = (string)reader["E"],
                                F = (string)reader["F"],
                                G = (string)reader["G"],
                                H = (string)reader["H"],
                                I = (string)reader["I"],
                                J = (string)reader["J"],
                                K = (string)reader["K"],
                                L = (string)reader["L"],
                                M = (string)reader["M"],
                                N = (string)reader["N"],
                                O = (string)reader["O"],
                                P = (string)reader["P"],
                                Q = (string)reader["Q"],
                                R = (string)reader["R"],
                                S = (string)reader["S"],
                                T = (string)reader["T"],
                                U = (string)reader["U"],
                                V = (string)reader["V"],
                                W = (string)reader["W"],
                                X = (string)reader["X"],
                                Y = (string)reader["Y"],
                                Z = (string)reader["Z"]

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return Interval;
        }
        private void IntervalStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<CampaignsReport> Interval = GetIntervalStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendIntervalStats(Interval);

                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }


        public List<AgentsReport> GetAgentsStats(bool IsRequested)
        {
            var AgentStats = new List<AgentsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select CampaignID,StateTime ,A,B,C,D,E,F,G,H,I,J,K,L,M,N,O,P,Q,R,S,T,U,V,W,X,Y,Z ";
                    query += "FROM [dbo].[AgentsReport] where updatedon>=@date and (A<>'')";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AgentsStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            string difference = GetTimeDiff(GetEasternTime(), Convert.ToDateTime(reader["StateTime"]));
                            AgentStats.Add(item: new AgentsReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                A = (string)reader["A"],
                                B = (string)reader["B"],
                                C = (string)reader["C"],
                                D = (string)difference,
                                E = (string)difference,
                                F = (string)reader["F"],
                                G = (string)reader["G"],
                                H = (string)reader["H"],
                                I = (string)reader["I"],
                                J = (string)reader["J"],
                                K = (string)reader["K"],
                                L = (string)reader["L"],
                                M = (string)reader["M"],
                                N = (string)reader["N"],
                                O = (string)reader["O"],
                                P = (string)reader["P"],
                                Q = (string)reader["Q"],
                                R = (string)reader["R"],
                                S = (string)reader["S"],
                                T = (string)reader["T"],
                                U = (string)reader["U"],
                                V = (string)reader["V"],
                                W = (string)reader["W"],
                                X = (string)reader["X"],
                                Y = (string)reader["Y"],
                                Z = (string)reader["Z"]

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return AgentStats;
        }
        private void AgentsStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AgentsReport> Agent = GetAgentsStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAgentsStats(Agent);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }

        public List<OBDispositionReport> GetOBDispositionStats(bool IsRequested)
        {
            var OBDisp = new List<OBDispositionReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    string query = "select CampaignID,DispType,Disposition,Description,CallsPerDispType,CallCount from dbo.OBDispositionReport";
                    query += " where updatedon>=@date and Callcount<>0 ";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(GetOBDispositionStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            OBDisp.Add(item: new OBDispositionReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                DispType = (string)reader["DispType"],
                                CallsPerDispType = (int)reader["CallsPerDispType"],
                                CallCount = (int)reader["CallCount"],
                                Disposition = (string)(reader["Disposition"] + "-" + reader["Description"]),

                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return OBDisp;
        }
        private void GetOBDispositionStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<OBDispositionReport> OBDisp = GetOBDispositionStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendOBDispositionStats(OBDisp);

                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }


        public List<LeadsReport> GetLeadsStats(bool IsRequested)
        {
            var LeadStats = new List<LeadsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT CampaignID,LeadsAvailable FROM [dbo].[HomeReport]  where updatedon>=@date ";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(LeadsStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            LeadStats.Add(item: new LeadsReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                Available = (int)reader["LeadsAvailable"]


                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return LeadStats;
        }
        private void LeadsStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<LeadsReport> Lead = GetLeadsStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendLeadsStats(Lead);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<AvayaSkillStat> GetAvayaSkillStats(bool IsRequested)
        {
            var SkillStats = new List<AvayaSkillStat>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select Skill,CampaignID,CallsWaiting,OldestCallWaiting,DirectAgentCallWaiting,ServiceLevel,ServiceLevelPercent,ACDCalls,ABNCalls,AvgSpeedAns,AvgACDTime,AvgAbanTime from dbo.AvayaSkillStats ";
                    query += "where updatedon>=@date";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AvayaSkillStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            SkillStats.Add(item: new AvayaSkillStat
                             {
                                 Skill = (int)reader["Skill"],
                                 CampaignID = (string)reader["CampaignID"],
                                 CallsWaiting = (int)reader["CallsWaiting"],
                                 OldestCallWaiting = (string)reader["OldestCallWaiting"],
                                 DirectAgentCallWaiting = (int)reader["DirectAgentCallWaiting"],
                                 ServiceLevel = (int)reader["ServiceLevel"],
                                 ServiceLevelPercent = (string)reader["ServiceLevelPercent"],
                                 ACDCalls = (int)reader["ACDCalls"],
                                 ABNCalls = (int)reader["ABNCalls"],
                                 AvgAbanTime = (string)reader["AvgAbanTime"],
                                 AvgACDTime = (string)reader["AvgACDTime"],
                                 AvgSpeedAns = (string)reader["AvgSpeedAns"]
                             });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return SkillStats;
        }
        private void AvayaSkillStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AvayaSkillStat> SkillStats = GetAvayaSkillStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAvayaSkillStats(SkillStats);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<AvayaAgentState> GetAvayaAgentStates(bool IsRequested)
        {
            var AgentStates = new List<AvayaAgentState>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select Skill,CampaignID,Avail,AUX,ACD,Other,Ring,ACW,Staffed from dbo.AvayaAgentStates ";
                    query += "where updatedon>=@date";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AvayaAgentStates_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentStates.Add(item: new AvayaAgentState
                            {
                                Skill = (int)reader["Skill"],
                                CampaignID = (string)reader["CampaignID"],
                                Avail = (int)reader["Avail"],
                                AUX = (int)reader["AUX"],
                                ACD = (int)reader["ACD"],
                                Other = (int)reader["Other"],
                                Ring = (int)reader["Ring"],
                                ACW = (int)reader["ACW"],
                                Staffed = (int)reader["Staffed"]


                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return AgentStates;
        }
        private void AvayaAgentStates_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AvayaAgentState> AgentStates = GetAvayaAgentStates(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAvayaAgentStates(AgentStates);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<AvayaAgentStat> GetAvayaAgentStats(bool IsRequested)
        {
            var AgentStats = new List<AvayaAgentStat>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select Skill,AgentID,AgentName,Extension,Role,[Percent],AUXReason,State,Direction,Level,StateTime,VDNName,CampaignID,StaffTime,OtherTime,AvailTime,ACDTime,ACWTime,AUXTime,RingTime ";
                    query += "from dbo.AvayaAgentStats ";
                    query += "where updatedon>=@date";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AvayaAgentStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            string difference = GetTimeDiff(GetEasternTime(), Convert.ToDateTime(reader["StateTime"]));
                            AgentStats.Add(item: new AvayaAgentStat
                            {
                                Skill = (int)reader["Skill"],
                                CampaignID = (string)reader["CampaignID"],
                                AgentID = (int)reader["AgentID"],
                                AgentName = (string)reader["AgentName"],
                                Extension = (int)reader["Extension"],
                                AUXReason = (string)reader["AUXReason"],
                                State = (string)reader["State"],
                                Direction = (string)reader["Direction"],
                                StateTime = difference,
                                VDNName = (string)reader["VDNName"],
                                StaffTime = (string)reader["StaffTime"],
                                ACDTime = (string)reader["ACDTime"],
                                RingTime = (string)reader["RingTime"],
                                ACWTime = (string)reader["ACWTime"],
                                AUXTime = (string)reader["AUXTime"],
                                AvailTime = (string)reader["AvailTime"],
                                OtherTime = (string)reader["OtherTime"]
                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return AgentStats;
        }
        private void AvayaAgentStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AvayaAgentStat> AgentStats = GetAvayaAgentStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAvayaAgentStats(AgentStats);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }


        public List<HomeReport> GetDialingStatus(bool IsRequested)
        {
            var Dial = new List<HomeReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT [Campaignid]  ,[Client],[ImagePath] ,[CampaignType]  ,[Agents]      ,[DialStatus]   FROM [dbo].[HomeReport] where updatedon>=@date";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0).AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(DialingStatus_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Dial.Add(item: new HomeReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                DialStatus = (string)reader["DialStatus"],
                                Client = (string)reader["Client"],
                                ImagePath = (string)reader["ImagePath"],
                                CampaignType = (string)reader["CampaignType"],
                                Agents = (int)reader["Agents"],


                            });
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return Dial;
        }
        private void DialingStatus_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<HomeReport> Dial = GetDialingStatus(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendDialingStatus(Dial);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }

        public List<LiveUsers> GetActiveUsers(bool IsRequested)
        {
            int num = 0;
            var LiveUsers = new List<LiveUsers>();
            List<LiveUsers> LiveUsersDistinct = new List<LiveUsers>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select u.userId,u.username,updatedon UpdatedOn from dbo.activeconnections a ";
                    query += "inner join dbo.userprofile u on u.userId=a.userId ";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        //DateTime dt = GetEasternTime().Date.AddDays(0).AddDays(0);
                        //command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(ActiveUsers_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {

                            LiveUsers.Add(item: new LiveUsers
                            {
                                UserId = (int)reader["UserID"],
                                UserName = (string)reader["username"],
                                LastActivityTime = Convert.ToDateTime(reader["UpdatedOn"]).ToString()


                            });
                        }
                        //num = (from x in list
                        //       select x).Distinct().Count();
                        LiveUsersDistinct = LiveUsers.GroupBy(r => r.UserId).Select(g => g.OrderByDescending(r => r.LastActivityTime).First()).ToList();
                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return LiveUsersDistinct;
        }
        private void ActiveUsers_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<LiveUsers> LiveUsers = GetActiveUsers(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendActiveUsers(LiveUsers);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
        public List<ThresholdModel> GetThreshold(bool IsRequested)
        {

            var Threshold = new List<ThresholdModel>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"select u.userId,u.name,email,tm.campaignid,m.metricid,description,value,ThresholdValue,ifabove  ";
                    query += "from dbo.userthresholds t inner join dbo.thresholdmetrics tm on ";
                    query += "t.metricid=tm.metricid and t.campaignid=tm.campaignid  ";
                    query += "and ((t.ThresholdValue <tm.Value )  or (t.ThresholdValue>=tm.Value and t.ifabove=0)) ";
                    query += "inner join dbo.metrics m on m.metricid=tm.metricid inner join dbo.userprofile u on u.userId=t.userId where ";
                    query += "tm.Isactive=1 and t.isactive=1 and t.issent=0 and isaccountactive=1";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        //DateTime dt = GetEasternTime().Date.AddDays(0).AddDays(0);
                        //command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(Threshold_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();
                        while (reader.Read())
                        {
                            //list.Add((int)reader["UserId"]);
                            Threshold.Add(item: new ThresholdModel
                            {
                                UserID = (string)(reader["UserID"] + ""),
                                Name = (string)reader["Name"],
                                Email = (string)reader["Email"],
                                CampaignID = (string)reader["CampaignID"],
                                MetricID = (int)reader["MetricID"],
                                MetricDescription = (string)reader["Description"],
                                CurrentValue = (decimal)reader["Value"],
                                ThresholdValue = (decimal)reader["ThresholdValue"],
                                IfAbove = (bool)reader["IfAbove"]


                            });
                        }

                    }
                }
            }
            catch (Exception ex)
            {
                logger.Error("Statistics Repository", ex);
            }
            return Threshold;
        }
        private void Threshold_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<ThresholdModel> Threshold = GetThreshold(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.NotifyThresholdUsers(Threshold);
                }
                catch (Exception ex)
                {
                    logger.Error("Statistics Repository", ex);
                }
            }
        }
    }
}