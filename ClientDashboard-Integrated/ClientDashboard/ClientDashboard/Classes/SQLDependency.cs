﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using ClientDashboard.Hubs;
using Microsoft.AspNet.SignalR;

namespace ClientDashboard.Classes
{
    public class SQLDependency : IRegisteredObject
    {
        private readonly IHubContext _hub;
        string connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;

        public SQLDependency()
        {
            SqlDependency.Start(connString);
            _hub = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            Register();


        }
        private void Register()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            //Common
            _StatisticsRepository.GetDialingStatus(false);
            _StatisticsRepository.GetAgentCountStats(false);
            _StatisticsRepository.GetAgentStatusStats(false);
            _StatisticsRepository.GetIntervalStats(false);
            _StatisticsRepository.GetAgentsStats(false);

            //Inbound
            _StatisticsRepository.GetRoutingQueueStats(false);
            _StatisticsRepository.GetTFNQueueStats(false);

            //Outbound        
            _StatisticsRepository.GetLeadsStats(false);
            _StatisticsRepository.GetOBDispositionStats(false);

            //General
            _StatisticsRepository.GetActiveUsers(false);
            _StatisticsRepository.GetThreshold(false);

            //Avaya CMS
            _StatisticsRepository.GetAvayaSkillStats(false);
            _StatisticsRepository.GetAvayaAgentStats(false);
            _StatisticsRepository.GetAvayaAgentStates(false);

        }
        public void Stop(bool immediate)
        {
            Email obj = new Email();
            Exception ex = new Exception("SQL Dependency has stopped");
            // obj.SendExceptionEmail(ex, "None");
            SqlDependency.Stop(connString);
            HostingEnvironment.UnregisterObject(this);
        }
    }
}