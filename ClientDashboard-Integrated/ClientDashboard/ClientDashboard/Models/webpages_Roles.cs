//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientDashboard.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class webpages_Roles
    {
        public webpages_Roles()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
    
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> ClientID { get; set; }
        public string CampaignType { get; set; }
        public string SwitchType { get; set; }
    
        public virtual Client Client { get; set; }
        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
}
