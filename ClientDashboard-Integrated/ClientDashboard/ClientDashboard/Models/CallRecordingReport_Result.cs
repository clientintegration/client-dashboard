﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Classes;

namespace ClientDashboard.Models
{
    public class CallRecordingReport_Result
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);

        public string Call_Type { get; set; }
        public string Campaign_ID { get; set; }
        public string Unique_ID { get; set; }
        public string Call_ID { get; set; }
        public string Agent_ID { get; set; }
        public string Number { get; set; }
        public System.DateTime Calldate { get; set; }
        public string Start_Time { get; set; }
        public string Duration { get; set; }
        public string Call_Outcome { get; set; }
        public string Filter1 { get; set; }
        public string Filter2 { get; set; }
        public string Filter3 { get; set; }
        public string Filter4 { get; set; }
        public string Filter5 { get; set; }
        public string Provider_Type { get; set; }
        public string NR_Segment { get; set; }

        public List<CallRecordingReport_Result> GetSearchResults(string startdate, string enddate, string uniqueid, string tel, string agentid, string campaignid, string termcd,
            string filter1, string filter2, string filter3, string filter4, string filter5, string filterval1, string filterval2, string filterval3, string filterval4, string filterval5)
        {
            var result = new List<CallRecordingReport_Result>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();

            SqlCmd.CommandTimeout = 300;
            SqlCmd.CommandText = "CallRecordingReport";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startdate;
            SqlCmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = enddate;
            SqlCmd.Parameters.Add("@UniqueID", SqlDbType.VarChar).Value = uniqueid;
            SqlCmd.Parameters.Add("@number", SqlDbType.VarChar).Value = tel;
            SqlCmd.Parameters.Add("@AgentID", SqlDbType.VarChar).Value = agentid;
            SqlCmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = campaignid;
            SqlCmd.Parameters.Add("@Termcd", SqlDbType.VarChar).Value = termcd;
            SqlCmd.Parameters.Add("@Filter1", SqlDbType.VarChar).Value = filter1;
            SqlCmd.Parameters.Add("@Filter2", SqlDbType.VarChar).Value = filter2;
            SqlCmd.Parameters.Add("@Filter3", SqlDbType.VarChar).Value = filter3;
            SqlCmd.Parameters.Add("@Filter4", SqlDbType.VarChar).Value = filter4;
            SqlCmd.Parameters.Add("@Filter5", SqlDbType.VarChar).Value = filter5;
            SqlCmd.Parameters.Add("@Filterval1", SqlDbType.VarChar).Value = filterval1;
            SqlCmd.Parameters.Add("@Filterval2", SqlDbType.VarChar).Value = filterval2;
            SqlCmd.Parameters.Add("@Filterval3", SqlDbType.VarChar).Value = filterval3;
            SqlCmd.Parameters.Add("@Filterval4", SqlDbType.VarChar).Value = filterval4;
            SqlCmd.Parameters.Add("@Filterval5", SqlDbType.VarChar).Value = filterval5;
            SqlCmd.Connection = SqlConn;

            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    var reader = SqlCmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(item: new CallRecordingReport_Result
                        {
                            Agent_ID = (string)reader["Agent ID"],
                            Call_ID = (string)reader["Call ID"],
                            Call_Outcome = (string)reader["Call Outcome"],
                            Call_Type = (string)reader["Call Type"],
                            Calldate = Convert.ToDateTime((string)reader["Calldate"]),
                            Campaign_ID = (string)reader["Campaign ID"],
                            Duration = (string)reader["Duration"],
                            Unique_ID = (string)reader["Unique ID"],
                            Filter1 = (string)reader["Filter1"],
                            Filter2 = (string)reader["Filter2"],
                            Filter3 = (string)reader["Filter3"],
                            Filter4 = (string)reader["Filter4"],
                            Filter5 = (string)reader["Filter5"],
                            Provider_Type = (string)reader["Provider Type"],
                            Number = (string)reader["Number"],
                            NR_Segment = (string)reader["NR Segment"]

                        });
                    }
                }


            }
            catch (Exception ex)
            {
                logger.Error("CallRecordingReport_Result", ex);
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return result;
        }
    }

}