﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;
using System.Globalization;
using System.Web.Mvc;
using System.Web.Security;

namespace ClientDashboard.Models
{
    public class EditUserViewModel
    {

        [Editable(false)]

        [Display(Name = "User ID")]
        public string UserId { get; set; }
        [Editable(false)]
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        [Required]
        [Display(Name = "Email")]
        [RegularExpression(@"^([a-zA-Z0-9_\.\-])+\@(([a-zA-Z0-9\-])+\.)+([a-zA-Z0-9]{2,4})+$", ErrorMessage = "Please enter valid Email Address")]
        public string Email { get; set; }
        [Display(Name = "Full Name")]
        public string FullName { get; set; }

        [Display(Name = "Account Status")]
        public bool IsAccountActive { get; set; }

        [Display(Name = "Two Factor Authentication")]
        public bool TwoFactorEnabled { get; set; }

        public List<Role> RolesList { get; set; }
        public List<Module> ModulesList { get; set; }

    }
    public class LiveUsers
    {


        [Display(Name = "User ID")]
        public int UserId { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Last Activity Time")]
        public string LastActivityTime { get; set; }



    }
    public class UserActivityHistory
    {


        [Display(Name = "User ID")]
        public int UserId { get; set; }

        [Display(Name = "User Name")]
        public string UserName { get; set; }

        [Display(Name = "Module")]
        public string Module { get; set; }

        [Display(Name = "Campaign")]
        public string Campaign { get; set; }

        [Display(Name = "Visits")]
        public string Visits { get; set; }

    }
    public class CampaignViewModel
    {
        public IEnumerable<SelectListItem> CampaignType
        {
            get
            {
                return new[]
            {
                new SelectListItem { Value = "Inbound Program", Text = "Inbound Program" },
                new SelectListItem { Value = "Outbound Program", Text = "Outbound Program" },
                new SelectListItem { Value = "Verification Program", Text = "Verification Program" },
            };
            }
        }
    }
    public class MailModel
    {

        public string Email { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        [StringLength(7500)]
        [UIHint("tinymce_full_compressed")]
        [AllowHtml]
        public string Body { get; set; }
    }
    public class ThresholdModel
    {
        public string UserID { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public string CampaignID { get; set; }
        public int MetricID { get; set; }
        public string MetricDescription { get; set; }
        public decimal CurrentValue { get; set; }
        public decimal ThresholdValue { get; set; }
        public bool IfAbove { get; set; }
    }
    public partial class TfnQueueDispositionReport
    {
        public string TfnQueue { get; set; }
        public int CallsOffered { get; set; }
        public int CallsHandled { get; set; }
        public int CallsAbandon { get; set; }
        public string CampaignID { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public int SalesCall { get; set; }
        public int CallCount { get; set; }
        public string Disposition { get; set; }
    }

    public partial class LeadsReport
    {
        public string CampaignID { get; set; }
        public int Available { get; set; }
    }
    public class MenuItems
    {
        public string Module { get; set; }
        public string Description { get; set; }
        public string CampaignID { get; set; }
        public string HTML { get; set; }
    }
    public class Header
    {
        public string ModuleDescription { get; set; }
        public string CampaignID { get; set; }
        public string HTML { get; set; }
        public string Client { get; set; }
        public string CampaignType { get; set; }
        public string SwitchType { get; set; }
    }
}