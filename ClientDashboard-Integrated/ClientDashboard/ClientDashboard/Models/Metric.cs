//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientDashboard.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class Metric
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Metric()
        {
            this.CampaignMetrics = new HashSet<CampaignMetric>();
            this.MetricGraphs = new HashSet<MetricGraph>();
            this.ThresholdMetrics = new HashSet<ThresholdMetric>();
            this.UserThresholds = new HashSet<UserThreshold>();
        }
    
        public int MetricID { get; set; }
        public string Name { get; set; }
        public int KeywordID { get; set; }
        public string Description { get; set; }
        public bool IsAvaya { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<CampaignMetric> CampaignMetrics { get; set; }
        public virtual Keyword Keyword { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<MetricGraph> MetricGraphs { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ThresholdMetric> ThresholdMetrics { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<UserThreshold> UserThresholds { get; set; }
    }
}
