﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace ClientDashboard.Models
{
    
    
    public class ModuleMetaData
    {
        public ModuleMetaData()
        {
            this.AspNetUserRoles = new HashSet<AspNetUserRole>();
            this.ReportHeaders = new HashSet<ReportHeader>();
        }
        [Display(Name = "Module ID")]
        public int ModuleID { get; set; }
        [Required]
        [Display(Name = "Module Name")]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [AllowHtml]
        [Display(Name = "Module Icon")]
        public string HTML { get; set; }

        public virtual ICollection<AspNetUserRole> AspNetUserRoles { get; set; }
        public virtual ICollection<ReportHeader> ReportHeaders { get; set; }
    }
    public class ClientMetaData
    {
        public ClientMetaData()
        {
            this.AspNetRoles = new HashSet<AspNetRole>();
        }

        [Display(Name = "Client ID")]
        public int ClientID { get; set; }
        [Required]
        [Display(Name = "Client Name")]
        public string Name { get; set; }
        [Display(Name = "Image")]
        public string ImagePath { get; set; }
        public string Website { get; set; }

        public virtual ICollection<AspNetRole> AspNetRoles { get; set; }
    }
    public class ReportHeaderMetaData
    {
        public string A { get; set; }
        public string B { get; set; }
        public string C { get; set; }
        public string D { get; set; }
        public string E { get; set; }
        public string F { get; set; }
        public string G { get; set; }
        public string H { get; set; }
        public string I { get; set; }
        public string J { get; set; }
        public string K { get; set; }
        public string L { get; set; }
        public string M { get; set; }
        public string N { get; set; }
        public string O { get; set; }
        public string P { get; set; }
        public string Q { get; set; }
        public string R { get; set; }
        public string S { get; set; }
        public string T { get; set; }
        public string U { get; set; }
        public string V { get; set; }
        public string W { get; set; }
        public string X { get; set; }
        public string Y { get; set; }
        public string Z { get; set; }
        [Key]
        [Column(Order = 1)]
        public string CampaignID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int ModuleID { get; set; }
        public System.DateTime UpdatedOn { get; set; }

        public virtual Module Module { get; set; }
    }
    public class ActiveConnectionMetaData
    {
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }
        [Key]
        [Column(Order = 2)]
        public string ConnectionID { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }

    public partial class UserThresholdMetaData
    {
        [Key]
        [Column(Order = 1)]
        public string UserID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int MetricID { get; set; }
        [Key]
        [Column(Order = 3)]
        public string CampaignID { get; set; }

        public Nullable<decimal> ThresholdValue { get; set; }
        public Nullable<bool> IsActive { get; set; }
        public Nullable<bool> IsSent { get; set; }
        public Nullable<bool> IfAbove { get; set; }
       
        public Nullable<System.DateTime> AddedOn { get; set; }
        public Nullable<System.DateTime> UpdatedOn { get; set; }
        public Nullable<System.DateTime> LastSentOn { get; set; }
        public virtual Metric Metric { get; set; }

        public virtual AspNetUser AspNetUser { get; set; }
    }
    public partial class CampaignMetricMetaData
    {
        [Key]
        [Column(Order = 1)]
        public int MetricID { get; set; }
        [Key]
        [Column(Order = 2)]
        public string CampaignID { get; set; }
        public string Formula { get; set; }
        public Nullable<bool> Isactive { get; set; }
        public string Value { get; set; }

        public virtual Metric Metric { get; set; }
    }
    public partial class MetricGraphMetaData
    {
        [Key]
        [Column(Order = 1)]
        public int GraphID { get; set; }
        [Key]
        [Column(Order = 2)]
        public int MetricID { get; set; }
        public string Description { get; set; }

        public virtual Graph Graph { get; set; }
        public virtual Metric Metric { get; set; }
    }
}