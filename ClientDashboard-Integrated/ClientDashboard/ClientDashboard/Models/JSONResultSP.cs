﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Classes;
using Newtonsoft.Json;
using System.Text;

namespace ClientDashboard.Models
{
    public class JSONResultSP
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        public string GetJSONResult(string spname,string startdate, string enddate, string campaignid)
        {

            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();

            SqlCmd.CommandTimeout = 300;
            SqlCmd.CommandText = spname;
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startdate;
            SqlCmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = enddate;
            SqlCmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = campaignid;

            SqlCmd.Connection = SqlConn;
            var JSONString = new StringBuilder();
            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    DataTable table = new DataTable();
                    table.Load(SqlCmd.ExecuteReader());

                  
                    if (table.Rows.Count > 0)
                    {
                        JSONString.Append("[");
                        for (int i = 0; i < table.Rows.Count; i++)
                        {
                            JSONString.Append("{");
                            for (int j = 0; j < table.Columns.Count; j++)
                            {
                                if (j < table.Columns.Count - 1)
                                {
                                    JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\",");
                                }
                                else if (j == table.Columns.Count - 1)
                                {
                                    JSONString.Append("\"" + table.Columns[j].ColumnName.ToString() + "\":" + "\"" + table.Rows[i][j].ToString() + "\"");
                                }
                            }
                            if (i == table.Rows.Count - 1)
                            {
                                JSONString.Append("}");
                            }
                            else
                            {
                                JSONString.Append("},");
                            }
                        }
                        JSONString.Append("]");
                    }
                   
                }


            }
            catch (Exception ex)
            {
                logger.Error("JSONResultSP", ex);
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return JSONString.ToString();  
        }

    }
}