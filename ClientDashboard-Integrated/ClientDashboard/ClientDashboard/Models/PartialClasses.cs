﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClientDashboard.Models
{
   

    [MetadataType(typeof(ModuleMetaData))]
    public partial class Module
    {

    }

    [MetadataType(typeof(ClientMetaData))]
    public partial class Client
    {

    }
    [MetadataType(typeof(ReportHeaderMetaData))]
    public partial class ReportHeader
    {

    }
    [MetadataType(typeof(ActiveConnectionMetaData))]
    public partial class ActiveConnection
    {

    }
    [MetadataType(typeof(UserThresholdMetaData))]
    public partial class UserThreshold
    {

    }
    [MetadataType(typeof(CampaignMetricMetaData))]
    public partial class CampaignMetric
    {

    }
    [MetadataType(typeof(MetricGraphMetaData))]
    public partial class MetricGraph
    {
    }
}