﻿var avayaAgentStats = [], avayaAgentStates = [], avayaSkillStats = []
var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", '#805b69', "#1E824C", '#2d3ab7', '#b7aa2d', '#3a6166', '#14a5ab', '#35817b', '#9179de', '#c0fff4', '#995460', '#528881', '#584778', '#4f62d8', '#7e708b', '#9a666e', '#2e2126', '#4a152a', '#6b1f3c', '#abffe3', '#ffd9df', '#f3c3c3', '#ffb5be', '#00028a', '#00027a', '#00025a', '#a93b1c', '#a43e32', '#a42f1e', '#953904', '#6c2801', '#fcfac9', '#fc4078', '#fc2b6a', '#b00070', '#c00090']

var selectedskill = "", selectedskills = [], selectedagent = "";
var skills = [], skillInfo = {}, agentName = {}, skillNames = []
$(document).ready(function () {

    $('#dialog').hide();
    $('#inputalert').hide();
    $('#lastUpdateTime').hide();
    $('#lastupdateon').hide();
    $('#metrics').select2()

    var selection = $('#cbCampaigns').val();
    cname = selection.substring(selection.indexOf("-") + 1).substring(0, 3);
    $('#queue-agent-table').dataTable({

        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": ["no-sort"]
        }],
        "bSortCellsTop": true,
        //scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,

        "order": [[0, "desc"]],
        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Queue Agent Report' + formatDateforTable(new Date()),

            }

        ]

    });
    $('#split-skill-table').dataTable({

        "aoColumnDefs": [{
            "bSortable": false,
            "aTargets": ["no-sort"]
        }],
        "bSortCellsTop": true,
        //scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,

        "order": [[0, "desc"]],
        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Split Skill Report' + formatDateforTable(new Date()),

            }

        ]

    });
    // fix dimensions of chart that was in a hidden element
    jQuery(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) { // on tab selection event
        Highcharts.each(Highcharts.charts, function (chart) {
            try {
                chart.reflow() // reflow that chart
            }
            catch (ex) { }
        });
        var table = $('#queue-agent-table').DataTable();
        table.draw(false);
        table = $('#split-skill-table').DataTable();
        table.draw(false);
    })
    //Multi Select List on Split/Skill Report for Metric - Avg Time per Split/Skill
    $('#split-skill-metric').select2()
    $("#split-skill-metric").val(["AvgSpeedAns"]);
    $('#split-skill-metric').select2().on("select2:select", function (e) {
        var val = e.params.data.id;

        SS_Time(avayaSkillStats)
    }).on("select2:unselect", function (e) {
        if (!e.params.originalEvent) {
            return;
        }

        e.params.originalEvent.stopPropagation();
        var val = e.params.data.id;

        SS_Time(avayaSkillStats)

    })

    //Multi Select List on Split/Skill Report for Skills
    $('#split-skill-skills').select2().on("select2:select", function (e) {
        var val = e.params.data.id;
        selectedskills = $("#split-skill-skills").val();
        SS_AgentStates(avayaAgentStates)
        SS_SkillTable(avayaSkillStats)
        SS_Calls(avayaSkillStats)
        SS_Time(avayaSkillStats)
    }).on("select2:unselect", function (e) {
        if (!e.params.originalEvent) {
            return;
        }

        e.params.originalEvent.stopPropagation();
        var val = e.params.data.id;
        selectedskills = $("#split-skill-skills").val();
        SS_AgentStates(avayaAgentStates)
        SS_Time(avayaSkillStats)
        SS_SkillTable(avayaSkillStats)
        SS_Calls(avayaSkillStats)
    })
    $("#split-skill-state").on('change', function () {
        selectedskill = $('#queue-agent-skills').val()
        SS_AgentStates(avayaAgentStates)
    });
    $('#queue-agent-skills').on('change', function () {
        selectedskill = $('#queue-agent-skills').val()
        QA_AgentStates(avayaAgentStates)
        QA_AgentTable(avayaAgentStats)
        QA_SkillStats(avayaSkillStats)
    });
    $('#agent-agents').on('change', function () {
        selectedagent = $('#agent-agents').val()
        Agent_Info(avayaAgentStats)
        Agent_TimeSpent(avayaAgentStats)
    });
    GetCampaignSkills();
    GetCampaignAgents();
    InitializeSignalREvents();


});
function InitializeSignalREvents() {
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {

            notifications.server.getAvayaSkillStats().done(function (stats) {

                avayaSkillStats = stats
                PopulateAvayaSkillStats(stats)

            });
            notifications.server.getAvayaAgentStats().done(function (stats) {
                avayaAgentStats = stats
                PopulateAvayaAgentStats(stats)

            });
            notifications.server.getAvayaAgentStates().done(function (stats) {
                avayaAgentStates = stats
                PopulateAvayaAgentStates(stats)

            });


        });
    }, 2000);
    notifications.client.updateAvayaSkillStats = function (stats) {

        avayaSkillStats = stats
        PopulateAvayaSkillStats(stats)

    };
    notifications.client.updateAvayaAgentStats = function (stats) {

        avayaAgentStats = stats
        PopulateAvayaAgentStats(stats)

    };
    notifications.client.updateAvayaAgentStates = function (stats) {

        avayaAgentStates = stats
        PopulateAvayaAgentStates(stats)
    };

}
function GetCampaignSkills() {
    $.ajax({
        url: "/Dashboard/GetAvayaSkills/" + cname,
        type: "GET",
        traditional: true,


        success: function (data) {


            for (var i = 0; i < data.result.length; i++) {
                if (data.result[i]["Skill"] != "0") {
                    $('#queue-agent-skills').append('<option value="' + data.result[i]["Skill"] + '">' + data.result[i]["Name"] + '</option>');

                    $('#split-skill-skills').append('<option value="' + data.result[i]["Skill"] + '">' + data.result[i]["Name"] + '</option>');
                    skillInfo[data.result[i]["Skill"]] = data.result[i]["Name"]
                    skills.push(data.result[i]["Skill"])
                    skillNames.push(data.result[i]["Name"])

                }

            }

            $('#queue-agent-skills').select2()
            $('#split-skill-skills').select2()
            $("#split-skill-skills").val(skills).trigger("change");
        }
    });
}
function GetCampaignAgents() {
    $.ajax({
        url: "/Dashboard/GetAvayaAgents/" + cname,
        type: "GET",
        traditional: true,


        success: function (data) {


            for (var i = 0; i < data.result.length; i++) {

                $('#agent-agents').append('<option value="' + data.result[i]["Avaya_ID"] + '">' + data.result[i]["Name"] + '</option>');


                agentName[data.result[i]["Avaya_ID"]] = data.result[i]["Name"]

            }

            $('#agent-agents').select2()
        }
    });
}

function PopulateAvayaAgentStats(stats) {
    selectedskill = $('#queue-agent-skills').val()
    selectedskills = $("#split-skill-skills").val();
    Agent_TimeSpent(stats)
    Agent_Info(stats)
    QA_AgentTable(stats)
}
function PopulateAvayaAgentStates(stats) {
    selectedskill = $('#queue-agent-skills').val()
    selectedskills = $("#split-skill-skills").val();
    QA_AgentStates(stats)
    SS_AgentStates(stats)
}
function PopulateAvayaSkillStats(stats) {
    selectedskill = $('#queue-agent-skills').val()
    selectedskills = $("#split-skill-skills").val();
    QA_SkillStats(stats)
    SS_Calls(stats)
    SS_SkillTable(stats)
    SS_Time(stats)
}
function QA_AgentStates(stats) {
    var agentArr = ["ACD", "ACW", "AUX", "Avail", "Other", "Ring"]
    var agentState = []
    $.each(stats, function (index, value) {
        if (value.Skill == selectedskill) {

            for (var i = 0; i < agentArr.length; i++) {

                var metric = agentArr[i];

                document.getElementById('queue-agent-' + metric.toLowerCase()).textContent = value[metric];

                if (parseInt(value[metric]) != 0)
                    agentState.push([metric, parseInt(value[metric])])


            }
        }
    });

    DrawDonutChart("queue-agent-state", "Agent Per State", "agents", agentState)
}
function QA_AgentTable(stats) {
    var dataset = []

    $.each(stats, function (index, value) {

        if (value.Skill == selectedskill) {
            var arr = []
            arr.push(agentName[value.AgentID]);
            arr.push(value.AgentID);
            arr.push(value.Extension);
            arr.push(value.AUXReason)
            arr.push(value.State);
            arr.push(value.Direction);
            arr.push(value.Skill);
            arr.push(value.StateTime)
            arr.push(value.VDNName)

            dataset.push(arr)
        }
    });
    var oTable = $('#queue-agent-table').DataTable();
    oTable.clear();

    $('#queue-agent-table').DataTable().rows.add(dataset).draw(true);
}
function QA_SkillStats(stats) {
    var skillArr = ["CallsWaiting", "OldestCallWaiting", "DirectAgentCallWaiting", "ServiceLevel", "ServiceLevelPercent", "ACDCalls", "ABNCalls"]
    for (var i = 0; i < skillArr.length; i++) {
        var metric = skillArr[i];
        document.getElementById('queue-agent-' + metric.toLowerCase()).textContent = "";
    }
    $.each(stats, function (index, value) {
        if (value.Skill == selectedskill) {

            for (var i = 0; i < skillArr.length; i++) {

                var metric = skillArr[i];

                document.getElementById('queue-agent-' + metric.toLowerCase()).textContent = value[metric];
            }
        }
    });

}
function SS_Calls(stats) {


    var callsArr = ["ACD Calls", "ABN Calls", "Calls Waiting"]


    var series = []
    for (var i = 0; i < callsArr.length; i++) {
        var name = callsArr[i]
        var arr = []
        var metric = name.replace(" ", "")

        $.each(stats, function (index, value) {
            if (selectedskills.indexOf(value.Skill + "") > -1)
                arr.push(parseInt(value[metric]))
        });

        var data = arr;
        if (data.length > 0)
            series.push({
                "name": name,
                "data": data,
                type: 'column',
                yAxis: 0,
                tooltip: {
                    valueSuffix: ' calls'
                }
            });
    }
    arr = []

    DrawMultiBarChart("split-skill-call", "Calls per Split/Skill", "Split/Skill", "Calls", "Skill", skillNames, series, "hours")
}
function SS_Time(stats) {

    var selectedmetrics = $("#split-skill-metric").val();
    var series = []
    for (var i = 0; i < selectedmetrics.length; i++) {
        var name = selectedmetrics[i]
        var arr = []
        console.log("name"+name)
        //    var metric = name.replace(" ", "")
        $.each(stats, function (index, value) {
            if (value.Skill != 0 && selectedskills.indexOf(value.Skill + "") > -1) {
               
                    arr.push(parseFloat(value[name]))
            }
        });
        console.log(arr)
        var data = arr;
        if (data.length > 0)
            series.push({
                "name": $("#split-skill-metric option[value=" + name + "]").text(),
                "data": data,
                type: 'spline',

                tooltip: {
                    valueSuffix: ' '
                }
            });
    }
    console.log(series)
    DrawSplineChart("split-skill-time", "Avg time per Split/Skill", "Split/Skill", "Hours", "Skill", skillNames, series,"hours")

}
function SS_AgentStates(stats) {
    var state = $("#split-skill-state").val()
    var agentState = []
    $.each(stats, function (index, value) {
        if (value.Skill != 0 && selectedskills.indexOf(value.Skill + "") > -1) {
            var skill = value.Skill;

            if (parseInt(value[state]) != 0)

                agentState.push({
                    name: skillInfo[skill],
                    y: parseInt(value[state])
                })

        }
    });

    DrawPieChart("split-skill-agents", "Agents per Split/Skill", "agents", agentState)
}
function SS_SkillTable(stats) {
    var dataset = []

    $.each(stats, function (index, value) {

        if (value.Skill != 0 && selectedskills.indexOf(value.Skill + "") > -1) {
            var arr = []
            arr.push(skillInfo[value.Skill]);
            arr.push(value.CallsWaiting);
            arr.push(value.OldestCallWaiting);
            arr.push(hoursTohhmm(value.AvgSpeedAns))
            //   arr.push(value.DirectAgentCallWaiting)
            //arr.push(value.ServiceLevel);
            //arr.push(value.ServiceLevelPercent);
            arr.push(value.ACDCalls);
            arr.push(hoursTohhmm(value.AvgACDTime))
            arr.push(value.ABNCalls)
            arr.push(hoursTohhmm(value.AvgAbanTime))

            dataset.push(arr)
        }
    });
    var oTable = $('#split-skill-table').DataTable();
    oTable.clear();

    $('#split-skill-table').DataTable().rows.add(dataset).draw(true);
}
function Agent_TimeSpent(stats) {
    var agentArr = ["ACDTime", "ACWTime", "AUXTime", "AvailTime", "OtherTime", "RingTime"]
    var agentState = []

    $.each(stats, function (index, value) {
        if (value.AgentID == selectedagent) {
            for (var i = 0; i < agentArr.length; i++) {

                var metric = agentArr[i];
                agentState.push([metric.replace("Time", ""), parseFloat(value[metric])])


            }
        }
    });



    DrawPieChart("agent-timespent", "Agent Time Spent Interval", "hours", agentState)
}
function Agent_Info(stats) {

    var agentInfoArr = ["AgentID", "Extension", "State", "StateTime", "Skill", "AUXReason", "ACDCalls", "StaffTime"];
    for (var i = 0; i < agentInfoArr.length; i++) {
        var metric = agentInfoArr[i];
        document.getElementById('agent-' + metric.toLowerCase()).textContent = "";
    }
    $.each(stats, function (index, value) {
        if (value.AgentID == selectedagent) {

            for (var i = 0; i < agentInfoArr.length; i++) {

                var metric = agentInfoArr[i];
                if (metric == "StaffTime")
                    document.getElementById('agent-' + metric.toLowerCase()).textContent = hoursTohhmm(value[metric]);
                else
                    document.getElementById('agent-' + metric.toLowerCase()).textContent = value[metric];
            }
        }
    });
}