﻿var cname = "", campaignMetrics = {}
$(function () {
    $.fn.editable.defaults.mode = 'popup';
    toastr.options = { "positionClass": "toast-top-right" }
    $("#lastupdateon").hide();
    $('.checkboxpicker').checkboxpicker({
        offLabel: "Below",
        onLabel: "Above"
    });
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).substring(0, 3);
    var table = $('#table').dataTable({
        "order": [[1, "asc"]]

    });
    GetCampaignMetrics();
    GetUserThresolds();
    $('#addRow').on('click', function () {
        ValidateRow()
    });
});
function ValidateRow() {
    var threshold = $("#threshold").val()
    var metric = $("#metric option:selected").text();
    var metricid = $("#metric").val()
    var ifAbove = $("#operator").is(':checked')
    if (threshold == "") {

        toastr.error("Please enter threshold value.");
    }
    else if (!isNumeric(threshold)) {
        toastr.error("Please enter numeric values as threshold.");
    }
    else {
        if ($('#table tr > td:contains(' + metric + ') ').length > 0) {    //+ td:contains(' + threshold + ')').length > 0) {

            toastr.error("A threshold value for this mertic is already defined.");
        }

        else {
            AddUserThreshold(metricid, metric, threshold, ifAbove)

        }
    }
}
function AddRow(metricid, metric, threshold, ifAbove) {

    var table = $('#table').DataTable();

    var count = table.data().count()
    num = count + 1

    ifAbove = ifAbove == true ? "Above " : "Below ";

    var thresholdHTMLID = "threshold" + num
    var thresholdHTML = '<span id=' + thresholdHTMLID + '>' + threshold + '</span>'
    var activeHTMLID = 'active' + num;
    var activeHTML = '<span id="' + activeHTMLID + '" >Yes</span>';
    var aboveHTMLID = 'above' + num;
    var aboveHTML = '<span id="' + aboveHTMLID + '">' + ifAbove + '</span>'

    table.row.add([
   metric,
   aboveHTML + thresholdHTML,
   activeHTML,
   '<button value="Edit" class="btn btn-default transparent" onclick="EditUserThreshold(' + metricid + ',\'' + thresholdHTMLID + '\',\'' + activeHTMLID + '\',\'' + aboveHTMLID + '\')"><span class="glyphicon glyphicon-pencil"></span></button>'

    ]).draw(false);


}
$(document).on('click', '.editable-cancel, .editable-submit', function () {
    $('.edit').show();
})
$(document).on('click', 'body *', function () {
    $('.edit').show();
})
function GetCampaignMetrics() {
    $.ajax({
        url: "/Dashboard/GetMetrics/" + cname,
        type: "GET",
        traditional: true,
        success: function (data) {
            for (var i = 0; i < data.result.length; i++) {
                campaignMetrics[data.result[i]["MetricID"]] = data.result[i]["Description"]
            }
            PopulateMetrics("metric")
        }
    });
}
function GetUserThresolds() {
    $.ajax({
        url: "/Dashboard/GetUserThresholds/" + cname,
        type: "GET",
        traditional: true,
        success: function (data) {
            console.log(data.result)
            var len = data.result.length;
            console.log(data);
            thresharr = []
            for (i = 0; i < len; i++) {
                var arr = [];

                var value = data.result[i];

                var ifAbove = value.IfAbove == true ? "Above " : "Below ";
                var isActive = value.IsActive == true ? "Yes" : "No";

                var thresholdHTMLID = 'threshold' + parseInt(i + 1);
                var thresholdHTML = '<span id=' + thresholdHTMLID + '>' + value.ThresholdValue + '</span>'
                var activeHTMLID = 'active' + parseInt(i + 1);
                var activeHTML = '<span id="' + activeHTMLID + '" >' + isActive + '</span>';
                var aboveHTMLID = 'above' + parseInt(i + 1);
                var aboveHTML = '<span id="' + aboveHTMLID + '">' + ifAbove + '</span>'



                arr.push(value.Description);
                arr.push(aboveHTML + thresholdHTML);
                arr.push(activeHTML)
                arr.push('<button value="Edit" class="btn btn-default transparent" onclick="EditUserThreshold(' + value.MetricID + ',\'' + thresholdHTMLID + '\',\'' + activeHTMLID + '\',\'' + aboveHTMLID + '\')"><span class="glyphicon glyphicon-pencil"></span></button>');
                thresharr.push(arr);

            }

            $('#table').DataTable().clear();
            $('#table').DataTable().rows.add(thresharr).draw(true);


        }
    });
}
function EditUserThreshold(metricid, thresholdID, activeID, aboveID) {
    var threshold = $("#" + thresholdID).text()
    var isActive = $("#" + activeID).text().trim()
    var ifAbove = $("#" + aboveID).text().trim();
    var metric = campaignMetrics[metricid];

    $("#editMetricId").text(metricid);
    $("#editMetric").text(metric);
    $("#editThreshold").val(threshold);

    if (isActive == "Yes") {
        $("#editActive").prop('checked', true);
    }
    else
        $("#editActive").prop('checked', false);

    if (ifAbove == "Above") {
        $("#editAbove").prop('checked', true);
    }
    else
        $("#editAbove").prop('checked', false);
    $("#ThresholdModal").modal("show");
}
function SaveUserThreshold() {
    var threshold = $("#editThreshold").val()
    var isActive = $("#editActive").is(':checked');
    var ifAbove = $("#editAbove").is(':checked');
    var metricId = $("#editMetricId").text()

    if (!isNumeric(threshold)) {
        toastr.error("Please enter numeric values as threshold.");
    }
    else {
        $.ajax({
            url: "/Dashboard/SaveUserThreshold/",
            type: "GET",
            traditional: true,
            data: { campaignid: cname, metricid: metricId, value: threshold, isactive: isActive, ifabove: ifAbove },

            success: function (data) {

                $("#ThresholdModal").modal("hide");
                GetUserThresolds();
                toastr.success("Changes have been saved successfully.");

            }
        });
    }
}
function AddUserThreshold(metricid, metric, threshold, ifAbove) {



    $.ajax({
        url: "/Dashboard/AddUserThreshold/",
        type: "GET",
        traditional: true,
        data: { campaignid: cname, metricid: metricid, value: threshold, ifabove: ifAbove },

        success: function (data) {
            AddRow(metricid, metric, threshold, ifAbove)

            toastr.success("Threshold details have been added successfully.");
        }
    });

}

function PopulateMetrics(id) {
    for (var i in campaignMetrics) {

        $('#' + id).append('<option value="' + i + '">' + campaignMetrics[i] + '</option>');
    }
}
function isNumeric(n) {
    return !isNaN(parseFloat(n)) && isFinite(n);
}
