﻿var intervalchart, TFNQpiechart, table, statuspiechart;
var sum = 0, intervalflag = false, statusflag = false, TFNQflag = false, cname;
var dataSet = [], total = []
var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", "#e74c3c", "#1E824C", "#a6dcf1"]
$(function () {
    $("#spinner1").show()
    $("#spinner2").show()
    $("#spinner3").show()
    $("#intervalgraphcontainer").hide()
    $("#TFNQpiecontainer").hide()
    $("#statuspiecontainer").hide()
    cname = $("#CampaignName").text().toLowerCase();
    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica'

            }
        }
    });


    $.fn.dataTableExt.sErrMode = 'throw';

    table = $('#campaigntable1').dataTable({
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] }],

        "bSortCellsTop": true,
        //scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,
        "bProcessing": true,
        "order": [[0, "desc"]],
        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Campaign Statistics' + formatDateforTable(new Date()),

            }

        ]

    });
    table.fnProcessingIndicator();
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });
            notifications.server.getAgentCountStats().done(function (Stats) {
                populateAgentCountStats(Stats)
            });
            notifications.server.getQueueStats().done(function (Stats) {
                // alert("getQueueStats");
                populateQueueStats(Stats)
            });
            notifications.server.getTFNQueueStats().done(function (Stats) {
                //alert("getTFNQStats");
                populateTFNQStats(Stats)
            });
            notifications.server.getAgentStatusStats().done(function (Stats) {

                populateAgentStatusStats(Stats)
            });
            notifications.server.getIntervalStats().done(function (Stats) {
                //  alert("updateIntervalStats");
                var oTable = $('#campaigntable1').DataTable();
                oTable.clear();
                $('#campaigntable1').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try{
                    oTable.button(0).enable(nRows > 0);
                }
                catch(Ex)
                {}
            });;
        });
    }, 1000);
    var tryingToReconnect = false;

    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            //$("#lastUpdateTime").text("Disconnected");
            $("#dialog").dialog("open");
            //notifyUserOfDisconnect(); // Your function to notify user.
        }
    });
    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateAgentCountStats = function (Stats) {
        // alert("updateAgentStateStats");
        populateAgentCountStats(Stats)
    };
    notifications.client.updateQueueStats = function (Stats) {
        // alert("updateQueueStats");
        populateQueueStats(Stats)
    };
    notifications.client.updateTFNQueueStats = function (Stats) {
        // alert("updateTFNQStats");
        populateTFNQStats(Stats)
    };
    notifications.client.updateAgentStatusStats = function (Stats) {

        populateAgentStatusStats(Stats)
    };
    notifications.client.updateIntervalStats = function (Stats) {
        //  alert("updateIntervalStats");
        var oTable = $('#campaigntable1').DataTable();
        oTable.clear();
        $('#campaigntable1').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
    };
    //***************************************************************************************//
    intervalflag = false
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        closeText: 'Close',
        draggable: false,
        minHeight: 100,
        dialogClass: 'main-dialog-class',
        close: function (event, ui) {
            window.location.href = "http://clientdashboard.ibexglobal.com/"
        }
    });

})
function refreshDialingStatus(result) {
    var list = result;
    var dialstatus;
    $("#DialingStatus").hide();
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            dialstatus = value.DialStatus;

        }
    });
    $("#status").text(cname.toUpperCase() + " is not dialing at the moment.");
    if (dialstatus == "Inactive") {
        alert("cam")
        $("#DialingStatus").show();
        $("#head_dialingStatus").text("Inactive")
    }
    else
        $("#head_dialingStatus").text("Active")
}
function populateAgentStatusStats(result) {

    var list = result;
    var states = [], agents = [], statusdata = []
    var i = 0;
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            states.push(value.AgentState);
            agents.push(value.AgentCount);
            var temp = new Array(states[i], agents[i]);
            statusdata[i] = temp;
            i++;
        }


    });

    $("#spinner3").hide()
    $("#statuspiecontainer").show()
    statuspiechart = $('#statuspiecontainer').highcharts();
    if (statusflag == false) {
        DrawStatusChart(statusdata)
        statusflag = true;
    }
    else {

        statuspiechart.series[0].setData(statusdata);
    }
    statuspiechart = $('#statuspiecontainer').highcharts();
    if (!statuspiechart.hasData()) {
        statuspiechart.hideNoData();
        statuspiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateQueueStats(result) {
    var list = result;
    var calls = 0
    $.each(list, function (index, value) {
       
        if (value.CampaignID.toLowerCase() == cname) {
            calls += value.QueuedCalls
            document.getElementById("hi-icon-que").textContent = calls;
        }
    });
    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateAgentCountStats(result) {
    var list = result;
    $("#onWrap").text(0);
    $("#onTalk").text(0);
    $("#onWait").text(0);
    $("#onBreak").text(0);
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            $("#onWrap").text(value.AgentsOnWrap);
            $("#onTalk").text(value.AgentsOnTalk);
            $("#onWait").text(value.AgentsOnWait);
            $("#onBreak").text(value.AgentsOnBreak);
        }



    });

    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateTFNQStats(result) {

    var categories = [], chartdata = []

    var list = result
    var i = 0
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            categories.push(value.TfnQueue)

            chartdata.push({
                y: value.CallsOffered,
                color: colors[i],
                name: categories[i],
                drilldown: {

                    categories: ['Answered', 'Abandoned'],
                    data: [value.CallsHandled, value.CallsAbandon]

                }
            })
            i++;
        }
    });

    var TFNQData = [], CallsData = [], i, j, dataLen = chartdata.length, drillDataLen, brightness;


    // Build the data arrays
    i = 0;
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            TFNQData.push({
                name: value.TfnQueue,
                y: chartdata[i].y,
                color: chartdata[i].color
            });


            drillDataLen = chartdata[i].drilldown.data.length;
            for (j = 0; j < drillDataLen; j += 1) {
                brightness = 0.2 - (j / drillDataLen) / 5;
                CallsData.push({
                    name: chartdata[i].drilldown.categories[j],
                    y: chartdata[i].drilldown.data[j],
                    color: Highcharts.Color(chartdata[i].color).brighten(brightness).get()
                });
            }
            i++;
        }
    });
    $("#spinner1").hide()
    $("#TFNQpiecontainer").show()

    if (TFNQflag == false) {
        DrawTFNQPieChart(TFNQData, CallsData)
        TFNQflag = true;
    }
    else {
        TFNQpiechart.series[0].setData(TFNQData);
        TFNQpiechart.series[1].setData(CallsData);
    }
    if (!TFNQpiechart.hasData()) {
        TFNQpiechart.hideNoData();
        TFNQpiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
}
function populateIntervalStats(result) {
    dataSet = []
    var oTable = $('#campaigntable1').DataTable();
    oTable.clear();
    var list = result;
    $("#spinner2").hide()
    $("#intervalgraphcontainer").show()
    var time = [], ser1 = [], ser2 = [], ser3 = [], total = []
    document.getElementById("hi-icon-off").textContent = 0;
    document.getElementById("hi-icon-ans").textContent = 0;
    document.getElementById("hi-icon-aban").textContent = 0;
    document.getElementById("hi-icon-occ").textContent = "0%";
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            if (value.Interval != "Total") {
                time.push(value.Interval)
                ser1.push(parseFloat(value.CallsHandled))
                ser2.push(parseFloat(value.CallsAbandonSwitch) + parseFloat(value.CallsAbandonIVR))
                ser3.push(parseFloat(value.CallOffered))
            }
            else {
                document.getElementById("hi-icon-off").textContent = value.CallOffered;
                document.getElementById("hi-icon-ans").textContent = value.CallsHandled;
                document.getElementById("hi-icon-aban").textContent = parseFloat(value.CallsAbandonSwitch) + parseFloat(value.CallsAbandonIVR);
                document.getElementById("hi-icon-occ").textContent = value.Occupancy + "%";
            }


            var arr = [];
            if (value.Interval != "Total") {
                arr.push(value.Interval);
                arr.push(value.CallOffered);
                arr.push(value.CallsHandled);
                if (cname == "fbt")
                    arr.push(value.CallsAbandonIVR);
                arr.push(value.CallsAbandonSwitch);
                arr.push(value.AbandonRate);
                arr.push(value.AHT);
                arr.push(value.ServiceLevelCall);
                arr.push(value.MaxWaitTime);
                arr.push(value.ASA);
                arr.push(value.Occupancy);
                arr.push(value.Logintime);
                arr.push(value.Breaktime);
                arr.push(value.ProductionHours);

                dataSet.push(arr)
            }
            else {
                total.push(value.Interval);
                total.push(value.CallOffered);
                total.push(value.CallsHandled);
                if (cname == "fbt")
                    total.push(value.CallsAbandonIVR);
                total.push(value.CallsAbandonSwitch);
                total.push(value.AbandonRate);
                total.push(value.AHT);
                total.push(value.ServiceLevelCall);
                total.push(value.MaxWaitTime);
                total.push(value.ASA);
                total.push(value.Occupancy);
                total.push(value.Logintime);
                total.push(value.Breaktime);
                total.push(value.ProductionHours);

            }
        }
    });

    if (intervalflag == false) {
        DrawIntervalChart()
        intervalflag = true;
    }

    intervalchart.xAxis[0].setCategories(time);
    intervalchart.series[1].setData(ser1);
    intervalchart.series[0].setData(ser2);
    var xTitle = "Interval (EST)"
    if (cname == "fbt")
        xTitle = "Interval (PST)"
    intervalchart.xAxis[0].update({
        title: {
            text: xTitle
        }
    });
    $('#frow th').each(function (i) {

        if ($(this).text() != "Total")
            $(this).text("");

    });
    if (total.length != 0) {
        $('#frow th').each(function (i) {
            if (total[i] != undefined && $(this).text() != "Total")
                $(this).text(total[i]);

        });
    }
    if (!intervalchart.hasData()) {
        intervalchart.hideNoData();
        intervalchart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
    return dataSet;

}
function DrawStatusChart(statusdata) {

    statuspiechart = $('#statuspiecontainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agents Status',
            align: 'left'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        legend:
            {
                itemStyle: {
                    "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
                }
            },
        series: [{
            name: "Agents",
            colorByPoint: true,
            data: statusdata
        }]
    });
}

function DrawIntervalChart() {

    intervalchart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'intervalgraphcontainer'
        },


        title: {
            text: 'Calls Per Interval',
            align: 'left'

        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of calls'
            },
            allowDecimals: false,
            minTickInterval: 1
        },
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>Interval ' + this.x + '</caption><tbody>';
                //loop each point in this.points
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                points += '<tr><th>Calls Offered: </th>'
                + '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                + '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Calls Abandoned',
            data: [],
            color: "#a6dcf1"

        },
       {
           name: 'Calls Handled',
           data: [],
           color: "#52b3d9"

       }
        //, {
        //    name: 'Calls Offered',
        //    data: [],
        //    color: 'rgba(126,86,134,.9)'


        //}

        ]
    });


}
function DrawTFNQPieChart(TFNQData, CallsData) {

    // Create the chart
    TFNQpiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'TFNQpiecontainer'
        },
        title: {
            text: 'Calls Per TFN/Queue',
            align: 'left'
        },

        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            //layout: 'vertical',
            //align: 'left',
            //verticalAlign: 'bottom',
            //enabled: true,
            borderWidth: 0,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%'],

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            },
            allowPointSelect: false

        },

        tooltip: {
            valueSuffix: '',
            formatter: function () {
                var s = Math.round((this.y))
                return this.point.name + ':<b>' + s + '</b> calls<br/>'
            }
        },
        series: [{
            animation: false,
            name: 'Offered',
            data: TFNQData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return '';
                },
                color: 'white',
                distance: -40,
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" }
            },
            showInLegend: true,

        }, {
            animation: false,
            name: 'Calls',
            data: CallsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                formatter: function () {
                    // display only if larger than 1
                    var s = Math.round((this.y))
                    if (this.point.name == "Abandoned")
                        return this.y > 0 ? '<b style="color:red">' + this.point.name + ': ' + s + '</b>' : null;
                    else
                        return this.y > 0 ? '<b style="color:black">' + this.point.name + ':</b> ' + s + '' : null;
                }
            }
        }]

    });


}
function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}

//$.ajax({
//    url: "/Dashboard/ExtractAgentCountData",
//    type: "GET",
//    traditional: true,
//    contentType: 'application/json',
//    datatype: 'json',
//    success: function (data) {

//        $("#lastUpdateTime").text(formatDate(new Date()));
//        populateAgentCountStats(data.AgentCount);
//    },

//});

//$.ajax({
//    url: "/Dashboard/ExtractRoutingQueueData",
//    type: "GET",
//    traditional: true,
//    contentType: 'application/json',
//    datatype: 'json',
//    success: function (data) {
//        dt = formatDate(new Date());
//        $("#lastUpdateTime").text(formatDate(new Date()));
//        populateQueueStats(data.Queue);
//    },

//});

//$.ajax({
//    url: "/Dashboard/ExtractTFNQueueData",
//    type: "GET",
//    traditional: true,
//    contentType: 'application/json',
//    datatype: 'json',
//    success: function (data) {
//        $("#lastUpdateTime").text(formatDate(new Date()));
//        populateTFNQStats(data.TFNQueue);
//    },

//});
//$.ajax({
//    url: "/Dashboard/ExtractAgentStatusData1",
//    type: "GET",
//    traditional: true,
//    contentType: 'application/json',
//    datatype: 'json',
//    success: function (data) {
//        $("#lastUpdateTime").text(formatDate(new Date()));
//        populateAgentStatusStats(data.AgentStatus);
//    },

//});