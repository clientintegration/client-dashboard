﻿$(document).ready(function () {
    $("#DialingStatus").hide()

    // set default dates
    var start = new Date();
    // set end date to max one year period:
    var end = new Date(new Date().setYear(start.getFullYear() + 1));
    $('#inputalert').hide();
    $("#dateinputalert").hide();
    $('#agentdisposealert').hide();
    $("#dialog").hide();

    $("#lastUpdateTime").hide();
    $("#RefreshTime").hide();
    $("#refreshCheckbox").hide();
    $("#lastupdateon").hide();
    $("#autorefresh").hide();
    $("#refreshcheckboxlabel").hide();

    $('#startdate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        //  format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90))),
        endDate: new Date()
        // update "enddate" defaults whenever "startdate" changes
    }).on('changeDate', function () {
        // set the "enddate" start to not be later than "startdate" ends:
        $('#enddate').datepicker('setStartDate', new Date($(this).val()));
        var enddate = new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() + 15));
        if (enddate > new Date())
            $('#enddate').datepicker('setEndDate', new Date());
        else
            $('#enddate').datepicker('setEndDate', enddate);
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#enddate').val($(this).val());
        }
    });

    $('#enddate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        // format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90)))
        // update "startdate" defaults whenever "enddate" changes
    }).on('changeDate', function () {
        // set the "startdate" end to not be later than "enddate" starts:
        $('#startdate').datepicker('setEndDate', new Date($(this).val()));
        $('#startdate').datepicker('setStartDate', new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() - 15)));
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#startdate').val($(this).val());
        }
    });




    $('#Callrecordings').DataTable({
        responsive: true,
    });
    $('[data-toggle="tooltip"]').tooltip();


    //var records = '@TempData["Panel"]';
    //alert(records);
    //if (records == "false") {

    //    $("#collapseOne").removeClass('in');
    //    $("#collapseOne").addClass('collapsed');

    //    $("#collapseTwo").removeClass('collapsed');
    //    $("#collapseTwo").addClass('in');




    //}

    //else {

    //    $("#collapseOne").addClass('in');
    //    $("#collapseOne").removeClass('collapsed');

    //    $("#collapseTwo").addClass('collapsed');
    //    $("#collapseTwo").removeClass('in');



    //}

    $("#collapseOne").addClass('in');
    $("#collapseOne").removeClass('collapsed');

    $("#collapseTwo").addClass('collapsed');
    $("#collapseTwo").removeClass('in');    

    $("#collapseThree").addClass('collapsed');
    $("#collapseThree").removeClass('in');

   

    $('.collapse').on('shown.bs.collapse', function () {
        $('.accordion-body').each(function () {
            if ($(this).hasClass('in')) {
                console.log(this)
                $(this).collapse('toggle');
            }
        });
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
      
    }).on('hidden.bs.collapse', function () {
        $('#accordion').each(function () {
            if ($(this).hasClass('in')) {
                console.log(this)
                $(this).collapse('toggle');
            }
        });
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });


});
$(window).load(function () {
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();

    //Ajax Call to populate Disposition list per campaign
    var input = { id: cname };
    $.ajax({
        url: "/Dashboard/DispositionList",
        type: "GET",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        data: input,
        success: function (data) {

            for (i = 0; i < data.result.length; i++) {
                $('#disposition').append('<option value="' + data.result[i].termcd + '">' + data.result[i].description + '</option>');
            }
            $('.disposition').SumoSelect({ selectAll: true, search: true });
        },
        error: function (msg) {

        }
    });
});
function AlphaNumeric(e) {
    var regex = new RegExp("^[a-zA-Z0-9_\b]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
}

//$('#uniqueid').keypress(function (e) {
//    AlphaNumeric(e)
//});

//$('#agentid').keypress(function (e) {
//    AlphaNumeric(e)
//});

function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function Continue(e) {
    $('#inputalert').hide();

    $('#dateinputalert').hide();
    $('#agentdisposealert').hide();
    if ($('#startdate').val() == "" && $('#enddate').val() == "" && $('#uniqueid').val() == "" && $('#tel').val() == "" && $('#agentid').val() == "" && ($('#disposition').val() == "" || $('#disposition').val() == null)) {
        $('#inputalert').show();
        e.preventDefault();
    }
    else if (($('#startdate').val() != "" && $('#enddate').val() == "") || ($('#startdate').val() == "" && $('#enddate').val() != "")) {
        $('#dateinputalert').show();
        e.preventDefault();
    }
    else if ($('#agentid').val() != "" && ($('#startdate').val() == "" || $('#enddate').val() == "")) {
        $('#agentdisposealert').show();
        e.preventDefault();
    }
    else if ($('#disposition').val() != null && ($('#startdate').val() == "" || $('#enddate').val() == "")) {
        if ($('#disposition').val() != "" && ($('#startdate').val() == "" || $('#enddate').val() == "")) {
            $('#agentdisposealert').show();

            e.preventDefault();
        }
    }

    else {
        $.LoadingOverlay("show");
        $('#inputalert').hide();
        $('#dateinputalert').hide();
        //$('#startdate').attr('readonly', true);
        //$('#enddate').attr('readonly', true);
        //$('#uniqueid').attr('readonly', true);
        //$('#agentid').attr('readonly', true);
        //$('#tel').attr('readonly', true);

        //$("#collapseOne").removeClass('in');
        //$("#collapseOne").addClass('collapsed');

        //$("#collapseTwo").removeClass('collapsed');
        //$("#collapseTwo").addClass('in');

        //To hide Search criteria Panel and to show History Table Panel

        $('#collapseOne').collapse('hide');
        $('#collapseTwo').collapse('show');

     

        var selection = $("#cbCampaigns").val();
        cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();
        var search = {
            id: cname,
            startdate: $('#startdate').val(),
            enddate: $('#enddate').val(),
            uniqueid: $('#uniqueid').val(),
            tel: $('#tel').val(),
            agentid: $('#agentid').val(),
            termcd: $('#disposition').val()
        }

        $.ajax({
            url: "/Dashboard/SearchRecordings",
            type: "GET",
            traditional: true,
            contentType: 'application/json',
            datatype: 'json',
            data: search,
            success: function (data) {
                var len = data.result.length;
                if (len > 0) {
                    var resultset = [];
                    for (i = 0; i < len; i++) {
                        var resultarr = [];
                        var value = data.result[i];
                        var webnrurl = "https://nrplayer.ibexglobal.com/NRWeb/Recording/Play/" + value.Call_ID + "?platform=Avaya&mode=Audio"

                        resultarr.push(value.Unique_ID);
                        resultarr.push(value.Call_ID);
                        resultarr.push(value.Agent_ID);
                        resultarr.push(getJavaScriptDate(value.Calldate));
                        resultarr.push(value.Call_Outcome);
                        resultarr.push(value.Duration);
                        resultarr.push(value.Number);
                        resultarr.push('<a href="#" onclick="OpenWebPlayer(\'' + value.Call_ID + '\',\'' + value.NR_Segment + '\')" style="color:red">Play</a>');

                        resultarr.push('<a href="' + value.NR_Segment + '" target="_blank" style="color:red">Play</a>');
                        resultset.push(resultarr);
                    }
                    $('#Callrecordings').DataTable().clear();
                    $('#Callrecordings').DataTable().rows.add(resultset).draw(true);
                }


                $.LoadingOverlay("hide");

            },
            error: function (msg) {
                $.LoadingOverlay("hide");

            }
        });


    }
}
function OpenWebPlayer(callid, nrsegment) {

    var nrplayerForm = document.createElement("form");
    //nrplayerForm.target = "nrplayer-dev";
    nrplayerForm.target = "NRWebFrame"
    nrplayerForm.method = "POST";
    nrplayerForm.action = "https://nrplayer.ibexglobal.com/NRWeb/Recording/Play/" + callid + "?platform=Avaya&mode=Audio";

    // Create an input
    var nrplayerInput = document.createElement("input");
    nrplayerInput.type = "hidden";
    nrplayerInput.name = "nrplayer";
    nrplayerInput.value = nrsegment;
    // Add the input to the form
    nrplayerForm.appendChild(nrplayerInput);

    // Add the form to dom
    document.body.appendChild(nrplayerForm);

    // Just submit
    nrplayerForm.submit();

    // Remove nrplayerForm 
    document.body.removeChild(nrplayerForm);

    $('#collapseTwo').collapse('hide');
    $('#collapseThree').collapse('show');


    $('html, body').animate({
        scrollTop: $('#collapseTwo').offset().top
    }, 'slow');
    return false;
}
function getJavaScriptDate(calldate) {

    var d = new Date(parseInt(calldate.substr(6)));
    var month = d.getMonth() + 1;
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var secs = d.getSeconds();
    var day = d.getDate();

    month = month < 10 ? '0' + month : month;
    day = day < 10 ? '0' + day : day;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;

    var output = d.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + secs;
    return output;
}
