﻿var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", "#e74c3c", "#1E824C", "#a6dcf1"],chart
$(document).ready(function () {

    $("#dialog").hide();
    $("#lastUpdateTime").hide();
    $("#lastupdateon").hide();
    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica'

            }
        },
        lang: {
            drillUpText: '< Back'
        }
    });
    var input = { id: $("#CampaignName").text().toLowerCase() };

    //Ajax Call to populate Disposition list per campaign
    $.ajax({
        url: "/Dashboard/DispositionList",
        type: "GET",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        data: input,
        success: function (data) {

            for (i = 0; i < data.result.length; i++) {
                $('#disposition').append('<option value="' + data.result[i].termcd + '">' + data.result[i].description + '</option>');
            }
            $('.disposition').SumoSelect({ selectAll: true, search: true });
        },
        error: function (msg) {

        }
    });

    $("#DialingStatus").hide()
    $("#dialog").hide();
    $("#lastUpdateTime").hide();
    $("#lastupdateon").hide();
    // set default dates
    var start = new Date();
    // set end date to max one year period:
    var end = new Date(new Date().setYear(start.getFullYear() + 1));
    $('#inputalert').hide();
    $("#dateinputalert").hide();
    $('#agentdisposealert').hide();
    $("#lastUpdateTime").hide();
    $("#RefreshTime").hide();
    $("#refreshCheckbox").hide();
    $("#lastupdateon").hide();
    $("#autorefresh").hide();
    $("#refreshcheckboxlabel").hide();

    $('#startdate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        //  format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90))),
        endDate: new Date()
        // update "enddate" defaults whenever "startdate" changes
    }).on('changeDate', function () {
        // set the "enddate" start to not be later than "startdate" ends:
        $('#enddate').datepicker('setStartDate', new Date($(this).val()));
        var enddate = new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() + 15));
        if (enddate > new Date())
            $('#enddate').datepicker('setEndDate', new Date());
        else
            $('#enddate').datepicker('setEndDate', enddate);
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#enddate').val($(this).val());
        }
    });

    $('#enddate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        // format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90)))
        // update "startdate" defaults whenever "enddate" changes
    }).on('changeDate', function () {
        // set the "startdate" end to not be later than "enddate" starts:
        $('#startdate').datepicker('setEndDate', new Date($(this).val()));
        $('#startdate').datepicker('setStartDate', new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() - 15)));
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#startdate').val($(this).val());
        }
    });



    $('#history').DataTable({
        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Historical Report'
            }
        ]
    });
    $('[data-toggle="tooltip"]').tooltip();

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });

});

function AlphaNumeric(e) {
    var regex = new RegExp("^[a-zA-Z0-9_\b]+$");
    var str = String.fromCharCode(!e.charCode ? e.which : e.charCode);
    if (regex.test(str)) {
        return true;
    }

    e.preventDefault();
    return false;
}


function isNumberKey(evt) {
    var charCode = (evt.which) ? evt.which : event.keyCode
    if (charCode > 31 && (charCode < 48 || charCode > 57))
        return false;

    return true;
}

function Continue(e) {
    $('#inputalert').hide();

    $('#dateinputalert').hide();
    $('#agentdisposealert').hide();
    if ($('#startdate').val() == "" && $('#enddate').val() == "" && $('#uniqueid').val() == "" && $('#tel').val() == "" && $('#agentid').val() == "" && ($('#disposition').val() == "" || $('#disposition').val() == null)) {
        $('#inputalert').show();
        e.preventDefault();
    }
    else if (($('#startdate').val() != "" && $('#enddate').val() == "") || ($('#startdate').val() == "" && $('#enddate').val() != "")) {
        $('#dateinputalert').show();
        e.preventDefault();
    }
    else if ($('#agentid').val() != "" && ($('#startdate').val() == "" || $('#enddate').val() == "")) {
        $('#agentdisposealert').show();
        e.preventDefault();
    }

    else if ($('#disposition').val() != "" && ($('#startdate').val() == "" || $('#enddate').val() == "")) {
        $('#agentdisposealert').show();
        e.preventDefault();
    }

    else {

        $.LoadingOverlay("show");
        $('#inputalert').hide();
        $('#dateinputalert').hide();

        //To hide Search criteria Panel and to show History Table Panel

        $('#collapseOne').collapse('hide');
        $('#collapseTwo').collapse('show');

        //Ajax Call to populate History table
        var history = [];
        var search = {
            id: $("#CampaignName").text().toLowerCase(),
            startdate: $('#startdate').val(),
            enddate: $('#enddate').val(),
            uniqueid: $('#uniqueid').val(),
            tel: $('#tel').val(),
            agentid: $('#agentid').val(),
            termcd: $('#disposition').val()
        }

        $.ajax({
            url: "/Dashboard/HistoryReport",
            type: "GET",
            traditional: true,
            contentType: 'application/json',
            datatype: 'json',
            data: search,
            success: function (data) {
                var len = data.result.length;
                var outcomes = []
                for (i = 0; i < len; i++) {
                    var historyarr = [];
                    var chartarr = []
                    var value = data.result[i];
                    historyarr.push(value.Call_Type);
                    historyarr.push(value.Unique_ID);
                    historyarr.push(value.Call_ID);
                    historyarr.push(value.Agent_ID);
                    historyarr.push(value.Agent_Name);
                    historyarr.push(value.Number);
                    historyarr.push(getJavaScriptDate(value.Calldate));
                    historyarr.push(value.Duration);
                    historyarr.push(value.Call_Outcome);
                    historyarr.push(value.Connect);
                    history.push(historyarr);
                    outcomes.push(value.Call_Outcome);
                }
                var seriesData = []
                var uniqueoutcomes = outcomes.unique();
                //    alert(uniqueoutcomes.length)
                for (var j = 0; j < uniqueoutcomes.length; j++) {
                    var chartData = []
                    for (i = 0; i < len; i++) {
                        var chartarr = []
                        var value = data.result[i];
                        if (value.Call_Outcome == uniqueoutcomes[j] && value.Connect == "Yes") {

                            chartData.push({ x: Date.UTC(0, 0, 0, value.CallTimeHH, value.CallTimeMM, value.CallTimeSS), y: Date.UTC(0, 0, 0, 0, value.DurationMM, value.DurationSS), id: value.Unique_ID });
                        }
                    }
                    if (chartData.length > 0)
                        seriesData.push({
                            name: uniqueoutcomes[j],
                            data: chartData
                        });

                }
                $('#history').DataTable().clear();
                $('#history').DataTable().rows.add(history).draw(true);
                DrawHistoryChart(seriesData);
                //if (!chart.hasData()) {
                //    chart.hideNoData();
                //    chart.showNoData("No data available!");
                //}
                $.LoadingOverlay("hide");

            },
            error: function (msg) {
                $.LoadingOverlay("hide");

            }
        });


    }
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };

    Array.prototype.unique = function () {
        var arr = [];
        for (var i = 0; i < this.length; i++) {
            if (!arr.contains(this[i])) {
                arr.push(this[i]);
            }
        }
        return arr;
    }
}

function getJavaScriptDate(calldate) {

    var d = new Date(parseInt(calldate.substr(6)));
    var month = d.getMonth() + 1;
    var hours = d.getHours();
    var minutes = d.getMinutes();
    var secs = d.getSeconds();
    var day = d.getDate();

    month = month < 10 ? '0' + month : month;
    day = day < 10 ? '0' + day : day;
    hours = hours < 10 ? '0' + hours : hours;
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;

    var output = d.getFullYear() + '-' + month + '-' + day + ' ' + hours + ':' + minutes + ':' + secs;
    return output;
}

function DrawHistoryChart(chartData) {
    chart=$('#container').highcharts({
        chart: {
            type: 'scatter',
            zoomType: 'xy'
        },


        title: {
            text: 'Connected Calls Analysis'
        },
        subtitle:
            {
                text: 'Call Time vs Call Duration'
            },

        xAxis: {
            type: 'datetime',
            title: {
                enabled: true,
                text: 'Call Time (hh:mm)'

            },
            dateTimeLabelFormats: {
                hour: '%I ',
                minute: '%I:%M:%S '
            },
             min: Date.UTC(0, 0, 0, 13, 0, 0)
        },
    

        scrollbar: {
            enabled: true,
            barBackgroundColor: 'gray',
            barBorderRadius: 5,
            barBorderWidth: 0,
            buttonBackgroundColor: 'gray',
            buttonBorderWidth: 0,
            buttonArrowColor: 'white',
            buttonBorderRadius: 7,
            rifleColor: 'white',
            trackBackgroundColor: 'white',
            trackBorderWidth: 1,
            trackBorderColor: 'silver',
            trackBorderRadius: 5
        },
        rangeSelector: {
            selected: 1
        },
        yAxis: {
            min: Date.UTC(0, 0, 0, 0, 0, 0),
            type: 'datetime',
            title: {
                enabled: true,
                text: 'Call Duration (mm:ss)'
            },
            dateTimeLabelFormats: {
                millisecond: '%M:%S',
                second: '%M:%S',
                minute: '%M:%S',
                hour: '%M:%S',
                day: '%M:%S',
                week: '%M:%S',
                month: '%M:%S',
                year: '%M:%S'
            }
        },
        legend: {
            layout: 'horizontal',

        },
        plotOptions: {
            scatter: {
                marker: {
                    radius: 5,
                    states: {
                        hover: {
                            enabled: true,
                            lineColor: 'rgb(100,100,100)'
                        }
                    }
                },
                states: {
                    hover: {
                        marker: {
                            enabled: false
                        }
                    }
                },
                tooltip: {
                    headerFormat: '<b>{series.name}</b>-',
                    pointFormat: '<b>{point.id}</b><br>Time: {point.x:%H:%M:%S} <br>Duration: {point.y:%H:%M:%S}  '
                }
            }
        },
        series: chartData
    });
}
