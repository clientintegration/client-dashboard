﻿var roles = []
$(function () {
   
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getDialingStatus().done(function (Stats) {
                GetRoles(Stats);
            });

        });
    }, 1000);
   

    notifications.client.updateDialingStatus = function (Stats) {
       // alert("updated");
        refreshMenu(Stats)
    };
    

    $('#mi-slider').catslider();

});
function refreshMenu(result) {
    var list = result;
    var client = [], imagepath = [], type = [], campaigns = [], dialstatus = [], agents = []
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };
    $.each(list, function (index, value) {
        if (roles.contains(value.CampaignID)) {
            client.push(value.Client);
            imagepath.push(value.ImagePath);
            campaigns.push(value.CampaignID);
            type.push(value.CampaignType);
            dialstatus.push(value.DialStatus);
            agents.push(value.Agents);
        }
    });



    $("#menu li").each(function (index) {
        for (j = 0; j < campaigns.length; j++) {
            var campaign = $(this).find(".CampName").text()
            var htmlString = ""

            if (campaign.trim() == campaigns[j]) {

                if (dialstatus[j] == "Active")
                    htmlString = "<span class='glyphicon glyphicon-earphone ActiveDialStat'></span>Dialing Status: Active"
                else
                    htmlString = "<span class='glyphicon glyphicon-phone-alt InActiveDialStat'></span>Dialing Status: Inactive"

                $(this).find(".DStatus").html(htmlString);
                $(this).find(".Agents").html("<span class='glyphicon glyphicon-user LoggedAgentIco'></span>Logged Agents: " + agents[j]);
            }


        }
    });


}
function populateMenu(result) {
    var list = result;
    var client = [], imagepath = [], type = [], campaigns = [], dialstatus = [], agents = []
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };

    Array.prototype.unique = function () {
        var arr = [];
        for (var i = 0; i < this.length; i++) {
            if (!arr.contains(this[i])) {
                arr.push(this[i]);
            }
        }
        return arr;
    }
    $.each(list, function (index, value) {
        if (roles.contains(value.CampaignID) ) {
            client.push(value.Client);
            imagepath.push(value.ImagePath);
            campaigns.push(value.CampaignID);
            type.push(value.CampaignType);
            dialstatus.push(value.DialStatus);
            agents.push(value.Agents);
        }
    });
    var uniqueclient = client.unique();
    var uniqueimg = imagepath.unique();
    var datalen = uniqueclient.length;

    var htmlString = "";
    htmlString += '<div class="scontainer"><div class="main"><div id="mi-slider" class="mi-slider">'

    if (datalen > 0) {
        for (i = 0; i < datalen; i++) {
            htmlString += "<ul>  <li ><img style='width:180px;height:44px' src='" + uniqueimg[i] + "' alt='" + uniqueclient[i] + "'></li><br />";
            for (j = 0; j < campaigns.length; j++) {
                if (client[j] == uniqueclient[i]) {
                    htmlString += "   <li><a class='link' href='/Dashboard/Index/" + campaigns[j] + "' title='View Dashboard' ><h2><span class='CampName'>" + campaigns[j] + " </span><span  class='CampType'>" + type[j] + "</span></h2><div class='MipStatTxtItems'>";
                    if (dialstatus[j] == "Active")
                        htmlString += "<span class='InboundStatInfo DStatus'><span class='glyphicon glyphicon-earphone ActiveDialStat'></span>Dialing Status: Active</span>"
                    else
                        htmlString += "<span class='InboundStatInfo DStatus'><span class='glyphicon glyphicon-phone-alt InActiveDialStat'></span>Dialing Status: Inactive</span>"

                    htmlString += "<span class='InboundStatInfo Agents'><span class='glyphicon glyphicon-user LoggedAgentIco'></span>Logged Agents: " + agents[j] + "</span>"
                    ////htmlString += "<br/> <span class='glyphicon glyphicon-phone-alt' style='margin-left:-12px;font-weight:bold;font-size:15px;color: #DF314D; padding-right: 5px'></span>Logged Agents: 15</span>"

                    htmlString += "</div></a></li>"
                }
            }
            htmlString += "</ul>";

        }

        ////If Administrator
        //if (roles.contains("Administrator")) {
        //    htmlString += " <ul> <li><h2>User Profiles</h2><a href='/UserProfile/'>Manage</a> </li>";
        //    htmlString += " <li><h2>Clients</h2><a href='/Client/'>Manage</a> </li>";
        //    htmlString += "<li><h2>Campaigns</h2> <a href='/Campaign/'>Manage</a> </li>";
        //    htmlString += "<li><h2>Modules</h2><a href='/Module/'>Manage</a> </li> </ul>";
        //}
        htmlString += "<nav>";
        for (i = 0; i < uniqueclient.length; i++) {
            htmlString += "<a href='#'>" + uniqueclient[i] + "</a>"
        }
        //if (roles.contains("Administrator")) {
        //    htmlString += "<a href='#'>ADMINISTRATOR</a>"
        //}
        htmlString += "</nav> </div>    </div></div>";
    }
    else
        htmlString += "<ul>  <li > <span class='glyphicon glyphicon-ban-circle' style='color: #DF314D; padding-right: 5px'></span><span style='font-weight:bold;font-size:large'>No campaign is assigned!</span></ul></li><nav></nav></div>    </div></div>"
    $("#menu").html("");
    $(htmlString).appendTo("#menu");
    $('#mi-slider').catslider();
}
function GetRoles(Stats) {

    $.ajax({
        url: "/Home/GetRoles",
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {

            var datalen = data.rolelist.length;

            for (var i = 0 ; i < datalen; i++) {
              //  alert(data.rolelist[i]);
                roles.push(data.rolelist[i]);
            }


            populateMenu(Stats)

        },
        error: function (msg) {

        }
    });


}
