﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.Objects.SqlClient;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Hubs;
using ClientDashboard.Models;

namespace ClientDashboard.Classes
{
    public class StatisticsRepository
    {
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public DateTime GetEasternTime()
        {
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            return easternTime;
        }
        public List<AgentCountReport> GetAgentCountStats(bool IsRequested)
        {


            var AgentCount = new List<AgentCountReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT CampaignID,[AgentsOnBreak] ,[AgentsOnTalk],[AgentsOnWait],[AgentsOnWrap] FROM [dbo].[AgentCountReport] where updatedon>=@date", connection))
                    {
                        command.Notification = null;

                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AgentCountStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentCount.Add(item: new AgentCountReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                AgentsOnBreak = (int)reader["AgentsOnBreak"],
                                AgentsOnTalk = (int)reader["AgentsOnTalk"],
                                AgentsOnWait = (int)reader["AgentsOnWait"],
                                AgentsOnWrap = (int)reader["AgentsOnWrap"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return AgentCount;
        }
        private void AgentCountStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AgentCountReport> AgentCount = GetAgentCountStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAgentCountStats(AgentCount);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
        public List<AgentStatusReport> GetAgentStatusStats(bool IsRequested)
        {


            var AgentStatus = new List<AgentStatusReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT [CampaignID]      ,[AgentState]      ,[AgentCount]  FROM [dbo].[AgentStatusReport] where updatedon>=@date and [AgentCount]>0", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(AgentStatusStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentStatus.Add(item: new AgentStatusReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                AgentState = (string)reader["AgentState"],
                                AgentCount = (int)reader["AgentCount"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return AgentStatus;
        }
        private void AgentStatusStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<AgentStatusReport> AgentStatus = GetAgentStatusStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendAgentStatusStats(AgentStatus);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
        public List<RoutingQueueStat> GetRoutingQueueStats(bool IsRequested)
        {
            var Queue = new List<RoutingQueueStat>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    using (var command = new SqlCommand(@"SELECT CampaignID,sum([QueuedCalls]) QueuedCalls,count_big(*) as QueuedCalls FROM [dbo].[RoutingQueueStats] where updatedon>=@date group by CampaignID", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(RoutingQueueStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();
                        string campaignid = "";
                        while (reader.Read())
                        {
                            campaignid = ((string)reader["CampaignID"] == "MIM") ? "MIP" : (string)reader["CampaignID"];
                            Queue.Add(item: new RoutingQueueStat
                            {
                                CampaignID = campaignid,
                                QueuedCalls = (int)reader["QueuedCalls"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return Queue;
        }
        private void RoutingQueueStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<RoutingQueueStat> Queue = GetRoutingQueueStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendQueueStats(Queue);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
        public List<TfnQueueDispositionReport> GetTFNQueueStats(bool IsRequested)
        {
            var TFNQueue = new List<TfnQueueDispositionReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    // using (var command = new SqlCommand(@"select t.campaignid,t.Tfnqueue,Disposition,Description ,callsoffered,callshandled,callsabandon,callcount from ibdispositionreport d inner join tfnqueuereport t on t.campaignid=d.campaignid and t.tfnqueue=d.tfnqueue where t.updatedon>=@date and CallsOffered<>0 and Callcount<>0 ", connection))
                    using (var command = new SqlCommand(@"select t.campaignid,t.Tfnqueue,d.Disposition,d.Description ,t.callsoffered,t.callshandled,t.callsabandon,d.callcount from dbo.ibdispositionreport d inner join dbo.tfnqueuereport t on t.campaignid=d.campaignid and t.tfnqueue=d.tfnqueue where t.updatedon>=@date and CallsOffered<>0 and Callcount<>0 ", connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(TFNQueueStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            TFNQueue.Add(item: new TfnQueueDispositionReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                TfnQueue = (string)reader["TfnQueue"],
                                CallsHandled = (int)reader["CallsHandled"],
                                CallsAbandon = (int)reader["CallsAbandon"],
                                CallsOffered = (int)reader["CallsOffered"],
                                CallCount = (int)reader["CallCount"],
                                Disposition = (string)(reader["Disposition"] + "-" + reader["Description"]),

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return TFNQueue;
        }
        private void TFNQueueStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<TfnQueueDispositionReport> TFNQueue = GetTFNQueueStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendTFNQueueStats(TFNQueue);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
        public List<IBCampaignReport> GetIBIntervalStats(bool IsRequested)
        {
            var Interval = new List<IBCampaignReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT CampaignID,[Interval] ,[CallOffered],[CallsHandled],[CallsAbandonIVR] ,[CallsAbandonSwitch] ,[AbandonRate]   ,[ServiceLevelCall],[AHT]";
                    query += ",[MaxWaitTime]   ,[ASA]     ,[Occupancy]      ,[Logintime]     ,[Breaktime]      ,[ProductionHours]  FROM [dbo].[IBCampaignReport] where updatedon>=@date and (CallOffered<>0 or Logintime<>0)";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(IBIntervalStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Interval.Add(item: new IBCampaignReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                AbandonRate = (decimal)reader["AbandonRate"],
                                ASA = (int)reader["ASA"],
                                Interval = (string)reader["Interval"],
                                Breaktime = (decimal)reader["Breaktime"],
                                CallOffered = (int)reader["CallOffered"],
                                CallsAbandonIVR = (int)reader["CallsAbandonIVR"],
                                CallsAbandonSwitch = (int)reader["CallsAbandonSwitch"],
                                CallsHandled = (int)reader["CallsHandled"],
                                Logintime = (decimal)reader["Logintime"],
                                MaxWaitTime = (int)reader["MaxWaitTime"],
                                Occupancy = (decimal)reader["Occupancy"],
                                ProductionHours = (decimal)reader["ProductionHours"],
                                ServiceLevelCall = (decimal)reader["ServiceLevelCall"],
                                AHT = (decimal)reader["AHT"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return Interval;
        }
        private void IBIntervalStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<IBCampaignReport> Interval = GetIBIntervalStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendIBIntervalStats(Interval);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
     
      
        public List<IBAgentsReport> GetIBAgentStats(bool IsRequested)
        {
            var AgentStats = new List<IBAgentsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT [CampaignID],[AgentName]      ,[AgentID]      ,[Calls]      ,[State]      ,[StateTime]      ,[TimeSinceLastCall]      ,[AHT]   ,[LoginTime]      ,[BreakTime]";
                    query += " ,[AvailableTime]      ,[HandleTime]      ,[ProductionTime]      ,[Occupancy],updatedon  FROM [dbo].[IBAgentsReport] where updatedon>=@date and (AgentName<>'' or ProductionTime<>0)";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(IBAgentStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentStats.Add(item: new IBAgentsReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                AgentID = (string)reader["AgentID"],
                                AgentName = (string)reader["AgentName"],
                                AHT = (int)reader["AHT"],
                                BreakTime = (decimal)reader["Breaktime"],
                                AvailableTime = (decimal)reader["AvailableTime"],
                                Calls = (int)reader["Calls"],
                                HandleTime = (decimal)reader["HandleTime"],
                                LoginTime = (decimal)reader["LoginTime"],
                                ProductionTime = (decimal)reader["ProductionTime"],
                                State = (string)reader["State"],
                                StateTime = (DateTime)reader["StateTime"],
                                Occupancy = (decimal)reader["Occupancy"],
                                TimeSinceLastCall = (DateTime)reader["TimeSinceLastCall"],
                                UpdatedOn = (DateTime)reader["UpdatedOn"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return AgentStats;
        }
        private void IBAgentStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<IBAgentsReport> Agent = GetIBAgentStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendIBAgentStats(Agent);
                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
        
        public List<OBCampaignReport> GetOBIntervalStats(bool IsRequested)
        {
            var Interval = new List<OBCampaignReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT [CampaignID],[Interval],[Dials],[Connects],[ConnectsPerHour],[Contacts],[ContactsPerHour],[Completes],[CompletesPerHour],";
                    query += "[Abandoned],[AbandonedRate],[Sales],[ConversionRate],[Occupancy] ,[Logintime],[Breaktime],[ProductionHours] FROM [dbo].[OBCampaignReport] where updatedon>=@date and (Dials<>0 or Logintime<>0)";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(OBIntervalStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Interval.Add(item: new OBCampaignReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                Interval = (string)reader["Interval"],
                                Breaktime = (decimal)reader["Breaktime"],
                                Logintime = (decimal)reader["Logintime"],
                                Occupancy = (decimal)reader["Occupancy"],
                                ProductionHours = (decimal)reader["ProductionHours"],
                                Abandoned=(int)reader["Abandoned"],
                                AbandonedRate=(decimal)reader["AbandonedRate"],
                                Completes=(int)reader["Completes"],
                                CompletesPerHour=(decimal)reader["CompletesPerHour"],
                                Connects=(int)reader["Connects"],
                                ConnectsPerHour=(decimal)reader["ConnectsPerHour"],
                                Contacts=(int)reader["Contacts"],
                                ContactsPerHour=(decimal)reader["ContactsPerHour"],
                                ConversionRate=(decimal)reader["ConversionRate"],
                                Dials=(int)reader["Dials"],
                                Sales=(int)reader["Sales"]
                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return Interval;
        }
        private void OBIntervalStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<OBCampaignReport> Interval = GetOBIntervalStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendOBIntervalStats(Interval);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }

        public List<OBAgentsReport> GetOBAgentStats(bool IsRequested)
        {
            var AgentStats = new List<OBAgentsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT [CampaignID],[AgentName],[AgentID],[Connects],[Sales],[State],[StateTime],[TimeSinceLastCall],[LoginTime],";
                    query += "[BreakTime],[WaitTime],[HandleTime],[ProductionTime],[Occupancy],[UpdatedOn] FROM [dbo].[OBAgentsReport]  where updatedon>=@date and (AgentName<>'' or ProductionTime<>0)";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(OBAgentStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            AgentStats.Add(item: new OBAgentsReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                AgentID = (string)reader["AgentID"],
                                AgentName = (string)reader["AgentName"],
                                BreakTime = (decimal)reader["Breaktime"],
                                Connects = (int)reader["Connects"],
                                Sales = (int)reader["Sales"],
                                WaitTime = (decimal)reader["WaitTime"],
                                HandleTime = (decimal)reader["HandleTime"],
                                LoginTime = (decimal)reader["LoginTime"],
                                ProductionTime = (decimal)reader["ProductionTime"],
                                State = (string)reader["State"],
                                StateTime = (DateTime)reader["StateTime"],
                                Occupancy = (decimal)reader["Occupancy"],
                                TimeSinceLastCall = (DateTime)reader["TimeSinceLastCall"],
                                UpdatedOn = (DateTime)reader["UpdatedOn"]

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return AgentStats;
        }
        private void OBAgentStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<OBAgentsReport> Agent = GetOBAgentStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendOBAgentStats(Agent);
                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }

        public List<OBDispositionReport> GetOBDispositionStats(bool IsRequested)
        {
            var OBDisp = new List<OBDispositionReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    string query = "select CampaignID,DispType,Disposition,Description,CallsPerDispType,CallCount from dbo.OBDispositionReport";
                    query += " where updatedon>=@date and Callcount<>0 ";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(GetOBDispositionStats_OnChange);
                        }
                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            OBDisp.Add(item: new OBDispositionReport
                            {
                                CampaignID = (string)reader["CampaignID"],
                                DispType = (string)reader["DispType"],
                                CallsPerDispType = (int)reader["CallsPerDispType"],
                                CallCount = (int)reader["CallCount"],
                                Disposition = (string)(reader["Disposition"] + "-" + reader["Description"]),

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return OBDisp;
        }
        private void GetOBDispositionStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<OBDispositionReport> OBDisp = GetOBDispositionStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendOBDispositionStats(OBDisp);

                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }


        public List<LeadsReport> GetLeadsStats(bool IsRequested)
        {
            var LeadStats = new List<LeadsReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT CampaignID,LeadsAvailable FROM [dbo].[HomeReport]  where updatedon>=@date ";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        DateTime dt = GetEasternTime().Date.AddDays(0);
                        command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(LeadsStats_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            LeadStats.Add(item: new LeadsReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                Available = (int)reader["LeadsAvailable"]
                               

                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return LeadStats;
        }
        private void LeadsStats_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<LeadsReport> Lead = GetLeadsStats(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendLeadsStats(Lead);
                }
                catch (Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }


        public List<HomeReport> GetDialingStatus(bool IsRequested)
        {
            var Dial = new List<HomeReport>();
            try
            {
                using (var connection = new SqlConnection(_connString))
                {
                    connection.Open();
                    var query = @"SELECT [Campaignid]  ,[Client],[ImagePath] ,[CampaignType]  ,[Agents]      ,[DialStatus]   FROM [dbo].[HomeReport]";
                    using (var command = new SqlCommand(query, connection))
                    {
                        command.Notification = null;
                        //DateTime dt = GetEasternTime().Date.AddDays(0).AddDays(0);
                        //command.Parameters.Add(new SqlParameter("@date", dt));
                        if (!IsRequested)
                        {
                            var dependency = new SqlDependency(command);
                            dependency.OnChange += new OnChangeEventHandler(DialingStatus_OnChange);
                        }

                        if (connection.State == ConnectionState.Closed)
                            connection.Open();

                        var reader = command.ExecuteReader();

                        while (reader.Read())
                        {
                            Dial.Add(item: new HomeReport
                            {
                                CampaignID = (string)reader["Campaignid"],
                                DialStatus = (string)reader["DialStatus"],
                                Client = (string)reader["Client"],
                                ImagePath = (string)reader["ImagePath"],
                                CampaignType = (string)reader["CampaignType"],
                                Agents = (int)reader["Agents"],


                            });
                        }
                    }
                }
            }
            catch (Exception Ex)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(Ex, "None");
            }
            return Dial;
        }
        private void DialingStatus_OnChange(object sender, SqlNotificationEventArgs e)
        {
            if (e.Type == SqlNotificationType.Change)
            {
                try
                {
                    List<HomeReport> Dial = GetDialingStatus(false);
                    StatisticsHub statHub = new StatisticsHub();
                    statHub.SendDialingStatus(Dial);
                }
                catch(Exception Ex)
                {
                    Email obj = new Email();
                    obj.SendExceptionEmail(Ex, "None");
                }
            }
        }
    }
}