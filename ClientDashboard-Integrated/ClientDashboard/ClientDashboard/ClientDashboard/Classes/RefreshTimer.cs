﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Timers;
using Microsoft.AspNet.SignalR;
using System.Web.Script.Serialization;
using ClientDashboard.Hubs;
using ClientDashboard.Models;
using System.Data.SqlClient;
using System.Data;
using System.Configuration;

namespace ClientDashboard.Classes
{
    public class RefreshTimer : IRegisteredObject
    {
    private readonly IHubContext _hub;
        private Timer _timer;
        private int _timerInterval;
        readonly string _connString = ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
        public RefreshTimer()
        {
            //System.Diagnostics.Debug.WriteLine("HUBTIMER: CONSTRUCTOR");
 
            // GET HUB CONTEXT
            _hub = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
                       
            // IN MILLISECONDS (15000 = 15 SECONDS, 30000 = 30 SECONDS, 60000 = 60 SECONDS)
            _timerInterval = 5000;
 
            // START THE TIMER
            StartTimer();
        
            
        }
         
        private void StartTimer()
        {
            //System.Diagnostics.Debug.WriteLine("HUBTIMER: STARTTIMER");
 
            // SET TIMER UP WITH INTERVAL
            _timer = new Timer(_timerInterval);
 
            // ADD HANDLER TO TIMER.ELAPSED EVENT
            _timer.Elapsed += OnTimerElapsed;
 
            // START THE TIMER GOING
            _timer.Start();
        }
 
        private void OnTimerElapsed(Object source, ElapsedEventArgs e)
        {
           
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            _StatisticsRepository.GetDialingStatus(false);
            _StatisticsRepository.GetAgentCountStats(false);
            _StatisticsRepository.GetRoutingQueueStats(false);
            _StatisticsRepository.GetIBIntervalStats(false);
            _StatisticsRepository.GetTFNQueueStats(false);
            _StatisticsRepository.GetAgentStatusStats(false);
            _StatisticsRepository.GetIBAgentStats(false);
           
        }

        public void Stop(bool immediate)
        {
            _timer.Dispose();
            HostingEnvironment.UnregisterObject(this);
        }
    }
}