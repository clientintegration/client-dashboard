﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Classes;

namespace ClientDashboard.Models
{
    public class CallRecordingReport_Result
    {
        public string Call_Type { get; set; }
        public string Campaign_ID { get; set; }
        public string Unique_ID { get; set; }
        public string Call_ID { get; set; }
        public string Agent_ID { get; set; }
        public string Number { get; set; }
        public System.DateTime Calldate { get; set; }
        public string Start_Time { get; set; }
        public string Duration { get; set; }
        public string Call_Outcome { get; set; }
        public string Provider_Type { get; set; }
        public string NR_Segment { get; set; }

        public List<CallRecordingReport_Result> GetSearchResults(string startdate, string enddate, string uniqueid, string tel, string agentid, string campaignid)
        {
            var result=new List<CallRecordingReport_Result>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();
         
            SqlCmd.CommandTimeout = 300;
            SqlCmd.CommandText = "CallRecordingReport";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startdate;
            SqlCmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = enddate;
            SqlCmd.Parameters.Add("@UniqueID", SqlDbType.VarChar).Value = uniqueid;
            SqlCmd.Parameters.Add("@number", SqlDbType.VarChar).Value = tel;
            SqlCmd.Parameters.Add("@AgentID", SqlDbType.VarChar).Value = agentid;
            SqlCmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = campaignid;

            SqlCmd.Connection = SqlConn;

            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    var reader = SqlCmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(item: new CallRecordingReport_Result
                        {
                            Agent_ID = (string)reader["Agent ID"],
                            Call_ID= (string)reader["Call ID"],
                            Call_Outcome= (string)reader["Call Outcome"],
                            Call_Type= (string)reader["Call Type"],
                            Calldate= Convert.ToDateTime((string)reader["Calldate"]),
                            Campaign_ID= (string)reader["Campaign ID"],
                            Duration = (string)reader["Duration"],
                            Unique_ID = (string)reader["Unique ID"],
                          //  Start_Time = (string)reader["Start Time"],
                            Provider_Type = (string)reader["Provider Type"],
                            Number = (string)reader["Number"],
                            NR_Segment = (string)reader["NR Segment"]

                        });
                    }
                }


            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at GetSearchResults " + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                throw exp;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return result;
        }
    }

}