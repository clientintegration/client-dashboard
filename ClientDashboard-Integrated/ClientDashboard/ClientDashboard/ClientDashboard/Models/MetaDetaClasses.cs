﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientDashboard.Hubs;

namespace ClientDashboard.Models
{
    public class MailModel
    {

        public string Email { get; set; }
        [Required]
        public string Subject { get; set; }
        [Required]
        [StringLength(7500)]
        [UIHint("tinymce_full_compressed")]
        [AllowHtml]
        public string Body { get; set; }
    }
    public partial class TfnQueueDispositionReport
    {
        public string TfnQueue { get; set; }
        public int CallsOffered { get; set; }
        public int CallsHandled { get; set; }
        public int CallsAbandon { get; set; }
        public string CampaignID { get; set; }
        public System.DateTime UpdatedOn { get; set; }
        public int SalesCall { get; set; }
        public int CallCount { get; set; }
        public string Disposition { get; set; }
    }
 
    public partial class LeadsReport
    {
        public string CampaignID { get; set; }
        public int Available { get; set; }
    }
    public class MenuItems
    {
        public string Module { get; set; }
        public string Description { get; set; }
        public string CampaignID { get; set; }
        public string HTML { get; set; }
    }
    public class Header
    {
        public string ModuleDescription { get; set; }
        public string CampaignID { get; set; }
        public string HTML { get; set; }
        public string Client { get; set; }
    }
    public class UserProfileMetaData
    {
        public UserProfileMetaData()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
        [Key]
        public int UserId { get; set; }
        [Display(Name = "User Name")]
        public string UserName { get; set; }
        public string Client { get; set; }
        public string Email { get; set; }
        [Display(Name = "Full Name")]
        public string Name { get; set; }

        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
    public class UsersInRolesMetaData
    {
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int RoleId { get; set; }
        public string Description { get; set; }
        [Key]
        [Column(Order = 3)]
        public int ModuleID { get; set; }

        public virtual Module Module { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual webpages_Roles webpages_Roles { get; set; }

    }
    public class RolesMetaData
    {
        public RolesMetaData()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
        [Key]
        [Display(Name = "Campaign ID")]
        public int RoleId { get; set; }
        [Display(Name = "Campaign Name")]
        public string RoleName { get; set; }
        public Nullable<int> ClientID { get; set; }
        [Display(Name = "Campaign Type")]
        public string CampaignType { get; set; }
        [Display(Name = "Client")]
        public virtual Client Client { get; set; }
        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
    public class MembershipMetaData
    {
        [Key]
        public int UserId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string ConfirmationToken { get; set; }
        public Nullable<bool> IsConfirmed { get; set; }
        public Nullable<System.DateTime> LastPasswordFailureDate { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> PasswordChangedDate { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordVerificationToken { get; set; }
        public Nullable<System.DateTime> PasswordVerificationTokenExpirationDate { get; set; }
    }
    public class ModuleMetaData
    {
        public ModuleMetaData()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
        [Display(Name="Module ID")]
        public int ModuleID { get; set; }
        [Required]
        [Display(Name = "Module Name")]
        public string Name { get; set; }
        [Required]
        public string Description { get; set; }
        [AllowHtml]
        [Display(Name = "Module Icon")]
        public string HTML { get; set; }

        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
    public class ClientMetaData
    {
        public ClientMetaData()
        {
            this.webpages_Roles = new HashSet<webpages_Roles>();
        }

        [Display(Name="Client ID")]
        public int ClientID { get; set; }
        [Required]
        [Display(Name = "Client Name")]
        public string Name { get; set; }
        [Display(Name="Image")]
        public string ImagePath { get; set; }
        public string Website { get; set; }

        public virtual ICollection<webpages_Roles> webpages_Roles { get; set; }
    }
}