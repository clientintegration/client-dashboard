﻿var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", "#e74c3c", "#1E824C", "#a6dcf1"]
var barchart;
var table;
var occthreshold = -1;
var agentnames = [], agents = []
var flag = false, cname;

$(function () {
    $("#spinner").show()
    $("#graphcontainer").hide()
    cname = $("#CampaignName").text().toLowerCase();

    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica',
                fontWeight: 'normal'
            }
        }
    });
    $.fn.dataTableExt.sErrMode = 'throw';
    table = $('#agenttable').dataTable({
        // "aoColumnDefs": [{ "bSortable": false, "aTargets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] }],

        "bSortCellsTop": true,
        //scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,

        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Agent Statistics' + formatDateforTable(new Date())
            }
        ],

        "columnDefs": [
            {"bSortable": false, "aTargets": [2, 3,  4, 7, 8, 9, 10, 11, 12]},
            {
                "bSortable": false,
                "render": function (data, type, row) {
                    var cell = row[4];
                    var timestamp = "";
                    if (cell.toLowerCase().indexOf("out") < 0) {
                        timestamp = GetDateDiffInTime(data);
                    }

                    //return timestamp
                    return timestamp
                    //return date3;
                },
                targets: 5
            },
              {
                  "bSortable": false,
                  "render": function (data, type, row) {
                      var cell = row[4];
                      var timenocall = "";


                      if (cell == 'Wait') {
                          timenocall = GetDateDiffInTime(data);
                      }

                      return timenocall
                  },
                  targets: 6
              }
        ]


    });
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getOBAgentStats().done(function (Stats) {
                var oTable = $('#agenttable').DataTable();
                oTable.clear();
                $('#agenttable').DataTable().rows.add(populateAgentStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try {
                    oTable.button(0).enable(nRows > 0);
                }
                catch (Ex)
                { }
                $("#lastUpdateTime").text(formatDate(new Date()));
            });
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });

        });
    }, 1000);
    var tryingToReconnect = false;

    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            $("#dialog").dialog("open");
        }
    });
    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateOBAgentStats = function (Stats) {

     

        var oTable = $('#agenttable').DataTable();
        oTable.clear();
        $('#agenttable').DataTable().rows.add(populateAgentStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
        $("#lastUpdateTime").text(formatDate(new Date()));
    };
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        closeText: 'Close',
        draggable: false,
        minHeight: 100,
        minWidth: 380,
        dialogClass: 'main-dialog-class',
        open: function () {
            $(this).closest(".ui-dialog")
            .find(".ui-dialog-titlebar-close")
            .removeClass("ui-dialog-titlebar-close")
            .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
        },
        close: function (event, ui) {
             window.location.href = "http://clientdashboard.ibexglobal.com/"
        }
    });
    timer = setInterval(function () { reset() }, 1000);
});
function reset() {
    var oTable = $('#agenttable').DataTable();
    var info = oTable.page.info();
    var nRows = info.recordsTotal;
    for (var i = 0; i < nRows; i++) {
        var cell = oTable.cell(i, 4);
        cell.data(cell.data()).draw(false);
        
        cell = oTable.cell(i, 5);
        cell.data(cell.data()).draw(false);
    }

    //  alert(cell.data(0));
    //  $('#agenttable').DataTable().columns.adjust().draw(false);
}
function refreshDialingStatus(result) {
    var list = result;
    var dialstatus;
    $("#DialingStatus").hide();
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            dialstatus = value.DialStatus;

        }
    });
    $("#status").text(cname.toUpperCase() + " is not dialing at the moment.");
    if (dialstatus == "Inactive")
        $("#DialingStatus").show();
}

function populateAgentStats(result) {
    $("#spinner").hide()
    $("#graphcontainer").show()
    var staffed = [], wait = [], handle = [], hold = [], break1 = [], production = [], occupancy = []
    var dataSet = [], total = []
    agentnames = []
    agents = []
    var list = result;
    cname="mic"
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            if (value.AgentID != "Total") {
                
                agents.push(value.AgentID)
                agentnames.push(value.AgentName)
                staffed.push(parseFloat(value.LoginTime))
                wait.push(parseFloat(value.WaitTime))
                handle.push(parseFloat(value.HandleTime))
                break1.push(parseFloat(value.BreakTime))
                production.push(parseFloat(value.ProductionTime))
                occupancy.push(parseFloat(value.Occupancy))
            }

            var arr = [];
            if (value.AgentID != "Total") {

                arr.push(value.AgentID);
                arr.push(value.AgentName);
                arr.push(value.Connects);
                arr.push(value.Sales);
                arr.push(value.State);
                arr.push(value.StateTime);
                arr.push(value.TimeSinceLastCall);
                arr.push(value.LoginTime);
                arr.push(value.BreakTime);
                arr.push(value.WaitTime);
                arr.push(value.HandleTime);
                arr.push(value.ProductionTime);
                arr.push(value.Occupancy);
                //arr.push(value.UpdatedOn);
                dataSet.push(arr)

                // console.log(new Date(value.UpdatedOn) + " " + new Date());
            }
            else {

                total.push(value.AgentID);
                //total.push(value.AgentName);
                total.push("");
                total.push(value.Connects);
                total.push(value.Sales);
                // total.push(value.State);
                // total.push(value.StateTime);
                //total.push(value.TimeSinceLastCall);
                total.push("");
                total.push("");
                total.push("");
                total.push(value.LoginTime);
                total.push(value.BreakTime);
                total.push(value.WaitTime);
                total.push(value.HandleTime);
                total.push(value.ProductionTime);
                total.push(value.Occupancy);
                //total.push(value.UpdatedOn);
            }
        }
    });


    if (flag == false) {

        flag = true;
        DrawBarChart()
    }
    barchart.xAxis[0].setCategories(agents);
    barchart.yAxis[1].setExtremes(0, 100);
    barchart.series[0].setData(staffed);
    barchart.series[1].setData(wait);
    barchart.series[2].setData(handle);
    barchart.series[3].setData(break1);
    barchart.series[4].setData(production);
    barchart.series[5].setData(occupancy);
    $('#frow th').each(function (i) {

        if ($(this).text() != "Total")
            $(this).text("");

    });
    if (total.length != 0) {
        $('#frow th').each(function (i) {
            if (total[i] != undefined && $(this).text() != "Total")
                $(this).text("" + total[i]);

        });
    }
    if (!barchart.hasData()) {
        barchart.hideNoData();
        barchart.showNoData("No data available!");
    }
    return dataSet;
}
function convertServerDateToLocal(dateInput) {
    // EST - UTC offset: 5 hours
    var offset = 5.0,

        /*
          - calculate the difference between the server date and UTC
          - the value returned by the getTime method is the number of milliseconds since 1 January 1970 00:00:00 UTC.
          - the time-zone offset is the difference, in minutes, between UTC and local time
          - 60000 milliseconds = 60 seconds = 1 minute
        */
        serverDate = new Date(dateInput),
        utc = serverDate.getTime() - (serverDate.getTimezoneOffset() * 60000),

        /*
          - apply the offset between UTC and EST (5 hours)
          - 3600000 milliseconds = 3600 seconds = 60 minutes = 1 hour
        */
        clientDate = new Date(utc + (3600000 * offset));
   
    return clientDate.toLocaleString();
}
function DrawBarChart() {
    barchart = new Highcharts.Chart({

        chart: {
            type: 'column',
            renderTo: 'graphcontainer'
        },
        title: {
            text: 'Agent Performance',
            align: 'left'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} hrs',
                style: {
                    color: '#000'
                }
            },
            title: {
                text: 'Hours',
                style: {
                    color: '#000',
                    fontWeight: 'bold'
                }
            },
            tickInterval: 3

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Percentage',
                style: {
                    color: '#DF314D',
                    fontWeight: 'bold'
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: '#DF314D'

                }
            },
            opposite: true,
            threshold: 50


        }],
        legend: {

            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },
        tooltip: {
            formatter: function () {
                var points = this.points;
                var pointsLength = points.length;
                var s = '<b>' + this.x + '</b> - ';
                s += agentnames[agents.indexOf(this.x)]
                $.each(this.points, function (i, point) {
                    if (i != pointsLength - 1)
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + ' hrs';
                    else
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + ' %';
                });

                return s;
            },
            // headerFormat: '<span style="font-size:10px"> {agentnames[agents.indexOf(point.x)]} {point.x}</span>',
            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            //    '<td style="padding:0"><b>{point.y:.1f}</b><br/></td></tr>',
            //footerFormat: '</table>',
            shared: true,
            //useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Login Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[0]

        }, {
            name: 'Available Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[1]

        }, {
            name: 'Handle Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[2]

        }, {
            name: 'Break Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[3]

        }, {
            name: 'Production Time',
            color: '#ccc',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[4]

        }, {
            name: 'Occupancy %',
            data: [],
            type: 'spline',
            color: '#DF314D',
            yAxis: 1,
            tooltip: {
                valueSuffix: ' %'
            },
            threshold: occthreshold,
            negativeColor: '#eb525d'
        }

        ]
    });
}
function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}

function GetDateDiffInTime(data) {
 
    var ClientConvDateEST = new Date(ConverttoEST(new Date()));
    //Differences between two dates with time
    //data = data.replace('T', ' ');

    //For Safari
    var a = data.split(/[^0-9]/);
   
    var data=new Date (a[0],a[1]-1,a[2],a[3],a[4],a[5] );
  
  
  
    var serverinputdate = new Date(data);
    var diff = (ClientConvDateEST - serverinputdate) / 1000;
    //    console.log(diff);
    var diff = Math.abs(Math.floor(diff));

    var days = Math.floor(diff / (24 * 60 * 60));
    var leftSec = diff - days * 24 * 60 * 60;

    var hrs = Math.floor(leftSec / (60 * 60));
    var leftSec = leftSec - hrs * 60 * 60;

    var min = Math.floor(leftSec / (60));
    var leftSec = leftSec - min * 60;

    hrs = hrs < 10 ? '0' + hrs : hrs;
    min = min < 10 ? '0' + min : min;
    leftSec = leftSec < 10 ? '0' + leftSec : leftSec;

    return hrs + ':' + min + ':' + leftSec//data + '(' + row[3] + ')';
}

function ConverttoEST(clientDate) {
    //Convert local time to EST, because the server time is in EST
    offset = -5.0
    utc = clientDate.getTime() + (clientDate.getTimezoneOffset() * 60000);
    return new Date(utc + (3600000 * offset));
}