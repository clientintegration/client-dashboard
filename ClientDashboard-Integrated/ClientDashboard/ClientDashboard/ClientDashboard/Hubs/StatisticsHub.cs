﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using WebMatrix.WebData;
namespace ClientDashboard.Hubs
{
    [HubName("statisticsHub")]
    public class StatisticsHub : Hub
    {


        public void SendAgentCountStats(List<AgentCountReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentCountStats(Stats);
        }
        public void SendAgentStatusStats(List<AgentStatusReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentStatusStats(Stats);
        }
        public void SendQueueStats(List<RoutingQueueStat> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateQueueStats(Stats);
        }
        public void SendLeadsStats(List<LeadsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateLeadsStats(Stats);
        }
        public void SendTFNQueueStats(List<TfnQueueDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateTFNQueueStats(Stats);
        }
        public void SendOBDispositionStats(List<OBDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateOBDispositionStats(Stats);
        }
        public void SendIBIntervalStats(List<IBCampaignReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateIBIntervalStats(Stats);
        }    
        public void SendIBAgentStats(List<IBAgentsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateIBAgentStats(Stats);
        }
        public void SendOBIntervalStats(List<OBCampaignReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateOBIntervalStats(Stats);
        }

        public void SendOBAgentStats(List<OBAgentsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateOBAgentStats(Stats);
        }
        public void SendDialingStatus(List<HomeReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateDialingStatus(Stats);
        }
        public List<HomeReport> GetDialingStatus()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<HomeReport> MenuInfo = _StatisticsRepository.GetDialingStatus(true).ToList();
            return MenuInfo;
        }
        public List<AgentCountReport> GetAgentCountStats()
        {

            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentCountReport> AgentCount = _StatisticsRepository.GetAgentCountStats(true).ToList();
            return AgentCount;
        }
        public List<AgentStatusReport> GetAgentStatusStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentStatusReport> AgentStatus = _StatisticsRepository.GetAgentStatusStats(true).ToList();
            return AgentStatus;
        }
        public List<RoutingQueueStat> GetQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<RoutingQueueStat> Queue = _StatisticsRepository.GetRoutingQueueStats(true).ToList();
            return Queue;
        }
     
        public List<TfnQueueDispositionReport> GetTFNQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<TfnQueueDispositionReport> TFNQueue = _StatisticsRepository.GetTFNQueueStats(true).ToList();
            return TFNQueue;
        }
        public List<IBCampaignReport> GetIBIntervalStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<IBCampaignReport> IntervalStats = _StatisticsRepository.GetIBIntervalStats(true).ToList();
            return IntervalStats;
        }
        public List<IBAgentsReport> GetIBAgentStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<IBAgentsReport> AgentStats = _StatisticsRepository.GetIBAgentStats(true).ToList();
            return AgentStats;
        }
        public List<OBCampaignReport> GetOBIntervalStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<OBCampaignReport> IntervalStats = _StatisticsRepository.GetOBIntervalStats(true).ToList();
            return IntervalStats;
        }
        public List<OBAgentsReport> GetOBAgentStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<OBAgentsReport> AgentStats = _StatisticsRepository.GetOBAgentStats(true).ToList();
            return AgentStats;
        }
        public List<LeadsReport> GetLeadsStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<LeadsReport> LeadsStats = _StatisticsRepository.GetLeadsStats(true).ToList();
            return LeadsStats;
        }
        public List<OBDispositionReport> GetOBDispositionStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<OBDispositionReport> OBDispStats = _StatisticsRepository.GetOBDispositionStats(true).ToList();
            return OBDispStats;
        }
        public override System.Threading.Tasks.Task OnDisconnected()
        {
            return base.OnDisconnected();
        }


    }
}