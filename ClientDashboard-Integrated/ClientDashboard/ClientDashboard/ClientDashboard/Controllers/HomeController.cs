﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using WebMatrix.WebData;

namespace ClientDashboard.Controllers
{
    public class HomeController : Controller
    {
        ClientDashboardEntities db = new ClientDashboardEntities();
        [Authorize]
        public ActionResult Index()
        {

            return View();
        }
       

        [Authorize]
        public ActionResult Download()
        {
            return View();
        }

      


        public ActionResult DownloadFile(string windowstype)
        {
            try
            {
                string FilePath = "";
                if (windowstype == "XP")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Windows_XP_2003_DownloadPath"].ToString();
                }
                else if (windowstype == "Vista")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Vista_Higer_DownloadPath"].ToString();
                }

                string filename = System.IO.Path.GetFileName(FilePath);
                return File(FilePath, ".zip", filename);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Recordings", "Dashboard");
            }

        }

        public ActionResult GetRoles()
        {
            try
            {
                string[] roles = Roles.GetRolesForUser(User.Identity.Name);
                return Json(new { rolelist = roles.ToList() }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exp)
            {
             
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return Json(new { rolelist = "" }, JsonRequestBehavior.AllowGet);
            }

            


        }
     
    }
}
