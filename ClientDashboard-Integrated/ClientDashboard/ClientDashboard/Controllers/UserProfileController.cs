﻿using System;
using System.Globalization;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using ClientDashboard.Models;
using ClientDashboard.Classes;
using System.Collections.Generic;
using System.Data.Entity;

namespace ClientDashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserProfileController : Controller
    {
         private ApplicationUserManager _userManager;
        private ClientDashboardEntities db = new ClientDashboardEntities();

        //
        // GET: /UserProfile/

        public ActionResult Index()
        {
            return View(db.AspNetUsers.OrderBy(u => u.IsAccountActive).ToList());
        }

        //
        // GET: /UserProfile/Details/5

        public ActionResult Details(string id = "0")
        {
            AspNetUser userprofile = db.AspNetUsers.Find(id);
            var assigned = new List<string>();
            var amod = new List<string>();
            var c = new List<string>();
            var total = new List<string>();
            //all the camapigns with their respective client names
            var roles = (from r in db.AspNetRoles
                         join cl in db.Clients on r.ClientID equals cl.ClientID
                         select new { RoleName=r.Name, cl.Name, r.Id }).Where(x => x.RoleName != "Administrator");
            //all the modules
            var modules = (from m in db.Modules select new { m.Name, m.ModuleID }).Where(x => x.Name != "Administrator").ToList();
            //all the camapigns and modules assigned to user
            var campaigns = (from ur in db.AspNetUserRoles
                             join r in db.AspNetRoles on ur.RoleId equals r.Id
                             join mod in db.Modules on ur.ModuleID equals mod.ModuleID
                             where ur.UserId == id
                             select new
                             {
                                 RoleName=r.Name,
                                 mod.Name
                             }).ToList();



            List<Role> RolesList1 = new List<Role>();


            foreach (var result in roles)
            {
                List<RoleModule> RoleModList1 = new List<RoleModule>();
                foreach (var mod in modules)
                {
                    RoleModList1.Add(new RoleModule()
                    {
                        selected = campaigns.Exists(x => x.RoleName == result.RoleName && x.Name == mod.Name),
                        value = mod.ModuleID
                    });
                }
                RolesList1.Add(new Role()
                {

                    RoleId = Convert.ToInt32(result.Id),
                    Client = result.Name,
                    Campaign = result.RoleName,
                    RoleModList = RoleModList1
                });


            }

            List<Module> ModuleList1 = db.Modules.Where(x => x.Name != "Administrator").ToList();
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(new EditUserViewModel()
            {
                UserId =userprofile.Id,
                Email = userprofile.Email,
                FullName = userprofile.Name,
                UserName = userprofile.UserName,
                RolesList = RolesList1,
                ModulesList = ModuleList1
            });
        }

        //
        // GET: /UserProfile/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserProfile/Create

        [HttpPost]
        public async Task<ActionResult> Create(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    var user = new ApplicationUser { UserName = model.UserName, Email = model.Email, Name=model.FullName,EmailConfirmed=true,IsAccountActive=false };
                    var result = await UserManager.CreateAsync(user, model.Password);
                    //WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email, Name = model.FullName });
                    if (result.Succeeded)
                    {
                        string emailbody = "<p >Hi <b>" + model.UserName.ToUpper() + "</b>, <br/>You have been registered on <b>IBEX Global Client Dashboard</b>.<br/><br/>Please find below your account details.</p>";
                        emailbody += "<ul ><li>User name: <b>" + model.UserName + "</b></li>";
                        emailbody += "<li>Password: <b>" + model.Password + "</b></li>";
                        emailbody += "<li>Email: " + model.Email + "</li>";
                        emailbody += "<li>Website: <a href='clientdashboard.ibexglobal.com'>IBEX | Client Dashboard</a>" + "</li></ul>";
                        Email obj = new Email();
                        obj.SendEmailToUser("Registration | Client Dashboard", model.Email, emailbody);
                        //WebSecurity.Login(model.UserName, model.Password);
                        return RedirectToAction("Index", "UserProfile");
                    }
                    ModelState.AddModelError("", result.Errors.First());
                    return View(model);
                }
                catch (Exception ex)
                {

                }
            }
            
            //if (ModelState.IsValid)
            //{
            //    db.UserProfiles.Add(userprofile);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            return View(model);
        }
        public ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            private set
            {
                _userManager = value;
            }
        }
      
        //
        // GET: /UserProfile/Edit/5

        public ActionResult Edit(string id = "0")
        {
            AspNetUser userprofile = db.AspNetUsers.Find(id);
            var assigned = new List<string>();
            var amod = new List<string>();
            var c = new List<string>();
            var total = new List<string>();
            //all the camapigns with their respective client names
            var roles = (from r in db.AspNetRoles
                         join cl in db.Clients on r.ClientID equals cl.ClientID
                         select new { RoleName=r.Name, cl.Name, r.Id }).Where(x => x.RoleName != "Administrator");
            //all the modules
            var modules = (from m in db.Modules select new { m.Name, m.ModuleID }).Where(x => x.Name != "Administrator").ToList();
            //all the camapigns and modules assigned to user
            var campaigns = (from ur in db.AspNetUserRoles
                             join r in db.AspNetRoles on ur.RoleId equals r.Id
                             join mod in db.Modules on ur.ModuleID equals mod.ModuleID
                             where ur.UserId == id
                             select new
                             {
                                 RoleName=r.Name,
                                 mod.Name
                             }).ToList();



            List<Role> RolesList1 = new List<Role>();


            foreach (var result in roles)
            {
                List<RoleModule> RoleModList1 = new List<RoleModule>();
                foreach (var mod in modules)
                {
                    RoleModList1.Add(new RoleModule()
                    {
                        selected = campaigns.Exists(x => x.RoleName == result.RoleName && x.Name == mod.Name),
                        value = mod.ModuleID
                    });
                }
                RolesList1.Add(new Role()
                {

                    RoleId = Convert.ToInt32(result.Id),
                    Client = result.Name,
                    Campaign = result.RoleName,
                    RoleModList = RoleModList1
                });


            }
            RolesList1 = RolesList1.OrderBy(x => x.Client).ToList();
            List<Module> ModuleList1 = db.Modules.Where(x => x.Name != "Administrator").ToList();
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(new EditUserViewModel()
            {
                UserId =userprofile.Id,
                Email = userprofile.Email,
                FullName = userprofile.Name,
                UserName = userprofile.UserName,
                RolesList = RolesList1,
                ModulesList = ModuleList1,
                IsAccountActive = Convert.ToBoolean(userprofile.IsAccountActive),
                TwoFactorEnabled = Convert.ToBoolean(userprofile.TwoFactorEnabled)
            });
        }

        //
        // POST: /UserProfile/Edit/5

        [HttpPost]
        public ActionResult Edit(EditUserViewModel edituser)
        {
            AspNetUser userprofile = db.AspNetUsers.Find(edituser.UserId);
            userprofile.Id = edituser.UserId.ToString();
            userprofile.UserName = edituser.UserName;
            userprofile.Email = edituser.Email;
            userprofile.Name = edituser.FullName;
            userprofile.IsAccountActive = edituser.IsAccountActive;
            userprofile.TwoFactorEnabled = edituser.TwoFactorEnabled;
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;

                foreach (var r in db.AspNetRoles)
                {
                    foreach (var m in db.Modules)
                    {
                        AspNetUserRole usersinroles = db.AspNetUserRoles.Find(edituser.UserId, r.Id, m.ModuleID);
                        int rid =Convert.ToInt32( r.Id);
                        if (usersinroles == null && edituser.RolesList.Exists(x => x.RoleId == rid && x.RoleModList.Exists(rm => rm.value == m.ModuleID && rm.selected == true)))
                        {
                            db.AspNetUserRoles.Add(new AspNetUserRole()
                            {
                                UserId = edituser.UserId.ToString(),
                                ModuleID = m.ModuleID,
                                RoleId = r.Id.ToString()
                            });


                        }
                        else if (usersinroles != null && usersinroles.RoleId != "4" && !edituser.RolesList.Exists(x => x.RoleId ==rid && x.RoleModList.Exists(rm => rm.value == m.ModuleID && rm.selected == true)))
                        {
                            db.AspNetUserRoles.Remove(usersinroles);
                        }

                    }
                }
                db.SaveChanges();


                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        //
        // GET: /UserProfile/Delete/5

        public ActionResult Delete(string id = "")
        {
            AspNetUser userprofile = db.AspNetUsers.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /UserProfile/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(string id="")
        {
            AspNetUser userprofile = db.AspNetUsers.Find(id);
            db.AspNetUsers.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}