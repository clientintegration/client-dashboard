﻿using ClientDashboard.Classes;
using ClientDashboard.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace ClientDashboard.Controllers
{
    public class CampaignStatsController : ApiController
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
       
        ClientDashboardEntities db = new ClientDashboardEntities();
        [HttpGet]
        public CampaignStats Get(string campaignid = null)
        {
            var campaignstats = new CampaignStats();
            bool flag = false;
            if (campaignid != null)
            {
                try
                {
                    flag = db.AspNetRoles.Where(x => x.Name != "Administrator" && x.CampaignType == "Inbound Program" && x.ClientID == 4).Any(x => x.Name == campaignid);
                }
                catch (Exception ex)
                {
                    logger.Error("Web API", ex);
                    var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                    {
                        Content = new StringContent(string.Format("Could not connect to database server.")),
                        ReasonPhrase = "Internal Server Error"
                    };

                    throw new HttpResponseException(resp);
                }
                if (flag == true)
                {

                    try
                    {
                        var agentswaiting = db.AgentCountReports.Where(x => x.CampaignID == campaignid).Select(x => x.AgentsOnWait).FirstOrDefault();
                        var callsinqueue = db.RoutingQueueStats.Where(x => x.CampaignID == campaignid).Select(x => x.QueuedCalls).Sum();
                        campaignstats = new CampaignStats { agents_waiting = agentswaiting.ToString(), calls_in_queue = callsinqueue.ToString() };

                    }
                    catch (Exception ex)
                    {
                        logger.Error("Web API", ex);
                        var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                        {
                            Content = new StringContent(string.Format("Could not connect to database server.")),
                            ReasonPhrase = "Internal Server Error"
                        };

                        throw new HttpResponseException(resp);
                    }

                    if (campaignstats != null)
                        return campaignstats;
                    else
                    {
                        var resp = new HttpResponseMessage(HttpStatusCode.InternalServerError)
                        {
                            Content = new StringContent(string.Format("Could not connect to database server.")),
                            ReasonPhrase = "Internal Server Error"
                        };

                        throw new HttpResponseException(resp);
                    }

                }
                else
                {

                    var resp = new HttpResponseMessage(HttpStatusCode.NotFound)
                    {
                        Content = new StringContent(string.Format("Campaign id '{0}' does not exist or you are not authorized to view the information.", campaignid)),
                        ReasonPhrase = "Campaign not found"
                    };

                    throw new HttpResponseException(resp);
                }
            }
            else
            {

                var resp = new HttpResponseMessage(HttpStatusCode.BadRequest)
                {
                    Content = new StringContent(string.Format("Campaign id is not provided.", campaignid)),
                    ReasonPhrase = "Campaign not provided"
                };

                throw new HttpResponseException(resp);
            }






        }


        //public HttpResponseMessage Options()
        //{

        //    var response = new HttpResponseMessage();
        //    response.Headers.Add("Access-Control-Allow-Headers", "Authorization");
        //    response.Headers.Add("Access-Control-Allow-Origin", "*");
        //    response.Headers.Add("Access-Control-Allow-Method", "GET");
        //    response.Headers.Add("Access-Control-Allow-Credentials","true");

        //    return response;
        //}
    }
    public class CampaignStats
    {

        public string agents_waiting { get; set; }

        public string calls_in_queue { get; set; }
    }
}