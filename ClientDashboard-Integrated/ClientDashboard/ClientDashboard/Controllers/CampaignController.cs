﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientDashboard.Models;

namespace ClientDashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class CampaignController : Controller
    {
        private ClientDashboardEntities db = new ClientDashboardEntities();
        List<SelectListItem> CampaignType = new List<SelectListItem>
    {
        new SelectListItem {  Text = "Inbound Program", Value = "Inbound Program"},
        new SelectListItem {Text ="Outbound Program", Value = "Outbound Program"},
        new SelectListItem { Text = "Verification Program", Value = "Verification Program"},
    };
        List<SelectListItem> SwitchType = new List<SelectListItem>
    {
         new SelectListItem {  Text = "IBEX Avaya", Value = "IBEX Avaya"},
        new SelectListItem {  Text = "IBEX Web Dialer", Value = "IBEX Web Dialer"},
         new SelectListItem { Text = "IBEX Web Dialer - Asterisk", Value = "IBEX Web Dialer - Asterisk"},
        new SelectListItem {Text ="IBEX Web Dialer - Avaya", Value = "IBEX Web Dialer - Avaya"},
       
    };
        //
        // GET: /Campaign/

        public ActionResult Index()
        {
            var roles = db.AspNetRoles.Include(w => w.Client);
            return View(roles.ToList());
        }

        //
        // GET: /Campaign/Details/5

        public ActionResult Details(int id = 0)
        {
            AspNetRole roles = db.AspNetRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        //
        // GET: /Campaign/Create

        public ActionResult Create()
        {
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name");
            ViewBag.CampaignType = new SelectList(CampaignType , "Value", "Text");
            ViewBag.SwitchType = new SelectList(SwitchType, "Value", "Text");
            return View();
        }

        //
        // POST: /Campaign/Create

        [HttpPost]
        public ActionResult Create(AspNetRole roles)
        {
            if (ModelState.IsValid)
            {
                db.AspNetRoles.Add(roles);
                db.SaveChanges();
                return RedirectToAction("Index");
               
            }

            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", roles.ClientID);
            ViewBag.CampaignType = new SelectList(CampaignType, "Value", "Text");
            ViewBag.SwitchType = new SelectList(SwitchType, "Value", "Text");
            return View(roles);
        }

        //
        // GET: /Campaign/Edit/5

        public ActionResult Edit(int id = 0)
        {
            AspNetRole roles = db.AspNetRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", roles.ClientID);
            ViewBag.CampaignType = new SelectList(CampaignType, "Value", "Text", roles.CampaignType);
            ViewBag.SwitchType = new SelectList(SwitchType, "Value", "Text", roles.SwitchType);
            return View(roles);
        }

        //
        // POST: /Campaign/Edit/5

        [HttpPost]
        public ActionResult Edit(AspNetRole roles)
        {
            if (ModelState.IsValid)
            {
                db.Entry(roles).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ClientID = new SelectList(db.Clients, "ClientID", "Name", roles.ClientID);
            ViewBag.CampaignType = new SelectList(CampaignType, "Value", "Text",roles.CampaignType);
            ViewBag.SwitchType = new SelectList(SwitchType, "Value", "Text", roles.SwitchType);
            return View(roles);
        }

        //
        // GET: /Campaign/Delete/5

        public ActionResult Delete(int id = 0)
        {
            AspNetRole roles = db.AspNetRoles.Find(id);
            if (roles == null)
            {
                return HttpNotFound();
            }
            return View(roles);
        }

        //
        // POST: /Campaign/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            AspNetRole roles = db.AspNetRoles.Find(id);
            db.AspNetRoles.Remove(roles);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}