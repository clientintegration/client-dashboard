﻿using ClientDashboard.Classes;
using ClientDashboard.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using Microsoft.AspNet.Identity;
namespace ClientDashboard.Controllers
{
    public class HomeController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
        ClientDashboardEntities db = new ClientDashboardEntities();
        [Authorize]
        public ActionResult Index()
        {

            return View();
        }


        [Authorize]
        public ActionResult Download()
        {
            return View();
        }

        public ActionResult Error()
        {
            return View();
        }


        public ActionResult DownloadFile(string windowstype)
        {
            try
            {
                string FilePath = "";
                if (windowstype == "XP")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Windows_XP_2003_DownloadPath"].ToString();
                }
                else if (windowstype == "Vista")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Vista_Higer_DownloadPath"].ToString();
                }

                string filename = System.IO.Path.GetFileName(FilePath);
                return File(FilePath, ".zip", filename);
            }
            catch (Exception ex)
            {

                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Recordings", "Dashboard");
            }

        }

        public ActionResult GetRoles()
        
        
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var roles = (from r in db.AspNetRoles
                             join ur in db.AspNetUserRoles on r.Id equals ur.RoleId
                             where (ur.UserId == userId)
                             select r.Name).Distinct().ToList();

                return Json(new { rolelist = roles }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {

                logger.Error(User.Identity.Name, ex);
                return Json(new { rolelist = "" }, JsonRequestBehavior.AllowGet);
            }




        }
    }
}