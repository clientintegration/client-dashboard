﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using Microsoft.AspNet.Identity;
using ClientDashboard.Models;
using Newtonsoft.Json;

using System.Data.Entity;
using ClientDashboard.Filters;
namespace ClientDashboard.Controllers
{
    //[SessionExpire]
    [Authorize]
    public class DashboardController : Controller
    {
        readonly log4net.ILog logger = log4net.LogManager.GetLogger(System.Reflection.MethodBase.GetCurrentMethod().DeclaringType);
     
        ClientDashboardEntities db = new ClientDashboardEntities();
        public ActionResult Index(string id)
        {
            try
            {
                if (id != null)
                {
                    bool agent = false, campaign = false, recordings = false;

                    string userId = User.Identity.GetUserId();


                    var roles = (from r in db.AspNetRoles
                                 join ur in db.AspNetUserRoles on r.Id equals ur.RoleId
                                 where (ur.UserId == userId)
                                 select r.Name).Distinct().ToList();

                    //To get the modules assigned to user against the campaign
                    var roleid = (from r in db.AspNetRoles
                                  where (r.Name == id)
                                  select r.Id).ToList().FirstOrDefault();

                    var modules = new List<string>();
                    var query = (from ur in db.AspNetUserRoles
                                 join module in db.Modules on ur.ModuleID equals module.ModuleID
                                 where ur.RoleId == roleid && ur.UserId == userId
                                 select new
                                 {
                                     module.Name
                                 }).ToList();
                    foreach (var result in query)
                    {
                        if (result.Name == "Agent")
                            agent = true;

                        if (result.Name == "Campaign")
                            campaign = true;

                        if (result.Name == "Recordings")
                            recordings = true;

                    }
                    if (campaign == true)
                        return RedirectToAction("Campaign", "Dashboard", new { id = id });
                    else if (agent == true)
                        return RedirectToAction("Agent", "Dashboard", new { id = id });
                    else if (recordings == true)
                        return RedirectToAction("Recordings", "Dashboard", new { id = id });
                    else
                        return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
            }
            return RedirectToAction("Index", "Home");
        }

        [Log(Activity = "Visted Agent Performance")]
        [CustomAuthorize("Agent")]
        public ActionResult Agent(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {

                    return View();

                }

            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }

        [Log(Activity = "Visited Campaign Performance")]
        [CustomAuthorize("Campaign")]
        public ActionResult Campaign(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    Session["CampaignName"] = id;
                    string type = db.HomeReports.Where(x => x.CampaignID == id).Select(x => x.CampaignType).FirstOrDefault().ToString();
                    Session["CampaignType"] = type;

                    return View();

                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }

        [Log(Activity = "Visited Trends")]
        [CustomAuthorize("Trends")]
        public ActionResult Trends(string id)
        {
            try
            {
                if (id == null)
                {


                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    return View();

                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }
        [Log(Activity = "Visited Avaya CMS")]
        [CustomAuthorize("Avaya")]
        public ActionResult Avaya(string id)
        {
            try
            {
                if (id == null)
                {


                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    return View();

                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }

        [Log(Activity = "Visited Call History")]
        [CustomAuthorize("History")]
        public ActionResult History(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    Session["CampaignName"] = id;
                    return View();
                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }

        [HttpGet]
        [Log(Activity = "Visited Call Recordings")]
        [CustomAuthorize("Recordings")]
        public ActionResult Recordings(string id)
        {
            try
            {

                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                    return View();
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }

        [Authorize]
        [Log(Activity = "Visited Glossary")]
        [CustomAuthorize("Glossary")]
        public ActionResult Glossary(string id)
        {
            try
            {

                if (id == null)
                    return RedirectToAction("Index", "Home");
                else
                    return View(db.Glossaries.Where(x => x.CampaignID == id).OrderBy(x => x.Term).ToList());
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }
        }

        [HttpGet]
        [Log(Activity = "Visited Treshold Notification")]
        [CustomAuthorize("Thresholds")]
        public ActionResult Thresholds(string id)
        {
            try
            {

                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    var userthresholds = db.UserThresholds.Include(u => u.Metric).Include(u => u.AspNetUser);
                    return View(userthresholds.ToList());

                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return RedirectToAction("Index", "Home");
            }

        }
        #region Historical Report
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }
        [Log(Activity = "Searched History")]
        public JsonResult HistoryReport(string id, string startdate, string enddate, string uniqueid, string tel, string agentid, string[] termcd)
        {
            string Termcdlist = string.Join(",", termcd);
            List<HistoricalReport_Result> data = new List<HistoricalReport_Result>();
            HistoricalReport_Result obj = new HistoricalReport_Result();
            var test = obj.GetHistoricalReport_Result(startdate.Trim(), enddate.Trim(), uniqueid.Trim(), tel.Trim(), agentid.Trim(), id.Trim(), Termcdlist.Trim());

            data = obj.GetHistoricalReport_Result(startdate.Trim(), enddate.Trim(), uniqueid.Trim(), tel.Trim(), agentid.Trim(), id.Trim(), Termcdlist.Trim());
            return Json(new { result = data }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DispositionList(string id)
        {
            List<DispositionList_Result> disposition = new List<DispositionList_Result>();
            disposition = db.DispositionList(id).ToList();
            return Json(new { result = disposition }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Threshold Notifications
        public JsonResult GetThresholdMetrics(string id)
        {
            var metrics = from mt in db.Metrics
                          join tm in db.ThresholdMetrics on mt.MetricID equals tm.MetricID
                          where tm.CampaignID == id && tm.IsActive == true
                          select new { mt.Name, mt.Description, mt.MetricID };
            return Json(new { result = metrics.ToList() }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetUserThresholds(string id)
        {
            string userId = User.Identity.GetUserId();
            var thresholds = from ut in db.UserThresholds
                             join m in db.Metrics on ut.MetricID equals m.MetricID
                             where ut.CampaignID == id && ut.UserID == userId
                             select new { m.Description, ut.ThresholdValue, ut.IsSent, ut.IsActive, m.MetricID, ut.IfAbove };
            return Json(new { result = thresholds.ToList() }, JsonRequestBehavior.AllowGet);

        }
        public void AddUserThreshold(string campaignid, string metricid, string value, string ifabove)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var userthreshold = new UserThreshold();
                userthreshold.UserID = userId;
                userthreshold.ThresholdValue = Convert.ToDecimal(value);
                userthreshold.MetricID = Convert.ToInt32(metricid);
                userthreshold.CampaignID = campaignid.ToUpper();
                userthreshold.AddedOn = DateTime.Now;
                userthreshold.IsActive = true;
                userthreshold.IsSent = false;
                userthreshold.IfAbove = Convert.ToBoolean(ifabove);
                db.UserThresholds.Add(userthreshold);
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }

        }
        public void SaveUserThreshold(string campaignid, string metricid, string value, string isactive, string ifabove)
        {
            try
            {
                string userId = User.Identity.GetUserId();
                var userthreshold = db.UserThresholds.Find(userId, Convert.ToInt32(metricid), campaignid);
                userthreshold.ThresholdValue = Convert.ToDecimal(value);
                userthreshold.IsActive = Convert.ToBoolean(isactive);
                userthreshold.IfAbove = Convert.ToBoolean(ifabove);
                userthreshold.UpdatedOn = DateTime.Now;
                userthreshold.IsSent = false;
                db.Entry(userthreshold).State = EntityState.Modified;
                db.SaveChanges();
            }
            catch (Exception ex)
            {

            }

        }
        #endregion
        #region Trends Analysis

        public JsonResult GetMetrics(string id)
        {
            var metrics = from mt in db.Metrics
                          join cmt in db.CampaignMetrics on mt.MetricID equals cmt.MetricID
                          join gm in db.MetricGraphs on mt.MetricID equals gm.MetricID
                          join g in db.Graphs on gm.GraphID equals g.GraphID
                          where cmt.CampaignID == id && cmt.Isactive == true
                          select new { mt.Name, mt.Description, g.HtmlID, mt.MetricID };
            return Json(new { result = metrics.ToList() }, JsonRequestBehavior.AllowGet);

        }

        [Log(Activity = "Searched Trends")]
        public string GetTrends(string id, string startdate, string enddate)
        {
            JSONResultSP js = new JSONResultSP();
            string s = js.GetJSONResult("CallTrends", startdate, enddate, id);
            return s;
        }
        public string GetDispositionTrends(string id, string startdate, string enddate)
        {
            JSONResultSP js = new JSONResultSP();
            string s = js.GetJSONResult("DispositionTrends", startdate, enddate, id);
            return s;
        }
        #endregion

        #region Avaya CMS
        public JsonResult GetAvayaSkills(string id)
        {
            var skills = from s in db.AvayaSkillInfoes
                         where s.CampaignID == id
                         select new { s.Skill, s.Name };
            return Json(new { result = skills.ToList() }, JsonRequestBehavior.AllowGet);

        }
        public JsonResult GetAvayaAgents(string id)
        {
            var agents = db.AgentInformation(id);
            return Json(new { result = agents.ToList() }, JsonRequestBehavior.AllowGet);

        }
        #endregion

        #region Call Recordings



        [Log(Activity = "Searched Recordings")]      
        public JsonResult SearchRecordings(string id, string startdate, string enddate, string uniqueid, string tel, string agentid, string[] termcd, string filter1, string filter2, string filter3,
            string filter4, string filter5, string filterval1, string filterval2, string filterval3, string filterval4, string filterval5)
        {
            CallRecordingReport_Result obj = new CallRecordingReport_Result();
            List<CallRecordingReport_Result> data = new List<CallRecordingReport_Result>();
            string CampaignID = id;
            string Termcdlist = string.Join(",", termcd);
            data = obj.GetSearchResults(startdate.Trim(), enddate.Trim(), uniqueid.Trim(), tel.Trim(), agentid.Trim(), CampaignID.Trim(), Termcdlist, filter1.Trim(),
                filter2.Trim(), filter3.Trim(), filter4.Trim(), filter5.Trim(), filterval1.Trim(), filterval2.Trim(), filterval3.Trim(), filterval4.Trim(), filterval5.Trim());




            #region NR Streaming Player 1.1
            string NRPlayer_IV = ConfigurationManager.AppSettings["NRPlayer_IV"].ToString();
            string NRPlayer_Key = ConfigurationManager.AppSettings["NRPlayer_Key"].ToString();
            byte[] IVbytes = Encoding.ASCII.GetBytes(NRPlayer_IV);
            byte[] Keybytes = Encoding.ASCII.GetBytes(NRPlayer_Key);

            string NRArgument = ConfigurationManager.AppSettings["Storage_locator_IP_Port"].ToString() + " " +
                ConfigurationManager.AppSettings["AltStorage_locator_IP_Port"].ToString() + " " +
                ConfigurationManager.AppSettings["Proxy_Server_IP_Port"].ToString() + " " +
                User.Identity.Name + " " +
                ConfigurationManager.AppSettings["Comment_string"].ToString() + " " +
                ConfigurationManager.AppSettings["Saving_string"].ToString() + " ";

            byte[] encryptedtext = null;

            foreach (var item in data)
            {
                encryptedtext = null;
                encryptedtext = EncryptStringToBytes(NRArgument + item.NR_Segment, Keybytes, IVbytes);
                item.NR_Segment = "nrplayer:" + Convert.ToBase64String(encryptedtext);

            }
            #endregion

            return Json(new { result = data }, JsonRequestBehavior.AllowGet);

        }
        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null && plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null && Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null && IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }
        public ActionResult FilterList(string id)
        {
            List<string> filter = new List<string>();
            filter = db.FilterList(id).ToList();
            return Json(new { result = filter }, JsonRequestBehavior.AllowGet);
        }

        #endregion
        #region Partial Views and AJAX requests
        [ChildActionOnly]
        public ActionResult GetMenuItems()
        {
            List<MenuItems> model = new List<MenuItems>();
            try
            {
                string userId = User.Identity.GetUserId();
                string id = RouteData.Values["id"] + Request.Url.Query;
                var roleid = (from r in db.AspNetRoles
                              where (r.Name == id)
                              select r.Id).ToList().FirstOrDefault();

                var modules = new List<string>();
                var query = (from ur in db.AspNetUserRoles
                             join module in db.Modules on ur.ModuleID equals module.ModuleID
                             where ur.RoleId == roleid && ur.UserId == userId
                             select new
                             {
                                 module.Name,
                                 module.Description,
                                 module.HTML


                             }).ToList().OrderBy(m => m.Description == "Glossary").ThenBy(m => m.Description);


                foreach (var result in query)
                {
                    model.Add(item: new MenuItems
                    {
                        CampaignID = id,
                        Module = result.Name,
                        Description = result.Description,
                        HTML = result.HTML

                    });
                }
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);

            }
            return PartialView("_SideMenu", model);
        }

        [ChildActionOnly]
        public ActionResult GetReportHeader()
        {


            try
            {
                string id = RouteData.Values["id"] + Request.Url.Query;
                int len = Request.Url.Segments.Length;
                string action = Request.Url.Segments[len - 2].Replace(@"/", string.Empty).Trim();


                var header = db.ReportHeaders.Include("Module").Where(x => x.CampaignID == id && x.Module.Name == action).ToList();

                if (action == "Agent")
                    return PartialView("_AgentTablePartial", header);
                else if (action == "Campaign")
                    return PartialView("_CampaignTablePartial", header);
                else
                    return null;
            }
            catch (Exception ex)
            {
                logger.Error(User.Identity.Name, ex);
                return PartialView("_ErrorPartial");
            }


        }
        public JsonResult GetHeaderContent(string id)
        {
            List<Header> model = new List<Header>();

            try
            {
                //   string[] roles = Roles.GetRolesForUser(User.Identity.Name);
                string userId = User.Identity.GetUserId();
                int uid = Convert.ToInt32(userId);
                string action = id;
                var client = (from r in db.AspNetRoles
                              join c in db.Clients on r.ClientID equals c.ClientID
                              join ur in db.AspNetUserRoles on r.Id equals ur.RoleId
                              join mod in db.Modules on ur.ModuleID equals mod.ModuleID
                              where ur.UserId == userId && mod.Name == action
                              orderby c.Name
                              select new
                              {
                                  clientName = c.Name,
                                  r.Name,
                                  mod.Description,
                                  mod.HTML,
                                  r.CampaignType,
                                  r.SwitchType
                              }
                              ).ToList();


                // Module result = db.Modules.Where(x => x.Name == action).FirstOrDefault();
                foreach (var item in client)
                {
                    model.Add(new Header
                    {
                        CampaignID = item.Name,
                        Client = item.clientName,
                        ModuleDescription = item.Description,
                        HTML = item.HTML,
                        CampaignType = item.CampaignType,
                        SwitchType = item.SwitchType
                    });
                }

            }
            catch (Exception ex)
            {

                logger.Error(User.Identity.Name, ex);

            }


            return Json(new { result = model.ToList() }, JsonRequestBehavior.AllowGet);
        }
        #endregion
    }
}
