﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Hosting;
using System.Web.Mvc;
using ClientDashboard.Classes;
using ClientDashboard.Models;

namespace ClientDashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class AdministratorController : Controller
    {
        //
        // GET: /Administrator/
        ClientDashboardEntities db = new ClientDashboardEntities();
        public ActionResult Index()
        {
            return View();
        }

        public ActionResult UserActivity()
        {
            return View();
        }

        public JsonResult GetUserActivity(string startdate, string enddate)
        {

            SearchUserActivity_Result obj = new SearchUserActivity_Result();
            List<SearchUserActivity_Result> data = new List<SearchUserActivity_Result>();
            data = db.SearchUserActivity(Convert.ToDateTime(startdate), Convert.ToDateTime(enddate)).ToList();



            return Json(new { result = data }, JsonRequestBehavior.AllowGet);

        }
        [Authorize]
        public ActionResult Emailer()
        {

            ViewBag.Email = new SelectList(db.AspNetUsers.Where(x => x.IsAccountActive == true), "Email", "Email");

            return View();
        }
        [HttpPost]
        public ActionResult Emailer(Models.MailModel model, HttpPostedFileBase file, string[] Email)
        {

            if (ModelState.IsValid)
            {

                Email obj = new Email();
                obj.SendEmailToUsers(model.Subject, Email, model.Body, file);
                ViewBag.Email = new SelectList(db.AspNetUsers.Where(x => x.IsAccountActive == true), "Email", "Email");
                return View();

            }
            else
            {
                return View("Emailer");
            }
        }
        //If SQL Depnedency halts and application restart from IIS is not possible
        public ActionResult SQLDependency()
        {
            return View();
        }
        [HttpPost]
        public ActionResult SQLDependency(string value)
        {
            HostingEnvironment.RegisterObject(new SQLDependency());

            return View();
        }
    }
}
