﻿var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", "#e74c3c", "#1E824C", "#a6dcf1"]
var barchart;
var table;
var occthreshold = -1;
var agentnames = [], agents = []
var flag = false, cname;

$(function () {

    $("#ag-spinner").show()
    $("#ag-graphcontainer").hide()
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();

    $.fn.dataTableExt.sErrMode = 'throw';
    try {
        table = $('#agenttable').dataTable({
            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }],
            "bSortCellsTop": true,
            //scrollY: false,
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,

            dom: '<Blf<rt>ip>',
            buttons: [
                {
                    extend: 'excelHtml5',

                    title: 'Agent Statistics' + formatDateforTable(new Date())
                }
            ]


        });
    }
    catch (e)
    { }
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getAgentsStats().done(function (Stats) {
                var oTable = $('#agenttable').DataTable();
                oTable.clear();
                $('#agenttable').DataTable().rows.add(populateAgentStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try {
                    oTable.button(0).enable(nRows > 0);
                }
                catch (Ex)
                { }
                $("#lastUpdateTime").text(formatDate(new Date()));
            });
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });
            notifications.server.getAgentCountStats().done(function (Stats) {
                populateAgentCountStats(Stats)
            });
        });
    }, 1000);
    var tryingToReconnect = false;

    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            //$("#lastUpdateTime").text("Disconnected");
            $("#dialog").dialog("open");
            //notifyUserOfDisconnect(); // Your function to notify user.
        }
    });
    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateAgentCountStats = function (Stats) {
        // alert("updateAgentStateStats");
        populateAgentCountStats(Stats)
    };
    notifications.client.updateAgentsStats = function (Stats) {



        var oTable = $('#agenttable').DataTable();
        oTable.clear();
        $('#agenttable').DataTable().rows.add(populateAgentStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
        $("#lastUpdateTime").text(formatDate(new Date()));
    };

});



function populateAgentStats(result) {
    $("#ag-spinner").hide()
    $("#ag-graphcontainer").show()
    var ser1 = [], ser4 = [], ser3 = [], ser2 = [], ser5 = [], ser6_percent = []
    var dataSet = [], total = []
    agents = [], agentnames = []
    var numofagents = 0
    var oTable = $('#agenttable').DataTable();
    var list = result;
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            if (value["B"].toLowerCase() != "total") {

                agentnames.push(value["A"])
                agents.push(value["B"])
                ser1.push(parseFloat(value["F"]))
                ser2.push(parseFloat(value["G"]))
                ser3.push(parseFloat(value["H"]))
                ser4.push(parseFloat(value["I"]))
                ser5.push(parseFloat(value["J"]))
                ser6_percent.push(parseFloat(value["K"]))
            }
            var cols = table.fnSettings().aoColumns.length
            var arr = [];
            if (value["B"].toLowerCase() != "total") {

                var colascii = 65;
                for (var i = 0; i < cols ; i++) {
                    if (colascii == 68) {//State Time
                        if (value["C"].toLowerCase().indexOf("out") < 0) {
                            arr.push(value[String.fromCharCode(colascii)])
                        }
                        else
                            arr.push("");

                    }
                    else if (colascii == 69) {//Time since last call
                        if (value["C"].toLowerCase() == "wait")
                            arr.push(value[String.fromCharCode(colascii)])
                        else
                            arr.push("");
                    }
                    else
                        arr.push(value[String.fromCharCode(colascii)])
                    colascii++;
                }


                dataSet.push(arr)

            }
            else {

                var colascii = 65;
                for (var i = 0; i < cols ; i++) {

                    if (colascii == 66 || colascii == 67 || colascii == 68 || colascii == 69)
                        total.push("")
                    else
                        total.push(value[String.fromCharCode(colascii)])
                    colascii++;
                }
            }
        }
    });


    if (flag == false) {

        flag = true;
        var s1 = $(oTable.column(5).header()).html()
        var s2 = $(oTable.column(6).header()).html()
        var s3 = $(oTable.column(7).header()).html()
        var s4 = $(oTable.column(8).header()).html()
        var s5 = $(oTable.column(9).header()).html()
        var s6 = $(oTable.column(10).header()).html()

        DrawBarChart(s1, s2, s3, s4, s5, s6)

    }
    if (agentnames.length - 1 > 15)
        numofagents = 15
    else
        numofagents = agentnames.length - 1

    barchart.xAxis[0].setCategories(agents);
    barchart.xAxis[0].setExtremes(null, numofagents);
    barchart.yAxis[1].setExtremes(0, 100);
    barchart.series[0].setData(ser1);
    barchart.series[1].setData(ser2);
    barchart.series[2].setData(ser3);
    barchart.series[3].setData(ser4);
    barchart.series[4].setData(ser5);
    barchart.series[5].setData(ser6_percent);

    $('#frow th').each(function (i) {

        if ($(this).text().toLowerCase() != "total")
            $(this).text("");

    });
    if (total.length != 0) {
        $('#frow th').each(function (i) {
            if (total[i] != undefined && $(this).text().toLowerCase() != "total")
                $(this).text("" + total[i]);

        });
    }
    if (!barchart.hasData()) {
        barchart.hideNoData();
        barchart.showNoData("No data available!");
    }
    return dataSet;
}

function DrawBarChart(ser1, ser2, ser3, ser4, ser5, ser6) {
    barchart = new Highcharts.Chart({

        chart: {
            type: 'column',
            renderTo: 'ag-graphcontainer'
        },
        title: {
            text: 'Agent Performance',
            align: 'left'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [],
            crosshair: true
            // max:max
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} hrs',
                style: {
                    color: '#000'
                }
            },
            title: {
                text: 'Hours',
                style: {
                    color: '#000',
                    fontWeight: 'bold'
                }
            },
            tickInterval: 3

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Percentage',
                style: {
                    color: '#DF314D',
                    fontWeight: 'bold'
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: '#DF314D'

                }
            },
            opposite: true,
            threshold: 50


        }],
        legend: {

            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },
        scrollbar: {
            enabled: true
        },
        tooltip: {
            formatter: function () {
                var points = this.points;
                var pointsLength = points.length;
                var s = '<b>' + this.x + '</b> - ';
                s += agentnames[agents.indexOf(this.x)]
                $.each(this.points, function (i, point) {

                    s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + '';

                });

                return s;
            },

            shared: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: ser1,
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[0]

        }, {
            name: ser2,
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[1]

        }, {
            name: ser3,
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[2]

        }, {
            name: ser4,
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[3]

        }, {
            name: ser5,
            color: '#ccc',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[4]

        }, {
            name: ser6,
            data: [],
            type: 'spline',
            color: '#DF314D',
            yAxis: 1,
            tooltip: {
                valueSuffix: ' %'
            },
            threshold: occthreshold,
            negativeColor: '#eb525d'
        }

        ]
    });
}