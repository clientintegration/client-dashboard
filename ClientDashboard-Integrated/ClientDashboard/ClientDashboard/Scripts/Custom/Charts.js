﻿function DrawDonutChart(chartID, title, unit, data) {
    console.log(data)
    $("#" + chartID).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: title,
            align: 'left',

        },
        tooltip: {
            valueSuffix: '',
            formatter: function () {
                var s = Math.round((this.y))
                return this.point.name + ':<b>' + s + '</b> ' + unit + '<br/>'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
            }
        },
        series: [{
            type: 'pie',
            name: title,
            innerSize: '50%',
            data: data
        }]
    });
}
function DrawDonutExtendedChart(chartID, title, subTitle,unit,s1Name,s1Data,s2Name,s2Data)
{
    TFNQpiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: chartID
        },


        title: {
            text: title,
            align: 'left'
        },
        subtitle: {
            text:subTitle,
            align: 'left',
            style: { fontSize: '11px' }
        },
        xAxis: {
          
            type: 'category',
            lineWidth: 0,
            labels: {
                formatter: function () {
                    if (this.value.substring(0, 1) == "S")
                        return '<span style="font-size:12px;font-weight:bold;color:#00b53c">' + this.value.substring(0, 2) + "<span>";
                    else
                        return '<p style="">' + this.value.substring(0, 2) + "<p>";
                }
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            //labelFormatter: function () {
            //    if (this.name.indexOf('-') === -1)
            //        return this.name;
            //    else
            //        return this.name.substring(0, 2);
            //},
            borderWidth: 0,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%'],

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            },
            allowPointSelect: false

        },


        series: [{
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> '+unit+'<br/>'
                }
            },
            animation: false,
            name: s1Name,
            data: s1Data,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return '';
                },
                color: 'white',
                distance: -40,
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" }
            },
            showInLegend: true,

        }
        , {
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> '+unit+'<br/>'
                }
            },
            animation: false,
            name: s2Name,
            data: s2Data,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                formatter: function () {
                    // display only if larger than 1
                    var s = Math.round((this.y))
                    if (this.point.name == "Abandoned")
                        return this.y > 0 ? '<b style="color:red">' + this.point.name + ': ' + s + '</b>' : null;

                    else
                        return this.y > 0 ? '<b style="color:black">' + this.point.name + ':</b> ' + s + '' : null;
                }
            }
        }
        ]

    });



}
function DrawMultiBarChart(chartID, title,xTitle,yTitle1, catTitle,categories,series,unit)
{
    $("#" + chartID).highcharts({
        chart: {
            type: 'column',
           
        },


        title: {
            text:title,
            align: 'left'

        },
        xAxis: {
            categories: categories,
            crosshair: true
        },
        yAxis: [{
            min: 0,
            title: {
                text:yTitle1
            },
            labels: {
                format: '{value} '+unit
                
            },
            allowDecimals: false,
            minTickInterval: 1
        }],
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>'+catTitle+':' + this.x + '</caption><tbody>';
                //loop each point in this.points
       
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                //points += '<tr><th>Calls Offered: </th>'
                //+ '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                //+ '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },

        plotOptions: {
            series: {
                pointWidth: 30//width of the column bars irrespective of the chart size
            }
        },
        series: series
    });
}
function DrawPieChart(chartID, title, unit, data) {
    console.log(data)
    $("#" + chartID).highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: 0,
            plotShadow: false
        },
        title: {
            text: title,
            align: 'left',

        },
        tooltip: {
            valueSuffix: '',
            formatter: function () {
                var s = this.y;
                if(unit=="hours")
                    return this.point.name + ':<b>' + hoursTohhmm(s) + '</b> <br/>'
                else
                    return this.point.name + ':<b>' + s + '</b>'+unit+' <br/>'
            }
        },
        plotOptions: {
            pie: {
                dataLabels: {
                    enabled: false
                },
                showInLegend: true,
            }
        },
        series: [{
            type: 'pie',
            name: title,
           
            data: data
        }]
    });
}
function DrawSplineChart(chartID, title,xTitle,yTitle,catTitle,categories,series,unit)
{
    $('#'+chartID).highcharts({
        chart: {
            type: 'spline'
        },
        title: {
            text: title,
            align: 'left'
        },
       
        xAxis: {
            categories: categories
        },
        yAxis: {
            title: {
                text: yTitle
            },
            labels: {
                formatter: function () {
                    return this.value + 'hrs';
                }
            }
        },
        tooltip: {
            crosshairs: true,
            shared: true,
            formatter: function () {
                var points = this.points;
                var pointsLength = points.length;
                var s = '<b>' + this.x + '</b> ';
               
                $.each(this.points, function (i, point) {
                    if(unit=="hours")
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + hoursTohhmm(point.y) + '';
                    else
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + '';

                });

                return s;
            },
        },
        plotOptions: {
            spline: {
                marker: {
                    radius: 4,
                    lineColor: '#666666',
                    lineWidth: 1
                }
            }
        },
        series: series
    });
}