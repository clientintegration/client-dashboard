﻿var agstatuspiechart, Disppiechart, TFNQpiechart, cname
var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", '#805b69', "#1E824C", '#2d3ab7', '#b7aa2d', '#3a6166', '#14a5ab', '#35817b', '#9179de', '#c0fff4', '#995460', '#528881', '#584778', '#4f62d8', '#7e708b', '#9a666e', '#2e2126', '#4a152a', '#6b1f3c', '#abffe3', '#ffd9df', '#f3c3c3', '#ffb5be', '#00028a', '#00027a', '#00025a', '#a93b1c', '#a43e32', '#a42f1e', '#953904', '#6c2801', '#fcfac9', '#fc4078', '#fc2b6a', '#b00070', '#c00090']
var agstatusflag = false, TFNQflag = false, Dispflag = false
$(function () {
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        closeText: 'Close',
        draggable: false,
        minHeight: 100,
        minWidth: 360,
        dialogClass: 'main-dialog-class',
        open: function () {
            $(this).closest(".ui-dialog")
            .find(".ui-dialog-titlebar-close")
            .removeClass("ui-dialog-titlebar-close")
            .html("<span class='ui-button-icon-primary ui-icon ui-icon-closethick'></span>");
        },
        close: function (event, ui) {
            window.location.href = "https://clientdashboard.ibexglobal.com/"
        }
    });


    try {
        Highcharts.setOptions({
            colors: colors,
            chart: {
                style: {
                    fontFamily: 'Helvetica'

                }
            },
            lang: {
                drillUpText: '< Back'
            }
        });


    }
    catch (ex)
    { console.log(ex) }
});
$(window).load(function () {

    var selection = $("#cbCampaigns").val();
    if (selection != null)
        cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();
});
function showThresholdNotification(result) {
    var list = result;
    toastr.options = { "positionClass": "toast-bottom-right" }
    console.log(result);

    $.each(list, function (index, value) {
        toastr.info(value.Metric + ":" + value.Value);
    });
}
function populateAgentStatusStats(result) {

    var list = result;
    var states = [], agents = [], statusdata = []
    var i = 0;
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            states.push(value.AgentState);
            agents.push(value.AgentCount);
            var temp = new Array(states[i], agents[i]);
            statusdata[i] = temp;
            i++;
        }


    });

    $("#agstatus-spinner").hide()
    $("#agstatus-piecontainer").show()
    agstatuspiechart = $('#agstatus-piecontainer').highcharts();
    if (agstatusflag == false) {
        DrawAgentStatusChart(statusdata)
        agstatusflag = true;
    }
    else {

        agstatuspiechart.series[0].setData(statusdata);
    }
    agstatuspiechart = $('#agstatus-piecontainer').highcharts();
    if (!agstatuspiechart.hasData()) {
        agstatuspiechart.hideNoData();
        agstatuspiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));

}
function DrawAgentStatusChart(statusdata) {

    agstatuspiechart = $('#agstatus-piecontainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agents Status',
            align: 'left'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        legend:
            {
                itemStyle: {
                    "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
                }
            },
        series: [{
            name: "Agents",
            colorByPoint: true,
            data: statusdata,
            dataLabels: {
                enabled: true,
                format: '{point.y:.0f}',
                distance: -30,
                //color: 'white'
            }
        }]
    });
}

function populateAgentCountStats(result) {
    var list = result;
    try {
        $("#onWrap").text(0);
        $("#onTalk").text(0);
        $("#onWait").text(0);
        $("#onBreak").text(0);
    }
    catch (ex) {

    }
    //$("#totalAgents").text(0);
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            try {
                $("#onWrap").text(value.AgentsOnWrap);
                $("#onTalk").text(value.AgentsOnTalk);
                $("#onWait").text(value.AgentsOnWait);
                $("#onBreak").text(value.AgentsOnBreak);
            }
            catch (ex) {

            }
            var total = parseInt(value.AgentsOnWrap) + parseInt(value.AgentsOnTalk) + parseInt(value.AgentsOnWait) + parseInt(value.AgentsOnBreak)

            var popOverHTML = $("#info").attr("data-content");
            var $html = $('<div />', { html: popOverHTML });
            $html.find('span#head_loggedAgents').html(total);
            $("#info").attr('data-content', $html.html());
        }



    });

    $("#lastUpdateTime").text(formatDate(new Date()));

}

function refreshDialingStatus(result) {
    var list = result;
    var dialstatus;
    $("#DialingStatus").hide();
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            dialstatus = value.DialStatus;

        }
    });
    $("#status").text(cname.toUpperCase() + " is not dialing at the moment.");

    var popOverHTML = $("#info").attr("data-content");
    var $html = $('<div />', { html: popOverHTML });
    if (dialstatus == "Inactive") {

        $("#DialingStatus").show();



        $html.find('span#head_dialingStatus').html("Inactive");

    }
    else
        $html.find('span#head_dialingStatus').html("Active");

    $("#info").attr('data-content', $html.html());
}

function populateLeadsStats(result) {
    var list = result;
    $.each(list, function (index, value) {

        if (value.CampaignID.toLowerCase() == cname) {
            try {
                document.getElementById("hi-icon-avail").textContent = value.Available;
            } catch (ex) {
            }
        }
    });
    $("#lastUpdateTime").text(formatDate(new Date()));

}

function populateQueueStats(result) {
    var list = result;
    var calls = 0
    $.each(list, function (index, value) {

        if (value.CampaignID.toLowerCase() == cname) {
            calls += value.QueuedCalls
            document.getElementById("hi-icon-que").textContent = calls;
        }
    });
    $("#lastUpdateTime").text(formatDate(new Date()));

}

//*****************************Inbound Campaigns*****************************//
function populateTFNQStats(result) {
    $("#tfn-disp-spinner").hide()
    $("#tfn-disp-piecontainer").show()
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };

    var categories = [], chartdata = [], drilldownSeries = []
    var TFNQData = [], CallsData = [], i, j, dataLen = chartdata.length, drillDataLen, brightness;
    var list = result
    var i = 0, tempData = []
    $.each(list, function (index, value) {
        tempData = []
        if (value.CampaignID.toLowerCase() == cname) {

            if (!categories.contains(value.TfnQueue)) {
                categories.push(value.TfnQueue)

                chartdata.push({
                    y: value.CallsOffered,
                    color: colors[i],
                    name: categories[i],
                    drilldown: {

                        categories: ['Answered', 'Abandoned'],
                        data: [value.CallsHandled, value.CallsAbandon]

                    }
                })
                //Disposition Drilldown
                $.each(list, function (index1, value1) {
                    if (value1.CampaignID.toLowerCase() == cname) {
                        if (value1.TfnQueue == value.TfnQueue) {
                            tempData.push([value1.Disposition, value1.CallCount]);
                            //tempData.push([value1.Disposition, '{y:'+value1.CallCount+',color:red}']);
                        }
                    }
                });
                TFNQData.push({
                    name: value.TfnQueue,
                    y: chartdata[i].y,
                    color: chartdata[i].color,
                    drilldown: value.TfnQueue,


                });


                drillDataLen = chartdata[i].drilldown.data.length;
                //alert(drillDataLen);
                for (j = 0; j < drillDataLen; j += 1) {
                    //  alert(chartdata[i].drilldown.data[j]);
                    brightness = 0.2 - (j / drillDataLen) / 5;
                    CallsData.push({
                        name: chartdata[i].drilldown.categories[j],
                        y: chartdata[i].drilldown.data[j],
                        color: Highcharts.Color(chartdata[i].color).brighten(brightness).get()
                    });

                }

                drilldownSeries.push(
                {


                    // pointWidth:15,

                    showInLegend: true,

                    dataLabels: {
                        style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                        formatter: function () {
                            // display only if larger than 1
                            var s = Math.round((this.y))
                            return this.y > 0 ? this.point.name.substring(0, 2) + ': ' + s + '</b>' : null;
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> calls<br/>'
                    },
                    legend: {

                        borderWidth: 0,
                        itemStyle: {
                            "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "normal"
                        }
                    },
                    dataLabels: {
                        enabled: true
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    innerSize: '50%',
                    "name": value.TfnQueue,
                    type: 'column',
                    data: tempData,
                    id: value.TfnQueue
                });
                i++;
            }

        }
    });


    $("#tfn-disp-spinner").hide()
    $("#tfn-disp-piecontainer").show()
    DrawTFNQPieChart(TFNQData, CallsData, drilldownSeries)
    TFNQflag = true;

    if (!TFNQpiechart.hasData()) {
        TFNQpiechart.hideNoData();
        TFNQpiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
}
function DrawTFNQPieChart(TFNQData, CallsData, drilldownSeries) {


    TFNQpiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'tfn-disp-piecontainer'
        },


        title: {
            text: 'Calls Per TFN/Queue',
            align: 'left'
        },
        subtitle: {
            text: ' Click the inner slices to view calls per disposition.',
            align: 'left',
            style: { fontSize: '11px' }
        },
        xAxis: {
            //title: {
            //    text: 'Disposition Codes',

            //},
            type: 'category',
            lineWidth: 0,
            labels: {
                formatter: function () {
                    if (this.value.substring(0, 1) == "S")
                        return '<span style="font-size:12px;font-weight:bold;color:#00b53c">' + this.value.substring(0, 2) + "<span>";
                    else
                        return '<p style="">' + this.value.substring(0, 2) + "<p>";
                }
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            labelFormatter: function () {
                if (this.name.indexOf('-') === -1)
                    return this.name;
                else
                    return this.name.substring(0, 2);
            },
            borderWidth: 0,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%'],

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            },
            allowPointSelect: false

        },


        series: [{
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> calls<br/>'
                }
            },
            animation: false,
            name: 'Calls Offered',
            data: TFNQData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return '';
                },
                color: 'white',
                distance: -40,
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" }
            },
            showInLegend: true,

        }
        , {
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> calls<br/>'
                }
            },
            animation: false,
            name: 'Calls',
            data: CallsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                formatter: function () {
                    // display only if larger than 1
                    var s = Math.round((this.y))
                    if (this.point.name == "Abandoned")
                        return this.y > 0 ? '<b style="color:red">' + this.point.name + ': ' + s + '</b>' : null;

                    else
                        return this.y > 0 ? '<b style="color:black">' + this.point.name + ':</b> ' + s + '' : null;
                }
            }
        }
        ],
        drilldown: {
            //animation: false,
            drillUpButton: {
                relativeTo: 'spacingBox',
                position: {
                    y: 20,
                    x: 0
                },
                theme: {
                    fill: 'white',
                    'stroke-width': 1,
                    stroke: 'silver',
                    r: 0,
                    states: {

                        select: {
                            stroke: '#039',
                            fill: '#bada55'
                        }
                    }
                }

            },
            series: drilldownSeries
        }

    });


}
//**************************************************************************//

//*****************************Outbound Campaigns*****************************//
function populateDispositionStats(result) {
    $("#tfn-disp-spinner").show()
    $("#tfn-disp-piecontainer").hide()
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };

    var categories = [], chartdata = [], drilldownSeries = []
    var DispData = [], CallsData = [], i, j, dataLen = chartdata.length, drillDataLen, brightness;
    var list = result
    var i = 0, tempData = []
    $.each(list, function (index, value) {
        tempData = []
        if (value.CampaignID.toLowerCase() == cname) {

            if (!categories.contains(value.DispType)) {
                categories.push(value.DispType)
                //  alert(value.DispType)
                chartdata.push({
                    y: value.CallsPerDispType,
                    color: colors[i],
                    name: categories[i]
                })
                //Disposition Drilldown
                $.each(list, function (index1, value1) {
                    if (value1.CampaignID.toLowerCase() == cname) {
                        if (value1.DispType == value.DispType) {
                            tempData.push([value1.Disposition, value1.CallCount]);
                        }
                    }
                });
                DispData.push({
                    name: value.DispType,
                    y: chartdata[i].y,
                    color: chartdata[i].color,
                    drilldown: value.DispType,


                });

                drilldownSeries.push(
                {


                    // pointWidth:15,

                    showInLegend: true,

                    dataLabels: {
                        style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                        formatter: function () {
                            // display only if larger than 1
                            var s = Math.round((this.y))
                            return this.y > 0 ? this.point.name.substring(0, 2) + ': ' + s + '</b>' : null;
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> calls<br/>'
                    },
                    legend: {

                        borderWidth: 0,
                        itemStyle: {
                            "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "normal"
                        }
                    },
                    dataLabels: {
                        enabled: true
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    innerSize: '50%',
                    "name": value.DispType,
                    type: 'column',
                    data: tempData,
                    id: value.DispType
                });
                i++;
            }

        }
    });

    console.log(DispData);
    console.log(CallsData);
    console.log(drilldownSeries);
    $("#tfn-disp-spinner").hide()
    $("#tfn-disp-piecontainer").show()


    DrawDispositionChart(DispData, CallsData, drilldownSeries)
    Dispflag = true;

    if (!Disppiechart.hasData()) {
        Disppiechart.hideNoData();
        Disppiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
}
function DrawDispositionChart(DispData, CallsData, drilldownSeries) {


    Disppiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'tfn-disp-piecontainer'
        },


        title: {
            text: 'Calls per Disposition Type',
            align: 'left'
        },
        subtitle: {
            text: ' Click the inner slices to view calls per disposition.',
            align: 'left',
            style: { fontSize: '11px' }
        },
        xAxis: {
            type: 'category',
            lineWidth: 0,
            labels: {
                formatter: function () {
                    if (this.value.substring(0, 1) == "S")
                        return '<span style="font-size:12px;font-weight:bold;color:#00b53c">' + this.value.substring(0, 2) + "<span>";
                    else
                        return '<p style="">' + this.value.substring(0, 2) + "<p>";
                }
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            enabled: true,
            labelFormatter: function () {
                if (this.name.indexOf('-') === -1)
                    return this.name;
                else
                    return this.name.substring(0, 2);
            },
            borderWidth: 0,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                },
                dataLabels: {
                    enabled: false
                },
            },
            allowPointSelect: false

        },


        series: [{
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> calls<br/>'
                }
            },
            animation: false,
            name: 'Calls',
            data: DispData,
            innerSize: '50%',
            showInLegend: true,

        }

        ],
        drilldown: {
            //animation: false,
            drillUpButton: {
                relativeTo: 'spacingBox',
                position: {
                    y: 20,
                    x: 0
                },
                theme: {
                    fill: 'white',
                    'stroke-width': 1,
                    stroke: 'silver',
                    r: 0,
                    states: {

                        select: {
                            stroke: '#039',
                            fill: '#bada55'
                        }
                    }
                }

            },
            series: drilldownSeries
        }

    });


}
//**************************************************************************//

function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}

function GetDateDiffInTime(data) {

    var ClientConvDateEST = new Date(ConverttoEST(new Date()));
    //Differences between two dates with time
    //data = data.replace('T', ' ');

    //For Safari
    var a = data.split(/[^0-9]/);

    var data = new Date(a[0], a[1] - 1, a[2], a[3], a[4], a[5]);



    var serverinputdate = new Date(data);
    var diff = (ClientConvDateEST - serverinputdate) / 1000;
    //    console.log(diff);
    var diff = Math.abs(Math.floor(diff));

    var days = Math.floor(diff / (24 * 60 * 60));
    var leftSec = diff - days * 24 * 60 * 60;

    var hrs = Math.floor(leftSec / (60 * 60));
    var leftSec = leftSec - hrs * 60 * 60;

    var min = Math.floor(leftSec / (60));
    var leftSec = leftSec - min * 60;

    hrs = hrs < 10 ? '0' + hrs : hrs;
    min = min < 10 ? '0' + min : min;
    leftSec = leftSec < 10 ? '0' + leftSec : leftSec;

    return hrs + ':' + min + ':' + leftSec//data + '(' + row[3] + ')';
}

function ConverttoEST(clientDate) {
    //Convert local time to EST, because the server time is in EST
    offset = -5.0
    utc = clientDate.getTime() + (clientDate.getTimezoneOffset() * 60000);
    return new Date(utc + (3600000 * offset));
}
function titleCase(str) {
    var newstr = str.split(" ");
    for (i = 0; i < newstr.length; i++) {
        var copy = newstr[i].substring(1).toLowerCase();
        newstr[i] = newstr[i][0].toUpperCase() + copy;
    }
    newstr = newstr.join(" ");
    return newstr;
}
function hoursTohhmm(hours) {
    var sign = hours < 0 ? "-" : "";
    var hour = Math.floor(Math.abs(hours));
    var min = Math.floor((Math.abs(hours) * 60) % 60);
    return sign + (hour < 10 ? "0" : "") + hour + ":" + (min < 10 ? "0" : "") + min;
}