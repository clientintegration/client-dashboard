﻿var chart;
$(document).ready(function () {
    DrawGauge();
    $('#dateinputalert').hide();

    $('#history').DataTable({

    });
    $('#liveusers').DataTable({

    });
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getActiveUsers().done(function (LiveUsers) {
              
                var oTable = $('#liveusers').DataTable();
                oTable.clear();
                var liveusersarr=[]
                var list=LiveUsers
                $.each(list, function (index, value) {
                    var arr = [];


                    arr.push(value["UserId"]);
                    arr.push(value["UserName"]);

                    arr.push(value["LastActivityTime"]);
                    liveusersarr.push(arr);

                });
                $('#liveusers').DataTable().rows.add(liveusersarr).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
             
                var point = chart.series[0].points[0]

                point.update(nRows);
            });


        });
    }, 1000);
    notifications.client.updateActiveUsers = function (LiveUsers) {
        var oTable = $('#liveusers').DataTable();
        oTable.clear();
        var liveusersarr = []
        var list = LiveUsers
        $.each(list, function (index, value) {
            var arr = [];


            arr.push(value["UserId"]);
            arr.push(value["UserName"]);

            arr.push(value["LastActivityTime"]);
            liveusersarr.push(arr);

        });
        $('#liveusers').DataTable().rows.add(liveusersarr).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
       
        var point = chart.series[0].points[0]

        point.update(nRows);
    };
    $('#startdate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        //  format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90))),
        endDate: new Date()
        // update "enddate" defaults whenever "startdate" changes
    }).on('changeDate', function () {
        // set the "enddate" start to not be later than "startdate" ends:
        $('#enddate').datepicker('setStartDate', new Date($(this).val()));
        var enddate = new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() + 15));
        if (enddate > new Date())
            $('#enddate').datepicker('setEndDate', new Date());
        else
            $('#enddate').datepicker('setEndDate', enddate);
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#enddate').val($(this).val());
        }
    });

    $('#enddate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        // format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90)))
        // update "startdate" defaults whenever "enddate" changes
    }).on('changeDate', function () {
        // set the "startdate" end to not be later than "enddate" starts:
        $('#startdate').datepicker('setEndDate', new Date($(this).val()));
        $('#startdate').datepicker('setStartDate', new Date(new Date($(this).val()).setDate(new Date($(this).val()).getDate() - 15)));
        var startdate = new Date($('#startdate').val());
        var enddate = new Date($('#enddate').val());

        if (enddate.getTime() < startdate.getTime()) {
            $('#startdate').val($(this).val());
        }
    });

    $('.collapse').on('shown.bs.collapse', function () {
        $(this).parent().find(".glyphicon-plus").removeClass("glyphicon-plus").addClass("glyphicon-minus");
    }).on('hidden.bs.collapse', function () {
        $(this).parent().find(".glyphicon-minus").removeClass("glyphicon-minus").addClass("glyphicon-plus");
    });
});

function Continue(e) {


    $('#dateinputalert').hide();
    $('#agentdisposealert').hide();
    if ($('#startdate').val() == "" || $('#enddate').val() == "") {
        $('#dateinputalert').show();
        e.preventDefault();
    }


    else {

        $.LoadingOverlay("show");

        $('#dateinputalert').hide();



        //Ajax Call to populate History table
        var activityarr = [];
        var search = {

            startdate: $('#startdate').val(),
            enddate: $('#enddate').val()

        }

        $.ajax({
            url: "/Administrator/GetUserActivity",
            type: "GET",
            traditional: true,
            contentType: 'application/json',
            datatype: 'json',
            data: search,
            success: function (data) {
                var len = data.result.length;
                console.log(data);

                for (i = 0; i < len; i++) {
                    var arr = [];

                    var value = data.result[i];
                    arr.push(value.UserID);
                    arr.push(value.UserName);
                    var url = value.Pageurl.split('/');

                    arr.push(url[3]);
                    arr.push(url[2]);
                    arr.push(value.Visits);
                    activityarr.push(arr);

                }

                $('#history').DataTable().clear();
                $('#history').DataTable().rows.add(activityarr).draw(true);

                $.LoadingOverlay("hide");

            },
            error: function (msg) {
                $.LoadingOverlay("hide");

            }
        });


    }

}

function DrawGauge() {

    chart = new Highcharts.Chart({
        chart: {
            renderTo: 'container',
            type: 'solidgauge',

        },


        title: null,

        pane: {
            center: ['50%', '85%'],
            size: '140%',
            startAngle: -90,
            endAngle: 90,
            background: {
                backgroundColor: (Highcharts.theme && Highcharts.theme.background2) || '#EEE',
                innerRadius: '60%',
                outerRadius: '100%',
                shape: 'arc'
            }
        },

        tooltip: {
            enabled: false
        },

        // the value axis
        yAxis: {

            lineWidth: 0,
            minorTickInterval: null,
            tickPixelInterval: 400,
            tickWidth: 0,
            title: {
                y: -70
            },
            labels: {
                y: 16
            },
            min: 0,
            max: 25,
            title: {
                text: null
            }
        },

        plotOptions: {
            solidgauge: {
                dataLabels: {
                    y: 5,
                    borderWidth: 0,
                    useHTML: true
                }
            }
        },

      
        series: [{
            name: 'Speed',
            data: [0],
            dataLabels: {
                format: '<div style="text-align:center"><span style="font-size:25px;color:' +
                    ((Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black') + '">{y}</span><br/>' +
                       '<span style="font-size:12px;color:silver">user(s)</span></div>'
            },
            tooltip: {
                valueSuffix: ' '
            }
        }]


    });

}
