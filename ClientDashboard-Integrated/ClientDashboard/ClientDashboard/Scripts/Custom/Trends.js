﻿var colors = ["#30bb74", "#2980b9", "#95a5a6", "#9a7fd1", "#F88F07", '#805b69', "#1E824C", '#2d3ab7', '#b7aa2d', '#3a6166', '#14a5ab', '#35817b', '#9179de', '#c0fff4', '#995460', '#528881', '#584778', '#4f62d8', '#7e708b', '#9a666e', '#2e2126', '#4a152a', '#6b1f3c', '#abffe3', '#ffd9df', '#f3c3c3', '#ffb5be', '#00028a', '#00027a', '#00025a', '#a93b1c', '#a43e32', '#a42f1e', '#953904', '#6c2801', '#fcfac9', '#fc4078', '#fc2b6a', '#b00070', '#c00090']

var graphMetric = {},
    metricDef = {}
var callsdatatable = [],
    dispdatatable = [];
var cname = "",
    intervalRenderer = [],
    dayRenderer = [],
    intervalData = {},
    dayData = {}
var disptype = [],
    treedata = [],
    bardata = [];
var weekday = new Array(7);
weekday[0] = "Sun";
weekday[1] = "Mon";
weekday[2] = "Tue";
weekday[3] = "Wed";
weekday[4] = "Thu";
weekday[5] = "Fri";
weekday[6] = "Sat";

$(document).ready(function () {
   
    $("#dialog").hide();
    $('#inputalert').hide();
    $("#lastUpdateTime").hide();
    $("#lastupdateon").hide();
    $('#metrics').select2()
    $("#disposition-spinner").hide();
   
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).substring(0, 3);

    // 
    SetDatePickers();
    
    var startdate = new Date();
    startdate.setDate(startdate.getDate() - 7);
 
    $('#startdate').datepicker("setDate", startdate);
    onStartChange($("#startdate"))


    $('#enddate').datepicker("setDate", new Date());
    onEndChange($("#enddate"))

  
   
   // $('#startdate').val(GetFormattedDate(new Date(startdate)))
  
  
    $.LoadingOverlay("show");
    $("#disposition-spinner").show();
    $('#inputalert').hide();
    $('#dateinputalert').hide();
   
    GetCampaignMetrics();
   
    SetHighChartsSync();
    SetMouseOutEvent("historical");
    SetMouseOutEvent("spiderCalls")
    // fix dimensions of chart that was in a hidden element
    jQuery(document).on('shown.bs.tab', 'a[data-toggle="tab"]', function (e) { // on tab selection event
        Highcharts.each(Highcharts.charts, function (chart) {
            try {
                chart.reflow() // reflow that chart
            }
            catch (ex) { }
        });
        var table = $('#displayTable').DataTable();

        table.draw(false);

    })
   
});

function GetCampaignMetrics() {
    $.ajax({
        url: "/Dashboard/GetMetrics/" + cname,
        type: "GET",
        traditional: true,


        success: function (data) {
            GetCallTrends();
            var HtmlIDs = []
            for (var i = 0; i < data.result.length; i++) {
                HtmlIDs.push(data.result[i]["HtmlID"]);
                metricDef[data.result[i]["Name"]] = data.result[i]["Description"];
                //$('#metrics').append('<option value="' + data.result[i]["Name"] + '">' + data.result[i]["Description"] + '</option>');
                //graphMetric[data.result[i]["Name"]] = data.result[i]["HtmlID"]

            }


            for (var j = 0; j < HtmlIDs.length; j++) {
                var metrics = []
                for (var i = 0; i < data.result.length; i++) {
                    if (HtmlIDs[j] == data.result[i]["HtmlID"])
                        metrics.push(data.result[i]["Name"])
                }
                graphMetric[HtmlIDs[j]] = metrics
            }
            $.each(graphMetric.lineCalls, function (x, obj) {

                $('#metrics').append('<option value="' + graphMetric.lineCalls[x] + '">' + metricDef[graphMetric.lineCalls[x]] + '</option>');

            });

            $.each(graphMetric.barHours, function (x, obj) {
                $('#metrics').append('<option value="' + graphMetric.barHours[x] + '">' + metricDef[graphMetric.barHours[x]] + '</option>');


            });
            $('#metrics').select2().on("select2:select", function (e) {
                var val = e.params.data.id;
                AddSeries(val);

            }).on("select2:unselect", function (e) {
                if (!e.params.originalEvent) {
                    return;
                }

                e.params.originalEvent.stopPropagation();
                var val = e.params.data.id;
                RemoveSeries(val)
            })

        }
    });
}

function onIntervalMetricChange(select) {
    var id = jQuery(select).attr("id").substring(11)
    var chart = $('#chart-container-i-' + id).highcharts();


    chart.series[0].remove();
    chart.setTitle({
        text: metricDef[select.value]
    });

    chart.addSeries({
        data: intervalData[select.value],
        name: metricDef[select.value],
        type: 'column',
        color: colors[id],
        fillOpacity: 0.3,
        tooltip: {
            valuePrefix:'Average '+((select.value.indexOf("(H)") > -1) ? " hours" : " calls")+':'
          
        }
    });
}

function onDayMetricChange(select) {
    var id = jQuery(select).attr("id").substring(11)
    var chart = $('#chart-container-d-' + id).highcharts();


    chart.series[0].remove();
    chart.setTitle({
        text: metricDef[select.value]
    });

    chart.addSeries({
        data: dayData[select.value],
        name: metricDef[select.value],
        type: 'column',
        color: colors[id],
        fillOpacity: 0.3,
        tooltip: {
            valuePrefix: 'Average ' + ((select.value.indexOf("(H)") > -1) ? " hours" : " calls") + ':'

        }
    });
}

function RemoveSeries(metric) {
    var chart = $('#historical').highcharts();

    for (var i = 0; i < chart.series.length; i++) {
        if (chart.series[i].name == metric) {
            chart.series[i].remove();
        }
    }
    if (!chart.hasData()) {
        chart.hideNoData();
        chart.showNoData("No metrics selected!");
    }
    //ResetChartVisibility(graphMetric[metric]);
}

function AddSeries(metric) {
    if (callsdatatable.length > 0) {
        var historicalLine = [],
            historicalBar = []
        $.each(graphMetric.lineCalls, function (x, obj) {

            historicalLine.push(graphMetric.lineCalls[x]);

        });
        $.each(graphMetric.barHours, function (x, obj) {

            historicalBar.push(graphMetric.barHours[x]);

        });

        var chart = $('#historical').highcharts();

        var data = []
        for (var i = 0; i < callsdatatable.length; i++) {


            if (callsdatatable[i]["CallTime"] != 0)
                data.push([parseFloat(callsdatatable[i]["CallTime"]), parseFloat(callsdatatable[i][metric])]);

        }
        var type = historicalLine.contains(metric) ? "line" : "column";
        var yaxis = historicalLine.contains(metric) ? 0 : 1;

        if (data.length > 0) {

            chart.addSeries({
                type: type,
                name: metric,
                data: data,
                yAxis: yaxis
            });
            // $('#' + graphMetric[metric]).show()
        }


    }
}

function Continue(e) {

    if ($('#startdate').val() == "" || $('#enddate').val() == ""
        //|| ($('#metrics').val() == "" || $('#metrics').val() == null)
    ) {
        $('#inputalert').show();
        e.preventDefault();
    } else {

        $.LoadingOverlay("show");
        $("#disposition-spinner").show();
        $('#inputalert').hide();
        $('#dateinputalert').hide();
        GetCallTrends();

    }
}

function GetCallTrends() {
    var search = {
        id: cname,
        startdate: $('#startdate').val(),
        enddate: $('#enddate').val()

    }
    var json;
    $.ajax({
        url: "/Dashboard/GetTrends",
        type: "GET",
        traditional: true,
        data: search,
        success: function (data) {
 
            if (data != null && data != "") {
                callsdatatable = jQuery.parseJSON(data)
                var dataset = [],
                    totals = []
              
                for (var i = 0; i < callsdatatable.length; i++)
                    if (callsdatatable[i].CallTime != 0)
                        dataset.push(callsdatatable[i])
                    else
                        totals.push(callsdatatable[i])
                //For data table
                var tabledata = []
                for (var i = 0; i < callsdatatable.length; i++) {
                    var arr = []
                    if (callsdatatable[i].CallTime != 0) {
                        dt = new Date(parseInt(callsdatatable[i].CallTime))
                        y = dt.getUTCFullYear();
                        mon = dt.getUTCMonth() + 1 < 10 ? '0' + parseInt(dt.getUTCMonth() + 1) : parseInt(dt.getUTCMonth() + 1)
                        d = dt.getUTCDate() < 10 ? '0' + dt.getUTCDate() : dt.getUTCDate()
                        h = dt.getUTCHours() < 10 ? '0' + dt.getUTCHours() : dt.getUTCHours()
                        m = dt.getUTCMinutes() < 10 ? '0' + dt.getUTCMinutes() : dt.getUTCMinutes()

                        arr.push(mon + "/" + d + "/" + y + " " + h + ":" + m);
                        $.each(graphMetric.dataTable, function (j, val) {
                            arr.push(callsdatatable[i][val])

                        });
                        tabledata.push(arr);
                    }
                }

                var tableHeaders = "<th>INTERVAL</th>";
                $.each(graphMetric.dataTable, function (i, val) {
                    tableHeaders += "<th>" + val + "</th>";

                });

                $("#dataTable").empty();
                $("#dataTable").append('<table id="displayTable" class="Custable" cellspacing="0" width="100%"><thead><tr>' + tableHeaders + '</tr></thead></table>');
                $('#displayTable').dataTable({
                    "aoColumnDefs": [{
                        "bSortable": false,
                        "aTargets": ["no-sort"]
                    }],
                    "bSortCellsTop": true,
                    //scrollY: false,
                    scrollX: true,
                    scrollCollapse: true,
                    paging: true,
                    fixedColumns: true,
                    "bProcessing": true,
                    "order": [
                        [0, "desc"]
                    ],
                    dom: '<Blf<rt>ip>',
                    buttons: [{
                        extend: 'excelHtml5',

                        title: 'Trends '

                    }


                    ],
                    data: tabledata
                });
                //$("#tableDiv").find("table thead tr").append(tableHeaders);  
                DrawHistoricalChart();
                if (graphMetric.lineCalls.length > 0 && graphMetric.barHours.length > 0) {
                    $("#metrics").val([graphMetric.lineCalls[0], graphMetric.barHours[0]]).trigger("change");

                }
                var metrics = $('#metrics').val()
                if (metrics != null) {
                    for (var j = 0; j < metrics.length; j++) {
                        AddSeries(metrics[j]);
                    }
                } else {
                    var chart = $('#historical').highcharts();
                    if (!chart.hasData()) {
                        chart.hideNoData();
                        chart.showNoData("No metrics selected!");
                    }

                }
                $.LoadingOverlay("hide");
                GetSpiderCallsData(dataset);
                GetHistogramIntervalData(dataset)
                GetHistogramDayData(dataset);
                GetDispositionTrends();
                GetActivityPercentData(totals);


            }
            else
                $.LoadingOverlay("hide");

           
            
        }
    });
}

function GetHistogramIntervalData(dataset) {
    var xData = [],
        datasets = [];
    if (dataset != null && dataset != "") {
        var intervalAvg = d3.nest().key(function (d) {
            dt = new Date(parseInt(d.CallTime))
            h = dt.getUTCHours() < 10 ? '0' + dt.getUTCHours() : dt.getUTCHours()
            m = dt.getUTCMinutes() < 10 ? '0' + dt.getUTCMinutes() : dt.getUTCMinutes()
            return h + ':' + m;
        }).rollup(function (v) {
            result = []
            $.each(graphMetric.histogramInterval, function (i, obj) {
                var arr = {};
                arr[graphMetric.histogramInterval[i]] = parseFloat(d3.mean(v, function (d) {
                    return d[graphMetric.histogramInterval[i]];
                }));
                result.push(arr);
            });
            return result;
        }).entries(dataset);
  
        intervalAvg=sortByKey(intervalAvg, 'key')
       
        for (var i = 0; i < intervalAvg[0].value.length; i++) {
            var d = [];
            var m = ""
            $.each(intervalAvg, function (j, obj) {
                for (k in intervalAvg[j].value[i]) {
                    m = k;
                    d.push(intervalAvg[j].value[i][k])

                }
            });
            intervalData[m] = d
        }

        $.each(intervalAvg, function (i, obj) {
            xData.push(obj.key);
        });
      
        for (var key in intervalData) {

            datasets.push({
                "name": key,
                "data": intervalData[key],
                "unit": ((key.indexOf("(H)") > -1) ? " hours" : " calls")
            });
        }

    }
    DrawHistogramIntervalChart({
        "xData": xData,
        "datasets": datasets
    });
}
function sortByKey(array, key) {
    return array.sort(function (a, b) {
        var x = a[key]; var y = b[key];
        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
    });
}
function sortByDay(days, key) {
    var day_of_week = 1;

    var list = ["Sun", "Mon", "Tue", "Wed", "Thu", "Fri", "Sat"];
    var sorted_list = list.slice(day_of_week).concat(list.slice(0, day_of_week));
    return days.sort(function (a, b) {
    
        var x = a[key]; var y = b[key];
  
        return ((sorted_list.indexOf(x) < sorted_list.indexOf(y)) ? -1 : ((sorted_list.indexOf(x) > sorted_list.indexOf(y)) ? 1 : 0));
       // return sorted_list.indexOf(x) > sorted_list.indexOf(y);
    });
}
function DrawHistogramIntervalChart(activity) {
    $("#histogramInterval").empty();
    $.each(activity.datasets, function (i, dataset) {
        if (i < 3) //Three charts only
        { // Add X values
            dataset.data = Highcharts.map(dataset.data, function (val, j) {
                return [val];
            });
            $('<div class="row"><div class="section-grey"><span class="text-label">Metrics:</span><select id="cbMetric-i-' + i + '"  onchange=onIntervalMetricChange(this) ></select></div></div>')
                .appendTo('#histogramInterval')

            $.each(graphMetric.histogramInterval, function (x, obj) {
                $('#cbMetric-i-' + i).append('<option value="' + graphMetric.histogramInterval[x] + '">' + metricDef[graphMetric.histogramInterval[x]] + '</option>');


            });
            $('#cbMetric-i-' + i).val(dataset.name);
            $('#cbMetric-i-' + i).select2();
            var renderTo = 'chart-container-i-' + i;
            intervalRenderer.push(renderTo);

            $('<div id="' + renderTo + '" class="chart">')
                .appendTo('#histogramInterval')
                .highcharts({
                    chart: {
                        renderTo: renderTo,
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20
                    },
                    title: {
                        text: metricDef[dataset.name],
                        align: 'left',
                        margin: 0,
                        x: 30
                    },
                    plotOptions: {
                        column: {
                            borderWidth: .5,
                            borderColor: '#ccc',
                            pointPadding: 0,
                            groupPadding: 0,

                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        crosshair: true,
                        events: {
                            setExtremes: syncExtremes
                        },
                        categories: activity.xData

                    },
                    yAxis: {
                        title: {
                            text: null
                        }
                    },
                    tooltip: {
                        positioner: function () {
                            return {
                                x: this.chart.chartWidth - this.label.width, // right aligned
                                y: -1 // align to title
                            };
                        },
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px'
                        },
                        valueDecimals: 2
                    },
                    series: [{
                        data: dataset.data,
                        name: dataset.name,
                        type: 'column',
                        color: colors[i],
                        fillOpacity: 0.3,
                        tooltip: {
                            valuePrefix:'Average '+((dataset.name.indexOf("(H)") > -1) ? " hours" : " calls")+':'
                        }
                    }]
                });
        }
    });


}

function GetHistogramDayData(dataset) {
    var xData = [],
        datasets = [];
    if (dataset != null && dataset != "") {
        var sumDataset = GetDaySum(dataset)
        var dayAvg = d3.nest().key(function (d) {
            dt = new Date(d.CallTime)
            return weekday[dt.getUTCDay()];
        }).rollup(function (v) {
            result = []
            $.each(graphMetric.histogramDay, function (i, obj) {
                var arr = {};
                arr[graphMetric.histogramDay[i]] = parseFloat(d3.mean(v, function (d) {
                    return d[graphMetric.histogramDay[i]];
                }));
                result.push(arr);
            });
            return result;
        }).entries(sumDataset);
        dayAvg = sortByDay(dayAvg, 'key')
        for (var i = 0; i < dayAvg[0].value.length; i++) {
            var d = [];
            var m = ""
            $.each(dayAvg, function (j, obj) {
                for (k in dayAvg[j].value[i]) {
                    m = k;
                    d.push(dayAvg[j].value[i][k])

                }
            });
            dayData[m] = d
        }
        
        $.each(dayAvg, function (i, obj) {
            xData.push(obj.key);
        });
      
       
        for (var key in dayData) {

            datasets.push({
                "name": key,
                "data": dayData[key],
                "unit": ((key.indexOf("(H)") > -1) ? " hours" : " calls")
            });
        }

    }
    DrawHistogramDayChart({
        "xData": xData,
        "datasets": datasets
    });
}

function DrawHistogramDayChart(activity) {
    $("#histogramDay").empty();
    $.each(activity.datasets, function (i, dataset) {
        if (i < 2) //Three charts only
        { // Add X values
            dataset.data = Highcharts.map(dataset.data, function (val, j) {
                return [val];
            });
            $('<div id="t' + i + '" class="col-md-6">')
                .appendTo('#histogramDay')
            $('<div class="section-grey"><span class="text-label">Metrics:</span><select id="cbMetric-d-' + i + '"  onchange=onDayMetricChange(this) ></select></div>')
                .appendTo('#t' + i)

            $.each(graphMetric.histogramDay, function (x, obj) {
                $('#cbMetric-d-' + i).append('<option value="' + graphMetric.histogramDay[x] + '">' + metricDef[graphMetric.histogramDay[x]] + '</option>');


            });
            $('#cbMetric-d-' + i).val(dataset.name);
            $('#cbMetric-d-' + i).select2();
            var renderTo = 'chart-container-d-' + i;
            dayRenderer.push(renderTo);

            $('<div id="' + renderTo + '" class="chart col-md-12">')
                .appendTo('#t' + i)
                .highcharts({
                    chart: {
                        renderTo: renderTo,
                        marginLeft: 40, // Keep all charts left aligned
                        spacingTop: 20,
                        spacingBottom: 20
                    },
                    title: {
                        text: metricDef[dataset.name],
                        align: 'left',
                        margin: 0,
                        x: 30
                    },
                    plotOptions: {
                        column: {


                        }
                    },
                    credits: {
                        enabled: false
                    },
                    legend: {
                        enabled: false
                    },
                    xAxis: {
                        crosshair: true,
                        events: {
                            setExtremes: syncExtremes
                        },
                        categories: activity.xData

                    },
                    yAxis: {
                        title: {
                            text: null
                        }
                    },
                    tooltip: {
                        positioner: function () {
                            return {
                                x: this.chart.chartWidth - this.label.width, // right aligned
                                y: -1 // align to title
                            };
                        },
                        borderWidth: 0,
                        backgroundColor: 'none',
                        pointFormat: '{point.y}',
                        headerFormat: '',
                        shadow: false,
                        style: {
                            fontSize: '18px'
                        },
                        valueDecimals: 2
                    },
                    series: [{
                        data: dataset.data,
                        name: dataset.name,
                        type: 'column',
                        color: colors[i],
                        fillOpacity: 0.3,
                        tooltip: {
                            valuePrefix: 'Average ' + ((dataset.name.indexOf("(H)") > -1) ? " hours" : " calls") + ':'
                        }
                    }]
                });

        }
    });


}

function GetDaySum(dataset) {

    var daySum = d3.nest().key(function (d) {
        // dt = new Date(parseInt(d.CallTime))
        //  return weekday[dt.getUTCDay()];
        dt = new Date(parseInt(d.CallTime))
        d = new Date(dt.getFullYear(), dt.getMonth(), dt.getUTCDate(), 12, 0, 0)
        return d;
    }).rollup(function (v) {
        result = []
        $.each(graphMetric.histogramDay, function (i, obj) {
            var arr = {};

            arr[graphMetric.histogramDay[i]] = parseFloat(d3.sum(v, function (d) {

                return d[graphMetric.histogramDay[i]];
            }));
            result.push(arr);
        });
        return result;
    }).entries(dataset);


    var sumDataset = [];
    for (var i = 0; i < daySum.length; i++) {
        var obj = {
            CallTime: daySum[i].key
        };
        for (var j = 0; j < daySum[i].value.length; j++) {
            for (k in daySum[i].value[j]) {

                obj[k] = daySum[i].value[j][k]
            }

        }
        sumDataset.push(obj);
    }



    return sumDataset;
}

function ResetChartVisibility(chartId) {
    var chart = $('#' + chartId).highcharts();
    if (!chart.hasData()) {
        $('#' + chartId).hide()
    } else
        $('#' + chartId).show()
}

function DrawHistoricalChart() {
    // create the chart
    var chart = $('#historical').highcharts('StockChart', {

        rangeSelector: {
            inputDateFormat: '%Y-%m-%d',
            inputEditDateFormat: '%Y-%m-%d',
            inputBoxBorderColor: 'gray',
            inputBoxWidth: 120,
            inputBoxHeight: 18,
            inputStyle: {
                textAlign: 'left'
            },
            labelStyle: {
                color: 'gray',

            },
            selected: 1
        },
        title: {
            text: 'Historical Trends',
            align: 'left',
            margin: 0,
            x: 30,
            style: {
                "color": "#333333",
                "fontSize": "18px"
            }
        },
        plotOptions: {
            line: {
                marker: {
                    enabled: true
                }

            }
        },
        yAxis: [{
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Calls'
            },
            height: '50%',

        }, {
            labels: {
                align: 'right',
                x: -3
            },
            title: {
                text: 'Hours'
            },
            top: '55%',
            height: '45%',
            offset: 0,

        }],

        series: []
    }, function (chart) {

        // apply the date pickers
        setTimeout(function () {
            $('input.highcharts-range-selector', $(chart.container).parent())
                .datepicker({
                    format: "yyyy-mm-dd",
                    onSelect: function (dateText) {
                        this.onchange();
                        this.onblur();
                    }
                });
        }, 0);
    });

    try {
        chart.reflow() // reflow that chart
    }
    catch (ex) { }

    //ResetChartVisibility("lineCalls")
    //ResetChartVisibility("barHours")
}

function GetDispositionTrends() {
    disptype = [], treedata = [], bardata = [];

    var search = {
        id: cname,
        startdate: $('#startdate').val(),
        enddate: $('#enddate').val()

    }
    $.ajax({
        url: "/Dashboard/GetDispositionTrends",
        type: "GET",
        traditional: true,
        data: search,

        success: function (data) {
            $("#disposition-spinner").hide();
            if (data != null && data != "") {
             //   $("#disposition").show();
                $("#dispositiontab").show();
                gdata = []
                var piedata = [],
                    drilldownSeries = []
                dispdatatable = jQuery.parseJSON(data)

                for (var i = 0; i < dispdatatable.length; i++) {

                    disptype.push(dispdatatable[i]["DispTypeDescription"]);
                }
                disptype = disptype.unique();
                //For Bar Chart
                for (var i = 0; i < disptype.length; i++) {
                    var count = 0
                    for (var j = 0; j < dispdatatable.length; j++) {
                        if (dispdatatable[j]["DispTypeDescription"] == disptype[i])
                            count += parseInt(dispdatatable[j]["callcount"])
                    }
                    bardata.push({
                        y: count,
                        color: colors[i]
                    })

                }
                for (var i = 0; i < disptype.length; i++) {
                    var tempData = []
                    var count = 0
                    for (var j = 0; j < dispdatatable.length; j++) {
                        if (dispdatatable[j]["DispTypeDescription"] == disptype[i])
                            count += parseInt(dispdatatable[j]["callcount"])
                    }
                    piedata.push({
                        y: count,
                        color: colors[i],
                        name: disptype[i],

                        drilldown: disptype[i]
                    })
                    for (var x = 0; x < dispdatatable.length; x++) {
                        if (dispdatatable[x]["DispTypeDescription"] == disptype[i])
                            tempData.push([dispdatatable[x]["termcd"] + '-' + dispdatatable[x]["description"], parseInt(dispdatatable[x]["callcount"])]);
                    }

                    drilldownSeries.push({


                        // pointWidth:15,

                        showInLegend: true,

                        dataLabels: {
                            style: {
                                "color": "contrast",
                                "fontSize": "11px",
                                "fontWeight": "normal",
                                "textShadow": "0 0 6px contrast, 0 0 3px contrast"
                            },
                            formatter: function () {
                                // display only if larger than 1
                                var s = Math.round((this.y))
                                return this.y > 0 ? this.point.name.substring(0, 2) + ': ' + s + '</b>' : null;
                            }
                        },
                        tooltip: {
                            headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                            pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> calls<br/>'
                        },
                        legend: {

                            borderWidth: 0,
                            itemStyle: {
                                "color": "#333333",
                                "cursor": "pointer",
                                "fontSize": "11.5px",
                                "fontWeight": "normal"
                            }
                        },
                        dataLabels: {
                            enabled: true
                        },
                        startAngle: -90,
                        endAngle: 90,
                        center: ['50%', '75%'],
                        innerSize: '50%',
                        "name": disptype[i],
                        type: 'column',
                        data: tempData,
                        id: disptype[i]
                    });

                }


                ////For Tree Chart
                //for (var i = 0; i < disptype.length; i++) {

                //    treedata.push({
                //        id: disptype[i],
                //        name: disptype[i],
                //        color: colors[i]
                //    });
                //}


                //for (var i = 0; i < dispdatatable.length; i++) {

                //    treedata.push({

                //        name: dispdatatable[i]["description"],
                //        parent: dispdatatable[i]["DispTypeDescription"],
                //        value: parseInt(dispdatatable[i]["callcount"])
                //    });



                //}
                DrawPieDispositionChart(piedata, drilldownSeries)
                DrawBarDispositionChart(disptype, bardata);

            }
            else {
            //    $("#disposition").hide();
                $("#dispositiontab").hide();
            }
           
            //DrawTreeDispositionChart(treedata);
           
        }
    });
}

function DrawPieDispositionChart(DispData, drilldownSeries) {


    var Disppiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'treeDisposition'
        },


        title: {
            text: 'Calls per Disposition Type',
            align: 'left'
        },
        subtitle: {
            text: ' Click the inner slices to view calls per disposition.',
            align: 'left',
            style: {
                fontSize: '11px'
            }
        },
        xAxis: {
            type: 'category',
            lineWidth: 0,
            labels: {
                formatter: function () {
                    if (this.value.substring(0, 1) == "S")
                        return '<span style="font-size:12px;font-weight:bold;color:#00b53c">' + this.value.substring(0, 2) + "<span>";
                    else
                        return '<p style="">' + this.value.substring(0, 2) + "<p>";
                }
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            enabled: true,
            labelFormatter: function () {
                if (this.name.indexOf('-') === -1)
                    return this.name;
                else
                    return this.name.substring(0, 2);
            },
            borderWidth: 0,
            itemStyle: {
                "color": "#333333",
                "cursor": "pointer",
                "fontSize": "11.5px",
                "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                },
                dataLabels: {
                    enabled: false
                },
            },
            allowPointSelect: false

        },


        series: [{
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> calls<br/>'
                }
            },
            animation: false,
            name: 'Calls',
            data: DispData,
            innerSize: '50%',
            showInLegend: true,

        }

        ],
        drilldown: {
            //animation: false,
            drillUpButton: {
                relativeTo: 'spacingBox',
                position: {
                    y: 20,
                    x: 0
                },
                theme: {
                    fill: 'white',
                    'stroke-width': 1,
                    stroke: 'silver',
                    r: 0,
                    states: {

                        select: {
                            stroke: '#039',
                            fill: '#bada55'
                        }
                    }
                }

            },
            series: drilldownSeries
        }

    });


}

function DrawBarDispositionChart(categories, data) {
    $('#barDisposition').highcharts({
        chart: {
            type: 'bar'
        },
        title: {
            text: 'Calls per Disposition Type',
            align: 'left'
        },

        xAxis: {
            categories: categories,
            title: {
                text: null
            }
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Calls',
                align: 'high'
            },
            labels: {
                overflow: 'justify'
            }
        },
        tooltip: {
            valueSuffix: ' calls'
        },
        plotOptions: {
            bar: {
                dataLabels: {
                    enabled: true
                }
            }
        },

        legend: {
            enabled: false
        },
        series: [{
            name: 'Calls',
            data: data
        }]
    });
}

function DrawTreeDispositionChart(data) {

    $('#treeDisposition').highcharts({

        series: [{
            dataLabels: {
                enabled: true,
                style: {
                    fontWeight: 'normal',
                    textShadow: "0px"
                }
            },
            type: "treemap",
            layoutAlgorithm: 'stripes',
            alternateStartingDirection: true,
            levels: [{
                level: 1,
                layoutAlgorithm: 'squarified',
                dataLabels: {
                    enabled: true,
                    align: 'left',
                    verticalAlign: 'top',
                    style: {
                        fontSize: '12px',
                        fontWeight: 'bold'
                    }
                }
            }],
            data: data
        }],
        title: {
            text: 'Calls per Disposition',
            align: 'left'

        }
    });
}

function GetSpiderCallsData(dataset) {
    cat = [], series = [];
    if (dataset != null && dataset != "") {
        var sumDataset = GetDaySum(dataset);

        var dayAvg = d3.nest().key(function (d) {
            dt = new Date(d.CallTime)
            return weekday[dt.getUTCDay()];
        }).rollup(function (v) {
            result = []
            $.each(graphMetric.spiderCalls, function (i, obj) {
                var arr = {};
                arr[graphMetric.spiderCalls[i]] = parseInt(d3.mean(v, function (d) {
                    return d[graphMetric.spiderCalls[i]];
                }));
                result.push(arr);
            });
            return result;
        }).entries(sumDataset);



        $.each(dayAvg, function (j, obj) {
            cat.push(obj.key);
        });
        for (var i = 0; i < dayAvg[0].value.length; i++) {
            var d = [];
            var m = ""
            $.each(dayAvg, function (j, obj) {
                for (k in dayAvg[j].value[i]) {
                    m = k;
                    d.push(dayAvg[j].value[i][k])

                }
            });
            series.push({
                name: m,
                data: d,
                pointPlacement: 'on',
                color: colors[i]
            });

        }
    }
    DrawSpiderCallsChart(cat, series);
    // DrawHistogramChart(cat, series);

}

function DrawSpiderCallsChart(categories, series) {
    $('#spiderCalls').highcharts({

        chart: {
            polar: true,
            type: 'line'
        },

        title: {
            text: '',

            align: 'left',
            margin: 0,
            x: 30
        },

        pane: {
            size: '80%'
        },

        xAxis: {
            categories: categories,
            tickmarkPlacement: 'on',
            lineWidth: 0
        },

        yAxis: {
            gridLineInterpolation: 'polygon',
            lineWidth: 0,
            min: 0
        },

        tooltip: {
            shared: true,
            pointFormat: '<span style="color:{series.color}">{series.name}: <b>{point.y:,.0f} calls</b><br/>'
        },

        legend: {
            labelFormatter: function () {
                return '<span style="text-weight:bold;color:' + this.userOptions.color + '"> Average ' + this.name + '</span>';
            },
            symbolWidth: 0,
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            x: 24,
            layout: 'vertical'
        },

        series: series

    });
    var chart = $('#spiderCalls').highcharts();
    if (series.length > 0)
        chart.setTitle({
            text: '' + metricDef[chart.series[0].name] + ' vs ' + metricDef[chart.series[1].name]
        });
}

function GetActivityPercentData(dataset) {
    var series = []
    if (dataset != null && dataset != "") {

        var radius = ['100%', '75%', '50%']
        var innerradius = ['100%', '75%', '50%']
        $.each(graphMetric.activityPercent, function (i, obj) {

            series.push({
                name: graphMetric.activityPercent[i],
                borderColor: Highcharts.getOptions().colors[i],
                color: Highcharts.getOptions().colors[i],
                data: [{
                    color: Highcharts.getOptions().colors[i],
                    radius: radius[i],
                    innerRadius: innerradius[i],
                    y: parseFloat(dataset[0][graphMetric.activityPercent[i]])
                }],
                marker: {
                    enabled: false
                },
                showInLegend: true
            });
        });
    }
    DrawActivityPercentChart(series)
}

function DrawActivityPercentChart(series) {
    var outerRadius = ['112%', '87%', '62%'];
    var innerRadius = ['88%', '63%', '38%'];
    var background = []
    for (var i = 0; i < series.length; i++) {
        background.push({
            outerRadius: outerRadius[i],
            innerRadius: innerRadius[i],
            backgroundColor: Highcharts.Color(Highcharts.getOptions().colors[i]).setOpacity(0.3).get(),
            borderWidth: 0
        })
    }

    $('#activityPercent').highcharts({
        chart: {
            type: 'solidgauge',
            marginTop: 50
        },

        title: {
            text: 'Performance Rate',
            align: 'left',
            margin: 0,
            x: 30
        },

        tooltip: {
            borderWidth: 0,
            backgroundColor: 'none',
            shadow: false,
            style: {
                fontSize: '16px'
            },
            pointFormat: '{series.name}<br><span style="font-size:2em; color: {point.color}; font-weight: bold">{point.y:.0f}%</span>',
            positioner: function (labelWidth, labelHeight) {
                var divWidth = (parseFloat($("#activityPercent").width()) / 2) + 90
              
                return {
                    x: divWidth - 25 - labelWidth / 2,
                    y: 135
                };
            }
        },
        legend: {
            labelFormatter: function () {
                return '<span style="text-weight:bold;color:' + this.userOptions.color + '">' + metricDef[this.name] + '</span>';
            },
            symbolWidth: 0,
            align: 'left',
            verticalAlign: 'top',
            y: 20,
            x: 17,
            layout: 'vertical'
        },
        pane: {
            startAngle: 0,
            endAngle: 360,
            background: background
        },

        yAxis: {
            min: 0,
            max: 100,
            lineWidth: 0,
            tickPositions: []
        },

        plotOptions: {
            solidgauge: {
                borderWidth: '24px',
                dataLabels: {
                    enabled: false
                },
                linecap: 'round',
                stickyTracking: false
            }
        },

        series: series
    });




}

Array.prototype.contains = function (v) {
    for (var i = 0; i < this.length; i++) {
        if (this[i] === v) return true;
    }
    return false;
};
Array.prototype.unique = function () {
    var arr = [];
    for (var i = 0; i < this.length; i++) {
        if (!arr.contains(this[i])) {
            arr.push(this[i]);
        }
    }
    return arr;
}

function titleCase(str) {
    var newstr = str.split(" ");
    for (i = 0; i < newstr.length; i++) {
        var copy = newstr[i].substring(1).toLowerCase();
        newstr[i] = newstr[i][0].toUpperCase() + copy;
    }
    newstr = newstr.join(" ");
    return newstr;
}

function SetHighChartsSync() {
    /**
     * In order to synchronize tooltips and crosshairs, override the
     * built-in events with handlers defined on the parent element.
     */

    $('#histogramInterval').bind('mousemove touchmove touchstart', function (e) {
        var chart,
            point,
            i,
            event;

        for (i = 0; i < intervalRenderer.length; i = i + 1) {
            chart = $("#" + intervalRenderer[i]).highcharts();;
            event = chart.pointer.normalize(e.originalEvent); // Find coordinates within the chart
            point = chart.series[0].searchPoint(event, true); // Get the hovered point

            if (point) {
                point.highlight(e);
            }
        }
    });
    //$('#histogramDay').bind('mousemove touchmove touchstart', function (e) {
    //    var chart,
    //        point,
    //        i,
    //        event;

    //    for (i = 0; i < dayRenderer.length; i = i + 1) {
    //        chart = $("#" + dayRenderer[i]).highcharts();;
    //        event = chart.pointer.normalize(e.originalEvent); // Find coordinates within the chart
    //        point = chart.series[0].searchPoint(event, true); // Get the hovered point

    //        if (point) {
    //            point.highlight(e);
    //        }
    //    }
    //});
    /**
     * Override the reset function, we don't need to hide the tooltips and crosshairs.
     */

    Highcharts.Pointer.prototype.reset = function () {
        return undefined;
    };

    ///**
    // * Highlight a point by showing tooltip, setting hover state and draw crosshair
    // */
    Highcharts.Point.prototype.highlight = function (event) {
        this.onMouseOver(); // Show the hover marker
        this.series.chart.tooltip.refresh(this); // Show the tooltip
        this.series.chart.xAxis[0].drawCrosshair(event, this); // Show the crosshair
    };
}

function SetMouseOutEvent(chartid) {
    $("#" + chartid).bind('mouseleave mouseout ', function (e) {
        var chart,
            point,
            i,
            event;
        // alert("here")

        chart = $("#" + chartid).highcharts();;

        event = chart.pointer.normalize(e.originalEvent);
        point = chart.series[0].searchPoint(event, true);

        point.onMouseOut();
        chart.tooltip.hide(point);
        chart.xAxis[0].hideCrosshair();

    });
}
/**
 * Synchronize zooming through the setExtremes event handler.
 */
function syncExtremes(e) {
    var thisChart = this.chart;

    if (e.trigger !== 'syncExtremes') { // Prevent feedback loop
        Highcharts.each(Highcharts.charts, function (chart) {
            if (chart !== thisChart) {
                if (chart.xAxis[0].setExtremes) { // It is null while updating
                    chart.xAxis[0].setExtremes(e.min, e.max, undefined, false, {
                        trigger: 'syncExtremes'
                    });
                }
            }
        });
    }
}

function SetDatePickers() {
    $('#startdate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        //  format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90))),
        endDate: new Date()
        // update "enddate" defaults whenever "startdate" changes
    }).on('changeDate', function () {
       
        onStartChange(this);
    });

    $('#enddate').datepicker({
        // "setDate": new Date(),
        autoclose: true,
        // format: "yyyy-mm-dd",
        startDate: new Date(new Date(new Date().setDate(new Date().getDate() - 90)))
        // update "startdate" defaults whenever "enddate" changes
    }).on('changeDate', function () {
        onEndChange(this)
      
    });
}
function onStartChange(e) {
   
    // set the "enddate" start to not be later than "startdate" ends:
    $('#enddate').datepicker('setStartDate', new Date($(e).val()));
    var enddate = new Date(new Date($(e).val()).setDate(new Date($(e).val()).getDate() + 30));
    if (enddate > new Date())
        $('#enddate').datepicker('setEndDate', new Date());
    else
        $('#enddate').datepicker('setEndDate', enddate);
    var startdate = new Date($('#startdate').val());
    var enddate = new Date($('#enddate').val());

    if (enddate.getTime() < startdate.getTime()) {
        $('#enddate').val($(e).val());
    }
}
function onEndChange(e) {
    // set the "startdate" end to not be later than "enddate" starts:
    $('#startdate').datepicker('setEndDate', new Date($(e).val()));
    $('#startdate').datepicker('setStartDate', new Date(new Date($(e).val()).setDate(new Date($(e).val()).getDate() - 30)));
    var startdate = new Date($('#startdate').val());
    var enddate = new Date($('#enddate').val());

    if (enddate.getTime() < startdate.getTime()) {
        $('#startdate').val($(e).val());
    }
}
function GetFormattedDate(d)
{
   console.log(d)

    var curr_date = d.getDate();
    var curr_month = d.getMonth()+1;
    var curr_year = d.getFullYear();
    if (curr_month < 10)
        curr_month = "0" + curr_month;
    if (curr_date < 10)
        curr_date = "0" + curr_date;
    return curr_month+"/"+curr_date + "/" + curr_year;
}