﻿var intervalchart, table;
var sum = 0, cname;
var dataSet = [], total = [], intervalflag = false
var colors = ["#30bb74", "#2980b9", "#9a7fd1", "#F88F07", "#95a5a6", "#e74c3c", "#1E824C", "#a6dcf1"]
$(function () {


    $("#tfn-disp-spinner").show()
    $("#interval-spinner").show()
    $("#agstatus-spinner").show()
    $("#interval-graphcontainer").hide()
    $("#tfn-disp-piecontainer").hide()
    $("#agstatus-piecontainer").hide()
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();


    $.fn.dataTableExt.sErrMode = 'throw';
    try {
        table = $('#campaigntable').dataTable({

            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }],
            "bSortCellsTop": true,
            //scrollY: false,
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            "bProcessing": true,
            "order": [[0, "desc"]],
            dom: '<Blf<rt>ip>',
            buttons: [
                {
                    extend: 'excelHtml5',

                    title: 'Campaign Statistics' + formatDateforTable(new Date()),

                }

            ]

        });
        table.fnProcessingIndicator();
    }
    catch (e)
    { }

    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {

            notifications.server.getLeadsStats().done(function (Stats) {

                populateLeadsStats(Stats)
            });
            notifications.server.getOBDispositionStats().done(function (Stats) {
                // alert("getDispStats");
                populateDispositionStats(Stats)
            });
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });
            notifications.server.getAgentCountStats().done(function (Stats) {
                populateAgentCountStats(Stats)
            });

            notifications.server.getAgentStatusStats().done(function (Stats) {

                populateAgentStatusStats(Stats)
            });
            notifications.server.getIntervalStats().done(function (Stats) {
                //  alert("updateIntervalStats");
                var oTable = $('#campaigntable').DataTable();
                oTable.clear();
                $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try {
                    oTable.button(0).enable(nRows > 0);
                }
                catch (Ex)
                { }
            });;

        });
    }, 2000);


    notifications.client.updateLeadsStats = function (Stats) {
        // alert("updateQueueStats");
        populateLeadsStats(Stats)
    };
    notifications.client.updateOBDispositionStats = function (Stats) {
        //alert("updateDispStats");
        populateDispositionStats(Stats)
    };
    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateAgentCountStats = function (Stats) {
        // alert("updateAgentStateStats");
        populateAgentCountStats(Stats)
    };

    notifications.client.updateAgentStatusStats = function (Stats) {

        populateAgentStatusStats(Stats)
    };
    notifications.client.notifyThresholdUsers = function (Notifs) {
        showThresholdNotification(Notifs)
    };
    notifications.client.updateIntervalStats = function (Stats) {
        //  alert("updateIntervalStats");
        var oTable = $('#campaigntable').DataTable();
        oTable.clear();
        $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
    };
    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            //$("#lastUpdateTime").text("Disconnected");
            $("#dialog").dialog("open");
            //notifyUserOfDisconnect(); // Your function to notify user.
        }
    });
    //***************************************************************************************//
    intervalflag = false

})



function populateIntervalStats(result) {
    dataSet = []
    var oTable = $('#campaigntable').DataTable();
    oTable.clear();
    var list = result;
    var timezone = "";
    $("#interval-spinner").hide()
    $("#interval-graphcontainer").show()
    var time = [], ser1 = [], ser2 = [], ser3 = [], total = [], ser4 = [], ser5 = []
    document.getElementById("hi-icon-1").textContent = 0;
    document.getElementById("hi-icon-2").textContent = 0;
    document.getElementById("hi-icon-3").textContent = 0;
    document.getElementById("hi-icon-4").textContent = 0;
    try {
        document.getElementById("hi-icon-aban").textContent = 0;
    }
    catch (ex) {
    }
    //  document.getElementById("hi-icon-occ").textContent = "0%";
    try {
        $.each(list, function (index, value) {
            if (value.CampaignID.toLowerCase() == cname) {
                timezone = value.TimeZone;
                if (value["A"].toLowerCase() != "total") {
                    time.push(value["A"])

                    ser1.push(parseFloat(value["C"]))
                    ser2.push(parseFloat(value["E"]))
                    ser3.push(parseFloat(value["G"]))
                    ser4.push(parseFloat(value["I"]))

                }
                else {

                    document.getElementById("hi-icon-1").textContent = value["C"]
                    document.getElementById("hi-icon-2").textContent = value["E"]
                    document.getElementById("hi-icon-3").textContent = value["G"]
                    document.getElementById("hi-icon-4").textContent = value["I"]

                }

                var cols = table.fnSettings().aoColumns.length
                var arr = [];
                if (value["A"].toLowerCase() != "total") {

                    var colascii = 65;
                    for (var i = 0; i < cols ; i++) {

                        arr.push(value[String.fromCharCode(colascii)])
                        colascii++;
                    }


                    dataSet.push(arr)
                }
                else {
                    var colascii = 65;
                    for (var i = 0; i < cols ; i++) {

                        total.push(value[String.fromCharCode(colascii)])
                        colascii++;
                    }


                }
            }
        });

        if (intervalflag == false) {
            var s1 = titleCase($(oTable.column(2).header()).html())
            var s2 = titleCase($(oTable.column(4).header()).html())
            var s3 = titleCase($(oTable.column(6).header()).html())
            var s4 = titleCase($(oTable.column(8).header()).html())
            document.getElementById("grid1").textContent = s1
            document.getElementById("grid2").textContent = s2
            document.getElementById("grid3").textContent = s3
            document.getElementById("grid4").textContent = s4
            DrawIntervalChart(s1, s2, s3, s4)
            intervalflag = true;
        }

        intervalchart.xAxis[0].setCategories(time);
        intervalchart.series[0].setData(ser1);
        intervalchart.series[1].setData(ser2);
        intervalchart.series[2].setData(ser3);
        intervalchart.series[3].setData(ser4);
        var xTitle = ""
        if (timezone == "")
            xTitle = "Interval (EST)"
        else
            xTitle = "Interval (" + timezone + ")"

        intervalchart.xAxis[0].update({
            title: {
                text: xTitle
            }
        });

        $('#frow th').each(function (i) {

            if ($(this).text().toLowerCase() != "total")
                $(this).text("");

        });
        if (total.length != 0) {
            $('#frow th').each(function (i) {
                if (total[i] != undefined && $(this).text().toLowerCase() != "total")
                    $(this).text(total[i]);

            });
        }
        if (!intervalchart.hasData()) {
            intervalchart.hideNoData();
            intervalchart.showNoData("No data available!");
        }
        $("#lastUpdateTime").text(formatDate(new Date()));
    } catch (ex) {
        if (intervalflag == false) {
            DrawIntervalChart("", "", "", "", "");
        }
        if (!intervalchart.hasData()) {

            intervalchart.hideNoData();
            intervalchart.showNoData("No data available!");
        }
    }
    return dataSet;

}
function DrawIntervalChart(ser1, ser2, ser3, ser4) {

    intervalchart = new Highcharts.Chart({
        chart: {
            type: 'spline',
            renderTo: 'interval-graphcontainer',
            zoomType: 'x'
        },


        title: {
            text: 'Calls Per Interval',
            align: 'left'

        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of calls'
            },
            allowDecimals: false,
            minTickInterval: 1
        },
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>Interval ' + this.x + '</caption><tbody>';
                //loop each point in this.points
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                //points += '<tr><th>Calls Offered: </th>'
                //+ '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                + '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: ser1,
            data: [],

        },
       {
           name: ser2,
           data: [],

       },
       {
           name: ser3,
           data: [],

       },
       {
           name: ser4,
           data: [],

       }

        ]
    });


}



