﻿var intervalchart, table;
var sum = 0, intervalflag = false, cname;
var dataSet = [], total = []
var colors = ["#30bb74", "#95a5a6", "#9a7fd1", "#F88F07", "#2980b9", '#805b69', "#1E824C", '#2d3ab7', '#b7aa2d', '#3a6166', '#14a5ab', '#35817b', '#9179de', '#c0fff4', '#995460', '#528881', '#584778', '#4f62d8', '#7e708b', '#9a666e', '#2e2126', '#4a152a', '#6b1f3c', '#abffe3', '#ffd9df', '#f3c3c3', '#ffb5be', '#00028a', '#00027a', '#00025a', '#a93b1c', '#a43e32', '#a42f1e', '#953904', '#6c2801', '#fcfac9', '#fc4078', '#fc2b6a', '#b00070', '#c00090']


$(function () {

    $("#tfn-disp-spinner").show()
    $("#interval-spinner").show()
    $("#agstatus-spinner").show()
    $("#interval-graphcontainer").hide()
    $("#tfn-disp-piecontainer").hide()
    $("#agstatus-piecontainer").hide()
    var selection = $("#cbCampaigns").val();
    cname = selection.substring(selection.indexOf("-") + 1).toLowerCase();
 
 
    $.fn.dataTableExt.sErrMode = 'throw';
    try {
        table = $('#campaigntable').dataTable({

            "aoColumnDefs": [{
                "bSortable": false,
                "aTargets": ["no-sort"]
            }],
            "bSortCellsTop": true,
            //scrollY: false,
            scrollX: true,
            scrollCollapse: true,
            paging: true,
            fixedColumns: true,
            "bProcessing": true,
            "order": [[0, "desc"]],
            dom: '<Blf<rt>ip>',
            buttons: [
                {
                    extend: 'excelHtml5',

                    title: 'Campaign Statistics' + formatDateforTable(new Date()),

                }

            ]

        });
        table.fnProcessingIndicator();
    }
    catch (e)
    { }
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {

            notifications.server.getQueueStats().done(function (Stats) {

                populateQueueStats(Stats)
            });
            notifications.server.getTFNQueueStats().done(function (Stats) {

                populateTFNQStats(Stats)
            });
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });
            notifications.server.getAgentCountStats().done(function (Stats) {
                populateAgentCountStats(Stats)
            });

            notifications.server.getAgentStatusStats().done(function (Stats) {

                populateAgentStatusStats(Stats)
            });
            notifications.server.getIntervalStats().done(function (Stats) {
                //  alert("updateIntervalStats");
                var oTable = $('#campaigntable').DataTable();
                oTable.clear();
                $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try {
                    oTable.button(0).enable(nRows > 0);
                }
                catch (Ex)
                { }
            });;
        });
    }, 2000);
    var tryingToReconnect = false;


    notifications.client.updateQueueStats = function (Stats) {
        // alert("updateQueueStats");
        populateQueueStats(Stats)
    };
    notifications.client.updateTFNQueueStats = function (Stats) {
        //alert("updateTFNQStats");
        populateTFNQStats(Stats)
    };

    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateAgentCountStats = function (Stats) {
        // alert("updateAgentStateStats");
        populateAgentCountStats(Stats)
    };
    notifications.client.notifyThresholdUsers = function (Notifs) {
        showThresholdNotification(Notifs)
    };
    notifications.client.updateAgentStatusStats = function (Stats) {

        populateAgentStatusStats(Stats)
    };
    notifications.client.updateIntervalStats = function (Stats) {
        //  alert("updateIntervalStats");
        var oTable = $('#campaigntable').DataTable();
        oTable.clear();
        $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
    };
    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            //$("#lastUpdateTime").text("Disconnected");
            $("#dialog").dialog("open");
            //notifyUserOfDisconnect(); // Your function to notify user.
        }
    });
    //***************************************************************************************//
    intervalflag = false

})




function populateIntervalStats(result) {
    dataSet = []
    var oTable = $('#campaigntable').DataTable();
    oTable.clear();
    var timezone = "";
    var list = result;
    $("#interval-spinner").hide()
    $("#interval-graphcontainer").show()
    var time = [], ser1 = [], ser2 = [], ser3 = [], total = []
    document.getElementById("hi-icon-off").textContent = 0;
    document.getElementById("hi-icon-ans").textContent = 0;
    document.getElementById("hi-icon-aban").textContent = 0;
    document.getElementById("hi-icon-occ").textContent = "0%";
    try {
        var head = oTable.column(7).header();
        var title = $(head).html()
        if (title.substring(0, 3).toUpperCase() == "CON")
            title = "Conversion"
        else if (title.substring(0, 3).toUpperCase() == "OCC")
            title = "Occupancy"
        document.getElementById("hi-icon-occ-title").textContent = title

        $.each(list, function (index, value) {
            if (value.CampaignID.toLowerCase() == cname) {
                timezone = value.TimeZone;
                if (value["A"].toLowerCase() != "total") {
                    time.push(value["A"])//Interval
                    ser3.push(parseFloat(value["B"]))//Offered
                    ser2.push(parseFloat(value["D"]))//Abandoned 
                    ser1.push(parseFloat(value["C"]))//Answered

                }
                else {
                    document.getElementById("hi-icon-off").textContent = value["B"]
                    document.getElementById("hi-icon-ans").textContent = value["C"]
                    document.getElementById("hi-icon-aban").textContent = value["D"]
                    document.getElementById("hi-icon-occ").textContent = value["H"] + "%";
                }

                var cols = table.fnSettings().aoColumns.length
                var arr = [];
                if (value["A"].toLowerCase() != "total") {

                    var colascii = 65;
                    for (var i = 0; i < cols ; i++) {

                        arr.push(value[String.fromCharCode(colascii)])
                        colascii++;
                    }


                    dataSet.push(arr)
                }
                else {
                    var colascii = 65;
                    for (var i = 0; i < cols ; i++) {

                        total.push(value[String.fromCharCode(colascii)])
                        colascii++;
                    }


                }
            }
        });

        if (intervalflag == false) {
            DrawIntervalChart()
            intervalflag = true;
        }

        intervalchart.xAxis[0].setCategories(time);
        intervalchart.series[1].setData(ser1);
        intervalchart.series[0].setData(ser2);
        var xTitle = ""
        if (timezone == "")
            xTitle = "Interval (EST)"
        else
            xTitle = "Interval (" + timezone + ")"

        intervalchart.xAxis[0].update({
            title: {
                text: xTitle
            }
        });
        $('#frow th').each(function (i) {

            if ($(this).text().toLowerCase() != "total")
                $(this).text("");

        });
        if (total.length != 0) {
            $('#frow th').each(function (i) {
                if (total[i] != undefined && $(this).text().toLowerCase() != "total")
                    $(this).text(total[i]);

            });
        }
        if (!intervalchart.hasData()) {
            intervalchart.hideNoData();
            intervalchart.showNoData("No data available!");
        }
        $("#lastUpdateTime").text(formatDate(new Date()));
    }
    catch (ex) {
        if (intervalflag == false) {
            DrawIntervalChart("", "", "", "", "");
        }
        if (!intervalchart.hasData()) {

            intervalchart.hideNoData();
            intervalchart.showNoData("No data available!");
        }
    }
    return dataSet;

}


function DrawIntervalChart() {

    intervalchart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'interval-graphcontainer'
        },


        title: {
            text: 'Calls Per Interval',
            align: 'left'

        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of calls'
            },
            allowDecimals: false,
            minTickInterval: 1
        },
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>Interval ' + this.x + '</caption><tbody>';
                //loop each point in this.points
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                points += '<tr><th>Calls Offered: </th>'
                + '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                + '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Calls Abandoned',
            data: [],
            color: "#a6dcf1"

        },
       {
           name: 'Calls Handled',
           data: [],
           color: "#52b3d9"

       }
        //, {
        //    name: 'Calls Offered',
        //    data: [],
        //    color: 'rgba(126,86,134,.9)'


        //}

        ]
    });


}


