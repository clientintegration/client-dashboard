﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using Microsoft.AspNet.Identity;
using System.Data.Entity;
namespace ClientDashboard.Hubs
{
    [HubName("statisticsHub")]
    public class StatisticsHub : Hub
    {
        private ClientDashboardEntities db = new ClientDashboardEntities();
       
        public void SendAgentCountStats(List<AgentCountReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentCountStats(Stats);
        }
        public void SendAgentStatusStats(List<AgentStatusReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentStatusStats(Stats);
        }
        public void SendQueueStats(List<RoutingQueueStat> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateQueueStats(Stats);
        }
        public void SendLeadsStats(List<LeadsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateLeadsStats(Stats);
        }
        public void SendTFNQueueStats(List<TfnQueueDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateTFNQueueStats(Stats);
        }
        public void SendOBDispositionStats(List<OBDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateOBDispositionStats(Stats);
        }
        public void SendIntervalStats(List<CampaignsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateIntervalStats(Stats);
        }
        public void SendAgentsStats(List<AgentsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentsStats(Stats);
        }
        public void SendAvayaSkillStats(List<AvayaSkillStat> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAvayaSkillStats(Stats);
        }
        public void SendAvayaAgentStats(List<AvayaAgentStat> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAvayaAgentStats(Stats);
        }
        public void SendAvayaAgentStates(List<AvayaAgentState> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAvayaAgentStates(Stats);
        }
        public void SendDialingStatus(List<HomeReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateDialingStatus(Stats);
        }
        public void SendActiveUsers(List<LiveUsers> LiveUsers)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateActiveUsers(LiveUsers);
        }
        public List<HomeReport> GetDialingStatus()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<HomeReport> MenuInfo = _StatisticsRepository.GetDialingStatus(true).ToList();
            return MenuInfo;
        }
        public List<AgentCountReport> GetAgentCountStats()
        {

            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentCountReport> AgentCount = _StatisticsRepository.GetAgentCountStats(true).ToList();
            return AgentCount;
        }
        public List<AgentStatusReport> GetAgentStatusStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentStatusReport> AgentStatus = _StatisticsRepository.GetAgentStatusStats(true).ToList();
            return AgentStatus;
        }
        public List<RoutingQueueStat> GetQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<RoutingQueueStat> Queue = _StatisticsRepository.GetRoutingQueueStats(true).ToList();
            return Queue;
        }
        public List<TfnQueueDispositionReport> GetTFNQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<TfnQueueDispositionReport> TFNQueue = _StatisticsRepository.GetTFNQueueStats(true).ToList();
            return TFNQueue;
        }
        public List<CampaignsReport> GetIntervalStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<CampaignsReport> IntervalStats = _StatisticsRepository.GetIntervalStats(true).ToList();
            return IntervalStats;
        }
        public List<AgentsReport> GetAgentsStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentsReport> AgentStats = _StatisticsRepository.GetAgentsStats(true).ToList();
            return AgentStats;
        }
        public List<LeadsReport> GetLeadsStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<LeadsReport> LeadsStats = _StatisticsRepository.GetLeadsStats(true).ToList();
            return LeadsStats;
        }
        public List<OBDispositionReport> GetOBDispositionStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<OBDispositionReport> OBDispStats = _StatisticsRepository.GetOBDispositionStats(true).ToList();
            return OBDispStats;
        }
        public List<AvayaSkillStat> GetAvayaSkillStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AvayaSkillStat> SkillStats = _StatisticsRepository.GetAvayaSkillStats(true).ToList();
            return SkillStats;
        }
        public List<AvayaAgentStat> GetAvayaAgentStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AvayaAgentStat> AgentStats = _StatisticsRepository.GetAvayaAgentStats(true).ToList();
            return AgentStats;
        }
        public List<AvayaAgentState> GetAvayaAgentStates()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AvayaAgentState> AgentStates = _StatisticsRepository.GetAvayaAgentStates(true).ToList();
            return AgentStates;
        }
        public List<LiveUsers> GetActiveUsers()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<LiveUsers> LiveUsers = _StatisticsRepository.GetActiveUsers(true);
            return LiveUsers;
        }
     
        public void NotifyThresholdUsers(List<ThresholdModel> Threshold)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
          
            foreach (var item in Threshold)
            {
                List<ActiveConnection> activeUsers = db.ActiveConnections.Where(a => a.UserId == item.UserID).ToList();
                Email obj = new Email();
                obj.SendThresholdEmail(item);
                UserThreshold userthreshold = db.UserThresholds.Find(item.UserID, item.MetricID, item.CampaignID);
                userthreshold.IsSent = true;
                userthreshold.LastSentOn = DateTime.Now;
                db.Entry(userthreshold).State = EntityState.Modified;
                db.SaveChanges();
                if (activeUsers == null)
                {
                    //Clients.Caller.showErrorMessage("The user is no longer connected.");
                }
                else
                {
                    foreach (var connection in activeUsers)
                    {

                        context.Clients.Client(connection.ConnectionID).notifyThresholdUsers(Threshold);
                    }
                }


            }
        }
        public override Task OnConnected()
        {
            try
            {
                var connID = Context.ConnectionId;
                string userId = HttpContext.Current.User.Identity.GetUserId();
                ActiveConnection activeUsers = new ActiveConnection();

                DateTime dt = GetEasternTime();
                activeUsers.ConnectionID = connID;
                activeUsers.UserId = userId;
                activeUsers.UpdatedOn = dt;

                db.ActiveConnections.Add(activeUsers);
                db.SaveChanges();

                var num = db.ActiveConnections.Where(a => a.UserId == userId).Count();
                if (num == 1)
                {
                    AspNetUser users = db.AspNetUsers.Find(userId);
                    users.LastLoginDate = dt;
                    db.Entry(users).State = EntityState.Modified;
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }
            return base.OnConnected();
        }
        public override Task OnReconnected()
        {
            try
            {
                var connID = Context.ConnectionId;
                string userId = HttpContext.Current.User.Identity.GetUserId();
                ActiveConnection activeUsers = db.ActiveConnections.Find(userId, connID);
                if (activeUsers == null)
                {
                    activeUsers.ConnectionID = connID;
                    activeUsers.UserId = userId;
                    activeUsers.UpdatedOn = GetEasternTime();

                    db.ActiveConnections.Add(activeUsers);
                    db.SaveChanges();
                }
            }

            catch (Exception ex)
            {
            }
            return base.OnReconnected();
        }
        public override Task OnDisconnected(bool stopCalled)
        {
            try
            {
                var connID = Context.ConnectionId;

                string userId = HttpContext.Current.User.Identity.GetUserId();
                var num = db.ActiveConnections.Where(a => a.UserId == userId).Count();
                if (num == 1)
                {
                    AspNetUser users = db.AspNetUsers.Find(userId);
                    users.LastLogoutDate = GetEasternTime();
                    db.Entry(users).State = EntityState.Modified;
                    db.SaveChanges();
                }
                ActiveConnection activeUsers = db.ActiveConnections.Find(userId, connID);
                if (activeUsers != null)
                {
                    db.ActiveConnections.Remove(activeUsers);
                    db.SaveChanges();
                }
            }
            catch (Exception ex)
            {
            }

            return base.OnDisconnected(stopCalled);
        }
        public DateTime GetEasternTime()
        {
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            return easternTime;
        }



       
    }
}