-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE CDB.[AgentStatesReport] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime,
	@CampaignID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF 
	
-----------------------------------------------------------------------------------
Declare @TestAgents as varchar(500)

select * into #Variable
from [Hamreports01].webdialer.dte.reportvariables with (nolock)
where VariableName like '%agent%'
if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin 
Select @TestAgents = VariableValue from #Variable where VariableName = 'TestAgents'
End
Else
Begin
Select @TestAgents = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'TestAgents'
End	

	BEGIN
		CREATE TABLE #testAgents
		(
			AgentID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@TestAgents))
		BEGIN
		  SELECT @t = SUBSTRING(@TestAgents,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #testAgents SELECT REPLACE(SUBSTRING(@TestAgents, @I, CHARINDEX(',', @TestAgents + ',', @I) - @I),'''','')
			  where substring(','+@TestAgents,@I,1)=',' AND @I < LEN(@TestAgents) + 1
		  END
		  SET @I = @I + 1
		END
	END
-----------------------------------------------------------------------------------
	select * into #AgentCurrentState 
	from [Hamreports01].Webdialer.dte.AgentCurrentState with (nolock)
		where campaignid = @CampaignID
		and agentid not in (select agentid from #testAgents)
		and CONVERT(varchar(10),AgentTimeStamp,120) between CONVERT(varchar(10),@StartDate,120) 
														and CONVERT(varchar(10),@EndDate ,120)
	union all
	select *
	from [Hamreports01].Webdialer.Campaign.AgentCurrentState with (nolock)
		where campaignid = @CampaignID
		and agentid not in (select agentid from #testAgents)
		and CONVERT(varchar(10),AgentTimeStamp,120) between CONVERT(varchar(10),@StartDate,120) 
														and CONVERT(varchar(10),@EndDate ,120)

	select * into #States 
	from #AgentCurrentState
		where AgentTimeStamp in (
			select max(AgentTimeStamp)
			from #AgentCurrentState (nolock)
			group by agentid
		)

	Select isnull(SUM(case when agentstate = 'Talk' then 1 else 0 end ),0) as Talk,
			isnull(SUM(case when agentstate = 'Wrap' then 1 else 0 end ),0) as Wrap,
			isnull(SUM(case when agentstate = 'Wait' then 1 else 0 end ),0) as Wait,
			isnull(SUM(case when agentstate = 'Login' or agentstate like 'Brea%' then 1 else 0 end ),0) as [Break]
	from #States
	

drop table #AgentCurrentState
drop table #States
drop table #Variable
drop table #testagents

END