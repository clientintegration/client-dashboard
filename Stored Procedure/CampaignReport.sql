-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE CDB.[CampaignReport] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime,
	@CampaignID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF 

/* setting date variable into yyyy:mm:dd format without time*/
------------------------------------------------------
set @StartDate = CONVERT(varchar(10),@StartDate,120)
set @EndDate = CONVERT(varchar(10),@EndDate,120)
-------------------------------------------------------------------------------------------------
Declare @Billablebreak as varchar(500)
Declare @BlendedCampaigns as varchar(500)
Declare @IBCampaign as varchar(500)
Declare @Interval as varchar(500)
Declare @IntervalEnd as varchar(500)
Declare @IntervalStart as varchar(500)
Declare @OBCampaign as varchar(500)
Declare @ProductionTime as varchar(500)
Declare @TestAgents as varchar(500)
Declare @Testnumber as varchar(500)
-------------------------------------------------------------------------------------------------

/*Variables checks that are saved in database to provide ease on run time changes in */
select * into #Variable
from [Hamreports01].webdialer.dte.reportvariables with (nolock)

if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin 
Select @Billablebreak = VariableValue from #Variable where VariableName = 'Billablebreak'
Select @BlendedCampaigns = VariableValue from #Variable where VariableName = 'BlendedCampaigns'
Select @IBCampaign = VariableValue from #Variable where VariableName = 'IBCampaign'
Select @OBCampaign = VariableValue from #Variable where VariableName = 'OBCampaign'
Select @TestAgents = VariableValue from #Variable where VariableName = 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = 'Testnumber'
Select @ProductionTime = VariableValue from #Variable where VariableName = 'ProductionTime'
Select @Interval = VariableValue from #Variable where VariableName = 'Interval'
Select @IntervalEnd = VariableValue from #Variable where VariableName = 'IntervalEnd'
Select @IntervalStart = VariableValue from #Variable where VariableName = 'IntervalStart'
End
Else
Begin
Select @Billablebreak = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Billablebreak'
Select @BlendedCampaigns = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'BlendedCampaigns'
Select @IBCampaign = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'IBCampaign'
Select @OBCampaign = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'OBCampaign'
Select @TestAgents = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Testnumber'
Select @ProductionTime = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'ProductionTime'
Select @Interval = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Interval'
Select @IntervalEnd = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'IntervalEnd'
Select @IntervalStart = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'IntervalStart'
End



	BEGIN
		CREATE TABLE #testAgents
		(
			AgentID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@TestAgents))
		BEGIN
		  SELECT @t = SUBSTRING(@TestAgents,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #testAgents SELECT REPLACE(SUBSTRING(@TestAgents, @I, CHARINDEX(',', @TestAgents + ',', @I) - @I),'''','')
			  where substring(','+@TestAgents,@I,1)=',' AND @I < LEN(@TestAgents) + 1
		  END
		  SET @I = @I + 1
		END
	END

	BEGIN
		CREATE TABLE #testnumbers
		(
			Number VARCHAR(50)
		)
		DECLARE @t1 VARCHAR(MAX)
		DECLARE @I1 INT
		SELECT @I1 = 0
		WHILE(@I1 <=LEN(@Testnumber))
		BEGIN
		  SELECT @t1 = SUBSTRING(@Testnumber,@I1,1)
		  if(@t1<>','and @t1<>'')
		  BEGIN
			  Insert into #testnumbers SELECT REPLACE(SUBSTRING(@Testnumber, @I1, CHARINDEX(',', @Testnumber + ',', @I1) - @I1),'''','')
			  where substring(','+@Testnumber,@I1,1)=',' AND @I1 < LEN(@Testnumber) + 1
		  END
		  SET @I1 = @I1 + 1
		END
	END
	
		
-------------------------------------
/*selecting calltracks data to temp table to not hit real table each time */
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,case when Distime < Contime then Distime+86400 else Distime end - Contime 'talktime'   
	,0 'holdtime'   /*hold time is zero in webdialer because it is the part of talk time*/
	,case when Endtime < Distime then Endtime+86400 else Endtime end - DisTime 'wraptime'
	,case when ConTime >= WaitStart then ConTime-WaitStart else 86400-WaitStart+ConTime end 'waittime' 
	,contime - starttime 'queuetime'
	,WaitStart
	,0 'ringtime'
	,'1899-12-30 ' +  CONVERT(VARCHAR, DATEADD(s, contime, 0), 114) 'calltime'
	,termcd
	,contime
	,Endtime
	,uniqueid
	,campaign
into #rpt
from [Hamreports01].[webdialer].webdialer.calltracks with (nolock)
where calldate between @StartDate and @EndDate
       and number not in (select number from #testnumbers)
       and agentid not in (select agentid from #testAgents)
       AND STARTTIME >= @ProductionTime
       and campaign = @CampaignID and campaign <> 'FBT'
union all
/*View for FITBIT only becuase of a different structure we have to use IVR and web dialer databases 
so a vew was created to provide both tables as call history*/
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,talktime   
	,0 holdtime   /*hold time is zero in webdialer because it is the part of talk time*/
	,wraptime
	,waittime
	,contime - starttime 'queuetime'
	,WaitStart
	,0 ringtime
	,'1899-12-30 ' +  CONVERT(VARCHAR, DATEADD(s, contime, 0), 114) 'calltime'
	,termcd
	,contime
	,Endtime
	,uniqueid
	,campaign
from [Hamreports01].[webdialer].dbo.rpt_fitbit with (nolock)
where calldate between @StartDate and @EndDate
       and number not in (select number from #testnumbers)
       and agentid not in (select agentid from #testAgents)
       AND STARTTIME >= @ProductionTime
       and campaign = @CampaignID 

-----------------------------------------------------------------------------------

-----------------------------------------------------------------------------------
 /*Login data are being extracted from blended procedure it will entertain both simple and blended campaign*/
create table #Login
(
AgentID varchar(max) default '' not null,
LoginDate datetime default '1900-01-01',
CampaignID varchar(10) default '',
LoginStart int default 0,
LoginEnd int default 0,
DialerID int default 0,
LoginType varchar(50) default ''
)

/* Login is being calculated from stored procedure for blended calculation*/
insert into #Login
EXECUTE CDB.[BlendedHours] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak
  
--------------------------------------------------------------  
  /*Break data are being extracted from blended procedure it will entertain both simple and blended campaign*/
 create table #Break
(
AgentID varchar(max) default '' not null,
Breakdate datetime default '1900-01-01',
CampaignID varchar(10) default '',
BreakStart int default 0,
BreakEnd int default 0,
DialerID int default 0,
BreakCode varchar(50) default '',
BreakType varchar(50) default ''
)

/* break is being calculated from stored procedure for blended calculation*/
insert into #Break
EXECUTE CDB.[Blendedbreaks] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak

-------------------------------------------------------------

create table #IntervalHours
(
LoginDate datetime default '1900-01-01',
AgentID varchar(max) default '' not null,
[Interval Start] int default 0,
[Interval End] int default 0,
OBHours decimal(20,2) default 0,
IBHours decimal(20,2) default 0
)

/* production is being calculated from stored procedure for blended calculation*/
insert into #IntervalHours
EXECUTE CDB.[BlendedIntervalHoursbycamp] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak
  ,@IntervalStart
  ,@IntervalEnd
  ,@Interval
  -----------------------------------------------
  
 create table #AllInterval 
(
intervals varchar(5) default '',
[Interval] int default 0,
Int_Login int default 0,
Int_Break int default 0,
[HandelTime] int default 0,
[Prod_Hours] decimal default 0
)

create table #Intervallogin 
(
[Date] datetime default '',
[Agentid] varchar(5) default '',
[Int_start] int default 0,
[Int_end] int default 0,
[LoginStart] int default 0,
[LoginEnd] int default 0
)

create table #Intervalbreak
(
[Date] datetime default '',
[Agentid] varchar(5) default '',
[Int_start] int default 0,
[Int_end] int default 0,
[BreakStart] int default 0,
[BreakEnd] int default 0
)

create table #IntervalHandled
(
[Date] datetime default '',
[Agentid] varchar(5) default '',
[Int_start] int default 0,
[Int_end] int default 0,
[Contime] int default 0,
[Endtime] int default 0,
[WaitStart] int default 0,
uniqueid varchar(50) default ''
)

/* login break and talk wait wrap is dividing into intervals*/
declare @starttime int, @endtime int
Select @starttime = @IntervalStart
Select @endtime = @starttime + @Interval
while (@starttime <= @IntervalEnd)
begin
insert into #Intervalbreak
select breakdate, agentid,@starttime,@endtime,
case when breakstart > @endtime then 0 
	when breakstart < @starttime then @starttime
	when breakstart between @starttime and @endtime then breakstart else 0 end,
case when breakstart > @endtime then 0 
when breakend > @endtime then @endtime 
when breakend between @starttime and @endtime then breakend else 0 end en
from #break
where breakdate between @STARTDATE and @ENDDATE
and campaignid <> 'mic' 

insert into #Intervallogin
select logindate, agentid,@starttime,@endtime,
case when loginstart > @endtime then 0 
	when loginstart < @starttime then @starttime
	when loginstart between @starttime and @endtime then loginstart else 0 end,
case when loginstart > @endtime then 0 
when loginend > @endtime then @endtime 
when loginend between @starttime and @endtime then loginend else 0 end en
from #login
where logindate between @STARTDATE and @ENDDATE
and campaignid <> 'mic' 

insert into #IntervalHandled
select calldate, agentid,@starttime,@endtime,
case when contime > @endtime then 0 
	when contime < @starttime then @starttime
	when contime between @starttime and @endtime then contime else 0 end,
case when contime > @endtime then 0 
when Endtime > @endtime then @endtime 
when Endtime between @starttime and @endtime then Endtime else 0 end en,
case when contime > @endtime then 0 
when Endtime > @endtime then @endtime 
when Endtime between @starttime and @endtime then WaitStart else 0 end wait,
uniqueid
from #rpt
where calldate between @STARTDATE and @ENDDATE
and campaign <> 'mic' and agentid <> '' and calltype = 2

Select @starttime = @starttime + @Interval
Select @endtime = @endtime + @Interval
end


delete from #Intervallogin
where loginstart = 0 or loginend = 0

delete from #Intervalbreak
where breakstart = 0 or breakend = 0

delete from  #IntervalHandled
where contime = 0 or endtime = 0


insert into #AllInterval
select isnull(convert(varchar(5),CONVERT(VARCHAR, DATEADD(s, [Int_start], 0), 114)),'Total'),
isnull([Int_start],-1), sum(loginend - loginstart), 0, 0,0
from #Intervallogin
group by int_start  with rollup
order by 1 desc

update #AllInterval
set Int_Break = t.[break_time]
from 
(select isnull(int_start,-1) [Int_start], sum(breakend- breakstart) [break_time]
from #Intervalbreak
group by int_start with rollup
)t
where [Interval] = t.[Int_start]

update #AllInterval
set [HandelTime] = t.[HandelTime]
from 
(select isnull(int_start,-1) [Int_start], sum(Endtime - contime) [HandelTime]
from #IntervalHandled
group by int_start with rollup
)t
where [Interval] = t.[Int_start]

IF @CampaignID = 'MIP' or @CampaignID = 'MIC'
Begin
update #AllInterval
set [Prod_hours] = t.[Hours]
from
(select isnull([Interval Start],-1) [Interval Start], case when @CampaignID = 'MIP' then sum(IBHours) else sum(OBHours) end[Hours]
from #IntervalHours
group by [Interval Start] with rollup
)t
where [Interval] = t.[Interval Start]
End

IF @CampaignID <> 'MIP' and @CampaignID <> 'MIC'
Begin
update #AllInterval
set [Prod_hours] = t.[TWWTime]
from
(select isnull(int_start,-1) [Int_start], isnull(sum(Endtime - WaitStart),0) [TWWTime]
from #IntervalHandled
group by int_start with rollup
)t
where [Interval] = t.[Int_start]
End

/* Calls related stats is being calculated*/
select	
isnull(Case
			when (((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)%2=1
			then	Case 
						when Len(Cast(((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)-1)/2as varchar))=1
						then '0' + Cast(((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)-1)/2as varchar)+':30'
						else Cast(((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)-1)/2as varchar)+':30'
					end
			else	Case 
						when Len(Cast((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)/2 as varchar))=1
						Then '0' + Cast((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)/2 as varchar)+':00'
						else Cast((((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800)/2 as varchar)+':00'
					end
		end, 'Total') intervals
		,isnull(sum(case when calltype in (2) then 1 else 0 end),0) callsoffered
		,0 [CallsForcasted]
		,cast(0.00  as decimal (8,2))[PctofForcast]
		,0 [staffForcasted]
		,isnull(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0) callshandled
		,isnull(sum(case when calltype = 2 and agentid = '' then 1 else 0 end),0) callsabandoned
		,case when sum(case when calltype in (2) then 1 else 0 end) = 0 then 0 else
				isnull((sum(case when calltype = 2 and agentid = '' then 1 else 0 end))*1.0
				/sum(case when calltype in (2) then 1 else 0 end),0) end [AbandonRate%]
		,case when isnull(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0) = 0 then 0 else
				isnull(sum(talktime + wraptime)
				/isnull(Nullif(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0),1),0) end AHT 
		,case when isnull(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0) = 0 then 0 else
				isnull((sum(case when (queuetime + ringtime) < 40  and calltype = 2 and agentid <> '' then 1 else 0 end))*1.0
				/isnull(Nullif(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0),1),0) end [Service Level Call%]
		,isnull(Max(queuetime + ringtime),0) [Max Wait Time]
		,case when isnull(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0) = 0 then 0 else
				isnull((sum(case when calltype = 2 and agentid <> '' then (queuetime + ringtime) else 0 end))
				/isnull(Nullif(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0),1),0) end [Average Speed To Answer]
		 
into #interval
from #rpt with (nolock)
Group by ((DATEPART(HOUR, calltime) * 3600) +(DATEPART(MINUTE, calltime) * 60) +(DATEPART(SECOND, calltime)))/1800 with rollup

select 'Interval' [Interval] , 'Call Offered' [Call Offered] , 'Calls Handled' [Calls Handled] ,
'Calls Abandon' [Calls Abandon], 'Abandon Rate' [Abandon Rate] , 'AHT' AHT, 
'Service Level Call %' [Service Level Call %], 'Max Wait Time' [Max Wait Time],'ASA' as [ASA],
'Occupancy' [Occupancy] , 'Login' [Login], 'Break' [Break],
case when @CampaignID = 'MIP' or @CampaignID = 'MIC' then 'Production Hours' else 'TWW' end [Production Hours]
union all
select ai.Intervals intervals, 
cast(isnull(callsoffered,0) as varchar) callsoffered,	
cast(isnull(callshandled,0) as varchar) callshandled, 
cast(isnull(callsabandoned,0) as varchar) callsabandoned,	
cast(cast(isnull([AbandonRate%],0.00) * 100.00 as decimal(20,2)) as varchar) + '%' [AbandonRate%],
cast(isnull(AHT,0) as varchar) AHT,
cast(cast(isnull([Service Level Call%],0.00) * 100.00 as decimal(20,2)) as varchar) + '%' [Service Level Call%],	
cast(isnull([Max Wait Time],0) as varchar) [Max Wait Time],	
cast(isnull([Average Speed To Answer],0) as varchar) [Average Speed To Answer],
cast(case when (ai.Int_Login- ai.Int_Break) =  0 then 0.00
else cast(isnull( ai.[HandelTime]*1.0/((ai.Int_Login- ai.Int_Break)),0.00) * 100.00 as decimal(20,2)) end as varchar) + '%' [Occupancy],
cast(cast((ai.Int_Login)/3600.00 as decimal(20,2)) as varchar) [Login],
cast(cast((ai.Int_Break)/3600.00 as decimal(20,2)) as varchar) [Break],
cast(cast((ai.Prod_Hours)/3600.00 as decimal(20,2)) as varchar) [Production Hours]
from #AllInterval ai (nolock)
left outer join #interval i (nolock)
on ai.intervals = i.intervals


drop table #AllInterval
DROP TABLE #Login
drop table #Break
drop table #Intervallogin
drop table #Intervalbreak
drop table #rpt
drop table #IntervalHandled
drop table #IntervalHours
drop table #Variable
drop table #testnumbers
drop table #testagents
drop table #interval

END