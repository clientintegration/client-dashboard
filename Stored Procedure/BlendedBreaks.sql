-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CDB.[BlendedBreaks] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime, 
	@BlendedCampaigns varchar(MAX),
	@DedicatedInboundCampaign varchar(MAX),
	@DedicatedOutboundCampaign varchar(MAX),
	@billablebreakcodes varchar(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
----------------------------------------------------------
----------------------------------------------------------
--- select all blended campaigns in temp table
----------------------------------------------------------
	BEGIN
		CREATE TABLE #campaigns
		(
			Camp_ID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@BlendedCampaigns))
		BEGIN
		  SELECT @t = SUBSTRING(@BlendedCampaigns,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #campaigns SELECT SUBSTRING(@BlendedCampaigns, @I, CHARINDEX(',', @BlendedCampaigns + ',', @I) - @I)
			  where substring(','+@BlendedCampaigns,@I,1)=',' AND @I < LEN(@BlendedCampaigns) + 1
		  END
		  SET @I = @I + 1
		END
	END

------------------------------------------------------
------------------------------------------------------
--- select all billable break codes in temp table ----
------------------------------------------------------
	BEGIN
		CREATE TABLE #Breakcodes
		(
			break_codes VARCHAR(max)
		)
		DECLARE @t1 VARCHAR(MAX)
		DECLARE @I1 INT
		SELECT @I1 = 0
		WHILE(@I1 <=LEN(@billablebreakcodes))
		BEGIN
		  SELECT @t1 = SUBSTRING(@billablebreakcodes,@I1,1)
		  if(@t1<>','and @t1<>'')
		  BEGIN
			  Insert into #Breakcodes SELECT SUBSTRING(@billablebreakcodes, @I1, CHARINDEX(',', @billablebreakcodes + ',', @I1) - @I1)
			  where substring(','+@billablebreakcodes,@I1,1)=',' AND @I1 < LEN(@billablebreakcodes) + 1
		  END
		  SET @I1 = @I1 + 1
		END
	END


-----------------------------------------------------------------

/*selecting login in break into temp table to perform further operation*/

SELECT * 
INTO #OBLOGIN
FROM [Hamreports01].[webdialer].webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedOutboundCampaign --and agentid not in ('zzx','zzy','zzz')

SELECT * 
INTO #IBLOGIN
FROM [Hamreports01].[webdialer].webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedInboundCampaign  --and agentid not in ('zzx','zzy','zzz')

SELECT * 
INTO #AllBreak
FROM [Hamreports01].[webdialer].webdialer.TSRBREAKS with (nolock)
WHERE BREAKDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID in (select * from #campaigns) --and agentid not in ('zzx','zzy','zzz')


------------------------------------------------------------------------
/*calculating dedicated phase login time, 
where "LoginType" column is to use differerntiate what is the login type
"Normal" is being insert for dedicated mode login time,
loginstart time is used to check the state
if login start for both campaign is same then it means login is in blended mode
else dedicated,
Note: we are not taking blended time from below query
because the login end time is always not same in blended mode
as sometime agent is being forcefully log out from any of the campaign*/
------------------------------------------------------------------------
SELECT OB.*,
    CASE WHEN IB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
INTO #ALLLogins
FROM
    #OBLOGIN OB (nolock) 
    LEFT OUTER JOIN #IBLOGIN IB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)
Union All   
SELECT IB.*,
    CASE WHEN OB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
FROM
    #IBLOGIN IB (nolock) 
    LEFT OUTER JOIN #OBLOGIN OB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)

/*blended hours calculation*/
-----------------------------
/*taking only those records where login start is same for both campaign
and for login end taking those campaign login end time where it is lesser*/
---------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate, 'BLD' CampaignID, OB.LoginStart,
	case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
		 when OB.LoginEnd < IB.LoginEnd then OB.LoginEnd 
		 when OB.LoginEnd = IB.LoginEnd then OB.LoginEnd  end LoginEnd,
	OB.DialerID ,
	'Blended' LoginType
into #login
	FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART
	AND OB.AGENTID = IB.AGENTID
union all /*union all the login time which was consider as dedicated because of force fully logout*/
----------------------------------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate,  
	   case when OB.LoginEnd > IB.LoginEnd then OB.campaignid 
	   else  IB.campaignid end CampaignID,  
	   case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
	   else  OB.LoginEnd end LoginStart, 
	   case when OB.LoginEnd > IB.LoginEnd then OB.LoginEnd 
	   else IB.LoginEnd end LoginEnd,
	  OB.DialerID,
	  'Blended-ForceLogout' LoginType
FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART AND OB.LOGINEND <> IB.LOGINEND
	AND OB.AGENTID = IB.AGENTID
UNION All 
/*taking only dedicated mode login time because blended login 
and forcefully dedicated is already taken in above queriedds*/
---------------------------------------------------------------
SELECT * FROM #ALLLogins
	WHERE LoginType = 'Normal'
order by 2,1

/*Login part has been completed*/
--------------------------------------------------------------



/*temp table for all break time*/
create table #Break
(
Agentid varchar(5) default '',
breakdate datetime default '',
CampaignID varchar(7) default '',
breakstart int default 0,
breakend int default 0,
DialerID int default 0,
breakcode varchar(20) default '',
BreakType varchar(20) default ''
)

/*adding row number to put unique identity to iterate loop*/
declare @g int, @k int
declare @agentid varchar(10)
declare @date smalldatetime
declare @ConTime int
declare @endtime int
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #blendedlogin
from #login where campaignid = 'BLD'
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #OBlogins
from #login where campaignid = @DedicatedOutboundCampaign
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #IBlogins
from #login where campaignid = @DedicatedInboundCampaign

/*handle and break time for blended mode 
loop is used becuase we have to check in break and calltracks for each login entry*/

SELECT @g = 1
SELECT @k = count(*) from #blendedlogin
while(@g <= @k) 
begin
		select @agentid = agentid  from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @date = logindate from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @ConTime = loginstart from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @endtime = loginend from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		
		insert into #break
		select agentid,breakdate,'BLD' + campaignid,breakstart,breakend,dialerid,breakcode, 'BlendedBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*hanlde and break time for outbound campaign*/
SELECT @g = 1
SELECT @k = count(*) from #OBlogins
--select @g,@k
while(@g <= @k) 
begin
		select @agentid = agentid  from #OBlogins where rownumber = @g
		select @date = logindate from #OBlogins where rownumber = @g
		select @ConTime = loginstart from #OBlogins where rownumber = @g
		select @endtime = loginend from #OBlogins where rownumber = @g
		
		insert into #break
		select agentid,breakdate,@DedicatedOutboundCampaign,breakstart,breakend,dialerid,breakcode, 'OBBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*hanlde and break time for dedicated inbound campaign*/
SELECT @g = 1
SELECT @k = count(*) from #IBlogins
--select @g,@k
while(@g <= @k) 
begin
		select @agentid = agentid  from #IBlogins where rownumber = @g
		select @date = logindate from #IBlogins where rownumber = @g
		select @ConTime = loginstart from #IBlogins where rownumber = @g
		select @endtime = loginend from #IBlogins where rownumber = @g
		
		insert into #break
		select agentid,breakdate,@DedicatedInboundCampaign,breakstart,breakend,dialerid,breakcode, 'IBBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*for blended break we have to take only that interval of break where agent is on break for both IB and OB
not taking "oncall" break code in this */
select ROW_NUMBER() OVER (ORDER BY breakdate,agentid,breakstart) AS RowNumber,*
into #Brk
from #break
where campaignid like 'BLD%'
and breakcode not like 'oncall'

delete from #break
where campaignid like 'BLD%'
/*calculating blended break only those break would going to be consider on blended mode
in which break of IB overlap with break of OB, 
other than that either agent was in talk, wait or wrap mode in any of the campaign,
*/
insert into #break
select a.agentid,a.breakdate,a.campaignid,
case when a.breakstart <= b.breakstart then b.breakstart else a.breakstart end breakstart,-- reason for taking break start greater in normal case because we have to take over lap mode in both break
case when a.breakend <= b.breakend then a.breakend else b.breakend end breakend,
a.dialerid, 
case when a.breakcode in (select break_codes from #Breakcodes) then a.breakcode else b.breakcode end breakcode,
a.breaktype
from #brk a
inner join #brk b
on a.rownumber = b.rownumber - 1
where a.breakdate = b.breakdate and a.agentid = b.agentid
and ((a.breakstart between b.breakstart and b.breakend) or (b.breakstart between a.breakstart and a.breakend))

select * from #Break


DROP TABLE #OBLOGIN
DROP TABLE #IBLOGIN
DROP TABLE #ALLLogins
DROP TABLE #Login
drop table #blendedlogin
DROP TABLE #OBLOGINs
DROP TABLE #IBLOGINs
drop table #Break
drop table #AllBreak
drop table #Brk
drop table #campaigns
drop table #Breakcodes

END