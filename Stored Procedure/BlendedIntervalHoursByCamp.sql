-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE leadmanagement
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CDB.[BlendedIntervalHoursByCamp] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime, 
	@BlendedCampaigns varchar(MAX),
	@DedicatedInboundCampaign varchar(MAX),
	@DedicatedOutboundCampaign varchar(MAX),
	@billablebreakcodes varchar(MAX),
	@Int_Start int,
	@Int_End int,
	@Interval int

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

----------------------------------------------------------
----------------------------------------------------------
--- select all blended campaigns in temp table
----------------------------------------------------------
	BEGIN
		CREATE TABLE #campaigns
		(
			Camp_ID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@BlendedCampaigns))
		BEGIN
		  SELECT @t = SUBSTRING(@BlendedCampaigns,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #campaigns SELECT SUBSTRING(@BlendedCampaigns, @I, CHARINDEX(',', @BlendedCampaigns + ',', @I) - @I)
			  where substring(','+@BlendedCampaigns,@I,1)=',' AND @I < LEN(@BlendedCampaigns) + 1
		  END
		  SET @I = @I + 1
		END
	END

------------------------------------------------------
------------------------------------------------------
--- select all billable break codes in temp table ----
------------------------------------------------------
	BEGIN
		CREATE TABLE #Breakcodes
		(
			break_codes VARCHAR(max)
		)
		DECLARE @t1 VARCHAR(MAX)
		DECLARE @I1 INT
		SELECT @I1 = 0
		WHILE(@I1 <=LEN(@billablebreakcodes))
		BEGIN
		  SELECT @t1 = SUBSTRING(@billablebreakcodes,@I1,1)
		  if(@t1<>','and @t1<>'')
		  BEGIN
			  Insert into #Breakcodes SELECT SUBSTRING(@billablebreakcodes, @I1, CHARINDEX(',', @billablebreakcodes + ',', @I1) - @I1)
			  where substring(','+@billablebreakcodes,@I1,1)=',' AND @I1 < LEN(@billablebreakcodes) + 1
		  END
		  SET @I1 = @I1 + 1
		END
	END
/*temp table for calltracks to report handle time matrics -- this is the same query that is used for rpttrak view*/
select
cast(ConTime/3600 as varchar) + ':' + cast((ConTime/60)%60 as varchar) + ':' + cast(ConTime%60 as varchar) 'calltime',
(endtime-starttime) 'CallDuration',
case when starttime < 21600 and endtime < 21600 then dateadd(d,-1,calldate) else calldate end 'rptdate',  
case when Distime < Contime then Distime+86400 else Distime end - Contime 'talktime',   
case when ConTime >= WaitStart then ConTime-WaitStart else 86400-WaitStart+ConTime end 'waittime' ,   
case when Endtime < Distime then Endtime+86400 else Endtime end - DisTime 'wraptime', 
contime - starttime 'queuetime',
answertime - starttime 'ringtime',
country,number,uniqueid,termcd,callid,calldate,starttime,holdtime,contime,distime,endtime,agentid,campaign,
calltype
into #calltracks
from [Hamreports01].webdialer.webdialer.calltracks with (nolock)
where campaign in (select * from #campaigns) 
and calldate between @Startdate and @enddate

-----------------------------------------------------------------

/*selecting login in break into temp table to perform further operation*/

SELECT * 
INTO #OBLOGIN
FROM [Hamreports01].webdialer.webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedOutboundCampaign-- and agentid not in ('zzx','zzy','zzz')

SELECT * 
INTO #IBLOGIN
FROM [Hamreports01].webdialer.webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedInboundCampaign--  and agentid not in ('zzx','zzy','zzz')

SELECT * 
INTO #AllBreak
FROM [Hamreports01].webdialer.webdialer.TSRBREAKS with (nolock)
WHERE BREAKDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID in (select * from #campaigns)-- and agentid not in ('zzx','zzy','zzz')


------------------------------------------------------------------------
/*calculating dedicated phase login time, 
where "LoginType" column is to use differerntiate what is the login type
"Normal" is being insert for dedicated mode login time,
loginstart time is used to check the state
if login start for both campaign is same then it means login is in blended mode
else dedicated,
Note: we are not taking blended time from below query
because the login end time is always not same in blended mode
as sometime agent is being forcefully log out from any of the campaign*/
------------------------------------------------------------------------
SELECT OB.*,
    CASE WHEN IB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
INTO #ALLLogins
FROM
    #OBLOGIN OB (nolock) 
    LEFT OUTER JOIN #IBLOGIN IB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)
Union All   
SELECT IB.*,
    CASE WHEN OB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
FROM
    #IBLOGIN IB (nolock) 
    LEFT OUTER JOIN #OBLOGIN OB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)

/*blended hours calculation*/
-----------------------------
/*taking only those records where login start is same for both campaign
and for login end taking those campaign login end time where it is lesser*/
---------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate, 'BLD' CampaignID, OB.LoginStart,
	case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
		 when OB.LoginEnd < IB.LoginEnd then OB.LoginEnd 
		 when OB.LoginEnd = IB.LoginEnd then OB.LoginEnd  end LoginEnd,
	OB.DialerID ,
	'Blended' LoginType
into #login
	FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART
	AND OB.AGENTID = IB.AGENTID
union all /*union all the login time which was consider as dedicated because of force fully logout*/
----------------------------------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate,  
	   case when OB.LoginEnd > IB.LoginEnd then OB.campaignid 
	   else  IB.campaignid end CampaignID,  
	   case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
	   else  OB.LoginEnd end LoginStart, 
	   case when OB.LoginEnd > IB.LoginEnd then OB.LoginEnd 
	   else IB.LoginEnd end LoginEnd,
	  OB.DialerID,
	  'Blended-ForceLogout' LoginType
FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART AND OB.LOGINEND <> IB.LOGINEND
	AND OB.AGENTID = IB.AGENTID
UNION All 
/*taking only dedicated mode login time because blended login 
and forcefully dedicated is already taken in above queriedds*/
---------------------------------------------------------------
SELECT * FROM #ALLLogins
	WHERE LoginType = 'Normal'
order by 2,1

/*Login part has been completed*/
--------------------------------------------------------------


/*temp table for tracing all records for handle time*/
Create Table #Handled
(
Uniqueid varchar(max) default '',
callid varchar(max) default '',
AgentID varchar(10) default '',
Calldate datetime default '',
talktime int default 0,
wraptime int default 0,
waittime int default 0,
ConTime int default 0,
EndTime int default 0,
campaignid varchar(3) default'',
calltype int default 0,
number varchar(20) default '',
HandelType varchar(50) default''
)

/*temp table for all break time*/
create table #Break
(
Agentid varchar(5) default '',
breakdate datetime default '',
CampaignID varchar(7) default '',
breakstart int default 0,
breakend int default 0,
DialerID int default 0,
breakcode varchar(20) default '',
BreakType varchar(20) default ''
)

/*adding row number to put unique identity to iterate loop*/
declare @g int, @k int
declare @agentid varchar(10)
declare @date smalldatetime
declare @ConTime int
declare @endtime int
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #blendedlogin
from #login where campaignid = 'BLD'
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #OBlogins
from #login where campaignid = @DedicatedOutboundCampaign
select ROW_NUMBER() OVER (ORDER BY logindate) AS RowNumber, *
into #IBlogins
from #login where campaignid = @DedicatedInboundCampaign

/*handle and break time for blended mode 
loop is used becuase we have to check in break and calltracks for each login entry*/

SELECT @g = 1
SELECT @k = count(*) from #blendedlogin
while(@g <= @k) 
begin
		select @agentid = agentid  from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @date = logindate from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @ConTime = loginstart from #blendedlogin where campaignid = 'BLD' and rownumber = @g
		select @endtime = loginend from #blendedlogin where campaignid = 'BLD' and rownumber = @g

		insert into #Handled
		select uniqueid ,callid, agentid, calldate, talktime, wraptime, waittime, ConTime, EndTime, campaign,calltype,number, 'BlendedHandled' from #calltracks
		where ConTime between @ConTime and @endtime
		and agentid = @agentid
		and calldate = @date
		
		insert into #break
		select agentid,breakdate,'BLD' + campaignid,breakstart,breakend,dialerid,breakcode, 'BlendedBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*hanlde and break time for outbound campaign*/
SELECT @g = 1
SELECT @k = count(*) from #OBlogins
--select @g,@k
while(@g <= @k) 
begin
		select @agentid = agentid  from #OBlogins where rownumber = @g
		select @date = logindate from #OBlogins where rownumber = @g
		select @ConTime = loginstart from #OBlogins where rownumber = @g
		select @endtime = loginend from #OBlogins where rownumber = @g

		insert into #Handled
		select uniqueid ,callid, agentid, calldate, talktime, wraptime, waittime, ConTime, EndTime, campaign,calltype,number, 'OBHandled' from #calltracks
		where ConTime between @ConTime and @endtime
		and agentid = @agentid
		and calldate = @date
		
		insert into #break
		select agentid,breakdate,@DedicatedOutboundCampaign,breakstart,breakend,dialerid,breakcode, 'OBBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*hanlde and break time for dedicated inbound campaign*/
SELECT @g = 1
SELECT @k = count(*) from #IBlogins
--select @g,@k
while(@g <= @k) 
begin
		select @agentid = agentid  from #IBlogins where rownumber = @g
		select @date = logindate from #IBlogins where rownumber = @g
		select @ConTime = loginstart from #IBlogins where rownumber = @g
		select @endtime = loginend from #IBlogins where rownumber = @g
		
		insert into #Handled
		select uniqueid ,callid, agentid, calldate, talktime, wraptime, waittime, ConTime, EndTime, campaign,calltype,number, 'IBHandled' from #calltracks
		where ConTime between @ConTime and @endtime
		and agentid = @agentid
		and calldate = @date
		
		insert into #break
		select agentid,breakdate,@DedicatedInboundCampaign,breakstart,breakend,dialerid,breakcode, 'IBBreak'
		from #AllBreak
		where breakstart between @ConTime and @endtime
		and agentid = @agentid
		and breakdate = @date
	Select @g = @g + 1 	
end 

/*for blended break we have to take only that interval of break where agent is on break for both IB and OB
not taking "oncall" break code in this */
select ROW_NUMBER() OVER (ORDER BY breakdate,agentid,breakstart) AS RowNumber,*
into #Brk
from #break
where campaignid like 'BLD%'
and breakcode not like 'oncall'

delete from #break
where campaignid like 'BLD%'
/*calculating blended break only those break would going to be consider on blended mode
in which break of IB overlap with break of OB, 
other than that either agent was in talk, wait or wrap mode in any of the campaign,
*/
insert into #break
select a.agentid,a.breakdate,a.campaignid,
case when a.breakstart <= b.breakstart then b.breakstart else a.breakstart end breakstart,-- reason for taking break start greater in normal case because we have to take over lap mode in both break
case when a.breakend <= b.breakend then a.breakend else b.breakend end breakend,
a.dialerid, 
case when a.breakcode in (select break_codes from #Breakcodes) then a.breakcode else b.breakcode end breakcode,
a.breaktype
from #brk a
inner join #brk b
on a.rownumber = b.rownumber - 1
where a.breakdate = b.breakdate and a.agentid = b.agentid
and ((a.breakstart between b.breakstart and b.breakend) or (b.breakstart between a.breakstart and a.breakend))

/*Now dividing break on interval basis*/

create table #Intervallogin
(
[Date] datetime default '',
[Agentid] varchar(5) default '',
[Int_start] int default 0,
[Int_end] int default 0,
Campaignid varchar(50) default '' not null,
[LoginStart] int default 0,
[LoginEnd] int default 0,
DialerID int default 0,
LoginType varchar(200) default ''
)


create table #Intervalbreak
(
[Date] datetime default '',
[Agentid] varchar(5) default '',
[Int_start] int default 0,
[Int_end] int default 0,
CampaignID varchar(7) default '',
breakstart int default 0,
breakend int default 0,
DialerID int default 0,
breakcode varchar(20) default '',
BreakType varchar(20) default ''
)

Create Table #IntervalHandled
(
Uniqueid varchar(max) default '',
callid varchar(max) default '',
AgentID varchar(10) default '',
Calldate datetime default '',
[Int_start] int default 0,
[Int_end] int default 0,
talktime int default 0,
wraptime int default 0,
waittime int default 0,
ConTime int default 0,
EndTime int default 0,
campaignid varchar(3) default'',
calltype int default 0,
number varchar(20) default '',
HandelType varchar(50) default''
)


declare @intervalStart int, @intervalEnd int
Select @intervalStart = @Int_Start
Select @intervalEnd = @Int_Start + @Interval

while (@intervalStart <= @Int_End )
begin

insert into #Intervallogin
select logindate, agentid,@intervalStart,@intervalEnd,Campaignid,
case when loginstart > @intervalEnd then 0 
	when loginstart < @intervalStart then @intervalStart
	when loginstart between @intervalStart and @intervalEnd then loginstart else 0 end,
case when loginstart > @intervalEnd then 0 
when loginend > @intervalEnd then @intervalEnd 
when loginend between @intervalStart and @intervalEnd then loginend else 0 end en,DialerID,LoginType
from #login
where logindate between @STARTDATE and @ENDDATE

insert into #Intervalbreak
select breakdate, agentid,@intervalStart,@intervalEnd,CampaignID,
case when breakstart > @intervalEnd then 0 
	when breakstart < @intervalStart then @intervalStart
	when breakstart between @intervalStart and @intervalEnd then breakstart else 0 end,
case when breakstart > @intervalEnd then 0 
when breakend > @intervalEnd then @intervalEnd 
when breakend between @intervalStart and @intervalEnd then breakend else 0 end en,DialerID,breakcode,BreakType
from #break (nolock)
where breakdate between @STARTDATE and @ENDDATE

insert into #IntervalHandled
select Uniqueid,callid,agentid,calldate, @intervalStart,@intervalEnd,talktime,wraptime,waittime,
case when contime > @intervalEnd then 0 
	when contime < @intervalStart then @intervalStart
	when contime between @intervalStart and @intervalEnd then contime else 0 end,
case when contime > @intervalEnd then 0 
when Endtime > @intervalEnd then @intervalEnd 
when Endtime between @intervalStart and @intervalEnd then Endtime else 0 end en,
campaignid,calltype,number,HandelType
from #Handled
where calldate between @STARTDATE and @ENDDATE

Select @intervalStart = @intervalStart + @Interval
Select @intervalEnd = @intervalEnd + @Interval
end


delete from #Intervallogin
where loginstart = 0 or loginend = 0

delete from #IntervalHandled
where contime = 0 or endtime = 0

delete from #Intervalbreak
where breakstart = 0 or breakend = 0


-------------------------------------------------------

select agentid, [Date],[Int_start],[Int_end],campaignid,breakcode,breakend-breakstart [Duration]
,case when campaignid = @DedicatedInboundCampaign and breakcode in (select break_codes from #breakcodes) then breakend-breakstart end [IBbillable break]
,case when campaignid = @DedicatedOutboundCampaign and breakcode in (select break_codes from #breakcodes) then breakend-breakstart end [OBbillable break]
,case when campaignid like 'bld%' and breakcode in (select break_codes from #breakcodes) then breakend-breakstart end [blendedbillable break]
,case when campaignid = @DedicatedInboundCampaign then breakend-breakstart else 0 end [IB break]
,case when campaignid = @DedicatedOutboundCampaign then breakend-breakstart else 0 end [OB Break]
,case when campaignid like 'bld%' then breakend-breakstart else 0 end [Blended Break]
into #SumBreak
from #Intervalbreak
--group by agentid, breakdate,campaignid,breakcode


create table #Report
(
[Date] datetime default '',
[agent ID] varchar(5) default 0,
[Interval Start] int default 0,
[Interval End] int default 0,
[IB wait] int default 0,
[IB handle] int default 0,
[OB Wait] int default 0,
[OB handle] int default 0,
[Blended Total Wait Time (Login - All Non Billable Break - All handle)] int default 0,
[Blended IB handle] int default 0,
[Blended OB handle] int default 0
)
insert into #Report 
([Date],[agent ID],[Interval Start],[Interval End])
select distinct [Date],[agentID],[Int_start],[Int_end]
from #IntervalLogin

update #Report
set [IB handle] = t.[IB handle],
[OB handle] = t. [OB handle],
[Blended OB handle] = t.[Blended OB handle],
[Blended IB handle] = t.[Blended IB handle],
[Blended Total Wait Time (Login - All Non Billable Break - All handle)] = t.[Blended Total Wait Time (Login - All Non Billable Break - All handle)],
[IB wait] = t.[IB wait],
[OB wait] = t.[OB wait]
from(
select calldate,agentid,[Int_start],[Int_end],
isnull(sum(case when handeltype = 'IBHandled' and campaignid = @DedicatedInboundCampaign  then endtime - contime else 0 end),0) [IB handle] ,
isnull(sum(case when handeltype = 'OBHandled' and campaignid = @DedicatedOutboundCampaign  then endtime - contime else 0 end),0)[OB handle],
isnull(sum(case when handeltype = 'BlendedHandled' and campaignid = @DedicatedOutboundCampaign then endtime - contime else 0 end),0)[Blended OB handle],
isnull(sum(case when handeltype = 'BlendedHandled' and campaignid = @DedicatedInboundCampaign then endtime - contime else 0 end),0)[Blended IB handle],

-isnull(sum(case when handeltype = 'BlendedHandled'  and calltype = 3 then endtime - contime else 0 end),0)
-isnull(sum(case when handeltype = 'BlendedHandled' and campaignid = @DedicatedOutboundCampaign then endtime - contime else 0 end),0) 
-isnull(sum(case when handeltype = 'BlendedHandled' and campaignid = @DedicatedInboundCampaign then endtime - contime else 0 end),0) [Blended Total Wait Time (Login - All Non Billable Break - All handle)],

-isnull(sum(case when handeltype = 'IBHandled' and calltype = 3 then endtime - contime else 0 end),0)
-isnull(sum(case when handeltype = 'IBHandled' and campaignid = @DedicatedInboundCampaign then endtime - contime else 0 end),0) [IB wait],

-isnull(sum(case when handeltype = 'OBHandled' and calltype = 3 then endtime - contime else 0 end),0)
-isnull(sum(case when handeltype = 'OBHandled' and campaignid = @DedicatedOutboundCampaign then endtime - contime else 0 end),0) [OB wait]

from #IntervalHandled
group by calldate,agentid,[Int_start],[Int_end]
)t
where t.calldate = [Date] and t.agentid = [Agent id] and [Interval Start] = t.[Int_start] and [Interval End] = t.[Int_end]

--select * from #report
update #report
set [Blended Total Wait Time (Login - All Non Billable Break - All handle)] = [Blended Total Wait Time (Login - All Non Billable Break - All handle)] + t.bldlg,
[IB wait] = [IB wait] + t.IBlg,
[OB wait] = [OB wait] + t.OBlg
from 
(select [Date] logindate,agentid,[Int_start],[Int_end],
sum(case when campaignid = @DedicatedInboundCampaign then loginend-loginstart else 0 end ) IBlg,
sum(case when campaignid = @DedicatedOutboundCampaign then loginend-loginstart else 0 end) OBlg,
sum(case when campaignid = 'BLD' then loginend-loginstart else 0 end) bldlg
from #Intervallogin 
group by [Date],agentid,[Int_start],[Int_end]
)t
where t.logindate = [Date] and t.agentid = [Agent id] and [Interval Start] = t.[Int_start] and [Interval End] = t.[Int_end]
--select * from #report


update #report
set [Blended Total Wait Time (Login - All Non Billable Break - All handle)] = [Blended Total Wait Time (Login - All Non Billable Break - All handle)] + t.bldbrk,
[IB wait] = [IB wait]+ t.IBbrk,
[OB wait] = [OB wait] + t.OBbrk
from 
(select [Date] breakdate,agentid,[Int_start],[Int_end],
isnull(sum([IBBillable break]),0)- isnull(sum([IB break] ),0) IBbrk,
isnull(sum([OBBillable break]),0)- isnull(sum([OB break]),0)  OBbrk,
isnull(sum([BlendedBillable break]),0) - isnull(sum([blended break]),0) bldbrk,
isnull(sum([BlendedBillable break]),0)[Blended Billable Break Hours],
isnull(sum([blended break]),0) - isnull(sum([BlendedBillable break]),0) [Blended Non-Billable Break Hours]
from #SumBreak 
group by [Date],agentid,[Int_start],[Int_end]
)t
where t.breakdate = [Date] and t.agentid = [Agent id] and [Interval Start] = t.[Int_start] and [Interval End] = t.[Int_end]

select date, [agent id] ,[Interval Start],[Interval End],
(
	(
	isnull([OB Wait],0) + 
	isnull([OB handle],0) + 
	isnull([Blended OB handle],0)
	) 
+ 
	(
		case when 
	(
	isnull([Blended OB handle],0)
	) = 0 
	then 0 
	else
	(
	isnull([Blended Total Wait Time (Login - All Non Billable Break - All handle)],0)
	) 
*	(
 
	(
	(
	isnull([Blended OB handle],0)
	)* 1.0
	/
	(
	isnull([Blended OB handle],0)+
	isnull([Blended IB handle],0)
	)
	)
	
	)
	end
	)
)[OB hours],
((isnull([IB Wait],0) + isnull([IB handle],0) + isnull([Blended IB handle],0)) 
+ ((isnull([Blended Total Wait Time (Login - All Non Billable Break - All handle)],0)) 
* (case when (isnull([Blended OB handle],0)+isnull([Blended IB handle],0)) = 0 
then 0 else ((isnull([Blended IB handle],0))*1.0/
(isnull([Blended OB handle],0)+isnull([Blended IB handle],0)))
end))) [IB hours]

from #report

-------------------------------------------------------


DROP TABLE #OBLOGIN
DROP TABLE #IBLOGIN
DROP TABLE #ALLLogins
DROP TABLE #Login
drop table #Handled
drop table #blendedlogin
drop table #calltracks
DROP TABLE #OBLOGINs
DROP TABLE #IBLOGINs
drop table #Break
drop table #AllBreak
drop table #Brk
drop table #campaigns
drop table #Breakcodes
drop table #IntervalHandled
drop table #Intervalbreak
drop table #Intervallogin
drop table #SumBreak
drop table #report


END