-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE CDB.[QueueReport] 
	-- Add the parameters for the stored procedure here
	@CampaignID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF 
	
-----------------------------------------------------------------------------------
	
	Select case when @CampaignID = 'FBT' then cast(isnull(sum(QCalls),0) as varchar)
				when @CampaignID = 'MIP' then 'N/A' else '0' end [Call in Queue]
	from [hamreports01].[trgivr].[autoIVR].[FitBitQueue] with (nolock)

END