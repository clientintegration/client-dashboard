-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE CDB.[TFN_Queue_Report] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime,
	@CampaignID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF    

/* converting input date to only date with out time*/
------------------------------------------------------
set @StartDate = CONVERT(varchar(10),@StartDate,120)
set @EndDate = CONVERT(varchar(10),@EndDate,120)
------------------------------------------------------

Declare @TestAgents as varchar(500)
Declare @Testnumber as varchar(500)

select * into #Variable
from [Hamreports01].webdialer.dte.reportvariables with (nolock)

if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin
Select @TestAgents = VariableValue from #Variable where VariableName = 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = 'Testnumber'
End
Else
Begin
Select @TestAgents = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Testnumber'
End

	BEGIN
		CREATE TABLE #testAgents
		(
			AgentID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@TestAgents))
		BEGIN
		  SELECT @t = SUBSTRING(@TestAgents,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #testAgents SELECT REPLACE(SUBSTRING(@TestAgents, @I, CHARINDEX(',', @TestAgents + ',', @I) - @I),'''','')
			  where substring(','+@TestAgents,@I,1)=',' AND @I < LEN(@TestAgents) + 1
		  END
		  SET @I = @I + 1
		END
	END

	BEGIN
		CREATE TABLE #testnumbers
		(
			Number VARCHAR(50)
		)
		DECLARE @t1 VARCHAR(MAX)
		DECLARE @I1 INT
		SELECT @I1 = 0
		WHILE(@I1 <=LEN(@Testnumber))
		BEGIN
		  SELECT @t1 = SUBSTRING(@Testnumber,@I1,1)
		  if(@t1<>','and @t1<>'')
		  BEGIN
			  Insert into #testnumbers SELECT REPLACE(SUBSTRING(@Testnumber, @I1, CHARINDEX(',', @Testnumber + ',', @I1) - @I1),'''','')
			  where substring(','+@Testnumber,@I1,1)=',' AND @I1 < LEN(@Testnumber) + 1
		  END
		  SET @I1 = @I1 + 1
		END
	END

/*selecting calltracks data to temp table to not hit real table each time */
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,termcd
into #rpt
from [Hamreports01].[webdialer].webdialer.calltracks with (nolock)
where calldate between @StartDate and @EndDate
       and number not in (select number from #testnumbers)
       and agentid not in (select AgentID from #testagents)
       and campaign = @CampaignID and campaign <> 'FBT'
union all
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,termcd
from [Hamreports01].[webdialer].dbo.rpt_fitbit with (nolock)
where calldate between @StartDate and @EndDate
       and number not in (select number from #testnumbers)
       and agentid not in (select agentid from #testagents)
       and campaign = @CampaignID 

select * into #TFN_DNISMappping
from [Hamreports01].webdialer.campaign.TFN_DNISMappping with (nolock)

select * into #TFN_Description
from [Hamreports01].webdialer.campaign.TFN_Description with (nolock)


Select TFNID, isnull(sum(case when calltype = 2 then 1 else 0 end),0) [Call Offered], 
		isnull(sum(case when calltype = 2 and agentid <> '' then 1 else 0 end),0) [Calls Handled],
		isnull(sum(case when calltype = 2 and agentid = '' then 1 else 0 end),0) [Calls Abandon]
		into #temp 
		from #rpt rpt 
		inner join #TFN_DNISMappping tfn 
		on rpt.dnisdigits = tfn.dnis
		group by dnisdigits,TFNID
		
		select tfd.[Description] [Queue/TFN], sum([Call Offered]) [Call Offered], 
		sum([Calls Handled]) [Calls Handled], sum([Calls Abandon]) [Calls Abandon] 
		from #temp temp
		inner join #TFN_Description tfd
		on tfd.TFNID = temp.tfnid
		group by tfd.[Description]
		
		
		drop table #TFN_Description
		drop table #temp
		drop table #rpt
		drop table #TFN_DNISMappping
		drop table #variable
		drop table #testagents
		drop table #testnumbers	

END