-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
Alter PROCEDURE CDB.[AgentsReport] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime,
	@CampaignID varchar(10)
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;    
	SET FMTONLY OFF  

/* converting input date to only date with out time*/
------------------------------------------------------
set @StartDate = CONVERT(varchar(10),@StartDate,120)
set @EndDate = CONVERT(varchar(10),@EndDate,120)
------------------------------------------------------

Declare @Billablebreak as varchar(500)
Declare @BlendedCampaigns as varchar(500)
Declare @IBCampaign as varchar(500)
Declare @OBCampaign as varchar(500)
Declare @ProductionTime as varchar(500)
Declare @TestAgents as varchar(500)
Declare @Testnumber as varchar(500)

select * into #Variable
from [Hamreports01].webdialer.dte.reportvariables with (nolock)




if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin 
Select @Billablebreak = VariableValue from #Variable where VariableName = 'Billablebreak'
Select @BlendedCampaigns = VariableValue from #Variable where VariableName = 'BlendedCampaigns'
Select @IBCampaign = VariableValue from #Variable where VariableName = 'IBCampaign'
Select @OBCampaign = VariableValue from #Variable where VariableName = 'OBCampaign'
Select @TestAgents = VariableValue from #Variable where VariableName = 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = 'Testnumber'
Select @ProductionTime = VariableValue from #Variable where VariableName = 'ProductionTime'
End
Else
Begin
Select @Billablebreak = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Billablebreak'
Select @BlendedCampaigns = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'BlendedCampaigns'
Select @IBCampaign = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'IBCampaign'
Select @OBCampaign = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'OBCampaign'
Select @TestAgents = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'TestAgents'
Select @Testnumber = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'Testnumber'
Select @ProductionTime = VariableValue from #Variable where VariableName = ltrim(rtrim(@CampaignID)) + 'ProductionTime'
End

	BEGIN
		CREATE TABLE #testAgents
		(
			AgentID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@TestAgents))
		BEGIN
		  SELECT @t = SUBSTRING(@TestAgents,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #testAgents SELECT REPLACE(SUBSTRING(@TestAgents, @I, CHARINDEX(',', @TestAgents + ',', @I) - @I),'''','')
			  where substring(','+@TestAgents,@I,1)=',' AND @I < LEN(@TestAgents) + 1
		  END
		  SET @I = @I + 1
		END
	END

	BEGIN
		CREATE TABLE #testnumbers
		(
			Number VARCHAR(50)
		)
		DECLARE @t1 VARCHAR(MAX)
		DECLARE @I1 INT
		SELECT @I1 = 0
		WHILE(@I1 <=LEN(@Testnumber))
		BEGIN
		  SELECT @t1 = SUBSTRING(@Testnumber,@I1,1)
		  if(@t1<>','and @t1<>'')
		  BEGIN
			  Insert into #testnumbers SELECT REPLACE(SUBSTRING(@Testnumber, @I1, CHARINDEX(',', @Testnumber + ',', @I1) - @I1),'''','')
			  where substring(','+@Testnumber,@I1,1)=',' AND @I1 < LEN(@Testnumber) + 1
		  END
		  SET @I1 = @I1 + 1
		END
	END
	
/*selecting calltracks data to temp table to not hit real table each time */
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,case when Distime < Contime then Distime+86400 else Distime end - Contime 'talktime'   
	,0 'holdtime'   /*hold time is zero in webdialer because it is the part of talk time*/
	,case when Endtime < Distime then Endtime+86400 else Endtime end - DisTime 'wraptime'
	,case when ConTime >= WaitStart then ConTime-WaitStart else 86400-WaitStart+ConTime end 'waittime' 
	,contime - starttime 'queuetime'
	,0 'ringtime'
	,'1899-12-30 ' +  CONVERT(VARCHAR, DATEADD(s, starttime, 0), 114) 'calltime'
	,termcd
	,contime
	,Endtime
into #rpt
from [Hamreports01].[webdialer].webdialer.calltracks with (nolock)
where calldate between CONVERT(varchar(10),@StartDate,120) 
	   and CONVERT(varchar(10),@EndDate ,120)
       and number not in (select number from #testnumbers)
       and agentid not in (select agentid from #testAgents)
       AND STARTTIME >= @ProductionTime
       and campaign = @CampaignID and campaign <> 'FBT'
union all
select
	convert(varchar(10),calldate,101) calldate
	,calltype
	,dnisdigits
	,agentid
	,talktime  
	,waittime 
	,0 holdtime   /*hold time is zero in webdialer because it is the part of talk time*/
	,wraptime
	,contime - starttime 'queuetime'
	,0 ringtime
	,'1899-12-30 ' +  CONVERT(VARCHAR, DATEADD(s, starttime, 0), 114) 'calltime'
	,termcd
	,contime
	,Endtime
from [Hamreports01].[webdialer].dbo.rpt_fitbit with (nolock)
where calldate between CONVERT(varchar(10),@StartDate,120) 
	   and CONVERT(varchar(10),@EndDate ,120)		
       and number not in (select number from #testnumbers)
       and agentid not in (select agentid from #testAgents)
       AND STARTTIME >= @ProductionTime
       and campaign = @CampaignID 
       
       
select * into #AgentCurrentState 
	from [Hamreports01].Webdialer.dte.AgentCurrentState with (nolock)
		where campaignid = @CampaignID 
		and CONVERT(varchar(10),AgentTimeStamp,120) between CONVERT(varchar(10),@StartDate,120) 
														and CONVERT(varchar(10),@EndDate ,120)		
union all
select *
	from [Hamreports01].Webdialer.Campaign.AgentCurrentState with (nolock)
		where campaignid = @CampaignID
		and CONVERT(varchar(10),AgentTimeStamp,120) between CONVERT(varchar(10),@StartDate,120) 
														and CONVERT(varchar(10),@EndDate ,120)

select Agenttimestamp, AgentID, CampaignID, AgentState 
	into #States 
	from #AgentCurrentState
		where AgentTimeStamp in (
			select max(AgentTimeStamp)
			from #AgentCurrentState (nolock)
			group by agentid
		)													
														
	select CAC.CampaignID, CAC.AwayCodeID, AC.Code, AC.Description
	into #Breakcodesdescription
	from [Hamreports01].webdialer.config.awaycode AC with (nolock)
	inner join [Hamreports01].webdialer.config.campaignawaycode CAC with (nolock)
	on AC.ID = CAC.AwayCodeID
	where CAC.CampaignID = @Campaignid	
		
----------------------------------------------------------------------------------	
/*Hours calculation*/

create table #Hours
(
LoginDate datetime default '1900-01-01',
AgentID varchar(max) default '' not null,
OBHours decimal(20,2) default 0,
IBHours decimal(20,2) default 0
)

/* hours is being calculated from stored procedure for blended calculation*/
if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin
insert into #Hours
EXECUTE CDB.[BlendedHoursbycamp] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak
end

-------------------------------------------------------------------
 /*Login*/

create table #Login
(
AgentID varchar(max) default '' not null,
LoginDate datetime default '1900-01-01',
CampaignID varchar(10) default '',
LoginStart int default 0,
LoginEnd int default 0,
DialerID int default 0,
LoginType varchar(50) default ''
)

/* Login is being calculated from stored procedure for blended calculation*/
insert into #Login
EXECUTE CDB.[BlendedHours] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak
 
----------------------------------------------------------------------------------
 /*Break*/
 create table #Breaks
(
AgentID varchar(max) default '' not null,
Breakdate datetime default '1900-01-01',
CampaignID varchar(10) default '',
BreakStart int default 0,
BreakEnd int default 0,
DialerID int default 0,
BreakCode varchar(50) default '',
BreakType varchar(50) default ''
)

/* hours is being calculated from stored procedure for blended calculation*/
insert into #Breaks
EXECUTE CDB.[Blendedbreaks] 
   @StartDate
  ,@EndDate
  ,@BlendedCampaigns
  ,@IBCampaign
  ,@OBCampaign
  ,@Billablebreak

-------------------------------------------------------------
select  
	convert(varchar(max),'') [Emp Name]
	, isnull(l.agentid,'Total') [Avaya_ID]
	, cast(sum(Loginend - LoginStart)/3600.00 as decimal(10,2)) [Staffed Time]
	, cast(0.00 as decimal(10,2)) [Available Time]
	, cast(0.00 as decimal(10,2)) [Handle Time]
	, cast(0.00 as decimal(10,2)) [Hold Time]
	, cast(0.00 as decimal(10,2)) [Break Time]
	, cast(0 as int) [Calls Answered]
	, cast(0 as int) [Sale Calls]
	, 0 [AHT (sec)]
	, cast('' as varchar(100)) [Agent State]
	, cast('' as varchar(10)) [Time in Current State]
	, cast('' as varchar(10)) [Time since last call]
	, cast(0.00 as decimal(10,2)) [Production Time]
	, cast('0.00%' as varchar(10)) [Occupancy%]
into #Agents
from #Login l (nolock)
where (campaignid = 'BLD' or campaignid = @CampaignID)
and logindate between @StartDate and @EndDate
and agentid not in (select agentid from #testAgents)
group by Agentid with rollup
order by case when l.agentid is null then 1 else len(l.agentid) end

update #Agents
set [Break Time] = t.[Break Time]
from
	(select sum(Breakend - BreakStart)/3600.00 [Break Time], 
	isnull(Agentid,'Total') agentid
	from #breaks (nolock)
	where campaignid <> @OBCampaign
	and breakdate between @StartDate and @EndDate
	group by agentid with rollup)t
where t.agentid = Avaya_ID

update #Agents
set [Handle Time] = t.[Handle Time],
[AHT (sec)] = t.[AHT (sec)],
[Production Time] = t.[Production Time],
[Calls Answered] = t.[Calls Answered],
[Sale Calls] = t.[Sale Calls]
from
	(select sum(talktime+wraptime)/3600.00 [Handle Time], 
	isnull(Agentid,'Total') agentid,
	avg(talktime+wraptime) [AHT (sec)],
	sum(talktime+waittime+wraptime)/3600.00 [Production Time],
	count(*) [Calls Answered],
	isnull(sum(case when termcd in ('S0','S1') then 1 else 0 end),0) [Sale Calls]
	from #rpt (nolock)
	where calltype = 2 and agentid <> ''
	group by agentid with rollup)t
where t.agentid = Avaya_ID

/*setting agent names*/
	update s
	set s.[Emp Name]= isnull(upper(a.first_name) + ' ' + Upper(a.last_name ),'TOTAL')
	from #Agents s left outer join [Hamreports01].trgpayroll.dbo.employee a with (nolock) on a.strata_id =s.[Avaya_ID]

update #Agents
set [Occupancy%] = case when ([Staffed Time] - [Break Time])=0 then '0%' else cast(cast(([Handle Time] / ([Staffed Time] - [Break Time]) * 100.00) as decimal(10,2)) as varchar(10))  + '%' end

if @CampaignID = 'MIP' or @CampaignID = 'MIC'
begin
update #Agents
set [Production Time] = case when @Campaignid = 'MIP' then t.[IB Production Time] else t.[OB Production Time] end
from
(select isnull(Agentid, 'Total') AgentID,
isnull(sum(IBhours),0)/3600.00 [IB Production Time],sum(OBhours)/3600.00 [OB Production Time]
from #Hours (nolock)
group by agentid with rollup)t
where t.agentid = Avaya_ID
End

update #Agents
set [Agent State] = t.[Agent State],
[Time in Current State] = t.[Time in Current State],
[Time since last call] = t.[Time since last call]
from
(


	Select Replace(Case when ST.AgentState = 'Login' then 'Break' else
	isnull(case when BCD.Description = 'break' then 'Break' else case when BCD.Description like '%brea%' then BCD.Description else  'Break ' + BCD.Description  end end, ST.Agentstate) end, '-', ' ') [Agent State]
	, convert(varchar,getdate() - AgentTimeStamp,108) [Time in Current State]
	, case when agentstate = 'Wait' then convert(varchar,getdate() - AgentTimeStamp,108) else '' end [Time since last call]	
	, agentid
	from #States ST with (nolock)
	left join #Breakcodesdescription BCD with (nolock)
	on SUBSTRING(ST.AgentState, CHARINDEX('-', ST.AgentState) + 1, LEN(ST.AgentState)) = BCD.code	
	where agentstate in ('Wait','Login','Talk','Wrap','On Call') or agentstate like 'Brea%'
	group by AgentState,BCD.Description,agentid,AgentTimeStamp
	
)t
where t.agentid = Avaya_ID

update #Agents
set [Available Time] = case when @Campaignid = 'MIP' or @CampaignID = 'MIC' then [Staffed Time] - [Break Time] - [Handle Time]
						else [Production Time] - [Handle Time] end

/*Column Name is selected to made table dynamic in case if any column added or renamed then no need to change the buil*/
Select 'Agent Name' as [Agent Name] , 'Agent ID' [Agent ID], 'Calls' as [Calls] , 'State' as [State], 
'State Time' as [State Time] , 'Time Since Last Call' [Time Since Last Call] , 
'AHT' as [AHT] , 'Login Time'  as [Login Time] , 'Break Time' as [Break Time] , 
'Available Time' as [Available Time] , 'Handle Time' as [Handle Time] , 
case when @CampaignID = 'MIP' or @CampaignID = 'MIC' then 'Production Time' else 'TWW' end [Production Time] , 
'Occupancy%' as [Occupancy%]
union all
select [Emp Name] as [Agent Name] , case when [Avaya_ID] = 'Total' then '' else [Avaya_ID] end as [Agent ID],
cast([Calls Answered] as varchar) as [Calls] , [Agent State] as [State], [Time in Current State] , [Time Since Last Call] , 
cast([AHT (sec)] as varchar), cast([Staffed Time] as varchar), cast([Break Time] as varchar), cast([Available Time] as varchar),
cast([Handle Time] as varchar), cast([Production Time] as varchar) , cast([Occupancy%] as varchar)
from #Agents
--where Avaya_ID <> 'Total'


drop table #Agents
drop table #Login
drop table #Breaks
drop table #Hours
drop table #rpt
drop table #AgentCurrentState
drop table #Variable
drop table #testnumbers
drop table #testagents
drop table #Breakcodesdescription
drop table #states

END