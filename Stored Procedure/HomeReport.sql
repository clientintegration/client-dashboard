-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
ALTER PROCEDURE CDB.[HomeReport] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime,
	@UserID int
AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	SET FMTONLY OFF 
--------------------------------------------------------------------
/*Region to extract text agent with respect to campaign which was saved in DB as comma separated*/	
Declare @testagents varchar(1000)
set @testagents = ''
Declare @testagentscampaign varchar(1000)
set @testagentscampaign = ''

select * into #agents
from [Hamreports01].webdialer.dte.reportvariables with (nolock)
where variablename like '%Testagents'

select * into #campaigns
from [Hamreports01].webdialer.dte.reportvariables with (nolock)
where variablename like '%TestagentsC%'

select ROW_NUMBER() OVER (ORDER BY ag.variablename) AS RowNumber, ag.variablename,
ag.variablevalue [Agent],cam.variablevalue [Campaign]
Into #Testagentsdeatils
from #agents ag
inner join #campaigns cam
on left(ag.variablename,3) like left(cam.variablename,3) 


Declare @Agentcount int, @j int
Select @Agentcount = count(*) from #Testagentsdeatils
select @j = 1
while @j <= @Agentcount
begin
select @testagents = COALESCE(@testagents + ',', '') + [Agent] from #Testagentsdeatils where RowNumber = @j 
Select @testagentscampaign = COALESCE(@testagentscampaign + ',', '') + [Campaign] from #Testagentsdeatils where RowNumber = @j
set @j = @j + 1
end

select @testagents = substring(@testagents, 2, len(@testagents)) 
select @testagentscampaign = substring(@testagentscampaign, 2, len(@testagentscampaign)) 

	BEGIN
		CREATE TABLE #testAgents
		(
			AgentID VARCHAR(50),
			CampaignID Varchar(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@TestAgents))
		BEGIN
		  SELECT @t = SUBSTRING(@TestAgents,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #testAgents SELECT REPLACE(SUBSTRING(@TestAgents, @I, CHARINDEX(',', @TestAgents + ',', @I) - @I),'''',''),
			  REPLACE(SUBSTRING(@testagentscampaign, @I, CHARINDEX(',', @testagentscampaign + ',', @I) - @I),'''','')
			  where substring(','+@TestAgents,@I,1)=',' AND @I < LEN(@TestAgents) + 1
		  END
		  SET @I = @I + 1
		END
	END

-----------------------------------------------------------------------------------
/* Report to show stats on HOME page of client dashboard*/
create table #temp
(
	Campaign varchar(50),
	Client varchar(50),
	ImagePath varchar(max),
	CampaignType varchar(50),
	Agents int,
	DialStatus varchar(100)
)

insert into #temp 
	select r.RoleName, --Campaign ID 
		   c.Name, -- Client Name    
		   c.ImagePath, --Path of the image for client   
		   r.CampaignType, -- Description of camapign type like Inbound/Outbound etc.
		   0, -- Active agent count (will be update from Agent current state table)
		   'Inactive' -- Campaign dialing status (will be update from Agent current state table)
		from leadmanagement..webpages_Roles r
			join leadmanagement..Client c 
			on r.ClientID = c.ClientID
		where r.RoleName in (select distinct rolename from leadmanagement..webpages_roles r
			inner join leadmanagement..webpages_usersinroles ur 
			on r.roleid = ur.roleid
	where userid = @UserID)

--------------Agent Current State----------------
--=============================================--
select * into #AgentCurrentState --Saving all data in temp table to avoind multiple select to table using link server
	from [Hamreports01].Webdialer.dte.AgentCurrentState with (nolock)
		where campaignid in (select campaign from #temp)
		and CONVERT(varchar(10),AgentTimeStamp,120) between convert(varchar(10),@StartDate,120) and convert(varchar(10),@EndDate ,120)

union all
select *  
	from [Hamreports01].Webdialer.Campaign.AgentCurrentState with (nolock)
		where campaignid in (select campaign from #temp)
		and CONVERT(varchar(10),AgentTimeStamp,120) between CONVERT(varchar(10),@StartDate,120) 
														and CONVERT(varchar(10),@EndDate ,120)
														
select max(AgentTimeStamp) AgentTimeStamp,agentid,campaignid --selecting Maximum time stamp of an agent per campaign
	into #MaxTimeStamp
		from #AgentCurrentState (nolock)
		group by agentid,campaignid


select acs.AgentTimeStamp, acs.agentid,acs.campaignid,acs.agentstate --to get current state join
	into #States
		from #AgentCurrentState acs with (nolock)
		inner join #MaxTimeStamp mts with (nolock)
		on acs.AgentTimeStamp = mts.AgentTimeStamp
		and acs.agentid = mts.agentid
		and acs.campaignid = mts.campaignid	
		left join #testAgents tstag with (nolock)
		on tstag.agentid = acs.agentid
		and tstag.campaignid = acs.campaignid
		where tstag.agentid is null and tstag.campaignid is null


update #temp 
	set Agents = t.CurrentAgents,
	DialStatus = t.DialStatus
from
(
	select count(*) CurrentAgents,campaignid, 
	case when isnull(count(*),0) <> 0 then 'Active' else 'Inactive' end DialStatus
	from #States where Agentstate in ('Talk' ,'Wrap','Wait' ,'Login') or Agentstate like 'Brea%'
	group by campaignid
)t
where t.campaignid = campaign


select * from #temp

drop table #testAgents
drop table #campaigns
drop table #agents
drop table #Testagentsdeatils
drop table #temp
drop table #AgentCurrentState
drop table #States
drop table #MaxTimeStamp

END