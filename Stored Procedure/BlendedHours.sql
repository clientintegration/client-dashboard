-- ================================================
-- Template generated from Template Explorer using:
-- Create Procedure (New Menu).SQL
--
-- Use the Specify Values for Template Parameters 
-- command (Ctrl-Shift-M) to fill in the parameter 
-- values below.
--
-- This block of comments will not be included in
-- the definition of the procedure.
-- ================================================
USE Leadmanagement
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================
-- Author:		<Author,Tehmina,Noman>
-- Create date: <Create Date,,>
-- Description:	<Description,,>
-- =============================================
CREATE PROCEDURE CDB.[BlendedHours] 
	-- Add the parameters for the stored procedure here
	@StartDate datetime, 
	@EndDate datetime, 
	@BlendedCampaigns varchar(MAX),
	@DedicatedInboundCampaign varchar(MAX),
	@DedicatedOutboundCampaign varchar(MAX),
	@billablebreakcodes varchar(MAX)

AS
BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;

----------------------------------------------------------
----------------------------------------------------------
--- select all blended campaigns in temp table
----------------------------------------------------------
	BEGIN
		CREATE TABLE #campaigns
		(
			Camp_ID VARCHAR(50)
		)
		DECLARE @t VARCHAR(MAX)
		DECLARE @I INT
		SELECT @I = 0
		WHILE(@I <=LEN(@BlendedCampaigns))
		BEGIN
		  SELECT @t = SUBSTRING(@BlendedCampaigns,@I,1)
		  if(@t<>','and @t<>'')
		  BEGIN
			  Insert into #campaigns SELECT SUBSTRING(@BlendedCampaigns, @I, CHARINDEX(',', @BlendedCampaigns + ',', @I) - @I)
			  where substring(','+@BlendedCampaigns,@I,1)=',' AND @I < LEN(@BlendedCampaigns) + 1
		  END
		  SET @I = @I + 1
		END
	END

-----------------------------------------------------------------

/*selecting login in break into temp table to perform further operation*/

SELECT * 
INTO #OBLOGIN
FROM [Hamreports01].webdialer.webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedOutboundCampaign --and agentid not in ('zzx','zzy','zzz')

SELECT * 
INTO #IBLOGIN
FROM [Hamreports01].webdialer.webdialer.TSRLOGINS with (nolock)
WHERE LOGINDATE between @STARTDATE and @ENDDATE
AND CAMPAIGNID = @DedicatedInboundCampaign  --and agentid not in ('zzx','zzy','zzz')


------------------------------------------------------------------------
/*calculating dedicated phase login time, 
where "LoginType" column is to use differerntiate what is the login type
"Normal" is being insert for dedicated mode login time,
loginstart time is used to check the state
if login start for both campaign is same then it means login is in blended mode
else dedicated,
Note: we are not taking blended time from below query
because the login end time is always not same in blended mode
as sometime agent is being forcefully log out from any of the campaign*/
------------------------------------------------------------------------
SELECT OB.*,
    CASE WHEN IB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
INTO #ALLLogins
FROM
    #OBLOGIN OB (nolock) 
    LEFT OUTER JOIN #IBLOGIN IB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)
Union All   
SELECT IB.*,
    CASE WHEN OB.logindate IS NULL THEN 'Normal' ELSE 'Blended' END as LoginType
FROM
    #IBLOGIN IB (nolock) 
    LEFT OUTER JOIN #OBLOGIN OB
    ON (OB.LOGINDATE = IB.LOGINDATE AND OB.AGENTID = IB.AGENTID and OB.loginstart = IB.loginstart)

/*blended hours calculation*/
-----------------------------
/*taking only those records where login start is same for both campaign
and for login end taking those campaign login end time where it is lesser*/
---------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate, 'BLD' CampaignID, OB.LoginStart,
	case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
		 when OB.LoginEnd < IB.LoginEnd then OB.LoginEnd 
		 when OB.LoginEnd = IB.LoginEnd then OB.LoginEnd  end LoginEnd,
	OB.DialerID ,
	'Blended' LoginType
into #login
	FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART
	AND OB.AGENTID = IB.AGENTID
union all /*union all the login time which was consider as dedicated because of force fully logout*/
----------------------------------------------------------------------------------------------------
SELECT OB.AgentID, OB.LoginDate,  
	   case when OB.LoginEnd > IB.LoginEnd then OB.campaignid 
	   else  IB.campaignid end CampaignID,  
	   case when OB.LoginEnd > IB.LoginEnd then IB.LoginEnd 
	   else  OB.LoginEnd end LoginStart, 
	   case when OB.LoginEnd > IB.LoginEnd then OB.LoginEnd 
	   else IB.LoginEnd end LoginEnd,
	  OB.DialerID,
	  'Blended-ForceLogout' LoginType
FROM #OBLOGIN OB (NOLOCK)
	INNER JOIN #IBLOGIN IB (NOLOCK)
	ON OB.LOGINDATE = IB.LOGINDATE AND OB.LOGINSTART = IB.LOGINSTART AND OB.LOGINEND <> IB.LOGINEND
	AND OB.AGENTID = IB.AGENTID
UNION All 
/*taking only dedicated mode login time because blended login 
and forcefully dedicated is already taken in above queriedds*/
---------------------------------------------------------------
SELECT * FROM #ALLLogins
	WHERE LoginType = 'Normal'
order by 2,1

/*Login part has been completed*/
--------------------------------------------------------------


select * from #login


DROP TABLE #OBLOGIN
DROP TABLE #IBLOGIN
DROP TABLE #ALLLogins
DROP TABLE #Login
drop table #campaigns

END