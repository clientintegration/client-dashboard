-- ================================================
USE [Leadmanagement]
GO
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- =============================================    
-- Author:  <Author,Tehmina,Noman>    
-- Create date: <Create Date,,>    
-- Description: <Description,,>    
-- =============================================    
ALTER PROCEDURE CDB.[CallRecordingReport]      
 -- Add the parameters for the stored procedure here    
 @StartDate varchar(10),    
 @EndDate  varchar(10),    
 @number varchar(20),    
 @UniqueID varchar(50),    
 @AgentID varchar(10),    
 @CampaignID varchar(10)    
AS    
BEGIN    
 -- SET NOCOUNT ON added to prevent extra result sets from    
 -- interfering with SELECT statements.    
 SET NOCOUNT ON;    
 SET FMTONLY OFF      
-----------------------------------------------------------------------------------    
/*selecting calltracks data to temp table to not hit real table each time */    
    
/*1.2.6 NR Call Recording Segment Details    
SegmentID|AudioPath|AudioFileName|VideoPath|VideoFileName|AgentID|AgentName|CalledPty|CallingPty|CallStartTime|Duration|CallType    
|ID|EncryptedFileString    
Example:    
1|\\cornas01.tlsp.com\CallRecordings\NVR\\NRStreaming\Tagged\2013\07\02\65619|00003091321372777692|||63257|Abati, Joseph B.|    
84311|4695837867|07/02/2013 11:08:17|00:01:04|Inbound|57320949|0101010101010101*/    
    
Declare @MainQuery as Varchar(max)    
Set @MainQuery =     
'Select ' +     
'case when calltype = 1 then ''Outbound'' when calltype = 2 then ''Inbound'' when calltype = 3 then ''Manual Outbound'' else '''' end [Call Type],' +    
'Campaign [Campaign ID], ' +    
'UniqueID [Unique ID],' +    
'CallID [Call ID],' +    
'rpt.AgentID [Agent ID],' +  
'isnull(emp.first_name,'''') + '' '' + isnull(emp.last_name,'''') [Agent Name],' +  
'Number ,' +    
'convert(varchar(10),Calldate , 120) + '' '' + CONVERT(varchar, DATEADD(ss, starttime, 0), 108) Calldate, ' +       
'CONVERT(varchar, DATEADD(ss, Endtime - Contime , 0), 108) [Duration], ' +    
'Termcd [Call Outcome], ' +    
'isnull(ProviderType , '''') [Provider Type], ' +    
'''1''' + --SegmentID    
'+ ''|'' + case when providertype = ''Avaya'' then ltrim(rtrim(isnull(sourcevoxpath,''''))) else ltrim(rtrim(isnull(sourcevoxpath,''''))) end' + --AudioPath    
'+ ''|'' + case when providertype = ''Avaya'' then ltrim(rtrim(isnull(voxfilename,''''))) else ltrim(rtrim(isnull(voxfilename,''''))) end' + --AudioFileName    
'+ ''|'' + ''''' + --VideoPath (Empty becuase no video streaming is required)    
'+ ''|'' + ''''' + --VideoFileName (Empty becuase no video streaming is required)    
'+ ''|'' + ltrim(rtrim(isnull(rpt.AgentID,'''')))' + --AgentID    
'+ ''|'' + ltrim(rtrim(isnull(emp.first_name,''''))) + '' '' + ltrim(rtrim(isnull(emp.last_name,'''')))' + --AgentName    
'+ ''|'' + ltrim(rtrim(isnull(rpt.Anidigits,'''')))' + --CalledPty    
'+ ''|'' + ltrim(rtrim(isnull(rpt.Number,'''')))' + --CallingPty    
'+ ''|'' + convert(varchar(10), calldate, 101) + '' '' + CONVERT(varchar, DATEADD(ss, starttime, 0), 108)' + --CallStartTime    
'+ ''|'' + CONVERT(varchar, DATEADD(ss, Endtime - Contime , 0), 108)' + -- Duration    
'+ ''|'' + case when calltype = 1 then ''Outbound'' when calltype = 2 then ''Inbound'' when calltype = 3 then ''Manual Outbound'' else '''' end' + --CallType    
'+ ''|'' + ltrim(rtrim(isnull(uniqueid,'''')))' + --ID    
'+ ''|'' + ''0101010101010101'' as [NR Segment]' + --EncryptedFileString    
'from [Hamreports01].webdialer.webdialer.calltracks rpt with (nolock)    
left join [Hamreports01].trgpayroll.dbo.employee emp with (nolock)    
on rpt.agentid = emp.strata_id and strata_ID <> '''' and emp.active_f = ''A''   
where sourcevoxpath <> '''''   
--and calldate between @StartDate and @EndDate    
--and number = @number    
--and agentid = @AgentID    
--and campaign = @CampaignID    
if(@StartDate <> '' and @EndDate <> '' )    
BEGIN    
SET @MainQuery = @MainQuery + ' AND calldate between ''' + convert(varchar(10),@StartDate,120) + ''' and ''' + convert(varchar(10),@EndDate,120) + ''''    
END    
    
if(@number <> '' )    
BEGIN    
SET @MainQuery = @MainQuery + ' AND Number like ''%' + @number + '%'''    
END    
    
if(@UniqueID <> '')    
BEGIN    
SET @MainQuery = @MainQuery + ' AND UniqueID like ''%' + @UniqueID + '%'''    
END    
    
if(@AgentID <> '')    
BEGIN    
SET @MainQuery = @MainQuery + ' AND rpt.agentID like ''%' + @AgentID + '%'''    
END    
    
if(@CampaignID <> '')    
BEGIN    
SET @MainQuery = @MainQuery + ' AND campaign like ''%' + @CampaignID + '%'''    
END    
    

EXEC(@MainQuery)    
    
    
END