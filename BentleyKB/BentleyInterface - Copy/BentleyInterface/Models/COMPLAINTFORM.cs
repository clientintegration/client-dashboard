//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BentleyInterface.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class COMPLAINTFORM
    {
        public int ID { get; set; }
        public int SubCampaign { get; set; }
        public string Name { get; set; }
        public System.DateTime Date { get; set; }
        public string Telephone { get; set; }
        public string TimeofCall { get; set; }
        public string Address { get; set; }
        public string Email { get; set; }
        public string BCMS { get; set; }
        public string AgentName { get; set; }
        public string Details { get; set; }
        public string Severity { get; set; }
        public string TeamLeaderName { get; set; }
        public Nullable<System.DateTime> BentleyAdvisedDate { get; set; }
        public string BentleyContactName { get; set; }
        public string ScannedandEmailedto { get; set; }
        public System.DateTime DateAdded { get; set; }
    
        public virtual CFCampaing CFCampaing { get; set; }
    }
}
