﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace BentleyInterface.Models
{
    public class CCProfileMetadata
    {
        public int ID { get; set; }

        public Nullable<int> CampaignID { get; set; }

        [Required(ErrorMessage="This is Required.")]
        [Display(Name="Region")]
        public Nullable<int> RegionID { get; set; }

        
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Date { get; set; }
        
        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "Emails Received")]
        public Nullable<int> Emails_Received { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Emails Responded within SLA")]
        public Nullable<int> Emails_Responded_Within_SLA { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Fax Received")]
        public Nullable<int> Fax_Received { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Fax Responded within SLA")]
        public Nullable<int> Fax_Responded_Within_SLA { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "Letters Received")]
        public Nullable<int> Letters_Received { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "Letters Responded within SLA")]
        public Nullable<int> Letters_Responded_Within_SLA { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "Open Concern")]
        public Nullable<int> Open_Concern { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "Closed Concern")]
        public Nullable<int> Closed_Concern { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "InProgress Concern")]
        public Nullable<int> InProgress_Concern { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "DWS Leads")]
        public Nullable<int> DWS_Leads { get; set; }

        [Display(Name = "Corporate website leads")]
        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        public Nullable<int> PLE_Leads { get; set; }

        [Range(0, 32000,ErrorMessage="Please Enter a Valid Number")]
        [Display(Name = "HQ profile quantity")]
        public Nullable<int> HQ_Profile_Quality { get; set; }

        [Display(Name = "Hours Delivered (00:00)")]
        [RegularExpression("[0-9]+:[0-5]{1}[0-9]{1}", ErrorMessage = "The field Hours Delivered (00:00) must match the pattern XX:XX")]
        public string Hours_delivered { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "MSP Leads")]
        public int MSP { get; set; }

       
    }

    public class CEMDailyDataProfileData
    {
        public int ID { get; set; }

        [DataType(DataType.Date)]
        public Nullable<System.DateTime> Date { get; set; }

        [Required(ErrorMessage = "This is Required.")]
        [Display(Name = "Region")]
        public Nullable<int> RegionID { get; set; }

        [Range(0.0,300.0, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Hours Scheduled")]
        public Nullable<double> Hours_Scheduled { get; set; }

        [Range(0.0, 300.0, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Hours Delivered")]
        public Nullable<double> Hours_Delivered { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Data Added")]
        public Nullable<int> Data_Added { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Completes")]
        public Nullable<int> Completes { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Pending Records")]
        public Nullable<int> Pending_Records { get; set; }

        public virtual Region Region { get; set; }
    }

    public class RegionProfileData
    {
        [Display(Name="Region")]
        [Required(ErrorMessage = "This is Required.")]
        public string Region1 { get; set; }
    }

    public class CEMMonthlyDataProfileData
    {
        [Required(ErrorMessage = "This is Required.")]
        [Display(Name = "Region")]
        public Nullable<int> RegionID { get; set; }

        [Required(ErrorMessage = "This is Required.")]
        
        [DataType(DataType.Date)]
        public string Month { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Records Received")]
        public Nullable<int> Records_Received { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Pending Records")]
        public Nullable<int> Pending_Records { get; set; }

        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        [Display(Name = "Completes")]
        public Nullable<int> Completes { get; set; }
    }

    public partial class KnowledgeBaseProfileData
    {
        public int ID { get; set; }

        [Required()]
        [Display(Name = "Title")]
        [StringLength(500)]
        public string Title { get; set; }

        [Required()]
        [Display(Name = "Category")]
        [Range(0,32000,ErrorMessage="Category is Required.")]
        public int Category { get; set; }

        [Required()]
        [Display(Name = "Content")]
        [StringLength(7500)]
        [UIHint("tinymce_full_compressed")]
        [AllowHtml]
        public string Description { get; set; }


        [Display(Name = "Attachment", AutoGenerateField = false)]
        [StringLength(500)]
        public string Filepath { get; set; }

        [Display(Name = "Date Added", AutoGenerateField = false)]
        public System.DateTime date { get; set; }
    }

    public partial class CategoryProfileData
    {

        public int ID { get; set; }

        [Required()]
        [Display(Name = "Category")]
        [StringLength(500)]
        public string Category1 { get; set; }
        [Required(ErrorMessage = "Campaign is Required")]
        [Display(Name = "Campaign")]
        public int CampaignID { get; set; }       

        [Required()]
        [Display(Name = "Description")]
        [StringLength(7500)]

        public string Description { get; set; }

        [Display(Name = "Image")]
        [StringLength(500)]
        public string ImagePath { get; set; }
    }

    public partial class CampaignProfileData
    {

        public int ID { get; set; }

        [Required(ErrorMessage = "Campaign is Required")]
        [Display(Name = "Campaign")]
        [StringLength(500)]
        public string Campaign1 { get; set; }
    }

    public partial class CFProfileData
    {
        [Required()]
        [Display(Name = "Campaign")]
        [Range(1,32000)]
        public int SubCampaign { get; set; }

        [Required()]
        [Display(Name = "Customer Name")]
        [StringLength(500)]
        public string Name { get; set; }

        [Required()]
        [Display(Name = "Complaint Date")]
        [DataType(DataType.Date)]
        public System.DateTime Date { get; set; }

        [Required()]
        [Display(Name = "Contact Number")]
        [StringLength(50)]
        public string Telephone { get; set; }

        [Required()]
        [Display(Name = "Time of Call")]
        [StringLength(50)]
        [RegularExpression("[0-2]{0,1}[0-9]{0,1}:[0-5]{1}[0-9]{1}", ErrorMessage = "The field Time (00:00) must match the pattern XX:XX")]
        public string TimeofCall { get; set; }

        [Required()]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Address")]
        [StringLength(5000)]
        public string Address { get; set; }

        [Display(Name = "Email")]
        [StringLength(150)]
        [DataType(DataType.EmailAddress)]
        public string Email { get; set; }

        [Required()]
        [Display(Name = "BCMS")]
        [StringLength(50)]
        public string BCMS { get; set; }

        [Required()]
        [Display(Name = "Agent Name")]
        [StringLength(500)]
        public string AgentName { get; set; }

        [Required()]
        [DataType(DataType.MultilineText)]
        [Display(Name = "Details")]
        [StringLength(7000)]
        public string Details { get; set; }

        [Required()]
        [Display(Name = "Severity")]
        [StringLength(50)]
        public string Severity { get; set; }

        [Required()]
        [Display(Name = "Team Leader Name")]
        [StringLength(150)]
        public string TeamLeaderName { get; set; }

        [Required()]
        [Display(Name = "Bentley Advised Date")]
        [DataType(DataType.Date)]
        public Nullable<System.DateTime> BentleyAdvisedDate { get; set; }

        [Required()]
        [Display(Name = "Bentley Contact Name")]
        [StringLength(150)]
        public string BentleyContactName { get; set; }


        [Required()]
        [Display(Name = "Scanned And Emailed To")]
        [StringLength(150)]
        public string ScannedandEmailedto { get; set; }

        [Display(Name = "Date Added")]
        public System.DateTime DateAdded { get; set; }
    }

    public partial class CFCampaingProfileData
    {
        [Required()]
        [Display(Name = "Campaign")]
        [StringLength(500)]
        public string campaign { get; set; }
    }

    public partial class AbbottTeamDailyProfileData
    {
        [DataType(DataType.Date)]
        [Required(ErrorMessage="Date is required")]
        [Display(Name="Date")]
        public Nullable<System.DateTime> Date { get; set; }

        [Display(Name = "Individual Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Individual_Sales { get; set; }

        [Display(Name = "Individual Doses")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Individual_Doses { get; set; }

        [Display(Name = "BG Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> BG_Sales { get; set; }

        [Display(Name = "BG Doses")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> BG_Doses { get; set; }
    }

    public partial class AbbottWeeklyQualityProfileData
     {
        [DataType(DataType.Date)]
        [Required(ErrorMessage="Date is required")]
        [Display(Name="Week Start Date")]
        public Nullable<System.DateTime> Week_Start_Date { get; set; }

        [Required(ErrorMessage = "Agent ID is required")]
        [Display(Name="Agent ID")]
        public Nullable<int> AgentID { get; set; }

        [Display(Name = "Call Quality")]
        [Range(0.0,100.0, ErrorMessage = "Please Enter a Valid Number between 0 and 100")]
        public Nullable<double> Call_Quality { get; set; }

        [Display(Name = "Data Entry Quality")]
        [Range(0.0, 100.0, ErrorMessage = "Please Enter a Valid Number between 0 and 100")]
        public Nullable<double> Order_Data_Entry_Quality { get; set; }

        [Display(Name = "Notes Quality")]
        [Range(0.0, 100.0, ErrorMessage = "Please Enter a Valid Number between 0 and 100")]
        public Nullable<double> Notes_Quality { get; set; }
     }

    public partial class AbbottWeeklyTeamProfileData
    {

        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Date is required")]
        [Display(Name = "Week Start Date")]
        public Nullable<System.DateTime> Week_Start_Date { get; set; }

        [Display(Name = "Adverse Events")]
        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Adverse_Events { get; set; }

        [Display(Name = "Quality Complaints")]
        [Range(0, 32000, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Quality_Complaints { get; set; }
    }

    public partial class AbbottAgentsDailyProfileData
    {
        [DataType(DataType.Date)]
        [Required(ErrorMessage = "Date is required")]
        [Display(Name = "Date")]
        public Nullable<System.DateTime> Date { get; set; }

        [Required(ErrorMessage = "Agent ID is required")]
        [Display(Name = "Agent ID")]
        public Nullable<int> AgentID { get; set; }


        [Display(Name = "INDV Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Individual_Sales { get; set; }


        [Display(Name = "INDV Doses Influvac")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Individual_Doses_Influvac { get; set; }

        [Display(Name = "INDV Doses Imuvac")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Individual_Doses_Imuvac { get; set; }

        [Display(Name = "BG Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> BG_Sales { get; set; }

        [Display(Name = "BG Doses Influvac")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> BG_Doses_Influvac { get; set; }

        [Display(Name = "BG Doses Imuvac")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> BG_Doses_Imuvac { get; set; }


        [Display(Name = "New Customer Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> New_Customer_Sales { get; set; }

        [Display(Name = "New Customer Doses")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> New_Customer_Doses { get; set; }

        [Display(Name = "Lost Customer Sales")]
        [Range(0, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Lost_Customer_Sales { get; set; }

        [Display(Name = "Lost Customer Doses")]
        [Range(-2147483647, 2147483647, ErrorMessage = "Please Enter a Valid Number")]
        public Nullable<int> Lost_Customer_Doses { get; set; }
    }

    public partial class AbbottAgentProfileData
    {
        [Display(Name = "Agent Name")]
        public string AgentName { get; set; }
    }
}