//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace BentleyInterface.Models
{
    using System;
    using System.Collections.Generic;
    
    public partial class AbbottWeeklyQuality
    {
        public int ID { get; set; }
        public Nullable<System.DateTime> Week_Start_Date { get; set; }
        public Nullable<int> AgentID { get; set; }
        public Nullable<double> Call_Quality { get; set; }
        public Nullable<double> Order_Data_Entry_Quality { get; set; }
        public Nullable<double> Notes_Quality { get; set; }
    
        public virtual AbbottAgent AbbottAgent { get; set; }
    }
}
