﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace BentleyInterface.Models
{
    using System;
    using System.Collections.Generic;
    using System.ComponentModel.DataAnnotations;

    [MetadataType(typeof(CCProfileMetadata))]
    public partial class CCData
    {
        
    }

    [MetadataType(typeof(CEMDailyDataProfileData))]
    public partial class CEMDailyData
    {
    }

    [MetadataType(typeof(RegionProfileData))]
    public partial class Region
    {
    }

    [MetadataType(typeof(CEMMonthlyDataProfileData))]
    public partial class CEMMonthlyData
    {
    }
    
    [MetadataType(typeof(KnowledgeBaseProfileData))]
    public partial class KnowledgeBase
    {
    }

    [MetadataType(typeof(CategoryProfileData))]
    public partial class Category
    {
    }

    [MetadataType(typeof(CampaignProfileData))]
    public partial class campaign
    {

    }
    
    [MetadataType(typeof(CFProfileData))]
    public partial class COMPLAINTFORM
    {
    }

     [MetadataType(typeof(CFCampaingProfileData))]
    public partial class CFCampaing
    {
    }

    [MetadataType(typeof(AbbottAgentsDailyProfileData))]
     public partial class AbbottAgentsDaily
     {
     }

    [MetadataType(typeof(AbbottWeeklyQualityProfileData))]
    public partial class AbbottWeeklyQuality
    {
    }

    [MetadataType(typeof(AbbottWeeklyTeamProfileData))]
    public partial class AbbottWeeklyTeam
    {
    }

    [MetadataType(typeof(AbbottAgentProfileData))]
    public partial class AbbottAgent
    {

    }

}