﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    public class CCController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /CC/
        public ActionResult Index()
        {
            var CurrentDate = System.DateTime.Now.AddDays(-70);
            var ccdatas = db.CCDatas.Include(c => c.Region).Where(c => c.Date > CurrentDate).OrderByDescending(x => x.Date);
            return View(ccdatas.ToList());
        }

        // GET: /CC/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CCData ccdata = db.CCDatas.Find(id);
            if (ccdata == null)
            {
                return HttpNotFound();
            }
            return View(ccdata);
        }

        // GET: /CC/Create
        [Authorize(Roles = "TL")]
        public ActionResult Create()
        {
           
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1");
            return View();
        }

        // POST: /CC/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Create([Bind(Include="ID,RegionID,Date,Emails_Received,Emails_Responded_Within_SLA,Fax_Received,Fax_Responded_Within_SLA,Letters_Received,Letters_Responded_Within_SLA,Open_Concern,Closed_Concern,InProgress_Concern,DWS_Leads,PLE_Leads,HQ_Profile_Quality,Hours_delivered,MSP")] CCData ccdata)
        {
            if (ccdata.Date == null)
            {
                ccdata.Date = DateTime.Now;
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);

                ModelState.AddModelError("Date", "The Date Value is Invalid.");
                return View(ccdata);
            }
            int count;
            if (ccdata.Date != null && ccdata.RegionID != null)
                count = db.CCDatas.Where(y => y.Date == ccdata.Date).Where(y => y.RegionID == ccdata.RegionID).Count();
            else
                count = 0;
            if (count > 0)
            {
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);
                ViewBag.Exists = "This Date and Region Combination already exists";
                ModelState.AddModelError("Date", "This Date and Region Combination already exists");
                return View(ccdata);
            }
            if (ModelState.IsValid)
            {
                db.CCDatas.Add(ccdata);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);
            return View(ccdata);
        }

        // GET: /CC/Edit/5
        [Authorize(Roles = "TL")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CCData ccdata = db.CCDatas.Find(id);
            if (ccdata == null)
            {
                return HttpNotFound();
            }
       
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);
            return View(ccdata);
        }

        // POST: /CC/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Edit([Bind(Include="ID,RegionID,Date,Emails_Received,Emails_Responded_Within_SLA,Fax_Received,Fax_Responded_Within_SLA,Letters_Received,Letters_Responded_Within_SLA,Open_Concern,Closed_Concern,InProgress_Concern,DWS_Leads,PLE_Leads,HQ_Profile_Quality,Hours_delivered,MSP")] CCData ccdata)
        {
            if (ccdata.Date == null)
            {
                ccdata.Date = DateTime.Now;
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);

                ModelState.AddModelError("Date", "The Date Value is Invalid.");
                return View(ccdata);
            }

            int count;
            if (ccdata.Date != null && ccdata.RegionID != null)
                count = db.CCDatas.Where(y => y.Date == ccdata.Date).Where(y => y.RegionID == ccdata.RegionID).Count();
            else
                count = 0;

            CCData cd = new CCData();
            if (count > 0 && ccdata.Date != null && ccdata.RegionID != null)
                cd = db.CCDatas.Where(y => y.Date == ccdata.Date).Where(y => y.RegionID == ccdata.RegionID).Single();

            if (count > 0 && cd.ID != ccdata.ID)
            {
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);
                ViewBag.Exists = "This Date and Region Combination already exists";
                ModelState.AddModelError("Date", "This Date and Region Combination already exists");
                return View(ccdata);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if (count == 0)
                        db.Entry(ccdata).State = EntityState.Modified;
                    else
                        db.Entry(cd).CurrentValues.SetValues(ccdata);
                    
                    db.SaveChanges();
                }
                catch(InvalidOperationException)
                {
                    ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);

                    ModelState.AddModelError("Hours_delivered", "Invalid Operation");
                    return View(ccdata);
                }
                return RedirectToAction("Index");
            }
       
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", ccdata.RegionID);
            return View(ccdata);
        }

        // GET: /CC/Delete/5
        [Authorize(Roles = "TL")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CCData ccdata = db.CCDatas.Find(id);
            if (ccdata == null)
            {
                return HttpNotFound();
            }
            return View(ccdata);
        }

        // POST: /CC/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult DeleteConfirmed(int id)
        {
          //  CCData ccdata = db.CCDatas.Find(id);
            //db.CCDatas.Remove(ccdata);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
