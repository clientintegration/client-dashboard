﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    [Authorize(Roles = "AbbottTL")]
    public class AbbottWeeklyQualityController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /AbbottWeeklyQuality/
        public ActionResult Index()
        {
            var CurrentDate = DateTime.Now.AddDays(-60);
            var abbottweeklyqualities = db.AbbottWeeklyQualities.Include(a => a.AbbottAgent);
            return View(abbottweeklyqualities.Where(x => x.Week_Start_Date> CurrentDate).OrderByDescending(x => x.Week_Start_Date).ToList());
        }

        // GET: /AbbottWeeklyQuality/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyQuality abbottweeklyquality = db.AbbottWeeklyQualities.Find(id);
            if (abbottweeklyquality == null)
            {
                return HttpNotFound();
            }
            return View(abbottweeklyquality);
        }

        // GET: /AbbottWeeklyQuality/Create
        public ActionResult Create(int id = 0)
        {
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x=>x.Active == 1), "ID", "AgentName");
            if (id == 1)
                ViewBag.message = "The Record has been added.";
            return View();
        }

        // POST: /AbbottWeeklyQuality/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Week_Start_Date,AgentID,Call_Quality,Order_Data_Entry_Quality,Notes_Quality")] AbbottWeeklyQuality abbottweeklyquality)
        {
            if (abbottweeklyquality.Week_Start_Date == null)
            {
                abbottweeklyquality.Week_Start_Date = DateTime.Now;
                ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottweeklyquality.AgentID);

                ModelState.AddModelError("Week_Start_Date", "The Date Value is Invalid.");
                return View(abbottweeklyquality);
            }

            String Day = abbottweeklyquality.Week_Start_Date.Value.DayOfWeek.ToString();

            switch (Day)
            {
                case "Tuesday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-1);
                    break;
                case "Wednesday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-2);
                    break;
                case "Thursday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-3);
                    break;
                case "Friday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-4);
                    break;
                case "Saturday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-5);
                    break;
                case "Sunday":
                    abbottweeklyquality.Week_Start_Date = abbottweeklyquality.Week_Start_Date.Value.AddDays(-6);
                    break;
                default:
                    break;
            }

            int count;
            if (abbottweeklyquality.Week_Start_Date != null && abbottweeklyquality.AgentID != null)
                count = db.AbbottWeeklyQualities.Where(y => y.Week_Start_Date == abbottweeklyquality.Week_Start_Date).Where(y => y.AgentID == abbottweeklyquality.AgentID).Count();
            else
                count = 0;
            if (count > 0)
            {
                ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottweeklyquality.AgentID);
                ViewBag.Exists = "This Date and Agent ID Combination already exists";
                ModelState.AddModelError("Week_Start_Date", "This Date and Agent ID Combination already exists");
                return View(abbottweeklyquality);
            }

            if (ModelState.IsValid)
            {
                db.AbbottWeeklyQualities.Add(abbottweeklyquality);
                db.SaveChanges();
                return RedirectToAction("Create", new { id = 1 });
            }

            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottweeklyquality.AgentID);
            return View(abbottweeklyquality);
        }

        // GET: /AbbottWeeklyQuality/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyQuality abbottweeklyquality = db.AbbottWeeklyQualities.Find(id);
            if (abbottweeklyquality == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottweeklyquality.AgentID);
            return View(abbottweeklyquality);
        }

        // POST: /AbbottWeeklyQuality/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Week_Start_Date,AgentID,Call_Quality,Order_Data_Entry_Quality,Notes_Quality")] AbbottWeeklyQuality abbottweeklyquality)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abbottweeklyquality).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottweeklyquality.AgentID);
            return View(abbottweeklyquality);
        }

        // GET: /AbbottWeeklyQuality/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyQuality abbottweeklyquality = db.AbbottWeeklyQualities.Find(id);
            if (abbottweeklyquality == null)
            {
                return HttpNotFound();
            }
            return View(abbottweeklyquality);
        }

        // POST: /AbbottWeeklyQuality/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AbbottWeeklyQuality abbottweeklyquality = db.AbbottWeeklyQualities.Find(id);
            db.AbbottWeeklyQualities.Remove(abbottweeklyquality);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
