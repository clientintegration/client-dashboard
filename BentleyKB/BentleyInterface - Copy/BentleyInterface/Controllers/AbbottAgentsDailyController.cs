﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    [Authorize(Roles = "AbbottTL")]
    public class AbbottAgentsDailyController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /AbbottAgentsDaily/
        public ActionResult Index()
        {
            var CurrentDate = DateTime.Now.AddDays(-60);
            var abbottagentsdailies = db.AbbottAgentsDailies.Include(a => a.AbbottAgent);
            return View(abbottagentsdailies.Where(x=>x.Date > CurrentDate).OrderByDescending(x=>x.Date).ToList());
        }

        // GET: /AbbottAgentsDaily/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottAgentsDaily abbottagentsdaily = db.AbbottAgentsDailies.Find(id);
            if (abbottagentsdaily == null)
            {
                return HttpNotFound();
            }
            return View(abbottagentsdaily);
        }

        // GET: /AbbottAgentsDaily/Create
        public ActionResult Create(int id = 0)
        {
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x=>x.Active==1) , "ID", "AgentName");
            if(id == 1)
                ViewBag.message = "The Record has been added.";
            return View();
        }

        // POST: /AbbottAgentsDaily/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Date,AgentID,Individual_Sales,Individual_Doses_Influvac,Individual_Doses_Imuvac,BG_Sales,BG_Doses_Influvac,BG_Doses_Imuvac,New_Customer_Sales,New_Customer_Doses,Lost_Customer_Sales,Lost_Customer_Doses")] AbbottAgentsDaily abbottagentsdaily)
        {

            if (abbottagentsdaily.Date == null)
            {
                abbottagentsdaily.Date = DateTime.Now;
                ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x=>x.Active == 1), "ID", "AgentName", abbottagentsdaily.AgentID);

                ModelState.AddModelError("Date", "The Date Value is Invalid.");
                return View(abbottagentsdaily);
            }

            int count;
            if (abbottagentsdaily.Date != null && abbottagentsdaily.AgentID != null)
                count = db.AbbottAgentsDailies.Where(y => y.Date == abbottagentsdaily.Date).Where(y => y.AgentID == abbottagentsdaily.AgentID).Count();
            else
                count = 0;
            if (count > 0)
            {
                ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottagentsdaily.AgentID);
                ViewBag.Exists = "This Date and Agent ID Combination already exists";
                ModelState.AddModelError("Date", "This Date and Agent ID Combination already exists");
                return View(abbottagentsdaily);
            }

            if (ModelState.IsValid)
            {
                db.AbbottAgentsDailies.Add(abbottagentsdaily);
                db.SaveChanges();
                return RedirectToAction("Create", new { id=1});
            }

            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottagentsdaily.AgentID);
            return View(abbottagentsdaily);
        }

        // GET: /AbbottAgentsDaily/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottAgentsDaily abbottagentsdaily = db.AbbottAgentsDailies.Find(id);
            if (abbottagentsdaily == null)
            {
                return HttpNotFound();
            }
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottagentsdaily.AgentID);
            return View(abbottagentsdaily);
        }

        // POST: /AbbottAgentsDaily/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Date,AgentID,Individual_Sales,Individual_Doses_Influvac,Individual_Doses_Imuvac,BG_Sales,BG_Doses_Influvac,BG_Doses_Imuvac,New_Customer_Sales,New_Customer_Doses,Lost_Customer_Sales,Lost_Customer_Doses")] AbbottAgentsDaily abbottagentsdaily)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abbottagentsdaily).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.AgentID = new SelectList(db.AbbottAgents.Where(x => x.Active == 1), "ID", "AgentName", abbottagentsdaily.AgentID);
            return View(abbottagentsdaily);
        }

        // GET: /AbbottAgentsDaily/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottAgentsDaily abbottagentsdaily = db.AbbottAgentsDailies.Find(id);
            if (abbottagentsdaily == null)
            {
                return HttpNotFound();
            }
            return View(abbottagentsdaily);
        }

        // POST: /AbbottAgentsDaily/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AbbottAgentsDaily abbottagentsdaily = db.AbbottAgentsDailies.Find(id);
            db.AbbottAgentsDailies.Remove(abbottagentsdaily);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
