﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    public class KBController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /KB/
        public ActionResult Index()
        {


            var finalList = new List<List<KnowledgeBase>>();
            foreach(Category C in db.Categories)
            {
                if (User.IsInRole(C.Campaign.Campaign1))
                {
                    var knowledgebases = db.KnowledgeBases.Include(k => k.Category1).Where(k => k.Category == C.ID).OrderByDescending(k => k.date);

                    if (knowledgebases.Count() > 5)
                        finalList.Add(knowledgebases.Take(5).ToList());
                    else
                        finalList.Add(knowledgebases.ToList());
                }
            }
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Category1");
            return View(finalList);
        }

        public ActionResult Search(string SearchCriteria)
        {

            if (Request.Form["SearchCriteria"] != null)
                SearchCriteria = Request.Form["SearchCriteria"].ToString();
            else if(SearchCriteria == null)
                SearchCriteria = "";

            Session.Add("SearchCriteria", SearchCriteria);


            var finalList = new List<List<KnowledgeBase>>();

                foreach (Category C in db.Categories)
                {
                    if (User.IsInRole(C.Campaign.Campaign1))
                    {
                        var knowledgebases = from Knowledgebase in db.KnowledgeBases
                                             orderby Knowledgebase.date descending
                                             where ((Knowledgebase.Title.Contains(SearchCriteria) || Knowledgebase.Description.Contains(SearchCriteria)) && Knowledgebase.Category == C.ID)
                                             select Knowledgebase;

                        //db.KnowledgeBases.Where( .Where(k => k.Category == C.ID).Where(k => k.Title.Contains(Title)).Where(k=>k.Description.Contains(Content)).OrderByDescending(k => k.date);
                        if (knowledgebases.Count() > 0)
                            finalList.Add(knowledgebases.ToList());
                    }
                }
            
           
            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Category1");
            return View(finalList);
        }

        public ActionResult AAIC(int? id = 1)
        {
            var finalList = new List<List<KnowledgeBase>>();

          
                var knowledgebases = db.KnowledgeBases.Include(k => k.Category1).Where(k => k.Category == id).OrderByDescending(k => k.date);

                finalList.Add(knowledgebases.ToList());

            ViewBag.CategoryID = new SelectList(db.Categories, "ID", "Category1");
            return View(finalList);
        }

        // GET: /KB/Details/5
        public ActionResult Details(int? id,int? Choice)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnowledgeBase knowledgebase = db.KnowledgeBases.Find(id);
            if (knowledgebase == null)
            {
                return HttpNotFound();
            }
            ViewBag.Choice = Choice;
            return View(knowledgebase);
        }

        // GET: /KB/Create
        [Authorize(Roles = "KBEditor")]
        public ActionResult Create()
        {
            ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1");
            ViewBag.Category = new SelectList(db.Categories.Where(y=> y.CampaignID == -1), "ID", "Category1");
            return View();
        }

        // POST: /KB/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "KBEditor")]
        public ActionResult Create([Bind(Include="ID,Title,Category,Description")] KnowledgeBase knowledgebase)
        {
            HttpPostedFileBase file = Request.Files["Filepath"];
            string extension = System.IO.Path.GetExtension(file.FileName);

            if ((file != null) && file.ContentLength > 104857600)
            {
                ModelState.AddModelError("Filepath", "The File Size is greater than 100Mb.");

            }
            else if (extension.Contains(".Exe"))
            {
                ModelState.AddModelError("Filepath", "Executable file cannot be uploded.");
            }
            else if (ModelState.IsValid)
            {
                if ((file != null) && file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                {
                    string path1 = Server.MapPath("~/Files") +"\\"+ file.FileName;
                    if (System.IO.File.Exists(path1))
                        System.IO.File.Delete(path1);
                    file.SaveAs(path1);
                }

                if (file != null && file.ContentLength > 0)
                {
                    knowledgebase.Filepath = Request.Files["Filepath"].FileName;
                    knowledgebase.ContentType = file.ContentType;
                }
                knowledgebase.date = DateTime.Now;
                db.KnowledgeBases.Add(knowledgebase);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            if (knowledgebase.Category < 1 )
            {

                int CampaignID;
                if (Request["Campaigns"] != null && Request["Campaigns"] != "")
                    CampaignID = Convert.ToInt32(Request["Campaigns"]);
                else
                    CampaignID = -1;


                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == CampaignID), "ID", "Category1", knowledgebase.Category);
            }

            else
            {
                int CampaignID;
                if (Request["Campaigns"] != null && Request["Campaigns"] != "")
                    CampaignID = Convert.ToInt32(Request["Campaigns"]);
                else
                    CampaignID = -1;


                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == CampaignID), "ID", "Category1", knowledgebase.Category);
            }
            return View(knowledgebase);
        }

        // GET: /KB/Edit/5
        [Authorize(Roles = "KBEditor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnowledgeBase knowledgebase = db.KnowledgeBases.Find(id);
            if (knowledgebase == null)
            {
                return HttpNotFound();
            }
            if (knowledgebase.Category < 1 )
            {
                int CampaignID;
                if (Request["Campaigns"] != null && Request["Campaigns"] != "")
                    CampaignID = Convert.ToInt32(Request["Campaigns"]);
                else
                    CampaignID = -1;


                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == CampaignID), "ID", "Category1", knowledgebase.Category);
            }
            else
            {

                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", knowledgebase.Category1.CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == knowledgebase.Category1.CampaignID), "ID", "Category1", knowledgebase.Category);
            }
            return View(knowledgebase);
        }

        // POST: /KB/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "KBEditor")]
        public ActionResult Edit([Bind(Include="ID,Title,Category,Description,date,Filepath,ContentType")] KnowledgeBase knowledgebase)
        {
            HttpPostedFileBase file = Request.Files["FilepathNew"];

            if ((file != null) && file.ContentLength > 104857600)
            {
                ModelState.AddModelError("Filepath", "The File Size is greater than 100Mb.");

            }
            else if (ModelState.IsValid)
            {
                if ((file != null) && file.ContentLength > 0 && !string.IsNullOrEmpty(file.FileName))
                {
                    string extension = System.IO.Path.GetExtension(file.FileName);
                    string path1 = Server.MapPath("~/Files") + "\\" + file.FileName;
                    if (System.IO.File.Exists(path1))
                        System.IO.File.Delete(path1);

                    file.SaveAs(path1);
                }
                if (file != null && file.ContentLength > 0)
                {
                    knowledgebase.Filepath = Request.Files["FilepathNew"].FileName;
                    knowledgebase.ContentType = file.ContentType;
                }
                db.Entry(knowledgebase).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }



            if (knowledgebase.Category < 1)
            {
                int CampaignID;
                if (Request["Campaigns"] != null && Request["Campaigns"] != "")
                    CampaignID = Convert.ToInt32(Request["Campaigns"]);
                else
                    CampaignID = -1;


                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == CampaignID), "ID", "Category1", knowledgebase.Category);
                
            }
            else
            {
                int CampaignID;
                if (Request["Campaigns"] != null && Request["Campaigns"] != "")
                    CampaignID = Convert.ToInt32(Request["Campaigns"]);
                else
                    CampaignID = -1;


                ViewBag.Campaigns = new SelectList(GetCampaigns(), "ID", "Campaign1", CampaignID);
                ViewBag.Category = new SelectList(db.Categories.Where(y => y.CampaignID == CampaignID), "ID", "Category1", knowledgebase.Category);
            }

            return View(knowledgebase);
        }

        // GET: /KB/Delete/5
        [Authorize(Roles = "KBEditor")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            KnowledgeBase knowledgebase = db.KnowledgeBases.Find(id);
            if (knowledgebase == null)
            {
                return HttpNotFound();
            }
            return View(knowledgebase);
        }

        // POST: /KB/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "KBEditor")]
        public ActionResult DeleteConfirmed(int id)
        {
            KnowledgeBase knowledgebase = db.KnowledgeBases.Find(id);
            db.KnowledgeBases.Remove(knowledgebase);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        public FileResult Download(string FilePath,string ContentT)
        {
            string path1 = Server.MapPath("~/Files") + "\\" + FilePath;
            string extension = System.IO.Path.GetExtension(FilePath);
            if (System.IO.File.Exists(path1))
                return File(path1, ContentType(extension), FilePath);
            else 
            {
              path1 = Server.MapPath("~/Files") + "\\" + "FileNotFound.txt";
              extension = System.IO.Path.GetExtension("FileNotFound.txt");
              return File(path1, ContentType(extension), "FileNotFound.txt");
            }

        }

        //LoadCategoriesbyCampaign
        //[AcceptVerbs(HttpVerbs.Get)]
        public JsonResult LoadCategoriesbyCampaign(int? id = -1)
        {
            //Your Code For Getting Physicans Goes Here

            var categoryList = db.Categories.Where(y=>y.CampaignID == id);


            var categoryData = categoryList.Select(m => new SelectListItem()
            {
                Text = m.Category1,
                Value = m.ID.ToString(),

            });
            return Json(categoryData, JsonRequestBehavior.AllowGet);
        }

        private string ContentType(string extension)
        {
            switch (extension)
            { 
                case ".bmp":
                    return "image/bmp";
                case ".gif":
                    return "image/gif";
                case ".jpeg":
                    return ".jpeg";
                case ".jpg":
                    return ".jpeg";
                case ".png":
                    return "image/png";
                case ".tif":
                    return "image/tiff";
                case ".tiff":
                    return "image/tiff";
                case ".doc":
                    return "application/msword";
                case ".docx":
                    return "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
                case ".pdf":
                    return "application/pdf";
                case ".ppt":
                    return "application/vnd.ms-powerpoint";
                case ".pptx":
                    return "application/vnd.openxmlformats-officedocument.presentationml.presentation";
                case ".xlsx":
                    return "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
                case ".xls":
                    return "application/vnd.ms-excel";
                case ".csv":
                    return "text/csv";
                case ".xml":
                    return "text/xml";
                case ".txt":
                    return "text/plain";
                case ".zip":
                    return "application/zip";
                case ".rar":
                    return "application/zip";
                case ".ogg":
                    return "application/ogg";
                case ".mp3":
                    return "audio/mpeg";
                case ".wma":
                    return "audio/x-ms-wma";
                case ".wav":
                    return "audio/x-wav";
                case ".wmv":
                    return "audio/x-ms-wmv";
                case ".swf":
                    return "application/x-shockwave-flash";
                case ".avi":
                    return "video/avi";
                case ".mp4":
                    return "video/mp4";
                case ".mpeg":
                    return "video/mpeg";
                case ".mpg":
                    return "video/mpeg";
                case ".qt":
                    return "video/quicktime";
                default:
                    return "image/jpeg";
            }
        
        }

        private List<Campaign> GetCampaigns()
        {
            List<Campaign> FinalList = new List<Campaign>();

            foreach (Campaign C in db.Campaigns)
            {
                if (User.IsInRole(C.Campaign1))
                {
                    FinalList.Add(C);

                }
            }
            return FinalList;
        }
        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
