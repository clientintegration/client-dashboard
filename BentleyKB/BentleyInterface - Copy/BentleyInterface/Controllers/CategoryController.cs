﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;
using System.Web.Security;
using System.IO;

namespace BentleyInterface.Controllers
{
    
    public class CategoryController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /Category/
        public ActionResult Index()
        {
           var FinalList = new List<Category>();

            foreach (Category C in db.Categories)
            {
                if (User.IsInRole(C.Campaign.Campaign1))
                {
                    FinalList.Add(C);

                }
            }

            return View(FinalList.ToList());
        }

      

        // GET: /Category/Create
        [Authorize(Roles = "KBEditor")]
        public ActionResult Create()
        {
            ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1");
            return View();
        }

        // POST: /Category/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "KBEditor")]
        public ActionResult Create([Bind(Include = "ID,Category1,CampaignID,Description,ImagePath")] Category category, HttpPostedFileBase file)
        {
         //   var roleNames = Roles.GetRolesForUser().Where(x => x.StartsWith("Campaign"));


            int count;
            if (category.Category1 != null)
                count = db.Categories.Where(y => y.CampaignID == category.CampaignID).Where(y => y.Category1 == category.Category1).Count();
            else
                count = 0;
            if (count > 0)
            {

                ModelState.AddModelError("Category1", "This Category Already exists.");
                ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1");
                return View(category);
            }
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                    try
                    {
                        string path = Path.Combine(Server.MapPath("~/Content"), Path.GetFileName(file.FileName));
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                        file.SaveAs(path);
                        category.ImagePath = Path.GetFileName(file.FileName);
                    }
                    catch (Exception ex)
                    {
                        // ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                db.Categories.Add(category);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1");
            return View(category);
        }

        // GET: /Category/Edit/5
        [Authorize(Roles = "KBEditor")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Category category = db.Categories.Find(id);
            if (category == null)
            {
                return HttpNotFound();
            }
            ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1", category.CampaignID);
            return View(category);
        }

        // POST: /Category/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "KBEditor")]
        public ActionResult Edit([Bind(Include = "ID,Category1,CampaignID,Description,ImagePath")] Category category, HttpPostedFileBase file)
        {
         
            if (category.Category1 == null)
            {
                ModelState.AddModelError("Category1", "The Category is Invalid.");
                ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1", category.CampaignID);
                return View(category);
            }


            int count;
            if (category.Category1 != null)
                count = db.Categories.Where(y => y.CampaignID == category.CampaignID).Where(y => y.Category1 == category.Category1).Count();
            else
                count = 0;

            Category cd = new Category();
            if (count > 0 && category.Category1 != null)
                cd = db.Categories.Where(y => y.CampaignID == category.CampaignID).Where(y => y.Category1 == category.Category1).Single();

            if (count > 0 && cd.ID != category.ID)
            {
                ModelState.AddModelError("Category1", "This Category already exists");
                ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1", category.CampaignID);
                return View(category);
            }
            if ((file != null) && file.ContentLength > 104857600)
            {
                ModelState.AddModelError("Filepath", "The File Size is greater than 100Mb.");

            }
          
            else if (ModelState.IsValid)
            {
                try
                {
                    if (file != null && file.ContentLength > 0)
                        try
                        {
                            string path = Path.Combine(Server.MapPath("~/Content"), Path.GetFileName(file.FileName));
                            if (System.IO.File.Exists(path))
                                System.IO.File.Delete(path);
                            file.SaveAs(path);
                            category.ImagePath = Path.GetFileName(file.FileName);
                        }
                        catch (Exception ex)
                        {
                            // ViewBag.Message = "ERROR:" + ex.Message.ToString();
                        }
                    else
                        category.ImagePath = cd.ImagePath;
                    if (count == 0)
                        db.Entry(category).State = EntityState.Modified;
                    else
                        db.Entry(cd).CurrentValues.SetValues(category);

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                 
                catch (InvalidOperationException)
                {

                    ModelState.AddModelError("Category1", "Invalid Operation");
                    ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1", category.CampaignID);
                    return View(category);
                }


           
            }

           
            ViewBag.CampaignID = new SelectList(GetCampaigns(), "ID", "Campaign1", category.CampaignID);
            return View(category);
        }

       

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private List<Campaign> GetCampaigns()
        {
            List<Campaign> FinalList = new List<Campaign>();

            foreach (Campaign C in db.Campaigns)
            {
                if (User.IsInRole(C.Campaign1))
                {
                    FinalList.Add(C);

                }
            }
            return FinalList;
        }
        public JsonResult GetCategories()
        {
            var CatList = new List<string>();
            var IDList = new List<int>();
            foreach (Category C in db.Categories)
            {
                if (User.IsInRole(C.Campaign.Campaign1))
                {
                    var knowledgebases = db.KnowledgeBases.Where(k => k.Category == C.ID);
                    if (knowledgebases.Count() > 0)
                    {
                        CatList.Add(C.Category1.ToString());
                        IDList.Add(Convert.ToInt32(C.ID));
                    }

                }
            }

            return Json(new { category = CatList, id = IDList }, JsonRequestBehavior.AllowGet);
        }
    
    }
}
