﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    public class CEMMonthlyController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /CEMMonthly/
        public ActionResult Index()
        {
            var cemmonthlydatas = db.CEMMonthlyDatas.Include(c => c.Region).OrderByDescending(x => x.Month);
            return View(cemmonthlydatas.ToList());
        }

        // GET: /CEMMonthly/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMMonthlyData cemmonthlydata = db.CEMMonthlyDatas.Find(id);
            if (cemmonthlydata == null)
            {
                return HttpNotFound();
            }
            return View(cemmonthlydata);
        }

        // GET: /CEMMonthly/Create
        [Authorize(Roles = "TL")]
        public ActionResult Create()
        {
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1");
            return View();
        }

        // POST: /CEMMonthly/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Create([Bind(Include="ID,RegionID,Month,Records_Received,Pending_Records,Completes")] CEMMonthlyData cemmonthlydata)
        {

            if (cemmonthlydata.Month == null)
            {
                cemmonthlydata.Month = DateTime.Now.ToString();
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);

                ModelState.AddModelError("Month", "The Month Value is Invalid.");
                return View(cemmonthlydata);
            }
            else
            {
                cemmonthlydata.Month = cemmonthlydata.Month.Substring(0,7);
            }
            int count;
            if (cemmonthlydata.Month != null && cemmonthlydata.RegionID != null)
                count = db.CEMMonthlyDatas.Where(y => y.Month == cemmonthlydata.Month).Where(y => y.RegionID == cemmonthlydata.RegionID).Count();
            else
                count = 0;
            if (count > 0)
            {
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);
                ViewBag.Exists = "This Date and Region Combination already exists";
                ModelState.AddModelError("Month", "This Month and Region Combination already exists");
                return View(cemmonthlydata);
            }
            if (ModelState.IsValid)
            {
                
                db.CEMMonthlyDatas.Add(cemmonthlydata);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);
            return View(cemmonthlydata);
        }
        
        // GET: /CEMMonthly/Edit/5
        [Authorize(Roles = "TL")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMMonthlyData cemmonthlydata = db.CEMMonthlyDatas.Find(id);
            if (cemmonthlydata == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);
            return View(cemmonthlydata);
        }

        // POST: /CEMMonthly/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Edit([Bind(Include="ID,RegionID,Month,Records_Received,Pending_Records,Completes")] CEMMonthlyData cemmonthlydata)
        {
            if (cemmonthlydata.Month == null)
            {
                cemmonthlydata.Month = DateTime.Now.ToString();
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);

                ModelState.AddModelError("Month", "The Month Value is Invalid.");
                return View(cemmonthlydata);
            }
            else
            {
                cemmonthlydata.Month = cemmonthlydata.Month.Substring(0, 7);
            }

            int count;
            if (cemmonthlydata.Month != null && cemmonthlydata.RegionID != null)
                count = db.CEMMonthlyDatas.Where(y => y.Month == cemmonthlydata.Month).Where(y => y.RegionID == cemmonthlydata.RegionID).Count();
            else
                count = 0;

            CEMMonthlyData cd = new CEMMonthlyData();
            if (count> 0 && cemmonthlydata.Month != null && cemmonthlydata.RegionID != null)
                cd = db.CEMMonthlyDatas.Where(y => y.Month == cemmonthlydata.Month).Where(y => y.RegionID == cemmonthlydata.RegionID).Single();

            if (count > 0 && cd.ID != cemmonthlydata.ID)
            {
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);
                ViewBag.Exists = "This Date and Region Combination already exists";
                ModelState.AddModelError("Month", "This Date and Region Combination already exists");
                return View(cemmonthlydata);
            }
            if (ModelState.IsValid)
            {
                try
                {
                    if (count == 0)
                        db.Entry(cemmonthlydata).State = EntityState.Modified;
                    else
                        db.Entry(cd).CurrentValues.SetValues(cemmonthlydata);
                    
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                catch(InvalidOperationException)
                {
                    ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);

                    ModelState.AddModelError("Month", "Invalid Operation");
                    return View(cemmonthlydata);
                }
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemmonthlydata.RegionID);
            return View(cemmonthlydata);
        }

        // GET: /CEMMonthly/Delete/5
      /*  public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMMonthlyData cemmonthlydata = db.CEMMonthlyDatas.Find(id);
            if (cemmonthlydata == null)
            {
                return HttpNotFound();
            }
            return View(cemmonthlydata);
        }

        // POST: /CEMMonthly/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            CEMMonthlyData cemmonthlydata = db.CEMMonthlyDatas.Find(id);
            db.CEMMonthlyDatas.Remove(cemmonthlydata);
            db.SaveChanges();
            return RedirectToAction("Index");
        }
       * */

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
