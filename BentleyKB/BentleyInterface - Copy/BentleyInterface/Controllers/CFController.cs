﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    [Authorize(Roles = "CFManager")]
    public class CFController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /CF/
        public ActionResult Index()
        {
            var complaintforms = db.COMPLAINTFORMs.Include(c => c.CFCampaing);
            return View(db.COMPLAINTFORMs.ToList().OrderByDescending(x => x.ID));
        }

        // GET: /CF/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COMPLAINTFORM complaintform = db.COMPLAINTFORMs.Find(id);
            if (complaintform == null)
            {
                return HttpNotFound();
            }
            return View(complaintform);
        }

        // GET: /CF/Create
        public ActionResult Create()
        {
           
            ViewBag.Severity = SeverityList();
            ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign");
            return View();
        }

        // POST: /CF/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include = "ID,SubCampaign,Name,Date,Telephone,TimeofCall,Address,Email,BCMS,AgentName,Details,Severity,TeamLeaderName,BentleyAdvisedDate,BentleyContactName,ScannedandEmailedto")] COMPLAINTFORM complaintform)
        {
            int Hour = 0;

            bool b = Int32.TryParse(complaintform.TimeofCall.Substring(0, 2), out Hour);

            if (Hour > 23)
            {
                ModelState.AddModelError("TimeofCall", "Invalid Time");
                ViewBag.Severity = SeverityList();
                ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign", complaintform.SubCampaign);
                return View(complaintform);
            }

            if (ModelState.IsValid)
            {
                complaintform.DateAdded = System.DateTime.Now;
                db.COMPLAINTFORMs.Add(complaintform);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.Severity = SeverityList();
            ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign", complaintform.SubCampaign);
            return View(complaintform);
        }

        // GET: /CF/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COMPLAINTFORM complaintform = db.COMPLAINTFORMs.Find(id);
            if (complaintform == null)
            {
                return HttpNotFound();
            }
            ViewBag.Severity = SeverityList(complaintform.Severity);
            ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign", complaintform.SubCampaign);
            return View(complaintform);
        }

        // POST: /CF/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "ID,SubCampaign,Name,Date,Telephone,TimeofCall,Address,Email,BCMS,AgentName,Details,Severity,TeamLeaderName,BentleyAdvisedDate,BentleyContactName,ScannedandEmailedto,DateAdded")] COMPLAINTFORM complaintform)
        {
            int Hour = 0;

            bool b = Int32.TryParse(complaintform.TimeofCall.Substring(0, 2), out Hour);

            if (Hour > 23)
            {
                ModelState.AddModelError("TimeofCall", "Invalid Time");
                ViewBag.Severity = SeverityList(complaintform.Severity);
                ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign", complaintform.SubCampaign);
                return View(complaintform);
            }

            if (ModelState.IsValid)
            {
                db.Entry(complaintform).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.Severity = SeverityList(complaintform.Severity);
            ViewBag.SubCampaign = new SelectList(db.CFCampaings, "ID", "campaign", complaintform.SubCampaign);
            return View(complaintform);
        }

        // GET: /CF/Delete/5
        [Authorize(Roles="Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            COMPLAINTFORM complaintform = db.COMPLAINTFORMs.Find(id);
            if (complaintform == null)
            {
                return HttpNotFound();
            }
            return View(complaintform);
        }

        // POST: /CF/Delete/5
        [Authorize(Roles = "Admin")]
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            COMPLAINTFORM complaintform = db.COMPLAINTFORMs.Find(id);
            db.COMPLAINTFORMs.Remove(complaintform);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private SelectList SeverityList(string SelectedItem = "-1")
        {


            //Create a list of select list items - this will be returned as your select list
            List<SelectListItem> newList = new List<SelectListItem>();



                 newList.Add(new SelectListItem() { Value = "High", Text = "High" });
                newList.Add(new SelectListItem() { Value = "Medium", Text = "Medium" });
                newList.Add(new SelectListItem() { Value = "Low", Text = "Low" });
        

                //Add select list item to list of selectlistitems


            return new SelectList(newList, "Value", "Text",SelectedItem);

        }
    }
}
