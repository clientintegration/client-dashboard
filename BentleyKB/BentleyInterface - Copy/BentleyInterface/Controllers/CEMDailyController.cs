﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    public class CEMDailyController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /CEMDaily/
        public ActionResult Index()
        {
            var cemdailydatas = db.CEMDailyDatas.Include(c => c.Region).OrderByDescending(x => x.Date);
            return View(cemdailydatas.ToList());
        }

        // GET: /CEMDaily/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMDailyData cemdailydata = db.CEMDailyDatas.Find(id);
            if (cemdailydata == null)
            {
                return HttpNotFound();
            }
            return View(cemdailydata);
        }

        // GET: /CEMDaily/Create
        [Authorize(Roles = "TL")]
        public ActionResult Create()
        {
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1");
            return View();
        }

        // POST: /CEMDaily/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Create([Bind(Include="ID,Date,RegionID,Hours_Scheduled,Hours_Delivered,Data_Added,Completes,Pending_Records")] CEMDailyData cemdailydata)
        {
            if (cemdailydata.Date == null)
            {
                cemdailydata.Date = DateTime.Now;
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
                
                ModelState.AddModelError("Date", "The Date Value is Invalid.");
                return View(cemdailydata);
            }
            int count;
            if (cemdailydata.Date != null && cemdailydata.RegionID != null)
                count = db.CEMDailyDatas.Where(y => y.Date == cemdailydata.Date).Where(y => y.RegionID == cemdailydata.RegionID).Count();
            else
                count = 0;
           if (count > 0)
           {
               ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
               ViewBag.Exists = "This Date and Region Combination already exists";
               ModelState.AddModelError("Date", "This Date and Region Combination already exists");
               return View(cemdailydata);
           }
            if (ModelState.IsValid)
            {
                db.CEMDailyDatas.Add(cemdailydata);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
            return View(cemdailydata);
        }

        // GET: /CEMDaily/Edit/5
        [Authorize(Roles = "TL")]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMDailyData cemdailydata = db.CEMDailyDatas.Find(id);
            if (cemdailydata == null)
            {
                return HttpNotFound();
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
            return View(cemdailydata);
        }

        // POST: /CEMDaily/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "TL")]
        public ActionResult Edit([Bind(Include="ID,Date,RegionID,Hours_Scheduled,Hours_Delivered,Data_Added,Completes,Pending_Records")] CEMDailyData cemdailydata)
        {
            if (cemdailydata.Date == null)
            {
                cemdailydata.Date = DateTime.Now;
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);

                ModelState.AddModelError("Date", "The Date Value is Invalid.");
                return View(cemdailydata);
            }

            int count;
            if (cemdailydata.Date != null && cemdailydata.RegionID != null)
                count = db.CEMDailyDatas.Where(y => y.Date == cemdailydata.Date).Where(y => y.RegionID == cemdailydata.RegionID).Count();
            else
                count = 0;

            CEMDailyData cd = new CEMDailyData();
            if (count > 0 && cemdailydata.Date != null && cemdailydata.RegionID != null)
                cd = db.CEMDailyDatas.Where(y => y.Date == cemdailydata.Date).Where(y => y.RegionID == cemdailydata.RegionID).Single();

            if (count > 0 && cd.ID != cemdailydata.ID)
            {
                ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
                ViewBag.Exists = "This Date and Region Combination already exists";
                ModelState.AddModelError("Date", "This Date and Region Combination already exists");
                return View(cemdailydata);
            }

            if (ModelState.IsValid)
            {
                try
                {
                    if(count == 0)
                        db.Entry(cemdailydata).State = EntityState.Modified;
                    else
                        db.Entry(cd).CurrentValues.SetValues(cemdailydata);
                    db.SaveChanges();
                }
                catch (InvalidOperationException)
                {
                    ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);

                    ModelState.AddModelError("Pending_Records", "Invalid Operation");
                    return View(cemdailydata);
                }
                return RedirectToAction("Index");
            }
            ViewBag.RegionID = new SelectList(db.Regions, "ID", "Region1", cemdailydata.RegionID);
            return View(cemdailydata);
        }

        // GET: /CEMDaily/Delete/5
        [Authorize(Roles = "Admin")]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CEMDailyData cemdailydata = db.CEMDailyDatas.Find(id);
            if (cemdailydata == null)
            {
                return HttpNotFound();
            }
            return View(cemdailydata);
        }

        // POST: /CEMDaily/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        [Authorize(Roles = "Admin")]
        public ActionResult DeleteConfirmed(int id)
        {
         //   CEMDailyData cemdailydata = db.CEMDailyDatas.Find(id);
           // db.CEMDailyDatas.Remove(cemdailydata);
            //db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
