﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using BentleyInterface.Models;

namespace BentleyInterface.Controllers
{
    [Authorize(Roles = "AbbottTL")]
    public class AbbottWeeklyEventsController : Controller
    {
        private BentleyCRMEntities db = new BentleyCRMEntities();

        // GET: /AbbottWeeklyEvents/
        public ActionResult Index()
        {
            return View(db.AbbottWeeklyTeams.ToList().Where(x => x.Week_Start_Date > DateTime.Now.AddDays(-60)).OrderByDescending(x => x.Week_Start_Date));
        }

        // GET: /AbbottWeeklyEvents/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyTeam abbottweeklyteam = db.AbbottWeeklyTeams.Find(id);
            if (abbottweeklyteam == null)
            {
                return HttpNotFound();
            }
            return View(abbottweeklyteam);
        }

        // GET: /AbbottWeeklyEvents/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /AbbottWeeklyEvents/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Create([Bind(Include="ID,Week_Start_Date,Adverse_Events,Quality_Complaints")] AbbottWeeklyTeam abbottweeklyteam)
        {

            if (abbottweeklyteam.Week_Start_Date == null)
            {
                return View(abbottweeklyteam);
            }

            String Day = abbottweeklyteam.Week_Start_Date.Value.DayOfWeek.ToString();

            switch (Day)
            {
                case "Tuesday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-1);
                    break;
                case "Wednesday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-2);
                    break;
                case "Thursday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-3);
                    break;
                case "Friday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-4);
                    break;
                case "Saturday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-5);
                    break;
                case "Sunday":
                    abbottweeklyteam.Week_Start_Date = abbottweeklyteam.Week_Start_Date.Value.AddDays(-6);
                    break;
                default:
                    break;
            }

            int count;
            if (abbottweeklyteam.Week_Start_Date != null)
                count = db.AbbottWeeklyTeams.Where(y => y.Week_Start_Date == abbottweeklyteam.Week_Start_Date).Count();
            else
                count = 0;
            if (count > 0)
            {
                ModelState.AddModelError("Week_Start_Date", "This Date already exists");
                return View(abbottweeklyteam);
            }

            if (ModelState.IsValid)
            {
                db.AbbottWeeklyTeams.Add(abbottweeklyteam);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(abbottweeklyteam);
        }

        // GET: /AbbottWeeklyEvents/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyTeam abbottweeklyteam = db.AbbottWeeklyTeams.Find(id);
            if (abbottweeklyteam == null)
            {
                return HttpNotFound();
            }
            return View(abbottweeklyteam);
        }

        // POST: /AbbottWeeklyEvents/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ID,Week_Start_Date,Adverse_Events,Quality_Complaints")] AbbottWeeklyTeam abbottweeklyteam)
        {
            if (ModelState.IsValid)
            {
                db.Entry(abbottweeklyteam).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(abbottweeklyteam);
        }

        // GET: /AbbottWeeklyEvents/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AbbottWeeklyTeam abbottweeklyteam = db.AbbottWeeklyTeams.Find(id);
            if (abbottweeklyteam == null)
            {
                return HttpNotFound();
            }
            return View(abbottweeklyteam);
        }

        // POST: /AbbottWeeklyEvents/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            AbbottWeeklyTeam abbottweeklyteam = db.AbbottWeeklyTeams.Find(id);
            db.AbbottWeeklyTeams.Remove(abbottweeklyteam);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
