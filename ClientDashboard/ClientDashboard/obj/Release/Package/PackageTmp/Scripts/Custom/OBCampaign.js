﻿var intervalchart, Disppiechart, table, statuspiechart;
var sum = 0, intervalflag = false, statusflag = false, Dispflag = false, cname;
var dataSet = [], total = []
var colors = ["#30bb74", "#2980b9", "#9a7fd1", "#F88F07", "#95a5a6", "#e74c3c", "#1E824C", "#a6dcf1"]
$(function () {

    $("#spinner1").show()
    $("#spinner2").show()
    $("#spinner3").show()
    $("#intervalgraphcontainer").hide()
    $("#DispContainer").hide()
    $("#statuspiecontainer").hide()
    cname = $("#CampaignName").text().toLowerCase();
    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica'

            }
        },
        lang: {
            drillUpText: '< Back'
        }
    });


    $.fn.dataTableExt.sErrMode = 'throw';

    table = $('#campaigntable').dataTable({
        "aoColumnDefs": [{ "bSortable": false }],

        "bSortCellsTop": true,
        //scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,
        "bProcessing": true,
        "order": [[0, "desc"]],
        dom: '<Blf<rt>ip>',
        buttons: [
            {
                extend: 'excelHtml5',

                title: 'Campaign Statistics' + formatDateforTable(new Date()),

            }

        ]

    });
    table.fnProcessingIndicator();
    //***********************************SignalR Events***************************************//
    var notifications = $.connection.statisticsHub;
    setTimeout(function () {
        $.connection.hub.start().done(function () {
            notifications.server.getDialingStatus().done(function (Stats) {
                refreshDialingStatus(Stats)
            });
            notifications.server.getAgentCountStats().done(function (Stats) {
                populateAgentCountStats(Stats)
            });
            notifications.server.getLeadsStats().done(function (Stats) {

                populateLeadsStats(Stats)
            });
            notifications.server.getOBDispositionStats().done(function (Stats) {
                // alert("getDispStats");
                populateDispositionStats(Stats)
            });
            notifications.server.getAgentStatusStats().done(function (Stats) {

                populateAgentStatusStats(Stats)
            });
            notifications.server.getIntervalStats().done(function (Stats) {
                //  alert("updateIntervalStats");
                var oTable = $('#campaigntable').DataTable();
                oTable.clear();
                $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
                var info = oTable.page.info();
                var nRows = info.recordsTotal;
                try {
                    oTable.button(0).enable(nRows > 0);
                }
                catch (Ex)
                { }
            });;
        });
    }, 2000);
    var tryingToReconnect = false;

    $.connection.hub.reconnecting(function () {
        tryingToReconnect = true;
    });

    $.connection.hub.reconnected(function () {
        tryingToReconnect = false;
    });

    $.connection.hub.disconnected(function () {
        if (tryingToReconnect) {
            //$("#lastUpdateTime").text("Disconnected");
            $("#dialog").dialog("open");
            //notifyUserOfDisconnect(); // Your function to notify user.
        }
    });
    notifications.client.updateDialingStatus = function (Stats) {

        refreshDialingStatus(Stats)
    };
    notifications.client.updateAgentCountStats = function (Stats) {
        // alert("updateAgentStateStats");
        populateAgentCountStats(Stats)
    };
    notifications.client.updateLeadsStats = function (Stats) {
        // alert("updateQueueStats");
        populateLeadsStats(Stats)
    };
    notifications.client.updateOBDispositionStats = function (Stats) {
        //alert("updateDispStats");
        populateDispositionStats(Stats)
    };
    notifications.client.updateAgentStatusStats = function (Stats) {

        populateAgentStatusStats(Stats)
    };
    notifications.client.updateIntervalStats = function (Stats) {
        //  alert("updateIntervalStats");
        var oTable = $('#campaigntable').DataTable();
        oTable.clear();
        $('#campaigntable').DataTable().rows.add(populateIntervalStats(Stats)).draw(false);
        var info = oTable.page.info();
        var nRows = info.recordsTotal;
        try {
            oTable.button(0).enable(nRows > 0);
        }
        catch (Ex)
        { }
    };
    //***************************************************************************************//
    intervalflag = false
    $("#dialog").dialog({
        autoOpen: false,
        modal: true,
        resizable: false,
        closeText: 'Close',
        draggable: false,
        minHeight: 100,
        dialogClass: 'main-dialog-class',
        close: function (event, ui) {
            window.location.href = "http://clientdashboard.ibexglobal.com/"
        }
    });
    // $("#dialog").dialog("open");
})
function refreshDialingStatus(result) {
    var list = result;
    var dialstatus;
    $("#DialingStatus").hide();
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            dialstatus = value.DialStatus;

        }
    });
    $("#status").text(cname.toUpperCase() + " is not dialing at the moment.");
    if (dialstatus == "Inactive")
        $("#DialingStatus").show();
}
function populateAgentStatusStats(result) {

    var list = result;
    var states = [], agents = [], statusdata = []
    var i = 0;
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {

            states.push(value.AgentState);
            agents.push(value.AgentCount);
            var temp = new Array(states[i], agents[i]);
            statusdata[i] = temp;
            i++;
        }


    });

    $("#spinner3").hide()
    $("#statuspiecontainer").show()
    statuspiechart = $('#statuspiecontainer').highcharts();
    if (statusflag == false) {
        DrawStatusChart(statusdata)
        statusflag = true;
    }
    else {

        statuspiechart.series[0].setData(statusdata);
    }
    statuspiechart = $('#statuspiecontainer').highcharts();
    if (!statuspiechart.hasData()) {
        statuspiechart.hideNoData();
        statuspiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateLeadsStats(result) {
    var list = result;
    $.each(list, function (index, value) {

        if (value.CampaignID.toLowerCase() == cname) {
            document.getElementById("hi-icon-avail").textContent = value.Available;
        }
    });
    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateAgentCountStats(result) {
    var list = result;
    $("#onWrap").text(0);
    $("#onTalk").text(0);
    $("#onWait").text(0);
    $("#onBreak").text(0);
    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            $("#onWrap").text(value.AgentsOnWrap);
            $("#onTalk").text(value.AgentsOnTalk);
            $("#onWait").text(value.AgentsOnWait);
            $("#onBreak").text(value.AgentsOnBreak);
        }



    });

    $("#lastUpdateTime").text(formatDate(new Date()));

}
function populateDispositionStats(result) {
    $("#spinner1").show()
    $("#DispContainer").hide()
    Array.prototype.contains = function (v) {
        for (var i = 0; i < this.length; i++) {
            if (this[i] === v) return true;
        }
        return false;
    };

    var categories = [], chartdata = [], drilldownSeries = []
    var DispData = [], CallsData = [], i, j, dataLen = chartdata.length, drillDataLen, brightness;
    var list = result
    var i = 0, tempData = []
    $.each(list, function (index, value) {
        tempData = []
        if (value.CampaignID.toLowerCase() == cname) {

            if (!categories.contains(value.DispType)) {
                categories.push(value.DispType)
                //  alert(value.DispType)
                chartdata.push({
                    y: value.CallsPerDispType,
                    color: colors[i],
                    name: categories[i],
                    //drilldown: {

                    //    categories: ['Answered', 'Abandoned'],
                    //    data: [value.CallsHandled, value.CallsAbandon]

                    //}
                })
                //Disposition Drilldown
                $.each(list, function (index1, value1) {
                    if (value1.CampaignID.toLowerCase() == cname) {
                        if (value1.DispType == value.DispType) {
                            tempData.push([value1.Disposition, value1.CallCount]);
                        }
                    }
                });
                DispData.push({
                    name: value.DispType,
                    y: chartdata[i].y,
                    color: chartdata[i].color,
                    drilldown: value.DispType,


                });


                //drillDataLen = chartdata[i].drilldown.data.length;
                //alert(drillDataLen);
                //for (j = 0; j < drillDataLen; j += 1) {
                //    //  alert(chartdata[i].drilldown.data[j]);
                //    brightness = 0.2 - (j / drillDataLen) / 5;
                //    CallsData.push({
                //        name: chartdata[i].drilldown.categories[j],
                //        y: chartdata[i].drilldown.data[j],
                //        color: Highcharts.Color(chartdata[i].color).brighten(brightness).get()
                //    });

                //}

                drilldownSeries.push(
                {


                    // pointWidth:15,

                    showInLegend: true,

                    dataLabels: {
                        style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                        formatter: function () {
                            // display only if larger than 1
                            var s = Math.round((this.y))
                            return this.y > 0 ? this.point.name.substring(0, 2) + ': ' + s + '</b>' : null;
                        }
                    },
                    tooltip: {
                        headerFormat: '<span style="font-size:11px">{series.name}</span><br>',
                        pointFormat: '<span style="color:{point.color}">{point.name}</span>: <b>{point.y:.0f}</b> calls<br/>'
                    },
                    legend: {

                        borderWidth: 0,
                        itemStyle: {
                            "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "normal"
                        }
                    },
                    dataLabels: {
                        enabled: true
                    },
                    startAngle: -90,
                    endAngle: 90,
                    center: ['50%', '75%'],
                    innerSize: '50%',
                    "name": value.DispType,
                    type: 'column',
                    data: tempData,
                    id: value.DispType
                });
                i++;
            }

        }
    });


    $("#spinner1").hide()
    $("#DispContainer").show()


    DrawDispositionChart(DispData, CallsData, drilldownSeries)
    Dispflag = true;

    if (!Disppiechart.hasData()) {
        Disppiechart.hideNoData();
        Disppiechart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
}
function populateIntervalStats(result) {
    dataSet = []
    var oTable = $('#campaigntable').DataTable();
    oTable.clear();
    var list = result;
    var timezone = "";
    $("#spinner2").hide()
    $("#intervalgraphcontainer").show()
    var time = [], ser1 = [], ser2 = [], ser3 = [], total = [], ser4 = [], ser5 = []
    document.getElementById("hi-icon-cont").textContent = 0;
    document.getElementById("hi-icon-conn").textContent = 0;
    document.getElementById("hi-icon-comp").textContent = 0;
    document.getElementById("hi-icon-sale").textContent = 0;
    document.getElementById("hi-icon-aban").textContent = 0;
    document.getElementById("hi-icon-occ").textContent = "0%";

    $.each(list, function (index, value) {
        if (value.CampaignID.toLowerCase() == cname) {
            timezone = value.TimeZone;
            if (value["A"].toLowerCase() != "total") {
                time.push(value["A"])

                ser1.push(parseFloat(value["L"]))
                ser2.push(parseFloat(value["J"]))
                ser3.push(parseFloat(value["N"]))
                ser4.push(parseFloat(value["P"]))
                ser5.push(parseFloat(value["R"]))
            }
            else {

                document.getElementById("hi-icon-cont").textContent = value["L"];
                document.getElementById("hi-icon-conn").textContent = value["J"];
                document.getElementById("hi-icon-comp").textContent = value["N"];
                document.getElementById("hi-icon-sale").textContent = value["P"];
                document.getElementById("hi-icon-aban").textContent = value["R"];
                document.getElementById("hi-icon-occ").textContent = value["H"] + "%";
            }


            var arr = [];
            if (value["A"].toLowerCase() != "total") {

                var colascii = 65;
                for (var i = 0; i < cols ; i++) {

                    arr.push(value[String.fromCharCode(colascii)])
                    colascii++;
                }


                dataSet.push(arr)
            }
            else {
                var colascii = 65;
                for (var i = 0; i < cols ; i++) {

                    total.push(value[String.fromCharCode(colascii)])
                    colascii++;
                }


            }
        }
    });

    if (intervalflag == false) {
        DrawIntervalChart()
        intervalflag = true;
    }

    intervalchart.xAxis[0].setCategories(time);
    intervalchart.series[0].setData(ser1);
    intervalchart.series[1].setData(ser2);
    intervalchart.series[2].setData(ser3);
    intervalchart.series[3].setData(ser4);
    intervalchart.series[4].setData(ser5);
    var xTitle = ""
    if (timezone == "")
        xTitle = "Interval (EST)"
    else
        xTitle = "Interval (" + timezone + ")"

    intervalchart.xAxis[0].update({
        title: {
            text: xTitle
        }
    });

    $('#frow th').each(function (i) {

        if ($(this).text() != "Total")
            $(this).text("");

    });
    if (total.length != 0) {
        $('#frow th').each(function (i) {
            if (total[i] != undefined && $(this).text() != "Total")
                $(this).text(total[i]);

        });
    }
    if (!intervalchart.hasData()) {
        intervalchart.hideNoData();
        intervalchart.showNoData("No data available!");
    }
    $("#lastUpdateTime").text(formatDate(new Date()));
    return dataSet;

}
function DrawStatusChart(statusdata) {

    statuspiechart = $('#statuspiecontainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agents Status',
            align: 'left'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        legend:
            {
                itemStyle: {
                    "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
                }
            },
        series: [{
            name: "Agents",
            colorByPoint: true,
            data: statusdata
        }]
    });
}

function DrawIntervalChart() {

    intervalchart = new Highcharts.Chart({
        chart: {
            type: 'spline',
            renderTo: 'intervalgraphcontainer',
            zoomType: 'x'
        },


        title: {
            text: 'Calls Per Interval',
            align: 'left'

        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of calls'
            },
            allowDecimals: false,
            minTickInterval: 1
        },
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>Interval ' + this.x + '</caption><tbody>';
                //loop each point in this.points
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                //points += '<tr><th>Calls Offered: </th>'
                //+ '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                + '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Contacts',
            data: [],

        },
       {
           name: 'Connects',
           data: [],

       },
       {
           name: 'Completes',
           data: [],

       },
       {
           name: 'Sales',
           data: [],

       },
       {
           name: 'Abandoned',
           data: [],

       }

        ]
    });


}
function DrawDispositionChart(DispData, CallsData, drilldownSeries) {


    Disppiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'DispContainer'
        },


        title: {
            text: 'Calls per Disposition Type',
            align: 'left'
        },
        subtitle: {
            text: ' Click the inner slices to view calls per disposition.',
            align: 'left',
            style: { fontSize: '11px' }
        },
        xAxis: {
            //title: {
            //    text: 'Disposition Codes',

            //},
            type: 'category',
            lineWidth: 0,
            labels: {
                formatter: function () {
                    if (this.value.substring(0, 1) == "S")
                        return '<span style="font-size:12px;font-weight:bold;color:#00b53c">' + this.value.substring(0, 2) + "<span>";
                    else
                        return '<p style="">' + this.value.substring(0, 2) + "<p>";
                }
            }
        },
        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            //layout: 'vertical',
            //align: 'left',
            //verticalAlign: 'bottom',
            enabled: true,
            labelFormatter: function () {
                if (this.name.indexOf('-') === -1)
                    return this.name;
                else
                    return this.name.substring(0, 2);
            },
            borderWidth: 0,
            itemStyle: {
                "color": "#333333", "cursor": "pointer", "fontSize": "11.5px", "fontWeight": "bold"
            }
        },




        plotOptions: {
            pie: {
                shadow: false,
                //startAngle: -90,
                //endAngle: 90,
                //center: ['50%', '75%'],

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                },
                dataLabels: {
                    enabled: false
                },
            },
            allowPointSelect: false

        },


        series: [{
            tooltip: {
                valueSuffix: '',
                formatter: function () {
                    var s = Math.round((this.y))
                    return this.point.name + ':<b>' + s + '</b> calls<br/>'
                }
            },
            animation: false,
            name: 'Calls',
            data: DispData,
            //startAngle: -90,
            //endAngle: 90,
            //center: ['50%', '75%'],
            innerSize: '50%',
            //dataLabels: {
            //    formatter: function () {
            //        return '';
            //    },
            //    color: 'white',
            //    distance: -40,
            //    style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" }
            //},
            showInLegend: true,

        }
        //, {
        //    tooltip: {
        //        valueSuffix: '',
        //        formatter: function () {
        //            var s = Math.round((this.y))
        //            return this.point.name + ':<b>' + s + '</b> calls<br/>'
        //        }
        //    },
        //    animation: false,
        //    name: 'Calls',
        //    data: CallsData,
        //    size: '80%',
        //    innerSize: '60%',
        //    dataLabels: {
        //        style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
        //        formatter: function () {
        //            // display only if larger than 1
        //            var s = Math.round((this.y))
        //            if (this.point.name == "Abandoned")
        //                return this.y > 0 ? '<b style="color:red">' + this.point.name + ': ' + s + '</b>' : null;

        //            else
        //                return this.y > 0 ? '<b style="color:black">' + this.point.name + ':</b> ' + s + '' : null;
        //        }
        //    }
        //}
        ],
        drilldown: {
            //animation: false,
            drillUpButton: {
                relativeTo: 'spacingBox',
                position: {
                    y: 20,
                    x: 0
                },
                theme: {
                    fill: 'white',
                    'stroke-width': 1,
                    stroke: 'silver',
                    r: 0,
                    states: {

                        select: {
                            stroke: '#039',
                            fill: '#bada55'
                        }
                    }
                }

            },
            series: drilldownSeries
        }

    });


}
function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}

