﻿using System;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Threading;
using System.Web.Mvc;
using WebMatrix.WebData;
using ClientDashboard.Models;
using System.Web;
using System.Linq;
using System.Web.Routing;
namespace ClientDashboard.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public sealed class InitializeSimpleMembershipAttribute : ActionFilterAttribute
    {
        private static SimpleMembershipInitializer _initializer;
        private static object _initializerLock = new object();
        private static bool _isInitialized;

        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            // Ensure ASP.NET Simple Membership is initialized only once per app start
            LazyInitializer.EnsureInitialized(ref _initializer, ref _isInitialized, ref _initializerLock);
        }

        private class SimpleMembershipInitializer
        {
            public SimpleMembershipInitializer()
            {
                Database.SetInitializer<UsersContext>(null);

                try
                {
                    using (var context = new UsersContext())
                    {
                        if (!context.Database.Exists())
                        {
                            // Create the SimpleMembership database without Entity Framework migration schema
                            ((IObjectContextAdapter)context).ObjectContext.CreateDatabase();
                        }
                    }

                    // WebSecurity.InitializeDatabaseConnection("DefaultConnection", "UserProfile", "UserId", "UserName", autoCreateTables: false);
                }
                catch (Exception ex)
                {
                    throw new InvalidOperationException("The ASP.NET Simple Membership database could not be initialized. For more information, please see http://go.microsoft.com/fwlink/?LinkId=256588", ex);
                }
            }
        }
    }

    public class LogAttribute : ActionFilterAttribute
    {
        public string Activity { get; set; }
        public override void OnActionExecuting(ActionExecutingContext filterContext)
        {
            //Stores the Request in an Accessible object
            var request = filterContext.HttpContext.Request;

            int currentUserId = WebSecurity.GetUserId(filterContext.HttpContext.User.Identity.Name);
            var timeUtc = DateTime.UtcNow;
            TimeZoneInfo easternZone = TimeZoneInfo.FindSystemTimeZoneById("Eastern Standard Time");
            DateTime easternTime = TimeZoneInfo.ConvertTimeFromUtc(timeUtc, easternZone);
            ActivityLog log = new ActivityLog()
            {
                
                
                UserId = (request.IsAuthenticated) ? currentUserId : 0,
                PageUrl = request.RawUrl,      
                Activity=Activity,
                TimeStamp = easternTime
            };

            ClientDashboardEntities context = new ClientDashboardEntities();
            context.ActivityLogs.Add(log);
            context.SaveChanges();

            //Finishes executing the Action as normal 
            base.OnActionExecuting(filterContext);
        }
    }
    public class CustomAuthorizeAttribute : AuthorizeAttribute
    {
        ClientDashboardEntities context = new ClientDashboardEntities();   
        private readonly string module;
        public CustomAuthorizeAttribute(string moduleName)
        {
            module = moduleName;
        }
        protected override bool AuthorizeCore(HttpContextBase httpContext)
        {
            bool authorize = false;
            if (httpContext.Request.Url.Segments.Length > 3)
            {
                int len = httpContext.Request.Url.Segments.Length;
                string campaign = httpContext.Request.Url.Segments[len - 1];
                int currentUserId = WebSecurity.GetUserId(httpContext.User.Identity.Name);
                var user = context.webpages_UsersInRoles.Include(x => x.webpages_Roles).Include(x => x.Module).Where(x => x.UserId == currentUserId && x.Module.Name == module && x.webpages_Roles.RoleName == campaign);
                if (user.Count() > 0)
                {
                    authorize = true; /* return true if Entity has current user(active) with specific module and campaign*/
                }
            }
            else
                authorize = true;
            return authorize;
        }
        protected override void HandleUnauthorizedRequest(AuthorizationContext filterContext)
        {
            if (!filterContext.HttpContext.User.Identity.IsAuthenticated)
            {
                base.HandleUnauthorizedRequest(filterContext);
            }
            else
            {
                filterContext.Result = new RedirectToRouteResult(new
                RouteValueDictionary(new { controller = "Home", action = "Index" }));
            }
        }
    }  
}
