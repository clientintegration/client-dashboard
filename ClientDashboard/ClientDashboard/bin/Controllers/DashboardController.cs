﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using Newtonsoft.Json;
using WebMatrix.WebData;
namespace ClientDashboard.Controllers
{
    [Authorize]
    public class DashboardController : Controller
    {
        //
        // GET: /Dashboard/
        DateTime startdate = DateTime.Now.Date.AddDays(-3);
        DateTime enddate = DateTime.Now.Date.AddDays(-3);
        LeadmanagementEntities db = new LeadmanagementEntities();
        public ActionResult Index(string id)
        {
            try
            {
                if (id != null)
                {
                    Session["CampaignName"] = id;
                    Session["DialingStatus"] = null;
                    Session["Agent"] = "false";
                    Session["Campaign"] = "false";
                    Session["Recordings"] = "false";
                    int currentUserId = WebSecurity.GetUserId(User.Identity.Name);
                    LeadmanagementEntities db = new LeadmanagementEntities();
                    string[] roles = Roles.GetRolesForUser(User.Identity.Name);
                    //To get the current dialing status
                    //List<string> status = new List<string>();
                    //List<HomeReport_Result> status = db.HomeReport(startdate, enddate, currentUserId).ToList();
                    //foreach (var item in status)
                    //{
                    //    if (item.Campaign == id)
                    //    {
                    //        if (item.DialStatus == "Active")
                    //            Session["DialingStatus"] = null;
                    //        else
                    //            Session["DialingStatus"] = id + " is not dialling at the moment.";
                    //    }
                    //}
                    //To get the client name against the campaign 
                    var client = (from r in db.webpages_Roles
                                  join c in db.Clients on r.ClientID equals c.ClientID
                                  where r.RoleName == id
                                  select new
                                  {
                                      c.Name
                                  }).ToList().FirstOrDefault();
                    Session["ClientName"] = client.Name;
                    //To get the modules assigned to user against the campaign
                    var roleid = (from r in db.webpages_Roles
                                  where (r.RoleName == id)
                                  select r.RoleId).ToList().FirstOrDefault();

                    var modules = new List<string>();
                    var query = (from ur in db.webpages_UsersInRoles
                                 join module in db.Modules on ur.ModuleID equals module.ModuleID
                                 where ur.RoleId == roleid && ur.UserId == currentUserId
                                 select new
                                 {
                                     module.Name
                                 }).ToList();
                    foreach (var result in query)
                    {
                        if (result.Name == "Agent")
                            Session["Agent"] = "true";

                        if (result.Name == "Campaign")
                            Session["Campaign"] = "true";

                        if (result.Name == "Recordings")
                            Session["Recordings"] = "true";

                    }
                    if (Session["Campaign"].ToString() == "true")
                        return RedirectToAction("Campaign");
                    else if (Session["Agent"].ToString() == "true")
                        return RedirectToAction("Agent");
                    else if (Session["Recordings"].ToString() == "true")
                        return RedirectToAction("Recordings");
                    else
                        return View();
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at Index Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
            }
            return RedirectToAction("Index", "Home");
        }
        [Authorize]
        public ActionResult Agent()
        {
            try
            {
                if ((Session["CampaignName"] == null) || Session["Agent"].ToString() != "true")
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                    return View();
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at Agent Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return RedirectToAction("Index", "Home");
            }

        }
        [Authorize]
        public ActionResult Campaign()
        {
            try
            {
                if ((Session["CampaignName"] == null) || Session["Campaign"].ToString() != "true")
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                    return View();
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at Campaign Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return RedirectToAction("Index", "Home");
            }

        }
        [Authorize]
        [HttpGet]
        public ActionResult Recordings()
        {
            try
            {
                TempData["Panel"] = "true";
                TempData["startdate"] = "";
                TempData["enddate"] = "";
                TempData["uniqueid"] = "";
                TempData["tel"] = "";
                TempData["agentid"] = "";

                if ((Session["CampaignName"] == null ) || Session["Recordings"].ToString() != "true")
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                    return View();
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at Recordings Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return RedirectToAction("Index", "Home");
            }

        }
        #region Dashboard data extraction functions
        
        public ActionResult ExtractIntervalData(string id)
        {

            var timeList = new List<string>();
            var CallsOfferedList = new List<string>();
            var CallsHandledList = new List<string>();
            var CallsAbandonedList = new List<string>();
            var IVRAbandonedList = new List<string>();
            var ServiceLevelCallList = new List<string>();
            var AverageSpeedList = new List<string>();
            var OccupancyList = new List<string>();
            var ProductionHoursList = new List<string>();
            var AbandonRateList = new List<string>();
            var AHTList = new List<string>();
            var MWaitTimeList = new List<string>();
            var LoginList = new List<string>();
            var BreakList = new List<string>();
            try
            {
                if (id != null)
                {
                    string CampaignID = id;
                    List<CampaignReport_Result> data = new List<CampaignReport_Result>();
                    data = db.CampaignReport(startdate, enddate, CampaignID).ToList();


                    foreach (var item in data)
                    {
                        timeList.Add(item.Interval.ToString());
                        CallsOfferedList.Add(item.Call_Offered.ToString());
                        CallsHandledList.Add(item.Calls_Handled.ToString());
                        CallsAbandonedList.Add(item.Calls_Abandon.ToString());
                        IVRAbandonedList.Add(item.Calls_Abandon_IVR.ToString());
                        AbandonRateList.Add(item.Abandon_Rate.ToString());
                        AHTList.Add(item.AHT.ToString());
                        ServiceLevelCallList.Add(item.Service_Level_Call__.ToString());
                        MWaitTimeList.Add(item.Max_Wait_Time.ToString());
                        AverageSpeedList.Add(item.ASA.ToString());
                        OccupancyList.Add(item.Occupancy.ToString());
                        LoginList.Add(item.Login.ToString());
                        BreakList.Add(item.Break.ToString());
                        ProductionHoursList.Add(item.Production_Hours.ToString());


                    }
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractCampaignData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));
            }
            return Json(new
            {
                time = timeList,
                CallsOffered = CallsOfferedList,
                CallsHandled = CallsHandledList,
                CallsAbandoned = CallsAbandonedList,
                IVRAbandoned = IVRAbandonedList,
                ServiceLevelCalls = ServiceLevelCallList,
                AHT = AHTList,
                AbandonRate = AbandonRateList,
                MwaitTime = MWaitTimeList,
                AverageSpeed = AverageSpeedList,
                Occupancy = OccupancyList,
                Login = LoginList,
                Break = BreakList,
                ProductionHours = ProductionHoursList
            }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult ExtractTFNQData(string id)
        {
            var offList = new List<double>();
            var ansList = new List<double>();
            var abanList = new List<double>();
            var TFNQList = new List<string>();
            try
            {
                if (id != null)
                {
                  
                    string CampaignID = id;
                    List<TFN_Queue_Report_Result> data = new List<TFN_Queue_Report_Result>();
                    data = db.TFN_Queue_Report(startdate, enddate, CampaignID).ToList();

                    foreach (var item in data)
                    {
                        TFNQList.Add(item.Queue_TFN.ToString());
                        offList.Add(Convert.ToDouble(item.Call_Offered));
                        ansList.Add(Convert.ToDouble(item.Calls_Handled));
                        abanList.Add(Convert.ToDouble(item.Calls_Abandon));

                    }
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractTFNQData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));   
            }
            return Json(new { TFNQ=TFNQList,Off=offList,Ans=ansList,Aban=abanList }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult ExtractAgentData(string id)
        {
            var agentList = new List<string>();
            var agentIDList = new List<string>();
            var stateList = new List<string>();
            var staffedList = new List<string>();
            var availableList = new List<string>();
            var handleList = new List<string>();
            var callsList = new List<string>();
            var breakList = new List<string>();
            var AHTList = new List<string>();
            var productionList = new List<string>();
            var occupancyList = new List<string>();
            var stateTimeList = new List<string>();
            var lastTimeList = new List<string>();
            try
            {
                if (id != null)
                {


                    List<AgentsReport_Result> data = new List<AgentsReport_Result>();
                    string CampaignID = id;
                    data = db.AgentsReport(startdate, enddate, CampaignID).ToList();

                    foreach (var item in data)
                    {
                        agentList.Add(item.Agent_Name.ToString());
                        agentIDList.Add(item.Agent_ID.ToString());
                        staffedList.Add(item.Login_Time.ToString());
                        availableList.Add(item.Available_Time.ToString());
                        handleList.Add(item.Handle_Time.ToString());
                        callsList.Add(item.Calls.ToString());
                        breakList.Add(item.Break_Time.ToString());
                        stateList.Add(item.State.ToString());
                        stateTimeList.Add(item.State_Time.ToString());
                        lastTimeList.Add(item.Time_Since_Last_Call.ToString());
                        productionList.Add(item.Production_Time.ToString());
                        AHTList.Add(item.AHT.ToString());
                        occupancyList.Add((item.Occupancy_.ToString()));
                    }
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractAgentData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));   
            }
            return Json(new { agents = agentList, agentid = agentIDList, state = stateList, staffed = staffedList, available = availableList, handle = handleList, calls = callsList, break1 = breakList, production = productionList, occupancy = occupancyList, stateTime = stateTimeList, lastTime = lastTimeList, AHT = AHTList }, JsonRequestBehavior.AllowGet);



        }
        public ActionResult ExtractAgentStateData(string id)
        {

            var countList = new List<double>();
            try
            {
                if (id != null)
                {
                    List<AgentStatesReport_Result> data = new List<AgentStatesReport_Result>();
                    string CampaignID = id;
                    data = db.AgentStatesReport(startdate, enddate, CampaignID).ToList();

                    countList.Add(Convert.ToDouble(data[0].Talk));
                    countList.Add(Convert.ToDouble(data[0].Wrap));
                    countList.Add(Convert.ToDouble(data[0].Wait));
                    countList.Add(Convert.ToDouble(data[0].Break));
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractCampaignStateData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));   
            }
            return Json(new { agentCount = countList }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult ExtractQueueData(string id)
        {

            var countList = new List<string>();
            try
            {
                if (id != null)
                {
                    List<string> data = new List<string>();
                    string CampaignID = id;
                    data = db.QueueReport( CampaignID).ToList();

                    countList.Add(data[0].ToString());
              
                   
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractQueueData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));
            }
            return Json(new { callCount = countList }, JsonRequestBehavior.AllowGet);


        }
        public ActionResult ExtractAgentStatusData(string id)
        {
            var stateList = new List<string>();
            var countList = new List<double>();
            try
            {
                if (id != null)
                {

                    List<AgentStatusReport_Result> data = new List<AgentStatusReport_Result>();
                    string CampaignID = id;
                    data = db.AgentStatusReport(startdate, enddate, CampaignID).ToList();


                    foreach (var item in data)
                    {
                        stateList.Add(item.Agentstate.ToString());
                       
                        countList.Add(Convert.ToDouble(item.Agent_Count));


                    }
                }
                return Json(new { states = stateList, agents = countList }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at ExtractCampaignSkillData Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return Json(Url.Action("Index", "Home"));   
            }
            


        }
        #endregion

        #region Call Recordings


        [HttpPost]
        public ActionResult Recordings(string startdate, string enddate, string uniqueid, string tel, string agentid)
        {
            try
            {
                LeadmanagementEntities db = new LeadmanagementEntities();
                List<CallRecordingReport_Result> data = new List<CallRecordingReport_Result>();
                string CampaignID = Session["CampaignName"].ToString();
                DataTable dt = DatabaseFunctions.GetData("CDB.[CallRecordingReport]", startdate, enddate, uniqueid, tel, agentid, CampaignID);

                if (dt != null && dt.Rows.Count > 0)
                {
                    for (int row = 0; row < dt.Rows.Count; row++)
                    {
                        CallRecordingReport_Result BRC = new CallRecordingReport_Result();
                        BRC.Agent_ID = dt.Rows[row]["Agent ID"].ToString();
                        BRC.Call_ID = dt.Rows[row]["Call ID"].ToString();
                        BRC.Call_Outcome = dt.Rows[row]["Call Outcome"].ToString();
                        BRC.Call_Type = dt.Rows[row]["Call Type"].ToString();
                        BRC.Calldate = Convert.ToDateTime( dt.Rows[row]["Calldate"].ToString());
                        BRC.Campaign_ID = dt.Rows[row]["Campaign ID"].ToString();
                        BRC.Duration = dt.Rows[row]["Duration"].ToString();
                        BRC.NR_Segment = dt.Rows[row]["NR Segment"].ToString();
                        BRC.Number = dt.Rows[row]["Number"].ToString();
                        BRC.Unique_ID = dt.Rows[row]["Unique ID"].ToString();
                        data.Add(BRC);
                    }

                }
             
 
                #region NR Streaming Player 1.1
                string NRPlayer_IV = ConfigurationManager.AppSettings["NRPlayer_IV"].ToString();
                string NRPlayer_Key = ConfigurationManager.AppSettings["NRPlayer_Key"].ToString();
                byte[] IVbytes = Encoding.ASCII.GetBytes(NRPlayer_IV);
                byte[] Keybytes = Encoding.ASCII.GetBytes(NRPlayer_Key);

                string NRArgument = ConfigurationManager.AppSettings["Storage_locator_IP_Port"].ToString() + " " +
                    ConfigurationManager.AppSettings["AltStorage_locator_IP_Port"].ToString() + " " +
                    ConfigurationManager.AppSettings["Proxy_Server_IP_Port"].ToString() + " " +
                    User.Identity.Name + " " +
                    ConfigurationManager.AppSettings["Comment_string"].ToString() + " " +
                    ConfigurationManager.AppSettings["Saving_string"].ToString() + " ";

                byte[] encryptedtext = null;

                foreach (var item in data)
                {
                    encryptedtext = null;
                    encryptedtext = EncryptStringToBytes(NRArgument + item.NR_Segment, Keybytes, IVbytes);
                    item.NR_Segment = "nrplayer:" + Convert.ToBase64String(encryptedtext);

                }
                #endregion
                TempData["Panel"] = "false";
                TempData["startdate"] = startdate;
                TempData["enddate"] = enddate;
                TempData["uniqueid"] = uniqueid;
                TempData["tel"] = tel;
                TempData["agentid"] = agentid;
                return View(data);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at Recordings Post Dashboard" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return RedirectToAction("Index", "Home");
            }
        }

        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null && plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null && Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null && IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }
        #endregion

    }
}
