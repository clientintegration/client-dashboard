﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Transactions;
using System.Web;
using System.Web.Mvc;
using System.Net.Mail;
using System.Data;
using System.Data.SqlClient;
using ClientDashboard.Classes;
namespace ClientDashboard.Controllers
{
    public static class DatabaseFunctions
    {
        static string connectionstring;
       
        public static DataTable SimpleDatatableStoredProcedure(string query)
        {
            connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();
            DataTable dtable = new DataTable();
            SqlCmd.CommandTimeout = 300;
            SqlCmd.CommandText = query;
            SqlCmd.CommandType = CommandType.Text;
            SqlCmd.Connection = SqlConn;

            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    dtable.Load(SqlCmd.ExecuteReader());
                }


            }
            catch (Exception exp)
            {
                throw exp;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return dtable;

        }
        public static DataTable GetData(string StoredProc, string startdate, string enddate, string uniqueid, string tel, string agentid, string campaignid)
        {


            connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();
            DataTable dtable = new DataTable();
            SqlCmd.CommandTimeout = 300;
            //SqlCmd.CommandText = query;
            //SqlCmd.CommandType = CommandType.Text;
            SqlCmd.CommandText = StoredProc;
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startdate;
            SqlCmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = enddate;
            SqlCmd.Parameters.Add("@UniqueID", SqlDbType.VarChar).Value = uniqueid;
            SqlCmd.Parameters.Add("@number", SqlDbType.VarChar).Value = tel;
            SqlCmd.Parameters.Add("@AgentID", SqlDbType.VarChar).Value = agentid;
            SqlCmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = campaignid;
           
            SqlCmd.Connection = SqlConn;

            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    dtable.Load(SqlCmd.ExecuteReader());
                }


            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at" + StoredProc + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                throw exp;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return dtable;

        }
      
       
    
    }
}
