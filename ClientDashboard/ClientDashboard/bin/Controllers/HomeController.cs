﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using WebMatrix.WebData;
namespace ClientDashboard.Controllers
{
    public class HomeController : Controller
    {
        LeadmanagementEntities db = new LeadmanagementEntities();
        [Authorize]
        public ActionResult Index()
        {
          
            return View();
        }

       
        [Authorize]
        public ActionResult Glossary()
        {

            return View(db.Glossaries.ToList());
        }

        [Authorize]
        public ActionResult Download()
        {
            return View();
        }

        public ActionResult DownloadFile(string windowstype) 
        {
            try
            {
                string FilePath = "";
                if (windowstype == "XP")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Windows_XP_2003_DownloadPath"].ToString();
                }
                else if (windowstype == "Vista")
                {
                    FilePath = ConfigurationManager.AppSettings["NR_Vista_Higer_DownloadPath"].ToString();
                }

                string filename = System.IO.Path.GetFileName(FilePath);
                return File(FilePath, ".zip", filename);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at DownloadFile Home" + User.Identity.Name + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                return RedirectToAction("Recordings","Dashboard");
            }
         
        }

        public ActionResult GetRoles()
        {
            
           
            string[] roles = Roles.GetRolesForUser(User.Identity.Name);

            var clients = new List<string>();
            var images = new List<string>();
            var types = new List<string>();
            var agents = new List<string>();
            var dstatus = new List<string>();
            var role1 = new List<string>();
            try
            {
                List<HomeReport_Result> data = new List<HomeReport_Result>();
              
                int currentUserId = WebSecurity.GetUserId(User.Identity.Name);
                data = db.HomeReport(DateTime.Now.Date, DateTime.Now.Date, currentUserId).ToList();

                foreach (var result in data)
                {
                    role1.Add(result.Campaign.ToString());
                    agents.Add(result.Agents.ToString());
                    dstatus.Add(result.DialStatus.ToString());
                    clients.Add(result.Client.ToString());
                    images.Add(result.ImagePath.ToString());
                    types.Add(result.CampaignType.ToString());
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at GetRoles Home" +User.Identity.Name+ DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
            }
            return Json(new { rolelist = role1, clientlist = clients, imagelist = images, typelist = types,agentlist=agents,dslist=dstatus }, JsonRequestBehavior.AllowGet);


        }

    }
}
