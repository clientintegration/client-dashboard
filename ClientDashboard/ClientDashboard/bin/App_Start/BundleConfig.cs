﻿using System.Web;
using System.Web.Optimization;

namespace ClientDashboard
{
    public class BundleConfig
    {
        // For more information on Bundling, visit http://go.microsoft.com/fwlink/?LinkId=254725
        public static void RegisterBundles(BundleCollection bundles)
        {
           bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryui").Include(
                        "~/Scripts/jquery-ui-{version}.js"));

            //bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
            //            "~/Scripts/jquery.unobtrusive*"//,
            //           // "~/Scripts/jquery.validate*"
            //            ));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new StyleBundle("~/Content/css").Include(
                "~/Content/jquery.dataTables.css",
                "~/Content/fixedColumns.dataTables.css",
             //   "~/Content/dataTables.bootstrap.css",
             //   "~/Content/responsive.bootstrap.css",
                "~/Content/dataTables.responsive.css",
                "~/Content/buttons.dataTables.css",
                "~/Content/site.css"));
            bundles.Add(new ScriptBundle("~/bundles/menu").Include("~/Scripts/jquery.touchSwipe.js"));
            bundles.Add(new ScriptBundle("~/bundles/DataTables").Include(
                       "~/Scripts/DataTables/js/jquery.dataTables.js",
                   //    "~/Scripts/DataTables/js/dataTables.bootstrap.js",
                       "~/Scripts/DataTables/js/dataTables.responsive.js",
                       "~/Scripts/DataTables/js/fnProcessingIndicator.js",
                       "~/Scripts/DataTables/js/dataTables.buttons.js",
                       "~/Scripts/DataTables/js/jszip.js",
                       "~/Scripts/DataTables/js/pdfmake.js",
                       "~/Scripts/DataTables/js/vfs_fonts.js",
                       "~/Scripts/DataTables/js/buttons.html5.js",
                       "~/Scripts/DataTables/js/datatables.fixedcolumns.js"
                       ));

            bundles.Add(new ScriptBundle("~/bundles/agentjs").Include(
                       "~/Scripts/HighCharts/js/highcharts.js",
                       "~/Scripts/HighCharts/js/modules/no-data-to-display.js",
                       "~/Scripts/HighCharts/js/modules/exporting.js",
                       "~/Scripts/HighCharts/js/modules/offline-exporting.js",
                       "~/Scripts/Agent.js"
                       ));
            bundles.Add(new ScriptBundle("~/bundles/campaignjs").Include(
                      "~/Scripts/HighCharts/js/highcharts.js",
                       "~/Scripts/HighCharts/js/modules/no-data-to-display.js",
                       "~/Scripts/HighCharts/js/modules/exporting.js",
                       "~/Scripts/HighCharts/js/modules/offline-exporting.js",
                      "~/Scripts/Campaign.js"

                      ));
            bundles.Add(new ScriptBundle("~/bundles/homejs").Include(
                      "~/Scripts/Slider/modernizr.custom.63321.js",
                       "~/Scripts/Slider/jquery.catslider.js",
                       "~/Scripts/Home.js"
                       
                      ));
            bundles.Add(new StyleBundle("~/Content/themes/base/css").Include(
                        "~/Content/themes/base/jquery.ui.core.css",
                        "~/Content/themes/base/jquery.ui.resizable.css",
                        "~/Content/themes/base/jquery.ui.selectable.css",
                        "~/Content/themes/base/jquery.ui.accordion.css",
                        "~/Content/themes/base/jquery.ui.autocomplete.css",
                        "~/Content/themes/base/jquery.ui.button.css",
                        "~/Content/themes/base/jquery.ui.dialog.css",
                        "~/Content/themes/base/jquery.ui.slider.css",
                        "~/Content/themes/base/jquery.ui.tabs.css",
                        "~/Content/themes/base/jquery.ui.datepicker.css",
                        "~/Content/themes/base/jquery.ui.progressbar.css",
                        "~/Content/themes/base/jquery.ui.theme.css"));
        
        }
    }
}