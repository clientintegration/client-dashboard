﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ClientDashboard.Classes
{
    public class Email
    {
        public bool SendEmail(string subject, string toDistro, string EmailMessage)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);
            
                body = body.Replace("@@BodyOfEmail@@", EmailMessage);

          
                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
                mail.To.Add(toDistro);

                mail.Bcc.Add(CDDistro);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
    }
}