﻿$(function () {
    $('#mi-slider').catslider();
    GetRoles();
});
function GetRoles() {

    $.ajax({
        url: "/Home/GetRoles",
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            Array.prototype.contains = function (v) {
                for (var i = 0; i < this.length; i++) {
                    if (this[i] === v) return true;
                }
                return false;
            };

            Array.prototype.unique = function () {
                var arr = [];
                for (var i = 0; i < this.length; i++) {
                    if (!arr.contains(this[i])) {
                        arr.push(this[i]);
                    }
                }
                return arr;
            }

            var uniqueclient = data.clientlist.unique();
            var uniqueimg = data.imagelist.unique();
            var datalen = uniqueclient.length;
            var htmlString = "";
            htmlString += '<div class="scontainer"><div class="main"><div id="mi-slider" class="mi-slider">'

            if (datalen > 0) {
                for (i = 0; i < datalen; i++) {
                    htmlString += "<ul>  <li ><img style='width:180px;height:44px' src='" + uniqueimg[i] + "' alt='" + uniqueclient[i] + "'></li><br />";
                    for (j = 0; j < data.rolelist.length; j++) {
                        if (data.clientlist[j] == uniqueclient[i]) {
                            htmlString += "   <li><a class='link' href='/Dashboard/Index/" + data.rolelist[j] + "' title='View Dashboard' ><h2><span class='CampName'>" + data.rolelist[j] + " </span><span  class='CampType'>" + data.typelist[j] + "</span></h2><div class='MipStatTxtItems'>";
                            if(data.dslist[j]=="Active")
                                htmlString += "<span class='InboundStatInfo'><span class='glyphicon glyphicon-earphone ActiveDialStat'></span>Dialing Status: Active</span>"
                            else
                                htmlString += "<span class='InboundStatInfo'><span class='glyphicon glyphicon-phone-alt InActiveDialStat'></span>Dialing Status: Inactive</span>"

                            htmlString += "<span class='InboundStatInfo'><span class='glyphicon glyphicon-user LoggedAgentIco'></span>Logged Agents: " + data.agentlist[j] + "</span>"
                            //htmlString += "<br/> <span class='glyphicon glyphicon-phone-alt' style='margin-left:-12px;font-weight:bold;font-size:15px;color: #DF314D; padding-right: 5px'></span>Logged Agents: 15</span>"

                            htmlString += "</div></a></li>"
                        }
                    }
                    htmlString += "</ul>";

                }


                htmlString += "<nav>";
                for (i = 0; i < uniqueclient.length; i++) {
                    htmlString += "<a href='#'>" + uniqueclient[i] + "</a>"
                }
                htmlString += "</nav> </div>    </div></div>";
            }
            else
                htmlString += "<ul>  <li > <span class='glyphicon glyphicon-ban-circle' style='color: #DF314D; padding-right: 5px'></span><span style='font-weight:bold;font-size:large'>No campaign is assigned!</span></ul></li><nav></nav></div>    </div></div>"

            $(htmlString).appendTo("#test");
            $('#mi-slider').catslider();
        },
        error: function (msg) {

        }
    });


}
