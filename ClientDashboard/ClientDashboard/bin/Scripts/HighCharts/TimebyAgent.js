﻿var chart1;
$(function () {
    DrawChart()
    GetData("SELECT agentid,count(*) as count1,count(*) as count2 from webdialer_PRODTEAM..CAMPAIGN_MIC where agentid<>'' group by agentid")

    
        var dataSet = [      ];
        $('#example').dataTable({
            "data": dataSet,

            columnDefs: [{
                targets: [0],
                orderData: [0, 1]
            }, {
                targets: [1],
                orderData: [1, 0]
            }, {
                targets: [4],
                orderData: [4, 0]
            }]
        });
  

});

function GetData(q) {
    var data = { key: q };

    $.ajax({
        url: "/Home/ExtractData",
        type: "POST",
        data: JSON.stringify({key:q}),
        traditional: true,
        contentType: 'application/json',
        datatype:'json',
        success: function (data) {
            
            var agents=[]
            for (i = 0; i < data.agents.length; i++) {
                agents.push(data.agents[i])
            }
            var count1=[],count2=[],count3=[],count4=[],count5=[]
            for (i = 0; i < data.count1.length; i++) {
                count1.push(data.count1[i])
                count2.push(data.count2[i])
                count3.push(data.count2[i])
                count4.push(data.count2[i])
                count5.push(data.count2[i])
            }
           
            chart1.xAxis[0].setCategories(agents);
            chart1.series[0].setData(count1);
            chart1.series[1].setData(count2);
            chart1.series[2].setData(count3);
            chart1.series[3].setData(count4);
            chart1.series[4].setData(count5);
           
            var oTable = $('#example').dataTable();
            var dataSet = []
            for (i = 0; i < data.count1.length; i++) {
                var arr=[];
                for (j = 0; j < 5; j++) {
                    arr.push(data.agents[i]);
                    arr.push(data.count1[i]);
                    arr.push(data.count2[i]);
                    arr.push(data.count3[i]);
                    arr.push(data.count4[i]);
                    arr.push(data.count5[i]);
                }
                dataSet.push(arr)

            }
            // Immediately 'nuke' the current rows (perhaps waiting for an Ajax callback...)
            oTable.fnClearTable();

            $('#example').dataTable().fnAddData(dataSet);
            var timer=setTimeout(GetData(q), 500000);
          
        },
        error: function (msg) { clearTimeout(timer) }
    });


}
function DrawChart() {
    chart1 = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'container'
        },
        title: {
            text: ''
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: ''
            }
        },
        tooltip: {
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y:.1f} mm</b></td></tr>',
            footerFormat: '</table>',
            shared: true,
            useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
               // animation: false
            }
        },
        series: [{
            name: 'Staffed Time',
            data: []

        }, {
            name: 'Available Time',
            data: []

        }, {
            name: 'Handle Time',
            data: []

        }, {
            name: 'Hold Time',
            data: []

        }, {
            name: 'Login Time',
            data: []

        }]
    });
}