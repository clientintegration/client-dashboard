﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Data;
using ClientDashboard.Classes;

namespace ClientDashboard.Models
{
    public class HistoricalReport_Result
    {
        public string Call_Type { get; set; }
        public string Campaign_ID { get; set; }
        public string Unique_ID { get; set; }
        public string Call_ID { get; set; }
        public string Agent_ID { get; set; }
        public string Agent_Name { get; set; }
        public string Number { get; set; }
        public System.DateTime Calldate { get; set; }        
        public string Duration { get; set; }
        public string Call_Outcome { get; set; }
        public string DurationMM { get; set; }
        public string DurationSS { get; set; }
        public string CallTimeHH { get; set; }
        public string CallTimeMM { get; set; }
        public string CallTimeSS { get; set; }
        public string Connect { get; set; }
        public List<HistoricalReport_Result> GetHistoricalReport_Result(string startdate, string enddate, string uniqueid, string tel, string agentid, string campaignid, string termcd)
        {
            var result = new List<HistoricalReport_Result>();
            string connectionstring = System.Configuration.ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString;
            SqlConnection SqlConn = new SqlConnection(connectionstring);
            SqlCommand SqlCmd = new SqlCommand();

            SqlCmd = new SqlCommand();

            SqlCmd.CommandTimeout = 300;
            SqlCmd.CommandText = "HistoricalReport";
            SqlCmd.CommandType = CommandType.StoredProcedure;
            SqlCmd.Parameters.Add("@StartDate", SqlDbType.VarChar).Value = startdate;
            SqlCmd.Parameters.Add("@EndDate", SqlDbType.VarChar).Value = enddate;
            SqlCmd.Parameters.Add("@UniqueID", SqlDbType.VarChar).Value = uniqueid;
            SqlCmd.Parameters.Add("@number", SqlDbType.VarChar).Value = tel;
            SqlCmd.Parameters.Add("@AgentID", SqlDbType.VarChar).Value = agentid;
            SqlCmd.Parameters.Add("@CampaignID", SqlDbType.VarChar).Value = campaignid;
            SqlCmd.Parameters.Add("@Termcd", SqlDbType.VarChar).Value = termcd;

            SqlCmd.Connection = SqlConn;

            try
            {
                if (SqlConn.State == ConnectionState.Closed)
                {
                    SqlConn.Open();
                    var reader = SqlCmd.ExecuteReader();
                    while (reader.Read())
                    {
                        result.Add(item: new HistoricalReport_Result
                        {
                            Agent_ID = (string)reader["Agent ID"],
                            Call_ID = (string)reader["Call ID"],
                            Call_Outcome = (string)reader["Call Outcome"],
                            Call_Type = (string)reader["Call Type"],
                            Calldate = Convert.ToDateTime((string)reader["Calldate"]),
                            Campaign_ID = (string)reader["Campaign ID"],
                            Duration = (string)reader["Duration"],
                            Unique_ID = (string)reader["Unique ID"],                           
                            Connect = (string)reader["Connect"],
                            Number = (string)reader["Number"],
                            DurationMM = (string)reader["DurationMM"],
                            DurationSS = (string)reader["DurationSS"],
                            CallTimeHH = (string)reader["CallTimeHH"],
                            CallTimeMM = (string)reader["CallTimeMM"],
                            CallTimeSS = (string)reader["CallTimeSS"],
                            Agent_Name = (string)reader["Agent Name"]

                        });
                    }
                }


            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendEmail("Exception at GetSearchResults " + DateTime.Now, "IBEXTech.ClientDashboard@ibexglobal.com", exp.ToString());
                throw exp;
            }
            finally
            {
                if (SqlConn.State == ConnectionState.Open)
                    SqlConn.Close();
            }
            return result;
        }
    }


}