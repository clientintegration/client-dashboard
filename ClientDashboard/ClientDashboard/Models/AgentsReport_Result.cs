//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientDashboard.Models
{
    using System;
    
    public partial class AgentsReport_Result
    {
        public string Agent_Name { get; set; }
        public string Agent_ID { get; set; }
        public string Calls { get; set; }
        public string State { get; set; }
        public string State_Time { get; set; }
        public string Time_Since_Last_Call { get; set; }
        public string AHT { get; set; }
        public string Login_Time { get; set; }
        public string Break_Time { get; set; }
        public string Available_Time { get; set; }
        public string Handle_Time { get; set; }
        public string Production_Time { get; set; }
        public string Occupancy_ { get; set; }
    }
}
