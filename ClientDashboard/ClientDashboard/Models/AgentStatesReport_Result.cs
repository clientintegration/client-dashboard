//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientDashboard.Models
{
    using System;
    
    public partial class AgentStatesReport_Result
    {
        public int Talk { get; set; }
        public int Wrap { get; set; }
        public int Wait { get; set; }
        public int Break { get; set; }
    }
}
