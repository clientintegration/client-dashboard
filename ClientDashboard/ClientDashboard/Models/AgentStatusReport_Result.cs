//------------------------------------------------------------------------------
// <auto-generated>
//    This code was generated from a template.
//
//    Manual changes to this file may cause unexpected behavior in your application.
//    Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace ClientDashboard.Models
{
    using System;
    
    public partial class AgentStatusReport_Result
    {
        public string Agentstate { get; set; }
        public Nullable<int> Agent_Count { get; set; }
    }
}
