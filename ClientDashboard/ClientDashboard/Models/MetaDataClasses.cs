﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations.Schema;
namespace ClientDashboard.Models
{
    public class UserProfileMetaData
    {
        public UserProfileMetaData()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
        [Key]
        public int UserId { get; set; }
        public string UserName { get; set; }
        public string Client { get; set; }
        public string Email { get; set; }
        public string Name { get; set; }
    
        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
    public class UsersInRolesMetaData
    {
        [Key]
        [Column(Order = 1)]
        public int UserId { get; set; }
        [Key]
        [Column(Order = 2)]
        public int RoleId { get; set; }
        public string Description { get; set; }
        [Key]
        [Column(Order = 3)]
        public int ModuleID { get; set; }

        public virtual Module Module { get; set; }
        public virtual UserProfile UserProfile { get; set; }
        public virtual webpages_Roles webpages_Roles { get; set; }

    }
    public class RolesMetaData
    {
        public RolesMetaData()
        {
            this.webpages_UsersInRoles = new HashSet<webpages_UsersInRoles>();
        }
        [Key]
        public int RoleId { get; set; }
        public string RoleName { get; set; }
        public Nullable<int> ClientID { get; set; }
        public string CampaignType { get; set; }

        public virtual Client Client { get; set; }
        public virtual ICollection<webpages_UsersInRoles> webpages_UsersInRoles { get; set; }
    }
    public class MembershipMetaData
    {
        [Key]
        public int UserId { get; set; }
        public Nullable<System.DateTime> CreateDate { get; set; }
        public string ConfirmationToken { get; set; }
        public Nullable<bool> IsConfirmed { get; set; }
        public Nullable<System.DateTime> LastPasswordFailureDate { get; set; }
        public int PasswordFailuresSinceLastSuccess { get; set; }
        public string Password { get; set; }
        public Nullable<System.DateTime> PasswordChangedDate { get; set; }
        public string PasswordSalt { get; set; }
        public string PasswordVerificationToken { get; set; }
        public Nullable<System.DateTime> PasswordVerificationTokenExpirationDate { get; set; }
    }
}