﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace ClientDashboard.Models
{
    [MetadataType(typeof(UserProfileMetaData))]
    public partial class UserProfile
    {

    }

    [MetadataType(typeof(RolesMetaData))]
    public partial class webpages_Roles
    {

    }

    [MetadataType(typeof(MembershipMetaData))]
    public partial class webpages_Membership
    {

    }

    [MetadataType(typeof(UsersInRolesMetaData))]
    public partial class webpages_UsersInRoles
    {

    }

    [MetadataType(typeof(ModuleMetaData))]
    public partial class Module
    {

    }

    [MetadataType(typeof(ClientMetaData))]
    public partial class Client
    {

    }
    [MetadataType(typeof(ReportHeaderMetaData))]
    public partial class ReportHeader
    {

    }
}