﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;

namespace ClientDashboard.Classes
{
    public class Email
    {

        public bool SendEmail(string subject, string toDistro, string EmailMessage)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);

                body = body.Replace("@@BodyOfEmail@@", EmailMessage);


                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
                mail.To.Add(toDistro);

                mail.Bcc.Add(CDDistro);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
        public bool SendExceptionEmail(Exception ex,  string username)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                //System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\IBEXEmailLogo.png"));
                //logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                //string body = File.ReadAllText(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);

                //body = body.Replace("@@BodyOfEmail@@", ex.Source.ToString() + " " + ex.Message.ToString());
                StackTrace st = new StackTrace();
                StackFrame sf = st.GetFrame(1);

                
                string function = sf.GetMethod().Name;
                string body = "Function: " + function+ Environment.NewLine;
                body+=ex.ToString();

                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
               
                mail.To.Add(CDDistro);
                
                mail.Subject = "Exception at "  + username +" " + DateTime.Now;
                mail.Body = body;
                mail.IsBodyHtml = true;
                //mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
        public bool SendEmailToUser(string subject, string[] toDistro,string EmailMessage, HttpPostedFileBase file)
        {
            bool success = false;
            string CDDistro = ConfigurationManager.AppSettings["CDDistro"].ToString();
            try
            {
                SmtpClient emailClient = new SmtpClient(ConfigurationManager.AppSettings["SMTPServer"].ToString());
                MailMessage mail = new MailMessage();
                System.Net.Mail.Attachment logo = new System.Net.Mail.Attachment(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Images\IBEXEmailLogo.png"));
                logo.ContentId = "elogo";
                //ENFORCING THE FORMAT, READ FROM AN HTML FILE
                string body = File.ReadAllText(Path.Combine(System.Web.HttpContext.Current.Request.PhysicalApplicationPath, @"Views\Shared\Email.html")).Replace("@@companylogo@@", logo.ContentId);

                body = body.Replace("@@BodyOfEmail@@", EmailMessage);

                if (file != null)
                {
                    string fileName = Path.GetFileName(file.FileName);
                    mail.Attachments.Add(new Attachment(file.InputStream, fileName));
                }
                    
                mail.From = new MailAddress(ConfigurationManager.AppSettings["FromEmail"].ToString(), "IBEX Global");
                mail.To.Add(CDDistro);
                if(toDistro!=null)
                foreach (var Addr in toDistro)
                    mail.Bcc.Add(Addr);
                mail.Subject = subject;
                mail.Body = body;
                mail.IsBodyHtml = true;
                mail.Attachments.Add(logo);
                emailClient.Timeout = 200000000;
                emailClient.Send(mail);
                success = true;
                mail.Dispose();
                emailClient.Dispose();
                return success;
            }
            catch (Exception exp)
            {

                throw exp;
            }
        }
    }
}