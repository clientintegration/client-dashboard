﻿var piechart1;
$(function () {
    
    DrawPieChart()
    GetPieData("SELECT agentid, cast((count(lcdate)*1.0/4982)*100 as decimal(8,2)) as test from webdialer_PRODTEAM..CAMPAIGN_MIC where agentid<>'' group by agentid")

});

function GetPieData(q) {
    var data = { key: q };

    $.ajax({
        url: "/Home/ExtractPieData",
        type: "POST",
        data: JSON.stringify({ key: q }),
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
           
            var arr = []
         
            for (i = 0; i < data.items[0].skills.length-6;i++)
                arr.push({ name: data.items[0].skills[i], y: data.items[0].calls[i] });
            piechart1.series[0].setData(arr);
        
            var timer=setTimeout(GetPieData(q), 600000);
        },
        error: function (msg) { clearTimeout(timer) }
    });


}
function DrawPieChart() {
    piechart1 = new Highcharts.Chart({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie',
            renderTo: 'piecontainer'
        },
        title: {
            text: ''
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: true,
                    format: '<b>{point.name}</b>: {point.percentage:.1f} %',
                    style: {
                        color: (Highcharts.theme && Highcharts.theme.contrastTextColor) || 'black'
                    }
                }
            }
        },
        series: [{
            name: "Brands",
            colorByPoint: true,
            data: []
        }]
 
    });
}