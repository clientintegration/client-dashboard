﻿var intervalchart, TFNQpiechart, table, statuspiechart;
var sum = 0, intervalflag = false, statusflag = false, TFNQflag = false;
var timer;
var colors = ["#8e44ad", "#AEA8D3", "#1BBC9B", "#52b3d9", "#6C7A89", "#DF314D", "#E08283"];
colors = ["#30bb74", "#95a5a6", "#F88F07", "#9a7fd1", "#2980b9", "#16a085", "#3498db", "#bdc3c7"]
colors = ["#16a085", "#2980b9", "#8e44ad", "#9a7fd1", "#30bb74", "#3498db", "#95a5a6", "#95a5a6"]
colors = ["#30bb74", "#95a5a6", "#2980b9", "#F88F07", "#9a7fd1", "#E87E04", "#1E824C", "#e74c3c"]

var dt = formatDate(new Date());

$(function () {
    $.fn.dataTableExt.sErrMode = 'throw';
    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica'

            }
        }
    });
    $("#lastUpdateTime").text(dt);
    var dataSet = [];

    table = $('#campaigntable1').dataTable({

       
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] }],
        
       
        ajax: {

            url: "/Dashboard/ExtractIntervalData/" + $("#CampaignName").text(),
            type: "POST",
            traditional: true,
            // data: { key: campaign },
            contentType: 'application/json',
            datatype: 'json',
            "dataSrc": function (data) { return GetIntervalData(data) }
        },
        "bSortCellsTop": true,
        scrollY: false,
        scrollX: true,
        scrollCollapse: true,
        paging: true,
        fixedColumns: true,
        dom: '<Blf<rt>ip>',
        buttons: [
			{
			    extend: 'excelHtml5',
			    title: 'Campaign Statistics' + formatDateforTable(new Date())
			}
        ]
    });
   // table.fnProcessingIndicator();
    $("#spinner1").show()
    $("#spinner2").show()
    $("#spinner3").show()
    $("#intervalgraphcontainer").hide()
    $("#TFNQpiecontainer").hide()
    $("#statuspiecontainer").hide()
    GetData()
    timer = setInterval(function () { GetData() }, 15000);
    //$('#refreshCheckbox').change(function () {
    //    GetData()

    //});


});

function GetData() {
    if (document.getElementById('refreshCheckbox').checked) {
        //  GetIntervalData()
        var oTable = $('#campaigntable').DataTable();
        oTable.ajax.reload(null, false);
        GetTFNQData()
        GetAgentStatusData()
        GetAgentStateData()
        GetQueueData()
    }
}
function GetQueueData() {
    $.ajax({
        url: "/Dashboard/ExtractQueueData/" + $("#CampaignName").text(),
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            document.getElementById("hi-icon-que").textContent = data.callCount[0];

        },
        error: function (msg) {

        }
    });

}
//***************************Extract data & draw graph for Agent Status************************************//
function GetAgentStatusData() {
    var statusdata = []
    $.ajax({
        url: "/Dashboard/ExtractAgentStatusData/" + $("#CampaignName").text(),
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            var datalen = data.states.length;
            var states = [], agents = []

            for (i = 0; i < datalen; i++) {
                states.push(data.states[i]);
                agents.push(data.agents[i]);
                var temp = new Array(states[i], agents[i]);
                statusdata[i] = temp;
            }

            $("#spinner3").hide()
            $("#statuspiecontainer").show()
            statuspiechart = $('#statuspiecontainer').highcharts();
            if (statusflag == false) {
                DrawStatusChart(statusdata)
                statusflag = true;
            }
            else {

                statuspiechart.series[0].setData(statusdata);
            }
            statuspiechart = $('#statuspiecontainer').highcharts();
            if (!statuspiechart.hasData()) {
                statuspiechart.hideNoData();
                statuspiechart.showNoData("No data available!");
            }

        },
        error: function (msg) {

        }
    });

}
function DrawStatusChart(statusdata) {

    statuspiechart = $('#statuspiecontainer').highcharts({
        chart: {
            plotBackgroundColor: null,
            plotBorderWidth: null,
            plotShadow: false,
            type: 'pie'
        },
        title: {
            text: 'Agents Status',
            align:'left'
        },
        tooltip: {
            pointFormat: '{series.name}: <b>{point.y:.0f}</b>'
        },
        plotOptions: {
            pie: {
                allowPointSelect: true,
                cursor: 'pointer',
                dataLabels: {
                    enabled: false
                },
                showInLegend: true
            }
        },
        series: [{
            name: "Agents",
            colorByPoint: true,
            data: statusdata
        }]
    });
}

//***************************Extract data & populate icons for number of agents on each state*****************************//
function GetAgentStateData() {
    $.ajax({
        url: "/Dashboard/ExtractAgentStateData/" + $("#CampaignName").text(),
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            $("#onWrap").text(data.agentCount[1]);
            $("#onTalk").text(data.agentCount[0]);
            $("#onWait").text(data.agentCount[2]);
            $("#onBreak").text(data.agentCount[3]);
            $("#DialingStatus").hide();
            if ((data.agentCount[0] + data.agentCount[1] + data.agentCount[2] + data.agentCount[3]) < 1) {

                $("#DialingStatus").show();
            }
        },
        error: function (msg) {

        }
    });

}

//***************************Extract data & draw graph for calls Per interval*****************************//
function GetIntervalData(data) {


    var dataSet = []
    $("#spinner2").hide()
    $("#intervalgraphcontainer").show()
    var time = [], ser1 = [], ser2 = [], ser3 = []
    var cname = $("#CampaignName").text();
    //Data Extraction for Bar Chart

    if (data.time.length > 2) {
      
        for (i = 2; i < data.time.length; i++) {
            time.push(data.time[i])
            ser1.push(parseFloat(data.CallsHandled[i]))
            ser2.push(parseFloat(data.CallsAbandoned[i]) + parseFloat(data.IVRAbandoned[i]))
            ser3.push(parseFloat(data.CallsOffered[i]))
        }

        if (intervalflag == false) {
            DrawIntervalChart()
            intervalflag = true;
        }

        intervalchart.xAxis[0].setCategories(time);
        intervalchart.series[1].setData(ser1);
        intervalchart.series[0].setData(ser2);
        // campaignchart.series[2].setData(ser3);



        //Population of circular icons for answered, abandoned, etc
        document.getElementById("hi-icon-off").textContent = data.CallsOffered[1];
        document.getElementById("hi-icon-ans").textContent = data.CallsHandled[1];
        document.getElementById("hi-icon-aban").textContent = parseFloat(data.CallsAbandoned[1]) + parseFloat(data.IVRAbandoned[1]);
        document.getElementById("hi-icon-occ").textContent = data.Occupancy[1];

        //Data Extraction for Grid/Table
        var oTable = $('#campaigntable').dataTable();
       var total=[]
       for (i = 1; i < data.time.length; i++) {
           var arr = [];
           if (i == 1) {
               total.push(data.time[i]);
               total.push(data.CallsOffered[i]);
               total.push(data.CallsHandled[i]);
               if (cname == "FBT")
                   total.push(data.IVRAbandoned[i]);
               total.push(data.CallsAbandoned[i]);
               total.push(data.AbandonRate[i]);
               total.push(data.AHT[i]);
               total.push(data.ServiceLevelCalls[i]);
               total.push(data.MwaitTime[i]);
               total.push(data.AverageSpeed[i]);
               total.push(data.Occupancy[i]);
               total.push(data.Login[i]);
               total.push(data.Break[i]);
               total.push(data.ProductionHours[i]);
           }
           else {

               arr.push(data.time[i]);
               arr.push(data.CallsOffered[i]);
               arr.push(data.CallsHandled[i]);
               if (cname == "FBT")
                   arr.push(data.IVRAbandoned[i]);
               arr.push(data.CallsAbandoned[i]);
               arr.push(data.AbandonRate[i]);
               arr.push(data.AHT[i]);
               arr.push(data.ServiceLevelCalls[i]);
               arr.push(data.MwaitTime[i]);
               arr.push(data.AverageSpeed[i]);
               arr.push(data.Occupancy[i]);
               arr.push(data.Login[i]);
               arr.push(data.Break[i]);
               arr.push(data.ProductionHours[i]);
               dataSet.push(arr)
           }
            

        }
      
        //var totalrow = "<tr id='frow' style='text-align:right;background-color:#fff;color:#000;font-size:10px;font-weight:bolder'><th>Total</th><th>" + data.CallsOffered[1] + "</th><th>" + data.CallsHandled[1] + "</th> "
        //if (cname == "FBT")
        //    totalrow += "<th>" + data.IVRAbandoned[1] + "</th>";
        //totalrow += "<th>" + data.CallsAbandoned[1] + "</th>";
        //totalrow += "<th>" + data.AbandonRate[1] + "</th><th>" + data.AHT[1] + "</th>  <th>" + data.ServiceLevelCalls[1] + "</th><th>" + data.MwaitTime[1] + "</th>";
        //totalrow += "<th>" + data.AverageSpeed[1] + "</th>  <th>" + data.Occupancy[1] + "</th>  <th>" + data.Login[1] + "</th><th>" + data.Break[1] + "</th><th>" + data.ProductionHours[1] + "</th></tr>"
        //$("#frow").replaceWith(totalrow)
       $('#frow th').each(function (i) {
           if (total[i] != undefined)
               $(this).text("" + total[i]);

       });
        dt = formatDate(new Date());
        $("#lastUpdateTime").text(dt);
    }
    else {

        document.getElementById("hi-icon-off").textContent = "0";
        document.getElementById("hi-icon-ans").textContent = "0";
        document.getElementById("hi-icon-aban").textContent = "0";
        document.getElementById("hi-icon-occ").textContent = "0";
        if (intervalflag == false) {
            DrawIntervalChart()
            intervalflag = true;
        }

       
        dt = formatDate(new Date());
        $("#lastUpdateTime").text(dt);
        intervalchart.xAxis[0].setCategories(time);
        intervalchart.series[1].setData(ser1);
        intervalchart.series[0].setData(ser2);
        if (!intervalchart.hasData()) {
            intervalchart.hideNoData();
            intervalchart.showNoData("No data available!");
        }
    }
    return dataSet;

}

function DrawIntervalChart() {

    intervalchart = new Highcharts.Chart({
        chart: {
            type: 'column',
            renderTo: 'intervalgraphcontainer'
        },


        title: {
            text: 'Calls Per Interval',
            align: 'left'

        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: {
            min: 0,
            title: {
                text: 'Number of calls'
            }
        },
        tooltip: {
            borderColor: '#ccc',
            formatter: function () {
                var points = '<table class="tip"><caption>Interval ' + this.x + '</caption><tbody>';
                //loop each point in this.points
                $.each(this.points, function (i, point) {
                    points += '<tr><th style="color: ' + point.series.color + '">' + point.series.name + ': </th>'
                          + '<td style="text-align: right">' + point.y + '</td></tr>'
                });
                points += '<tr><th>Calls Offered: </th>'
                + '<td style="text-align:right"><b>' + this.points[0].total + '</b></td></tr>'
                + '</tbody></table>';
                return points;
            },
            headerFormat: '<span style="font-size:10px">{point.key}</span><table>',
            pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
                '<td style="padding:0"><b>{point.y} </b></td></tr>',

            shared: true,
            useHTML: true
        },
        legend: {
            reversed: true
        },

        plotOptions: {
            column: {
                stacking: 'normal',
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Calls Abandoned',
            data: [],
            color: "#a6dcf1"

        },
        {
            name: 'Calls Handled',
            data: [],
            color: "#52b3d9"

        }
        //, {
        //    name: 'Calls Offered',
        //    data: [],
        //    color: 'rgba(126,86,134,.9)'


        //}

        ]
    });


}

//***************************Extract data & draw pie chart for calls Per TFN/Queue*****************************//
var total;

function GetTFNQData() {
    var categories = [], chartdata = []

    $.ajax({
        url: "/Dashboard/ExtractTFNQData/" + $("#CampaignName").text(),
        type: "POST",

        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            var datalen = data.TFNQ.length;
            set = [];

            for (i = 0; i < datalen; i++) {
                categories.push(data.TFNQ[i])

                chartdata.push({
                    y: data.Off[i],
                    color: colors[i],
                    name: categories[i],
                    drilldown: {

                        categories: ['Answered', 'Abandoned'],
                        data: [data.Ans[i], data.Aban[i]]

                    }
                })
            }

            var TFNQData = [], CallsData = [], i, j, dataLen = chartdata.length, drillDataLen, brightness;


            // Build the data arrays
            for (i = 0; i < dataLen; i += 1) {

                TFNQData.push({
                    name: categories[i],
                    y: chartdata[i].y,
                    color: chartdata[i].color
                });


                drillDataLen = chartdata[i].drilldown.data.length;
                for (j = 0; j < drillDataLen; j += 1) {
                    brightness = 0.2 - (j / drillDataLen) / 5;
                    CallsData.push({
                        name: chartdata[i].drilldown.categories[j],
                        y: chartdata[i].drilldown.data[j],
                        color: Highcharts.Color(chartdata[i].color).brighten(brightness).get()
                    });
                }
            }
            $("#spinner1").hide()
            $("#TFNQpiecontainer").show()

            if (TFNQflag == false) {
                DrawTFNQPieChart(TFNQData, CallsData)
                TFNQflag = true;
            }
            else {

                TFNQpiechart.series[0].setData(TFNQData);
                TFNQpiechart.series[1].setData(CallsData);
            }
            if (!TFNQpiechart.hasData()) {
                TFNQpiechart.hideNoData();
                TFNQpiechart.showNoData("No data available!");
            }




        },
        error: function (msg) {


            clearTimeout(timer)
        }
    });


}
function DrawTFNQPieChart(TFNQData, CallsData) {

    // Create the chart
    TFNQpiechart = new Highcharts.Chart({
        chart: {
            type: 'pie',
            renderTo: 'TFNQpiecontainer'
        },
        title: {
            text: 'Calls Per TFN/Queue',
            align: 'left'
        },

        yAxis: {
            title: {
                text: ''
            }
        },
        legend: {
            layout: 'vertical',
            align: 'left',
            verticalAlign: 'bottom',
            enabled: true,
            borderWidth: 0,
            //itemStyle: {
            //    "color": "#333333", "cursor": "pointer", "fontSize": "12px", "fontWeight": "bold" 
            //}
        },




        plotOptions: {
            pie: {
                shadow: false,
                center: ['50%', '50%'],

                showInLegend: false,
                events: {
                    legendItemClick: function () {
                        return false;
                    }
                }
            },
            allowPointSelect: false

        },

        tooltip: {
            valueSuffix: '',
            formatter: function () {
                var s = Math.round((this.y))
                return this.point.name + ':<b>' + s + '</b> calls<br/>'
            }
        },
        series: [{
            animation: false,
            name: 'Offered',
            data: TFNQData,
            size: '60%',
            dataLabels: {
                formatter: function () {
                    return '';
                },
                color: 'white',
                distance: -40,
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" }
            },
            showInLegend: true,

        }, {
            animation: false,
            name: 'Calls',
            data: CallsData,
            size: '80%',
            innerSize: '60%',
            dataLabels: {
                style: { "color": "contrast", "fontSize": "11px", "fontWeight": "normal", "textShadow": "0 0 6px contrast, 0 0 3px contrast" },
                formatter: function () {
                    // display only if larger than 1
                    var s = Math.round((this.y))
                    if (this.point.name == "Abandoned")
                        return this.y > 0 ? '<b style="color:red">' + this.point.name + ': ' + s + '</b>' : null;
                    else
                        return this.y > 0 ? '<b style="color:black">' + this.point.name + ':</b> ' + s + '' : null;
                }
            }
        }]

    });


}


function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}


