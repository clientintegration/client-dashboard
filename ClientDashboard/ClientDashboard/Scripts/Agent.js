﻿var test = 0;
var barchart;
var table;
var occthreshold = -1;
var agentnames = [], agents = []
var flag = false;
var colors = ["#1BBC9B", "#f39c12", "#52b3d9", "#8e44ad", "#95a5a6", "#2980b9"];
colors = ["#30bb74", "#E67E22", "#2980b9", "#7f8c8d", "#F88F07", "#E87E04", "#27ae60", "#e74c3c"]
var dt = formatDate(new Date());
$(function () {
    Highcharts.setOptions({
        colors: colors,
        chart: {
            style: {
                fontFamily: 'Helvetica',
                fontWeight: 'normal'
            }
        }
    });
    $("#lastUpdateTime").text(dt);
    var dataSet = [];
    table = $('#agenttable').dataTable({
        "aoColumnDefs": [{ "bSortable": false, "aTargets": [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12] }],
        ajax: {

            url: "/Dashboard/ExtractAgentData/" + $("#CampaignName").text(),
            type: "POST",
            traditional: true,
            contentType: 'application/json',
            datatype: 'json',
            "dataSrc": function (data) { return GetAgentData(data) }
        },
        "bSortCellsTop": true,
        scrollY:        false,
        scrollX:        true,
        scrollCollapse: true,
        paging:         true,		
        fixedColumns: true,
       
        dom: '<Blf<rt>ip>',
        buttons: [
			{
			    extend: 'excelHtml5',

			    title: 'Agent Statistics' + formatDateforTable(new Date())
			}
        ]
    });
    table.fnProcessingIndicator();
   // $('#agenttable thead').append('<tr id="frow" ></tr>');
    $("#spinner").show()
    $("#graphcontainer").hide()


    //GetData()
    timer = setInterval(function () { GetData() }, 15000);
    $('#refreshCheckbox').change(function () {
        GetData()

    });

});
function GetData() {
    if (document.getElementById('refreshCheckbox').checked) {
        GetDialingStatus()
        var oTable = $('#agenttable').DataTable();
        oTable.ajax.reload(null, false);
    }
}
function GetDialingStatus() {

    $.ajax({
        url: "/Dashboard/ExtractAgentStateData/" + $("#CampaignName").text(),
        type: "POST",
        traditional: true,
        contentType: 'application/json',
        datatype: 'json',
        success: function (data) {
            $("#DialingStatus").hide();
            if ((data.agentCount[0] + data.agentCount[1] + data.agentCount[2] + data.agentCount[3]) < 1) {

                $("#DialingStatus").show();
            }
        },
        error: function (msg) {
            try {
                clearTimeout(timer)
            }
            catch (err) {

            }
        }
    });

}
function GetAgentData(data) {
  
    $("#spinner").hide()
    $("#graphcontainer").show()
  

    var staffed = [], available = [], handle = [], hold = [], break1 = [], production = [], occupancy = []
    var dataSet = []
    var datalen = data.agents.length;
    if (datalen > 2) {
        for (i = 2; i < datalen ; i++) {
            agents.push(data.agentid[i])
            agentnames.push(data.agents[i])
            staffed.push(parseFloat(data.staffed[i]))
            available.push(parseFloat(data.available[i]))
            handle.push(parseFloat(data.handle[i]))
            break1.push(parseFloat(data.break1[i]))
            production.push(parseFloat(data.production[i]))
            occupancy.push(parseFloat(data.occupancy[i]))
        }

        // alert(staffed[0])
        occthreshold = parseFloat(data.occupancy[1])

        // alert(occthreshold);
        var total=[]
        for (i = 1; i < datalen; i++) {
            var arr = [];
            if (i == 1) {
                total.push(data.agents[i]);
                total.push(data.agentid[i]);
                total.push(data.calls[i]);
                total.push(data.state[i]);
                total.push(data.stateTime[i]);
                total.push(data.lastTime[i]);
                total.push(data.AHT[i]);
                total.push(data.staffed[i]);
                total.push(data.break1[i]);
                total.push(data.available[i]);
                total.push(data.handle[i]);
                total.push(data.production[i]);
                total.push(data.occupancy[i]);
            }
            else {
                arr.push(data.agents[i]);
                arr.push(data.agentid[i]);
                arr.push(data.calls[i]);
                arr.push(data.state[i]);
                arr.push(data.stateTime[i]);
                arr.push(data.lastTime[i]);
                arr.push(data.AHT[i]);
                arr.push(data.staffed[i]);
                arr.push(data.break1[i]);
                arr.push(data.available[i]);
                arr.push(data.handle[i]);
                arr.push(data.production[i]);
                arr.push(data.occupancy[i]);

                dataSet.push(arr)
            }

        }
        if (flag == false) {

            flag = true;
            DrawBarChart()
        }
        barchart.xAxis[0].setCategories(agents);
        barchart.yAxis[1].setExtremes(0, 100);
        barchart.series[0].setData(staffed);
        barchart.series[1].setData(available);
        barchart.series[2].setData(handle);
        barchart.series[3].setData(break1);
        barchart.series[4].setData(production);
        barchart.series[5].setData(occupancy);
       
        $('#frow th').each(function (i) {
            if(total[i]!=undefined)
                $(this).text("" + total[i]);
            
        });
       

    }
    else {
        if (flag == false) {

            flag = true;
            DrawBarChart()

        }
        barchart.xAxis[0].setCategories(agents);
        barchart.yAxis[1].setExtremes(0, 100);
        barchart.series[0].setData(staffed);
        barchart.series[1].setData(available);
        barchart.series[2].setData(handle);
        barchart.series[3].setData(break1);
        barchart.series[4].setData(production);
        barchart.series[5].setData(occupancy);
        if (!barchart.hasData()) {
            barchart.hideNoData();
            barchart.showNoData("No data available!");
        }
    }

    barchart.hideLoading()
    table.fnProcessingIndicator(false);
    dt = formatDate(new Date());
    $("#lastUpdateTime").text(dt);
    return dataSet;
}


function DrawBarChart() {
    barchart = new Highcharts.Chart({

        chart: {
            type: 'column',
            renderTo: 'graphcontainer'
        },
        title: {
            text: 'Agent Performance',
            align: 'left'
        },
        subtitle: {
            text: ''
        },
        xAxis: {
            categories: [],
            crosshair: true
        },
        yAxis: [{ // Primary yAxis
            labels: {
                format: '{value} hrs',
                style: {
                    color: '#000'
                }
            },
            title: {
                text: 'Hours',
                style: {
                    color: '#000',
                    fontWeight: 'bold'
                }
            },
            tickInterval: 3

        }, { // Secondary yAxis
            gridLineWidth: 0,
            title: {
                text: 'Percentage',
                style: {
                    color: '#DF314D',
                    fontWeight: 'bold'
                }
            },
            labels: {
                format: '{value} %',
                style: {
                    color: '#DF314D'

                }
            },
            opposite: true,
            threshold: 50


        }],
        tooltip: {
            formatter: function () {
                var points = this.points;
                var pointsLength = points.length;
                var s = '<b>' + this.x + '</b> - ';
                s += agentnames[agents.indexOf(this.x)]
                $.each(this.points, function (i, point) {
                    if (i != pointsLength - 1)
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + ' hrs';
                    else
                        s += '<br/><span style="color:' + point.series.color + '">\u25CF</span> ' + point.series.name + ': ' + point.y + ' %';
                });

                return s;
            },
            // headerFormat: '<span style="font-size:10px"> {agentnames[agents.indexOf(point.x)]} {point.x}</span>',
            //pointFormat: '<tr><td style="color:{series.color};padding:0">{series.name}: </td>' +
            //    '<td style="padding:0"><b>{point.y:.1f}</b><br/></td></tr>',
            //footerFormat: '</table>',
            shared: true,
            //useHTML: true
        },
        plotOptions: {
            column: {
                pointPadding: 0.2,
                borderWidth: 0,
                // animation: false
            }
        },
        series: [{
            name: 'Login Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[0]

        }, {
            name: 'Available Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[1]

        }, {
            name: 'Handle Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[2]

        }, {
            name: 'Break Time',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[3]

        }, {
            name: 'Production Time',
            color: '#ccc',
            data: [],
            tooltip: {
                valueSuffix: ' hrs'
            },
            color: colors[4]

        }, {
            name: 'Occupancy %',
            data: [],
            type: 'spline',
            color: '#DF314D',
            yAxis: 1,
            tooltip: {
                valueSuffix: ' %'
            },
            threshold: occthreshold,
            negativeColor: '#eb525d'
        }

        ]
    });
}
function formatDate(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear() + "  " + strTime;
}
function formatDateforTable(date) {
    var hours = date.getHours();
    var minutes = date.getMinutes();
    var secs = date.getSeconds();
    hours = hours % 12;
    hours = hours ? hours : 12; // the hour '0' should be '12'
    minutes = minutes < 10 ? '0' + minutes : minutes;
    secs = secs < 10 ? '0' + secs : secs;
    var strTime = hours + ':' + minutes + ':' + secs;
    return date.getMonth() + 1 + "/" + date.getDate() + "/" + date.getFullYear();
}