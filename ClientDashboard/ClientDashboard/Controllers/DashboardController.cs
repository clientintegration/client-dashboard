﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using ClientDashboard.Filters;
using ClientDashboard.Hubs;
using ClientDashboard.Models;
using Newtonsoft.Json;
using WebMatrix.WebData;
namespace ClientDashboard.Controllers
{
    //[SessionExpire]
    [Authorize]
    public class DashboardController : Controller
    {

        ClientDashboardEntities db = new ClientDashboardEntities();
        public ActionResult Index(string id)
        {
            try
            {
                if (id != null)
                {
                    //bool agent=false, campaign=false, recording=false;
                    Session["Agent"] = "false";
                    Session["Campaign"] = "false";
                    Session["Recordings"] = "false";
                    int currentUserId = WebSecurity.GetUserId(User.Identity.Name);

                    string[] roles = Roles.GetRolesForUser(User.Identity.Name);


                    //To get the modules assigned to user against the campaign
                    var roleid = (from r in db.webpages_Roles
                                  where (r.RoleName == id)
                                  select r.RoleId).ToList().FirstOrDefault();

                    var modules = new List<string>();
                    var query = (from ur in db.webpages_UsersInRoles
                                 join module in db.Modules on ur.ModuleID equals module.ModuleID
                                 where ur.RoleId == roleid && ur.UserId == currentUserId
                                 select new
                                 {
                                     module.Name
                                 }).ToList();
                    foreach (var result in query)
                    {
                        if (result.Name == "Agent")
                            Session["Agent"] = "true";

                        if (result.Name == "Campaign")
                            Session["Campaign"] = "true";

                        if (result.Name == "Recordings")
                            Session["Recordings"] = "true";

                    }
                    if (Session["Campaign"].ToString() == "true")
                        return RedirectToAction("Campaign", "Dashboard", new { id = id });
                    else if (Session["Agent"].ToString() == "true")
                        return RedirectToAction("Agent", "Dashboard", new { id = id });
                    else if (Session["Recordings"].ToString() == "true")
                        return RedirectToAction("Recordings", "Dashboard", new { id = id });
                    else
                        return RedirectToAction("Index", "Home");
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp,  User.Identity.Name);
            }
            return RedirectToAction("Index", "Home");
        }
     
        [Log(Activity = "Visted Agent Performance")]
        [CustomAuthorize("Agent")]
        public ActionResult Agent(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    string type = db.HomeReports.Where(x => x.CampaignID == id).Select(x => x.CampaignType).FirstOrDefault().ToString();
                    if (type == "Inbound Program")
                        return View("IBAgent");
                    else
                        return View("OBAgent");
                }
                    
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

        }
       
        [Log(Activity = "Visited Campaign Performance")]
        [CustomAuthorize("Campaign")]
        public ActionResult Campaign(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    Session["CampaignName"] = id;
                    string type = db.HomeReports.Where(x => x.CampaignID == id).Select(x => x.CampaignType).FirstOrDefault().ToString();
                    if (type == "Inbound Program")
                        return View("IBCampaign");
                    else
                        return View("OBCampaign");
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

        }
        [Log(Activity = "Visited Call History")]
        [CustomAuthorize("History")]
        public ActionResult History(string id)
        {
            try
            {
                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                {
                    Session["CampaignName"] = id;
                    return View();
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

        }
      
        [HttpGet]
        [Log(Activity = "Visited Call Recordings")]
        [CustomAuthorize("Recordings")]
        public ActionResult Recordings(string id)
        {
            try
            {
                TempData["Panel"] = "true";
                TempData["startdate"] = "";
                TempData["enddate"] = "";
                TempData["uniqueid"] = "";
                TempData["tel"] = "";
                TempData["agentid"] = "";

                if (id == null)
                {
                    return RedirectToAction("Index", "Home");

                }
                else
                    return View();
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

        }

        [Authorize]
        [Log(Activity = "Visited Glossary")]
        [CustomAuthorize("Glossary")]
        public ActionResult Glossary(string id)
        {
            try
            {
                if (id == null)
                    return RedirectToAction("Index", "Home");
                else
                    return View(db.Glossaries.Where(x => x.CampaignID == id).OrderBy(x => x.Term).ToList());
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }
        }

        #region Historical Report
        protected override JsonResult Json(object data, string contentType, System.Text.Encoding contentEncoding, JsonRequestBehavior behavior)
        {
            return new JsonResult()
            {
                Data = data,
                ContentType = contentType,
                ContentEncoding = contentEncoding,
                JsonRequestBehavior = behavior,
                MaxJsonLength = Int32.MaxValue
            };
        }
        public JsonResult HistoryReport(string id, string startdate, string enddate, string uniqueid, string tel, string agentid, string[] termcd)
        {
            string Termcdlist = string.Join(",", termcd);
            List<HistoricalReport_Result> data = new List<HistoricalReport_Result>();
            HistoricalReport_Result obj = new HistoricalReport_Result();
            data = obj.GetHistoricalReport_Result(startdate.Trim(), enddate.Trim(), uniqueid.Trim(), tel.Trim(), agentid.Trim(), id.Trim(), Termcdlist.Trim());
            return Json(new { result = data }, JsonRequestBehavior.AllowGet);

        }

        public ActionResult DispositionList(string id)
        {
            List<DispositionList_Result> disposition = new List<DispositionList_Result>();
            disposition = db.DispositionList(id).ToList();
            return Json(new { result = disposition }, JsonRequestBehavior.AllowGet);
        }
        #endregion

        #region Header and Modules
        public ActionResult GetMenuItems()
        {
            List<MenuItems> model = new List<MenuItems>();
            try
            {
                int currentUserId = WebSecurity.GetUserId(User.Identity.Name);
                string id = RouteData.Values["id"] + Request.Url.Query;
                var roleid = (from r in db.webpages_Roles
                              where (r.RoleName == id)
                              select r.RoleId).ToList().FirstOrDefault();

                var modules = new List<string>();
                var query = (from ur in db.webpages_UsersInRoles
                             join module in db.Modules on ur.ModuleID equals module.ModuleID
                             where ur.RoleId == roleid && ur.UserId == currentUserId
                             select new
                             {
                                 module.Name,
                                 module.Description,
                                 module.HTML


                             }).ToList().OrderBy(m=>m.Description);

                
                foreach (var result in query)
                {
                    model.Add(item: new MenuItems
                    {
                        CampaignID = id,
                        Module = result.Name,
                        Description = result.Description,
                        HTML = result.HTML

                    });
                }
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);

            }
            return PartialView("_SideMenu", model);
        }
        public ActionResult GetHeaderContent()
        {
            try
            {
                int currentUserId = WebSecurity.GetUserId(User.Identity.Name);
                string id = RouteData.Values["id"] + Request.Url.Query;
                int len = Request.Url.Segments.Length;
                string action = Request.Url.Segments[len - 2].Replace(@"/", string.Empty).Trim();
                var client = (from r in db.webpages_Roles
                              join c in db.Clients on r.ClientID equals c.ClientID
                              where r.RoleName == id
                              select new
                              {
                                  c.Name
                              }).ToList().FirstOrDefault();


                Module result = db.Modules.Where(x => x.Name == action).FirstOrDefault();

                Header model = new Header
                    {
                        CampaignID = id,
                        Client = client.Name,
                        ModuleDescription = result.Description,
                        HTML = result.HTML

                    };
                return PartialView("_HeaderPartial", model);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }
            
        }
        public ActionResult GetReportHeader()
        {
            try
            {

                string id = RouteData.Values["id"] + Request.Url.Query;
                int len = Request.Url.Segments.Length;
                string action = Request.Url.Segments[len - 2].Replace(@"/", string.Empty).Trim();


                var header = db.ReportHeaders.Include("Module").Where(x => x.CampaignID == id && x.Module.Name == action).ToList();

                if (action == "Agent")
                    return PartialView("_AgentPartial", header);
                else
                    return PartialView("_IntervalPartial", header);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }

        }
        #endregion

        #region Call Recordings


        [HttpPost]
        [Log(Activity = "Searched Recordings")]
        public ActionResult Recordings(string id, string startdate, string enddate, string uniqueid, string tel, string agentid)
        {
            try
            {
                CallRecordingReport_Result obj = new CallRecordingReport_Result();
                List<CallRecordingReport_Result> data = new List<CallRecordingReport_Result>();
                string CampaignID = id;
                data = obj.GetSearchResults(startdate.Trim(), enddate.Trim(), uniqueid.Trim(), tel.Trim(), agentid.Trim(), CampaignID.Trim());




                #region NR Streaming Player 1.1
                string NRPlayer_IV = ConfigurationManager.AppSettings["NRPlayer_IV"].ToString();
                string NRPlayer_Key = ConfigurationManager.AppSettings["NRPlayer_Key"].ToString();
                byte[] IVbytes = Encoding.ASCII.GetBytes(NRPlayer_IV);
                byte[] Keybytes = Encoding.ASCII.GetBytes(NRPlayer_Key);

                string NRArgument = ConfigurationManager.AppSettings["Storage_locator_IP_Port"].ToString() + " " +
                    ConfigurationManager.AppSettings["AltStorage_locator_IP_Port"].ToString() + " " +
                    ConfigurationManager.AppSettings["Proxy_Server_IP_Port"].ToString() + " " +
                    User.Identity.Name + " " +
                    ConfigurationManager.AppSettings["Comment_string"].ToString() + " " +
                    ConfigurationManager.AppSettings["Saving_string"].ToString() + " ";

                byte[] encryptedtext = null;

                foreach (var item in data)
                {
                    encryptedtext = null;
                    encryptedtext = EncryptStringToBytes(NRArgument + item.NR_Segment, Keybytes, IVbytes);
                    item.NR_Segment = "nrplayer:" + Convert.ToBase64String(encryptedtext);

                }
                #endregion
                TempData["Panel"] = "false";
                TempData["startdate"] = startdate;
                TempData["enddate"] = enddate;
                TempData["uniqueid"] = uniqueid;
                TempData["tel"] = tel;
                TempData["agentid"] = agentid;
                return View(data);
            }
            catch (Exception exp)
            {
                Email obj = new Email();
                obj.SendExceptionEmail(exp, User.Identity.Name);
                return RedirectToAction("Index", "Home");
            }
        }

        static byte[] EncryptStringToBytes(string plainText, byte[] Key, byte[] IV)
        {
            // Check arguments. 
            if (plainText == null && plainText.Length <= 0)
                throw new ArgumentNullException("plainText");
            if (Key == null && Key.Length <= 0)
                throw new ArgumentNullException("Key");
            if (IV == null && IV.Length <= 0)
                throw new ArgumentNullException("IV");
            byte[] encrypted;
            // Create an RijndaelManaged object 
            // with the specified key and IV. 
            using (RijndaelManaged rijAlg = new RijndaelManaged())
            {
                rijAlg.Key = Key;
                rijAlg.IV = IV;

                // Create a decrytor to perform the stream transform.
                ICryptoTransform encryptor = rijAlg.CreateEncryptor(rijAlg.Key, rijAlg.IV);

                // Create the streams used for encryption. 
                using (MemoryStream msEncrypt = new MemoryStream())
                {
                    using (CryptoStream csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    {
                        using (StreamWriter swEncrypt = new StreamWriter(csEncrypt))
                        {

                            //Write all data to the stream.
                            swEncrypt.Write(plainText);
                        }
                        encrypted = msEncrypt.ToArray();
                    }
                }
            }


            // Return the encrypted bytes from the memory stream. 
            return encrypted;

        }
        #endregion

    }
}
