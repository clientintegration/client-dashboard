﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using WebMatrix.WebData;

namespace ClientDashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class UserProfileController : Controller
    {
        private ClientDashboardEntities db = new ClientDashboardEntities();

        //
        // GET: /UserProfile/

        public ActionResult Index()
        {
            return View(db.UserProfiles.ToList());
        }

        //
        // GET: /UserProfile/Details/5

        public ActionResult Details(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            var assigned = new List<string>();
            var amod = new List<string>();
            var c = new List<string>();
            var total = new List<string>();
            //all the camapigns with their respective client names
            var roles = (from r in db.webpages_Roles
                         join cl in db.Clients on r.ClientID equals cl.ClientID
                         select new { r.RoleName, cl.Name, r.RoleId }).Where(x => x.RoleName != "Administrator");
            //all the modules
            var modules = (from m in db.Modules select new { m.Name, m.ModuleID }).Where(x=>x.Name!="Administrator").ToList();
            //all the camapigns and modules assigned to user
            var campaigns = (from ur in db.webpages_UsersInRoles
                             join r in db.webpages_Roles on ur.RoleId equals r.RoleId
                             join mod in db.Modules on ur.ModuleID equals mod.ModuleID
                             where ur.UserId == id
                             select new
                             {
                                 r.RoleName,
                                 mod.Name
                             }).ToList();



            List<Role> RolesList1 = new List<Role>();


            foreach (var result in roles)
            {
                List<RoleModule> RoleModList1 = new List<RoleModule>();
                foreach (var mod in modules)
                {
                    RoleModList1.Add(new RoleModule()
                    {
                        selected = campaigns.Exists(x => x.RoleName == result.RoleName && x.Name == mod.Name),
                        value = mod.ModuleID
                    });
                }
                RolesList1.Add(new Role()
                {

                    RoleId = result.RoleId,
                    Client = result.Name,
                    Campaign = result.RoleName,
                    RoleModList = RoleModList1
                });


            }

            List<Module> ModuleList1 = db.Modules.Where(x => x.Name != "Administrator").ToList();
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(new EditUserViewModel()
            {
                UserId = userprofile.UserId,
                Email = userprofile.Email,
                FullName = userprofile.Name,
                UserName = userprofile.UserName,
                RolesList = RolesList1,
                ModulesList = ModuleList1
            });
        }

        //
        // GET: /UserProfile/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /UserProfile/Create

        [HttpPost]
        public ActionResult Create(RegisterModel model)
        {
            if (ModelState.IsValid)
            {
                // Attempt to register the user
                try
                {
                    WebSecurity.CreateUserAndAccount(model.UserName, model.Password, new { Email = model.Email, Name = model.FullName });
                    
                    string emailbody = "<p >Hi <b>" + model.UserName.ToUpper() + "</b>, <br/>You have been registered on <b>IBEX Global Client Dashboard</b>.<br/><br/>Please find below your account details.</p>";
                    emailbody += "<ul ><li>User name: <b>" + model.UserName + "</b></li>";
                    emailbody += "<li>Password: <b>" + model.Password + "</b></li>";
                    emailbody += "<li>Email: " + model.Email + "</li>";
                    emailbody += "<li>Website: <a href='clientdashboard.ibexglobal.com'>IBEX | Client Dashboard</a>" + "</li></ul>";
                    Email obj = new Email();
                    obj.SendEmail("Registration | Client Dashboard", model.Email, emailbody);
                    //WebSecurity.Login(model.UserName, model.Password);
                    return RedirectToAction("Index", "UserProfile");
                }
                catch (MembershipCreateUserException e)
                {
                    ModelState.AddModelError("", ErrorCodeToString(e.StatusCode));
                }
            }
            //if (ModelState.IsValid)
            //{
            //    db.UserProfiles.Add(userprofile);
            //    db.SaveChanges();
            //    return RedirectToAction("Index");
            //}

            return View(model);
        }

        private string ErrorCodeToString(MembershipCreateStatus membershipCreateStatus)
        {
            throw new NotImplementedException();
        }

        //
        // GET: /UserProfile/Edit/5

        public ActionResult Edit(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            var assigned = new List<string>();
            var amod = new List<string>();
            var c = new List<string>();
            var total = new List<string>();
            //all the camapigns with their respective client names
            var roles = (from r in db.webpages_Roles
                         join cl in db.Clients on r.ClientID equals cl.ClientID
                         select new { r.RoleName, cl.Name, r.RoleId }).Where(x=>x.RoleName!="Administrator");
            //all the modules
            var modules = (from m in db.Modules select new { m.Name, m.ModuleID }).Where(x => x.Name != "Administrator").ToList();
            //all the camapigns and modules assigned to user
            var campaigns = (from ur in db.webpages_UsersInRoles
                             join r in db.webpages_Roles on ur.RoleId equals r.RoleId
                             join mod in db.Modules on ur.ModuleID equals mod.ModuleID
                             where ur.UserId == id
                             select new
                             {
                                 r.RoleName,
                                 mod.Name
                             }).ToList();



            List<Role> RolesList1 = new List<Role>();


            foreach (var result in roles)
            {
                List<RoleModule> RoleModList1 = new List<RoleModule>();
                foreach (var mod in modules)
                {
                    RoleModList1.Add(new RoleModule()
                    {
                        selected = campaigns.Exists(x => x.RoleName == result.RoleName && x.Name == mod.Name),
                        value = mod.ModuleID
                    });
                }
                RolesList1.Add(new Role()
                {
                   
                    RoleId = result.RoleId,
                    Client = result.Name,
                    Campaign=result.RoleName,
                    RoleModList = RoleModList1
                });


            }

            List<Module> ModuleList1 = db.Modules.Where(x => x.Name != "Administrator").ToList();
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(new EditUserViewModel()
            {
                UserId = userprofile.UserId,
                Email = userprofile.Email,
                FullName = userprofile.Name,
                UserName = userprofile.UserName,
                RolesList = RolesList1,
                ModulesList = ModuleList1
            });
        }

        //
        // POST: /UserProfile/Edit/5

        [HttpPost]
        public ActionResult Edit(EditUserViewModel edituser)
        {
            UserProfile userprofile = new UserProfile();
            userprofile.UserId = edituser.UserId;
            userprofile.UserName = edituser.UserName;
            userprofile.Email = edituser.Email;
            userprofile.Name = edituser.FullName;
            if (ModelState.IsValid)
            {
                db.Entry(userprofile).State = EntityState.Modified;
                foreach (var r in db.webpages_Roles)
                {
                    foreach (var m in db.Modules)
                    {
                        webpages_UsersInRoles webpages_usersinroles = db.webpages_UsersInRoles.Find(edituser.UserId, r.RoleId, m.ModuleID);

                        if (webpages_usersinroles == null && edituser.RolesList.Exists(x => x.RoleId == r.RoleId && x.RoleModList.Exists(rm => rm.value == m.ModuleID && rm.selected == true)))
                        {
                            db.webpages_UsersInRoles.Add(new webpages_UsersInRoles()
                            {
                                UserId = edituser.UserId,
                                ModuleID = m.ModuleID,
                                RoleId = r.RoleId
                            });


                        }
                        else if (webpages_usersinroles != null && webpages_usersinroles.RoleId!=4 && !edituser.RolesList.Exists(x => x.RoleId == r.RoleId && x.RoleModList.Exists(rm => rm.value == m.ModuleID && rm.selected == true)))
                        {
                            db.webpages_UsersInRoles.Remove(webpages_usersinroles);
                        }

                    }
                }
                db.SaveChanges();
              

                return RedirectToAction("Index");
            }
            return View(userprofile);
        }

        //
        // GET: /UserProfile/Delete/5

        public ActionResult Delete(int id = 0)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            if (userprofile == null)
            {
                return HttpNotFound();
            }
            return View(userprofile);
        }

        //
        // POST: /UserProfile/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            UserProfile userprofile = db.UserProfiles.Find(id);
            db.UserProfiles.Remove(userprofile);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}