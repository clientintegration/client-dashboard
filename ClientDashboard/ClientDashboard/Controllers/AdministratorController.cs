﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientDashboard.Classes;
using ClientDashboard.Models;

namespace ClientDashboard.Controllers
{
    public class AdministratorController : Controller
    {
        //
        // GET: /Administrator/
        ClientDashboardEntities db = new ClientDashboardEntities();
        public ActionResult Index()
        {
            return View();
        }
        [Authorize]
        public ActionResult Emailer()
        {

            ViewBag.Email = new SelectList(db.UserProfiles, "Email", "Email");

            return View();
        }
        [HttpPost]
        public ActionResult Emailer(Models.MailModel model, HttpPostedFileBase file, string[] Email)
        {
          
            if (ModelState.IsValid)
            {

                Email obj = new Email();
                obj.SendEmailToUser(model.Subject,Email, model.Body, file);
                ViewBag.Email = new SelectList(db.UserProfiles, "Email", "Email");
                return View();

            }
            else
            {
                return View("Emailer");
            }
        }

    }
}
