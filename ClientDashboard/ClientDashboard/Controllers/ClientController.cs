﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using ClientDashboard.Models;

namespace ClientDashboard.Controllers
{
    [Authorize(Roles = "Administrator")]
    public class ClientController : Controller
    {
        private ClientDashboardEntities db = new ClientDashboardEntities();

        //
        // GET: /Client/

        public ActionResult Index()
        {
            return View(db.Clients.ToList());
        }

        //
        // GET: /Client/Details/5

        public ActionResult Details(int id = 0)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        //
        // GET: /Client/Create

        public ActionResult Create()
        {
            return View();
        }

        //
        // POST: /Client/Create

        [HttpPost]
        public ActionResult Create(Client client, HttpPostedFileBase file)
        {
            if (ModelState.IsValid)
            {
                if (file != null && file.ContentLength > 0)
                    try
                    {
                        string path = Path.Combine(Server.MapPath("~/Images/"), Path.GetFileName(file.FileName));
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                        file.SaveAs(path);
                        client.ImagePath = "/Images/" + Path.GetFileName(file.FileName);
                    }
                    catch (Exception ex)
                    {
                        // ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                    client.ImagePath = client.ImagePath;
                db.Clients.Add(client);
                db.SaveChanges();
                return RedirectToAction("Index");
            }

            return View(client);
        }

        //
        // GET: /Client/Edit/5

        public ActionResult Edit(int id = 0)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        //
        // POST: /Client/Edit/5

        [HttpPost]
        public ActionResult Edit(Client client, HttpPostedFileBase file)
        {
           

            if (ModelState.IsValid)
            {
                db.Entry(client).State = EntityState.Modified;
                if (file != null && file.ContentLength > 0)
                    try
                    {
                        string path = Path.Combine(Server.MapPath("~/Images/"), Path.GetFileName(file.FileName));
                        if (System.IO.File.Exists(path))
                            System.IO.File.Delete(path);
                        file.SaveAs(path);
                        client.ImagePath = "/Images/" + Path.GetFileName(file.FileName);
                    }
                    catch (Exception ex)
                    {
                        // ViewBag.Message = "ERROR:" + ex.Message.ToString();
                    }
                else
                {
                    var ImageInDb = db.Clients.Where(s => s.ClientID==client.ClientID).Select(s => s.ImagePath).FirstOrDefault();
                    client.ImagePath = ImageInDb;
                }
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(client);
        }

        //
        // GET: /Client/Delete/5

        public ActionResult Delete(int id = 0)
        {
            Client client = db.Clients.Find(id);
            if (client == null)
            {
                return HttpNotFound();
            }
            return View(client);
        }

        //
        // POST: /Client/Delete/5

        [HttpPost, ActionName("Delete")]
        public ActionResult DeleteConfirmed(int id)
        {
            Client client = db.Clients.Find(id);
            db.Clients.Remove(client);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            db.Dispose();
            base.Dispose(disposing);
        }
    }
}