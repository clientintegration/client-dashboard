﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using ClientDashboard.Classes;
using ClientDashboard.Models;
using Microsoft.AspNet.SignalR;
using Microsoft.AspNet.SignalR.Hubs;
using WebMatrix.WebData;
namespace ClientDashboard.Hubs
{
    [HubName("statisticsHub")]
    public class StatisticsHub : Hub
    {


        public void SendAgentCountStats(List<AgentCountReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentCountStats(Stats);
        }
        public void SendAgentStatusStats(List<AgentStatusReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentStatusStats(Stats);
        }
        public void SendQueueStats(List<RoutingQueueStat> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateQueueStats(Stats);
        }
        public void SendLeadsStats(List<LeadsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateLeadsStats(Stats);
        }
        public void SendTFNQueueStats(List<TfnQueueDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateTFNQueueStats(Stats);
        }
        public void SendOBDispositionStats(List<OBDispositionReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateOBDispositionStats(Stats);
        }
        public void SendIntervalStats(List<CampaignsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateIntervalStats(Stats);
        }    
        public void SendAgentsStats(List<AgentsReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateAgentsStats(Stats);
        }
       
        public void SendDialingStatus(List<HomeReport> Stats)
        {
            IHubContext context = GlobalHost.ConnectionManager.GetHubContext<StatisticsHub>();
            context.Clients.All.updateDialingStatus(Stats);
        }
        public List<HomeReport> GetDialingStatus()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<HomeReport> MenuInfo = _StatisticsRepository.GetDialingStatus(true).ToList();
            return MenuInfo;
        }
        public List<AgentCountReport> GetAgentCountStats()
        {

            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentCountReport> AgentCount = _StatisticsRepository.GetAgentCountStats(true).ToList();
            return AgentCount;
        }
        public List<AgentStatusReport> GetAgentStatusStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentStatusReport> AgentStatus = _StatisticsRepository.GetAgentStatusStats(true).ToList();
            return AgentStatus;
        }
        public List<RoutingQueueStat> GetQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<RoutingQueueStat> Queue = _StatisticsRepository.GetRoutingQueueStats(true).ToList();
            return Queue;
        }
     
        public List<TfnQueueDispositionReport> GetTFNQueueStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<TfnQueueDispositionReport> TFNQueue = _StatisticsRepository.GetTFNQueueStats(true).ToList();
            return TFNQueue;
        }
        public List<CampaignsReport> GetIntervalStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<CampaignsReport> IntervalStats = _StatisticsRepository.GetIntervalStats(true).ToList();
            return IntervalStats;
        }
        public List<AgentsReport> GetAgentsStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<AgentsReport> AgentStats = _StatisticsRepository.GetAgentsStats(true).ToList();
            return AgentStats;
        }
       
        public List<LeadsReport> GetLeadsStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<LeadsReport> LeadsStats = _StatisticsRepository.GetLeadsStats(true).ToList();
            return LeadsStats;
        }
        public List<OBDispositionReport> GetOBDispositionStats()
        {
            StatisticsRepository _StatisticsRepository = new StatisticsRepository();
            List<OBDispositionReport> OBDispStats = _StatisticsRepository.GetOBDispositionStats(true).ToList();
            return OBDispStats;
        }
        public override System.Threading.Tasks.Task OnDisconnected()
        {
            return base.OnDisconnected();
        }


    }
}